## Generated SDC file "VfcHdTop.out.sdc"

## Copyright (C) 1991-2015 Altera Corporation. All rights reserved.
## Your use of Altera Corporation's design tools, logic functions 
## and other software and tools, and its AMPP partner logic 
## functions, and any output files from any of the foregoing 
## (including device programming or simulation files), and any 
## associated documentation or information are expressly subject 
## to the terms and conditions of the Altera Program License 
## Subscription Agreement, the Altera Quartus II License Agreement,
## the Altera MegaCore Function License Agreement, or other 
## applicable license agreement, including, without limitation, 
## that your use is for the sole purpose of programming logic 
## devices manufactured by Altera and sold by Altera or its 
## authorized distributors.  Please refer to the applicable 
## agreement for further details.


## VENDOR  "Altera"
## PROGRAM "Quartus II"
## VERSION "Version 15.0.0 Build 145 04/22/2015 SJ Full Version"

## DATE    "Mon Jun 01 17:42:10 2015"

##
## DEVICE  "5AGXMB1G4F40C4"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3


#**************************************************************
# Create Clock
#**************************************************************

create_clock -name {GbitTrxClkRefR_ik} -period 8.000 -waveform {0.000 4.000} [get_ports {GbitTrxClkRefR_ik}]

create_clock -name {Clk20VCOx_ik} -period 50.000 [get_ports {Clk20VCOx_ik}]

#**************************************************************
# Create Generated Clock
#**************************************************************

derive_pll_clocks


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

derive_clock_uncertainty

#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************

# splitting of PHY clocks based on pexarria5 project from GSI
set_clock_groups -asynchronous                                                              \
    -group { GbitTrxClkRefR_ik                                                              \
             i_VfcHdSystem|i_WrpcWrapper|cmp_xwrc_platform|*cmp_sys_clk_pll|*|general[0]*   \
             i_VfcHdSystem|i_WrpcWrapper|cmp_xwrc_platform|*cmp_sys_clk_pll|*|general[1]* } \
    -group { Clk20VCOx_ik                                                                   \
             i_VfcHdSystem|i_WrpcWrapper|cmp_xwrc_platform|*cmp_dmtd_clk_pll|* }            \
    -group { i_VfcHdSystem|i_WrpcWrapper|cmp_xwrc_platform|*cmp_phy|*.cdr_refclk*           \
             i_VfcHdSystem|i_WrpcWrapper|cmp_xwrc_platform|*cmp_phy|*.cmu_pll.*             \
             i_VfcHdSystem|i_WrpcWrapper|cmp_xwrc_platform|*cmp_phy|*|av_tx_pma|*           \
             i_VfcHdSystem|i_WrpcWrapper|cmp_xwrc_platform|*cmp_phy|*|inst_av_pcs|*|tx* }   \
    -group { i_VfcHdSystem|i_WrpcWrapper|cmp_xwrc_platform|*cmp_phy|*|clk90bdes             \
             i_VfcHdSystem|i_WrpcWrapper|cmp_xwrc_platform|*cmp_phy|*|clk90b                \
             i_VfcHdSystem|i_WrpcWrapper|cmp_xwrc_platform|*cmp_phy|*|rcvdclkpma }

#**************************************************************
# Set False Path
#**************************************************************

set_false_path -from [get_ports *]
set_false_path -to [get_ports *]

#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************



#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************


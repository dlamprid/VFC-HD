#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
# Maciej Lipinski (CERN BE-CO-HT) 01/07/2016 #
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

# Defining Quartus II path:
set QUARTUS_II_PATH C:\altera\15.1\quartus

# Define here the paths used for the different folders:
set DUT_PATH ../../../FpgaModules/ApplicationSpecific/BaseProject/
set VFC_PATH ../../../FpgaModules/
set MODELS_PATH ../../Models/
set TB_PATH ../
set WRBTRAIN_PATH ../../../FpgaModules/ApplicationSpecific/WrBtrain/
# Clearing the transcript window:
.main clear

echo ""
echo "########################"
echo "# Starting Compilation #"
echo "########################"

###############################################
# Verilog Compile Process
###############################################

proc compile_verilog {sc lib name} {
	file stat $name result
	if {$sc < $result(mtime)} {
	    set ok [vlog -work $lib $name]
	}
}

proc compile_vhdl {sc lib name} {
	file stat $name result
	if {$sc < $result(mtime)} {
	    set ok [vcom -2008 -work $lib $name]
	}
}
###############################################
# Defining Last Compilation
###############################################

echo ""
echo "-> Enabling Smart or Full Compilation..."
echo ""

# Checking whether a previous smart compilation exists:
if {![info exists sc]} {
    set sc 0
    set no_sc 1;
} 

if "$no_sc == 1" {
	echo "Smart compilation not possible or not enabled. Running full compilation..."
	# Deleting pre-existing libraries
	if {[file exists work]} {vdel -all -lib work}	
	# Creating and mapping working directory:
	vlib work
    vmap work work
	set no_sc 0;
} else {
	echo "Smart compilation enabled. Only out of date files will be compiled..."
	puts [clock format $sc -format {Previous compilation time: %A, %d of %B, %Y - %H:%M:%S}]
}

###############################################
# Compiling simulation files
###############################################

echo ""
echo "-> Starting Compilation..."
echo ""
    
# Device Under Test files:
compile_verilog $sc work $DUT_PATH/AddrDecoderWbApp.v
compile_verilog $sc work $DUT_PATH/VfcHdApplication.v

# VFC HD hierarchy:
compile_verilog $sc work $VFC_PATH/GeneralPurpose/Ip_OpenCores/generic_dpram.v
compile_verilog $sc work $VFC_PATH/GeneralPurpose/Ip_OpenCores/generic_fifo_dc_gray.v
compile_verilog $sc work $VFC_PATH/GeneralPurpose/Generic4InputRegs.v
compile_verilog $sc work $VFC_PATH/GeneralPurpose/Generic4OutputRegs.v
compile_verilog $sc work $VFC_PATH/GeneralPurpose/I2CMaster.v
compile_verilog $sc work $VFC_PATH/GeneralPurpose/I2CMasterNoBus.v
compile_verilog $sc work $VFC_PATH/GeneralPurpose/SpiMasterWB.v
compile_verilog $sc work $VFC_PATH/SystemSpecific/VmeInterface/InterruptManagerWb.v
compile_verilog $sc work $VFC_PATH/SystemSpecific/VmeInterface/VmeInterfaceWb.v
compile_verilog $sc work $VFC_PATH/SystemSpecific/AddrDecoderWbSys.v
compile_verilog $sc work $VFC_PATH/SystemSpecific/I2cMuxAndExpMaster.v
compile_verilog $sc work $VFC_PATH/SystemSpecific/UniqueIdReader.v
compile_verilog $sc work $VFC_PATH/SystemSpecific/VfcHdSystem.v
compile_verilog $sc work $VFC_PATH/VfcHdTop.v 

# VFC HD models files:
compile_verilog $sc work $MODELS_PATH/I2CSlave.v
compile_verilog $sc work $MODELS_PATH/ivt3205c25mhz.v
compile_verilog $sc work $MODELS_PATH/MAX5483.v
compile_verilog $sc work $MODELS_PATH/pca9534.v
compile_verilog $sc work $MODELS_PATH/pca9544a.v
compile_verilog $sc work $MODELS_PATH/si57x.v
compile_verilog $sc work $MODELS_PATH/sn74vmeh22501.v
compile_verilog $sc work $MODELS_PATH/VfcHd_v2_0.v
compile_verilog $sc work $MODELS_PATH/VmeBusModule.sv

# WR-Btrain stuff:

# stuff needed from general cores and wr-cores
compile_vhdl    $sc work $WRBTRAIN_PATH/general-cores/modules/genrams/genram_pkg.vhd
compile_vhdl    $sc work $WRBTRAIN_PATH/general-cores/modules/wishbone/wishbone_pkg.vhd
compile_vhdl    $sc work $WRBTRAIN_PATH/wr-cores/modules/fabric/wr_fabric_pkg.vhd
compile_vhdl    $sc work $WRBTRAIN_PATH/general-cores/modules/wishbone/wb_slave_adapter/wb_slave_adapter.vhd

# stuff needed from Btrain cores

compile_vhdl    $sc work $WRBTRAIN_PATH/PS-BTrain-over-WhiteRabbit/hdl/rtl/WRBtrainStuff/WRBtrain_wbgen2_pkg.vhd
compile_vhdl    $sc work $WRBTRAIN_PATH/PS-BTrain-over-WhiteRabbit/hdl/rtl/WRBtrainStuff/WRBTrain_pkg.vhd
compile_vhdl    $sc work $WRBTRAIN_PATH/PS-BTrain-over-WhiteRabbit/hdl/rtl/WRBtrainStuff/rxCtrlBtrain.vhd
compile_vhdl    $sc work $WRBTRAIN_PATH/PS-BTrain-over-WhiteRabbit/hdl/rtl/WRBtrainStuff/SynchroGen.vhd
compile_vhdl    $sc work $WRBTRAIN_PATH/PS-BTrain-over-WhiteRabbit/hdl/rtl/WRBtrainStuff/txCtrlBtrain.vhd
compile_vhdl    $sc work $WRBTRAIN_PATH/PS-BTrain-over-WhiteRabbit/hdl/rtl/WRBtrainStuff/WRBtrain_wb.vhd
compile_vhdl    $sc work $WRBTRAIN_PATH/PS-BTrain-over-WhiteRabbit/hdl/rtl/WRBtrainStuff/WRBTrain.vhd

# dedicated VFC-HD wrapper
compile_vhdl    $sc work $WRBTRAIN_PATH/WrBtrainWrapper.vhd

# Test Bench files:
compile_verilog $sc work $TB_PATH/tb_WrBtrainAccess.sv

###############################################
# Top file
###############################################

echo ""
echo "-> Setting Top File..."
echo ""

set top_level work.tb_BaseProjecAppAcceses

###############################################
# Acquiring Compilation Time
###############################################

echo ""
set sc [clock seconds];
puts [clock format $sc -format {Setting last compilation time to: %A, %d of %B, %Y - %H:%M:%S}]

echo ""
echo "-> Compilation Done..."
echo ""
echo ""
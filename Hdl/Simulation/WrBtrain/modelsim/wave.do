onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeAs_in
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeAm_ib6
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeA_iob31
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeLWord_ion
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeAOe_oen
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeADir_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeDs_inb2
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeWrite_in
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeD_iob32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeDOe_oen
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeDDir_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeDtAckOe_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeIrq_ob7
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeIack_in
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeIackIn_in
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeIackOut_on
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeSysClk_ik
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeSysReset_irn
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2cMuxSda_io
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2cMuxScl_ok
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2CMuxIntN0_in
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2CMuxIntN1_in
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2CIoExpIntApp12_in
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2CIoExpIntApp34_in
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2CIoExpIntBstEth_in
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2CIoExpIntBlmIn_in
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/BstDataIn_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/CdrClkOut_ik
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/CdrDataOut_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VAdcDout_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VAdcDin_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VAdcCs_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VAdcSclk_ok
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/GbitTrxClkRefR_ik
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VadjCs_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VadjSclk_ok
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VadjDin_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VfmcEnableN_oen
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/NoGa_ib5
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/UseGa_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/PcbRev_ib7
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WrPromSda_io
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WrPromScl_ok
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/TempIdDq_ioz
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/ResetFpgaConfigN_orn
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/Reset_orqp
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/ResetRequest_iqp
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbClk_ik
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterCyc_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterStb_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterAdr_ob25
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterWr_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterDat_ob32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterDat_ib32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterAck_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveCyc_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveStb_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveAdr_ib25
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveWr_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveDat_ib32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveDat_ob32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveAck_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/TopLed_ib2
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/BottomLed_ib4
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/BstOn_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/BunchClk_ok
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/TurnClk_op
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/BstByte_ob8
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/BstByteAddress_ob5
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/InterruptRequest_ipb24
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/StreamerClk_ik
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/StreamerData_ib32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/SreamerDav_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/StreamerPckt_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/StreamerWait_o
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/GpIo1DirOut_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/GpIo2DirOut_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/GpIo34DirOut_i
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeAccess
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeDtAck_n
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeIrq_nb7
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/ResetWb_xd2
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbCyc
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStb
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbWe
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAck
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAdr_b22
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatMiSo_b32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatMoSi_b32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/IntEnable
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/IntModeRoRa
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/IntSourceToRead
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/NewIntRequest
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/IntLevel_b3
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/IntVector_b8
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/IntRequestBus_ab32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeSysReset_d2
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckIntManager
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStbIntManager
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatMiSoIntManager_b32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckSpiMaster
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStbSpiMaster
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatSpiMaster_b32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckUniqueIdReader
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStbUniqueIdReader
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatUniqueIdReader_b32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckAppSlaveBus
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStbAppSlaveBus
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatAppSlaveBus_b32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStbI2cIoExpAndMux
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckI2cIoExpAndMux
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatI2cIoExpAndMux_b32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/SpiClk_k
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/SpiMoSi
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/SpiMiSo_b32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/SpiSs_nb32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterStb_xd2
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckAppSlaveBus_xd2
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeGa_b5
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeGap_n
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckI2cWrProm
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStbI2cWrProm
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatI2cWrProm_b32
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/Reset_rq
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/Clk_k
add wave -noupdate -group VfcHdSystem /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/Ga_b5
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/clk_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/rst_n_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/tx_data_o
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/tx_valid_o
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/tx_dreq_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/tx_last_o
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/tx_flush_o
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/rx_data_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/rx_valid_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/rx_first_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/rx_dreq_o
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/rx_last_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/Bvalue_o
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_adr_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_dat_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_dat_o
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_sel_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_we_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_cyc_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_stb_i
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_ack_o
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_err_o
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_rty_o
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_stall_o
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_slave_out
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/wb_slave_in
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/rx_BFramePayloads
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/tx_FrameHeader
add wave -noupdate -group WrBtrainWrapper /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/tx_BFramePayloads
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/clk_i
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/rst_n_i
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/tx_data_o
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/tx_valid_o
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/tx_dreq_i
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/tx_last_o
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/tx_flush_o
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/rx_data_i
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/rx_valid_i
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/rx_first_i
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/rx_dreq_o
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/rx_last_i
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/rx_FrameHeader_o
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/rx_BFramePayloads_o
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/rx_IFramePayloads_o
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/tx_FrameHeader_i
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/tx_BFramePayloads_i
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/tx_IFramePayloads_i
add wave -noupdate -expand -group WR_BTRAIN_STUFF -expand /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/wb_slave_i
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/wb_slave_o
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/send_btrain_frame
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/send_tick
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/sample_period
add wave -noupdate -expand -group WR_BTRAIN_STUFF -expand /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/regs_to_wb
add wave -noupdate -expand -group WR_BTRAIN_STUFF -expand /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/regs_from_wb
add wave -noupdate -expand -group WR_BTRAIN_STUFF -expand /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/wb_regs_slave_in
add wave -noupdate -expand -group WR_BTRAIN_STUFF /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/wb_regs_slave_out
add wave -noupdate -group rxCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_rxCtrl/clk_i
add wave -noupdate -group rxCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_rxCtrl/rst_n_i
add wave -noupdate -group rxCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_rxCtrl/rx_data_i
add wave -noupdate -group rxCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_rxCtrl/rx_valid_i
add wave -noupdate -group rxCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_rxCtrl/rx_first_i
add wave -noupdate -group rxCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_rxCtrl/rx_last_i
add wave -noupdate -group rxCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_rxCtrl/FrameHeader_o
add wave -noupdate -group rxCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_rxCtrl/BFramePayloads_o
add wave -noupdate -group rxCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_rxCtrl/IFramePayloads_o
add wave -noupdate -group rxCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_rxCtrl/rxframe_valid_o
add wave -noupdate -group rxCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_rxCtrl/frame_content
add wave -noupdate -group rxCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_rxCtrl/tmp_frame_content
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/clk_i
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/rst_n_i
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/ctrl_send_i
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/ctrl_dbg_mode_i
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/FrameHeader_i
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/BFramePayloads_i
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/IFramePayloads_i
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/tx_sent_o
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/tx_data_o
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/tx_valid_o
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/tx_dreq_i
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/tx_last_o
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/tx_flush_o
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/B_frame_s
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/I_frame_s
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/frame_header_s
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/tx_delay_s
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/tx_valid_s
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/counter_s
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/ctrl_send_d
add wave -noupdate -group txCtrl /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_txCtrl/ctrl_send_edge
add wave -noupdate -group SyynchroGen /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_SynchroGen/clk_i
add wave -noupdate -group SyynchroGen /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_SynchroGen/rst_n_i
add wave -noupdate -group SyynchroGen /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_SynchroGen/reg_sample_period_i
add wave -noupdate -group SyynchroGen /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_SynchroGen/send_tick_o
add wave -noupdate -group SyynchroGen /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_SynchroGen/sample_rate_counter
add wave -noupdate -group SyynchroGen /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_SynchroGen/zero_cnt
add wave -noupdate -group SyynchroGen /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_SynchroGen/tick
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/rst_n_i
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/clk_sys_i
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wb_adr_i
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wb_dat_i
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wb_dat_o
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wb_cyc_i
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wb_sel_i
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wb_stb_i
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wb_we_i
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wb_ack_o
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wb_stall_o
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/regs_i
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/regs_o
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wrbtrain_scr_tx_single_dly0
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wrbtrain_scr_tx_single_int
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wrbtrain_scr_tx_dbg_int
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wrbtrain_tx_period_value_int
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/ack_sreg
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/rddata_reg
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wrdata_reg
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/bwsel_reg
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/rwaddr_reg
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/ack_in_progress
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/wr_int
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/rd_int
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/allones
add wave -noupdate -group wb_regs /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_regs/allzeros
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/clk_sys_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/rst_n_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/sl_adr_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/sl_dat_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/sl_sel_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/sl_cyc_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/sl_stb_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/sl_we_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/sl_dat_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/sl_err_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/sl_rty_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/sl_ack_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/sl_stall_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/sl_int_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/slave_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/slave_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/ma_adr_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/ma_dat_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/ma_sel_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/ma_cyc_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/ma_stb_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/ma_we_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/ma_dat_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/ma_err_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/ma_rty_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/ma_ack_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/ma_stall_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/ma_int_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/master_i
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/master_o
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/fsm_state
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/master_in
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/master_out
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/slave_in
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/slave_out
add wave -noupdate -group wb_adapter /tb_WrBtrainAccess/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_WrBtrainWrapper/U_WR_BTRAIN_STUFF/cmp_wb_adapter/stored_we
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3726500 ps} 0}
configure wave -namecolwidth 340
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {32476800 ps}

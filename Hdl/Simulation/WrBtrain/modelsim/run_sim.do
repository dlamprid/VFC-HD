#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
# Manoel Barros Marin (CERN BE-BI-QP) 02/03/2016
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

# Cleaning the transcript window:
.main clear

###############################################
# Running simulation
###############################################

echo ""
echo "Starting Simulation..."
echo ""

vsim -gui -novopt work.tb_WrBtrainAccess
do wave.do
run -all

echo ""
echo ""
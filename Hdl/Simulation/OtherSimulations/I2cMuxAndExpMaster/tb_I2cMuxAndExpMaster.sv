//============================================================================================\\
//################################   Test Bench Information   ################################\\
//============================================================================================\\
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: <tb_ModuleName>.v  
// 
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - <date>        <version>    <author>           <description>               
//
// Language: Verilog 2005                                                              
//
// Module Under Test: <Module Name (ModuleName.v)>
//                                                                                       
// Targeted device:
//
//     - Vendor: <FPGA manufacturer if vendor specific code>
//     - Model:  <FPGA Model if model specific>
//
// Description:
//
//     <Brief description of the test bench.>                                                                                                       
//                                                                                                   
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

`timescale 1ns/100ps

module tb_I2cMuxAndExpMaster;
//=======================================  Declarations  =====================================\\    

//==== Local parameters ====\\ 

localparam c_VmeSlot = 5'hA;

//==== Wires & Regs ====\\ 
reg Clk_k = 1'b0;
reg Rst_rq = 1'b0;
wire I2cMuxScl, I2cMuxSda;
wire [1:0] I2cMuxInt_nb2;
wire I2cIoExpIntApp12_n, I2cIoExpIntApp34_n, I2cIoExpIntBstEth_n, I2cIoExpIntBlmIn_n;
reg AppSfp1TxFault = 1'b0, 
    AppSfp1TxDisable = 1'b0, 
    AppSfp1RateSelect = 1'b0, 
    AppSfp1ModeDef0 = 1'b0;
reg AppSfp2TxFault = 1'b0, 
    AppSfp2TxDisable = 1'b0, 
    AppSfp2RateSelect = 1'b0, 
    AppSfp2ModeDef0 = 1'b0;
reg AppSfp3TxFault = 1'b0, 
    AppSfp3TxDisable = 1'b0, 
    AppSfp3RateSelect = 1'b0, 
    AppSfp3ModeDef0 = 1'b0;
reg AppSfp4TxFault = 1'b0, 
    AppSfp4TxDisable = 1'b0, 
    AppSfp4RateSelect = 1'b0, 
    AppSfp4ModeDef0 = 1'b0;
reg BstSfpTxFault = 1'b0, 
    BstSfpTxDisable = 1'b0, 
    BstSfpRateSelect = 1'b0, 
    BstSfpModeDef0 = 1'b0;
reg EthSfpTxFault = 1'b0, 
    EthSfpTxDisable = 1'b0, 
    EthSfpRateSelect = 1'b0, 
    EthSfpModeDef0 = 1'b0;
wire [7:0] BlmIn_b8, FpLed_nb8;
wire EnTermGpio4, EnTermGpio3, EnTermGpio2, EnTermGpio1, Gpio34A2B, Gpio2A2B, Gpio1A2B;
wire Float_z = 1'bz;
wire P1P2Gap_n;
wire [4:0] P1P2Ga_nb5;
reg AppSfp1Los = 1'b0;
wire  AppSfp1ModeDef1, AppSfp1ModeDef2;   
reg AppSfp2Los = 1'b0;
wire  AppSfp2ModeDef1, AppSfp2ModeDef2; 
reg AppSfp3Los = 1'b0;
wire  AppSfp3ModeDef1, AppSfp3ModeDef2; 
reg AppSfp4Los = 1'b0;
wire  AppSfp4ModeDef1, AppSfp4ModeDef2; 
reg BstSfpLos = 1'b0;
wire  BstSfpModeDef1, BstSfpModeDef2; 
reg EthSfpLos = 1'b0;
wire  EthSfpModeDef1, EthSfpModeDef2; 
reg CdrLos = 1'b0, 
    CdrLol = 1'b0;
wire Si57xSda, Si57xScl;
wire CdrScl, CdrSda;
reg IoExpWrReq = 1'b0,  
    IoExpRdReq = 1'b0,  
    I2cMuxEnChReq = 1'b0,  
    I2cMuxIntRdReq = 1'b0,  
    FifoAccessReq = 1'b0;  
wire Busy, IoExpWrOn, IoExpRdOn, I2cEnChOn, I2cMuxRdIntOn, FifoAccessOn; 
wire NewByteRead, AckError;
wire [7:0] ByteOut_b8;
reg [2:0] IoExpAddr_b3 = 3'b0;
reg [1:0] IoExpRegAddr_b2 = 2'b00;
reg [7:0] IoExpData_b8;
reg I2cMuxAddress = 1'b0;
reg [1:0] I2cMuxChannel_b2 = 2'b00;
wire FifoFull, FifoEmpty;
reg [10:0] FifoCommand_b11 = 11'h0;
reg FifoCommandWr = 1'b0;
wire [7:0] FifoByteRead_b8;
reg [7:0] Dummy_b8;


//==== Tasks ====\\ 
task ReadIoExpReg (input [2:0] IoExpAddr_ib3, input [1:0] IoExpRegAddr_ib2, output [7:0] ReadByte_ob8);	
    if (Busy) @(negedge Busy);
    @(posedge Clk_k) #1;
    IoExpRdReq      = 1'b1;
    IoExpAddr_b3    = IoExpAddr_ib3;
    IoExpRegAddr_b2 = IoExpRegAddr_ib2;
    fork
    // Time out check
    begin
        repeat(1000000) @(posedge Clk_k);
        $display("Cycle time out: aborting request...");
        @(posedge Clk_k) #1;
        IoExpRdReq      = 1'b0;
        @(posedge Clk_k) #1;
        $stop();
        disable ReadIoExpReg;
    end
    // Wrong process on check
    begin
        while (1'b1) begin
            if (IoExpWrOn || I2cEnChOn || I2cMuxRdIntOn  || FifoAccessOn) begin
                $display("After a IoExpRead request unexpected assertion of another process:");
                if (IoExpWrOn) $display("    IoExpWrOn");
                if (I2cEnChOn) $display("    I2cEnChOn");
                if (I2cMuxRdIntOn) $display("    I2cMuxRdIntOn");
                if (FifoAccessOn) $display("    FifoAccessOn");
                $display("Aborting request....");
                @(posedge Clk_k) #1;
                IoExpRdReq      = 1'b0;
                @(posedge Clk_k) #1;
                $stop();
                disable ReadIoExpReg;  
            end
            @(posedge Clk_k);
        end   
    end
    // Abortion of process in the middle
    begin
        @(posedge Busy);
        while (IoExpRdOn) @(posedge Clk_k);
        if (Busy) begin
            $display("Unexpected deassertion of IoExpRdOn with Busy still on: aborting...");
            @(posedge Clk_k) #1;
            IoExpRdReq      = 1'b0;
            @(posedge Clk_k) #1;
            $stop();
            disable ReadIoExpReg;   
        end       
    end
    // Extra Byte read check
    begin
        @(posedge Busy);
        @(posedge NewByteRead);
        @(posedge Clk_k);
        while (1'b1) begin
            @(posedge Clk_k);
            if (NewByteRead) begin
                $display("Unexpected extra byte read: aborting...");
                @(posedge Clk_k) #1;
                IoExpRdReq      = 1'b0;
                @(posedge Clk_k) #1;
                $stop();
                disable ReadIoExpReg;   
            end
        end
    end
    // Wrong Ack check
    begin
        @(posedge Busy);
        @(posedge AckError);
        $display("Ack Error: aborting...");
        @(posedge Clk_k) #1;
        IoExpRdReq      = 1'b0;
        @(posedge Clk_k) #1;
        $stop();
        disable ReadIoExpReg;    
    end
    // Deassertion of IoExpRdOn check
    begin
        @(posedge Busy);           
        while (Busy) begin
            @(posedge Clk_k);
            if (~IoExpRdOn) begin
                $display("IoExpRdOn deasserted too early: aborting...");
              
                @(posedge Clk_k) #1;
                IoExpRdReq      = 1'b0;
                @(posedge Clk_k) #1;
                $stop();
                disable ReadIoExpReg;             
            end
        end
    end
    // Normal procedure
    begin
        @(posedge Busy);           
        @(posedge NewByteRead);
        @(posedge Clk_k) ReadByte_ob8 = ByteOut_b8;
        @(negedge Busy);
        @(posedge Clk_k);
    end
	join_any;
	disable fork;  
endtask:ReadIoExpReg

//=====================================  Status & Control  ====================================\\    


//==== Stimulus ====\\    
initial begin
    // Reset
    @(posedge Clk_k) #1 Rst_rq = 1'b1;
    @(posedge Clk_k) #1 Rst_rq = 1'b0;
    repeat (10) @(posedge Clk_k);
    // Reading the Geographical address
    ReadIoExpReg(3'd3, 2'd0, Dummy_b8); 
    if ((~^(Dummy_b8[4:0]))==Dummy_b8[5])
        $display("Board plugged in slot %d", ~Dummy_b8[4:0]);
    else $display("Error in the GA. GA pins detected as %b and GAP as %b", Dummy_b8[4:0], Dummy_b8[5]);
    $stop();
end  
   
//==== Clocks generation ====\\ 
always #4 Clk_k     = ~Clk_k; // Comment: 8ns period (125MHz)   
   
//=== Test logic ====\\ 
pca9544a i_Ic35(
    .A_ib3(3'b000),
    .Int_on(I2cMuxInt_nb2[0]),
    .Scl_io(I2cMuxScl),
    .Sda_io(I2cMuxSda),
    .Int0_in(AppSfp1Los),
    .Sc0_io(AppSfp1ModeDef1),
    .Sd0_io(AppSfp1ModeDef2),   
    .Int1_in(AppSfp2Los),
    .Sc1_io(AppSfp2ModeDef1),
    .Sd1_io(AppSfp2ModeDef2), 
    .Int2_in(AppSfp3Los),
    .Sc2_io(AppSfp3ModeDef1),
    .Sd2_io(AppSfp3ModeDef2), 
    .Int3_in(AppSfp4Los),
    .Sc3_io(AppSfp4ModeDef1),
    .Sd3_io(AppSfp4ModeDef2));

pullup i_PuAffSfp1Scl(AppSfp1ModeDef1);
pullup i_PuAffSfp1Sda(AppSfp1ModeDef2);

I2CSlave #(.g_Address(7'h50)) // Comment: SFP I2C interfaces use 2 addresses  
    i_AppSfp1I2c50(  
        .Scl_io(AppSfp1ModeDef1),
        .Sda_io(AppSfp1ModeDef2));    
  
I2CSlave #(.g_Address(7'h51))   // Comment: SFP I2C interfaces use 2 addresses  
    i_AppSfp1I2c51(  
        .Scl_io(AppSfp1ModeDef1),
        .Sda_io(AppSfp1ModeDef2)); 
 
pullup i_PuAffSfp2Scl(AppSfp2ModeDef1);
pullup i_PuAffSfp2Sda(AppSfp2ModeDef2);

I2CSlave #(.g_Address(7'h50)) // Comment: SFP I2C interfaces use 2 addresses
    i_AppSfp2I2c50(  
        .Scl_io(AppSfp2ModeDef1),
        .Sda_io(AppSfp2ModeDef2));    
  
I2CSlave #(.g_Address(7'h51)) // Comment: SFP I2C interfaces use 2 addresses
    i_AppSfp2I2c51(  
        .Scl_io(AppSfp2ModeDef1),
        .Sda_io(AppSfp2ModeDef2)); 
        
pullup i_PuAffSfp3Scl(AppSfp3ModeDef1);
pullup i_PuAffSfp3Sda(AppSfp3ModeDef2);
   
I2CSlave #(.g_Address(7'h50)) // Comment: SFP I2C interfaces use 2 addresses
    i_AppSfp3I2c50(  
        .Scl_io(AppSfp3ModeDef1),
        .Sda_io(AppSfp3ModeDef2));    
  
I2CSlave #(.g_Address(7'h51)) // Comment: SFP I2C interfaces use 2 addresses
    i_AppSfp3I2c51(  
        .Scl_io(AppSfp3ModeDef1),
        .Sda_io(AppSfp3ModeDef2)); 

pullup i_PuAffSfp4Scl(AppSfp4ModeDef1);
pullup i_PuAffSfp4Sda(AppSfp4ModeDef2);  

I2CSlave #(.g_Address(7'h50)) // Comment: SFP I2C interfaces use 2 addresses
    i_AppSfp4I2c50(  
        .Scl_io(AppSfp4ModeDef1),
        .Sda_io(AppSfp4ModeDef2));    
  
I2CSlave #(.g_Address(7'h51)) // Comment: SFP I2C interfaces use 2 addresses
    i_AppSfp4I2c51(  
        .Scl_io(AppSfp4ModeDef1),
        .Sda_io(AppSfp4ModeDef2));         
         
pca9544a i_Ic2(
    .A_ib3(3'b001),
    .Int_on(I2cMuxInt_nb2[1]),
    .Scl_io(I2cMuxScl),    
    .Sda_io(I2cMuxSda),
    .Int0_in(BstSfpLos),
    .Sc0_io(BstSfpModeDef1),
    .Sd0_io(BstSfpModeDef2),   
    .Int1_in(EthSfpLos),
    .Sc1_io(EthSfpModeDef1),
    .Sd1_io(EthSfpModeDef2), 
    .Int2_in(CdrLos),
    .Sc2_io(Si57xScl),
    .Sd2_io(Si57xSda),
    .Int3_in(CdrLol),
    .Sc3_io(CdrScl),
    .Sd3_io(CdrSda));
    
pullup i_PuBstSfpScl(BstSfpModeDef1);
pullup i_PuBstSfpSda(BstSfpModeDef2);
   
I2CSlave #(.g_Address(7'h50)) // Comment: SFP I2C interfaces use 2 addresses
    i_BstSfpI2c50(  
        .Scl_io(BstSfpModeDef1),
        .Sda_io(BstSfpModeDef2));    
  
I2CSlave #(.g_Address(7'h51)) // Comment: SFP I2C interfaces use 2 addresses
    i_BstSfpI2c51(  
        .Scl_io(BstSfpModeDef1),
        .Sda_io(BstSfpModeDef2)); 

pullup i_PuEthSfpScl(EthSfpModeDef1);
pullup i_PuEthSfpSda(EthSfpModeDef2);  

I2CSlave #(.g_Address(7'h50)) // Comment: SFP I2C interfaces use 2 addresses
    i_EthSfpI2c50(  
        .Scl_io(EthSfpModeDef1),
        .Sda_io(EthSfpModeDef2));    
  
I2CSlave #(.g_Address(7'h51)) // Comment: SFP I2C interfaces use 2 addresses
    i_EthSfpI2c51(  
        .Scl_io(EthSfpModeDef1),
        .Sda_io(EthSfpModeDef2));      
  
pullup i_PuSi57xScl(Si57xScl);
pullup i_PuSi57xSda(Si57xSda);
   
I2CSlave #(.g_Address(7'h55)) 
    i_Si57xI2c(  
        .Scl_io(Si57xScl),
        .Sda_io(Si57xSda)); 
  
pullup i_PuCdrScl(CdrScl);
pullup i_PuCdrSda(CdrSda);
   
I2CSlave #(.g_Address(7'h40)) 
    i_CdrI2c(  
        .Scl_io(CdrScl),
        .Sda_io(CdrSda)); 
  
pca9534 i_Ic21(
    .A_ib3(3'b000),
    .IO_iob8({AppSfp2ModeDef0, 
              AppSfp2RateSelect, 
              AppSfp2TxDisable, 
              AppSfp2TxFault, 
              AppSfp1ModeDef0, 
              AppSfp1RateSelect, 
              AppSfp1TxDisable, 
              AppSfp1TxFault}),
    .Int_on  (I2cIoExpIntApp12_n),
    .Scl_io(I2cMuxScl),    
    .Sda_io(I2cMuxSda)
);  
  
pca9534 i_Ic36(
    .A_ib3(3'b001),
    .IO_iob8({AppSfp4ModeDef0, 
              AppSfp4RateSelect, 
              AppSfp4TxDisable, 
              AppSfp4TxFault, 
              AppSfp3ModeDef0, 
              AppSfp3RateSelect, 
              AppSfp3TxDisable, 
              AppSfp3TxFault}),
    .Int_on  (I2cIoExpIntApp34_n),
    .Scl_io(I2cMuxScl),    
    .Sda_io(I2cMuxSda)
); 

pca9534 i_Ic4(
    .A_ib3(3'b010),
    .IO_iob8({EthSfpModeDef0, 
              EthSfpRateSelect, 
              EthSfpTxDisable, 
              EthSfpTxFault, 
              BstSfpModeDef0, 
              BstSfpRateSelect, 
              BstSfpTxDisable, 
              BstSfpTxFault}),
    .Int_on  (I2cIoExpIntBstEth_n),
    .Scl_io(I2cMuxScl),    
    .Sda_io(I2cMuxSda)
); 

assign P1P2Ga_nb5 = ~c_VmeSlot;
assign P1P2Gap_n = ~^(~c_VmeSlot);  

pca9534 i_Ic15(
    .A_ib3(3'b011),
    .IO_iob8({  Float_z,
                Float_z,
                P1P2Gap_n,
                P1P2Ga_nb5[4],
                P1P2Ga_nb5[3],
                P1P2Ga_nb5[2],
                P1P2Ga_nb5[1],
                P1P2Ga_nb5[0]}),
    .Int_on(/*Not connected*/),
    .Scl_io(I2cMuxScl),    
    .Sda_io(I2cMuxSda)
); 

pca9534 i_Ic22(
    .A_ib3(3'b100),
    .IO_iob8({  Float_z, 
                EnTermGpio4, 
                EnTermGpio3, 
                EnTermGpio2, 
                EnTermGpio1, 
                Gpio34A2B, 
                Gpio2A2B, 
                Gpio1A2B}),
    .Int_on(/*Not connected*/),
    .Scl_io(I2cMuxScl),    
    .Sda_io(I2cMuxSda)
); 

pca9534 i_Ic3(
    .A_ib3(3'b101),
    .IO_iob8(FpLed_nb8),
    .Int_on(/*Not connected*/),
    .Scl_io(I2cMuxScl),    
    .Sda_io(I2cMuxSda)
);   
  
pca9534 i_Ic28(
    .A_ib3(3'b110),
    .IO_iob8(BlmIn_b8),
    .Int_on(I2cIoExpIntBlmIn_n),
    .Scl_io(I2cMuxScl),    
    .Sda_io(I2cMuxSda)
);  

 
//==== DUT ====\\ 
I2cExpAndMuxMaster i_DUT (   
    //==== Clocks & Resets ====\\ 
    .Clk_ik(Clk_k),
    .Rst_irq(Rst_rq),
    //==== Status and Requests====\\ 
    .Busy_o(Busy),
    .IoExpWrReq_i(IoExpWrReq),    
    .IoExpRdReq_i(IoExpRdReq),     
    .I2cMuxEnChReq_i(I2cMuxEnChReq),      
    .I2cMuxIntRdReq_i(I2cMuxIntRdReq),     
    .FifoAccessReq_i(FifoAccessReq),  
    .IoExpWrOn_oq(IoExpWrOn),
    .IoExpRdOn_oq(IoExpRdOn),
    .I2cEnChOn_oq(I2cEnChOn),
    .I2cMuxRdIntOn_oq(I2cMuxRdIntOn),    
    .FifoAccessOn_oq(FifoAccessOn), 
    .NewByteRead_op(NewByteRead),
    .ByteOut_ob8(ByteOut_b8),
    .AckError_op(AckError),
    // IO Expanders parameters:
    .IoExpAddr_ib3(IoExpAddr_b3),
    .IoExpRegAddr_ib2(IoExpRegAddr_b2), 
    .IoExpData_ib8(IoExpData_b8),  
    // I2C Mux parameters:
    .I2cMuxAddress_i(I2cMuxAddress),
    .I2cMuxChannel_ib2(I2cMuxChannel_b2), 
    // FIFO access programming interface:
    .FifoFull_o(FifoFull),
    .FifoEmpty_o(FifoEmpty),    
    .FifoCommandWr_i(FifoCommandWr),    
    .FifoCommand_ib11(FifoCommand_b11),
    //==== I2C Bus ====\\ 
    .Scl_ioz(I2cMuxScl),
    .Sda_ioz(I2cMuxSda));   
   
pullup i_InternalSclPu (I2cMuxScl);
pullup i_InternalSdaPu (I2cMuxSda);
  
   
endmodule               
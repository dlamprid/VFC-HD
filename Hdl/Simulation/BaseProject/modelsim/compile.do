#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
# Manoel Barros Marin (CERN BE-BI-QP) 02/03/2016 #
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@#

# Defining Quartus II path:
set QUARTUS_II_PATH C:\altera\15.1\quartus

# Define here the paths used for the different folders:
set DUT_PATH ../../../../FpgaModules/ApplicationSpecific/BaseProject/
set VFC_PATH ../../../../FpgaModules/
set MODELS_PATH ../../../Models/
set TB_PATH ../../

# Clearing the transcript window:
.main clear

echo ""
echo "########################"
echo "# Starting Compilation #"
echo "########################"

###############################################
# Verilog Compile Process
###############################################

proc compile_verilog {sc lib name} {
	file stat $name result
	if {$sc < $result(mtime)} {
	    set ok [vlog -work $lib $name]
	}
}

###############################################
# Defining Last Compilation
###############################################

echo ""
echo "-> Enabling Smart or Full Compilation..."
echo ""

# Checking whether a previous smart compilation exists:
if {![info exists sc]} {
    set sc 0
    set no_sc 1;
} 

if "$no_sc == 1" {
	echo "Smart compilation not possible or not enabled. Running full compilation..."
	# Deleting pre-existing libraries
	if {[file exists work]} {vdel -all -lib work}	
	# Creating and mapping working directory:
	vlib work
    vmap work work
	# Maping Altera libraries:
	#vmap altera      "\$QUARTUS_II_PATH/eda/sim_lib/altera"
    #vmap altera_mf   "\$QUARTUS_II_PATH/eda/sim_lib/altera_mf"
    #vmap arriav      "\$QUARTUS_II_PATH/eda/sim_lib/arriav"
    #vmap arriav_hssi "\$QUARTUS_II_PATH/eda/sim_lib/arriav_hssi"
	set no_sc 0;
} else {
	echo "Smart compilation enabled. Only out of date files will be compiled..."
	puts [clock format $sc -format {Previous compilation time: %A, %d of %B, %Y - %H:%M:%S}]
}

###############################################
# Compiling simulation files
###############################################

echo ""
echo "-> Starting Compilation..."
echo ""
    
# Device Under Test files:
compile_verilog $sc work $DUT_PATH/AddrDecoderWbApp.v
compile_verilog $sc work $DUT_PATH/VfcHdApplication.v

# VFC HD hierarchy:
compile_verilog $sc work $VFC_PATH/GeneralPurpose/Ip_OpenCores/generic_dpram_mod.v
compile_verilog $sc work $VFC_PATH/GeneralPurpose/Ip_OpenCores/generic_fifo_dc_gray_mod.v
compile_verilog $sc work $VFC_PATH/GeneralPurpose/Generic4InputRegs.v
compile_verilog $sc work $VFC_PATH/GeneralPurpose/Generic4OutputRegs.v
compile_verilog $sc work $VFC_PATH/GeneralPurpose/I2CMasterWb.v
compile_verilog $sc work $VFC_PATH/GeneralPurpose/I2cMasterGeneric.v
compile_verilog $sc work $VFC_PATH/GeneralPurpose/SpiMasterWB.v
compile_verilog $sc work $VFC_PATH/SystemSpecific/VmeInterface/InterruptManagerWb.v
compile_verilog $sc work $VFC_PATH/SystemSpecific/VmeInterface/VmeInterfaceWb.v
compile_verilog $sc work $VFC_PATH/SystemSpecific/AddrDecoderWbSys.v
compile_verilog $sc work $VFC_PATH/SystemSpecific/I2cMuxAndExpMaster.v
compile_verilog $sc work $VFC_PATH/SystemSpecific/UniqueIdReader.v
compile_verilog $sc work $VFC_PATH/SystemSpecific/VfcHdSystem.v
compile_verilog $sc work $VFC_PATH/VfcHdTop.v 

# VFC HD models files:
compile_verilog $sc work $MODELS_PATH/I2CSlave.v
compile_verilog $sc work $MODELS_PATH/ivt3205c25mhz.v
compile_verilog $sc work $MODELS_PATH/MAX5483.v
compile_verilog $sc work $MODELS_PATH/pca9534.v
compile_verilog $sc work $MODELS_PATH/pca9544a.v
compile_verilog $sc work $MODELS_PATH/si57x.v
compile_verilog $sc work $MODELS_PATH/sn74vmeh22501.v
compile_verilog $sc work $MODELS_PATH/VfcHd_v2_0.v
compile_verilog $sc work $MODELS_PATH/VmeBusModule.sv

# Test Bench files:
compile_verilog $sc work $TB_PATH/tb_BaseProject.sv

###############################################
# Top file
###############################################

echo ""
echo "-> Setting Top File..."
echo ""

set top_level work.tb_BaseProjecAppAcceses

###############################################
# Acquiring Compilation Time
###############################################

echo ""
set sc [clock seconds];
puts [clock format $sc -format {Setting last compilation time to: %A, %d of %B, %Y - %H:%M:%S}]

echo ""
echo "-> Compilation Done..."
echo ""
echo ""
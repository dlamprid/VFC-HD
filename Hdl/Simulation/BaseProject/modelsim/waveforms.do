onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/As_in
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/AM_ib6
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/A_iob31
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/LWord_io
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/Ds_inb2
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/Wr_in
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/D_iob32
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/DtAck_on
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/Irq_onb7
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/Iack_in
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/IackIn_in
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/IackOut_on
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/SysResetN_irn
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/SysClk_ik
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/Ga_ionb5
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/Gap_ion
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcLaP_iob34
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcLaN_iob34
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcHaP_iob24
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcHaN_iob24
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcHbP_iob22
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcHbN_iob22
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcPrsntM2C_in
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcTck_ok
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcTms_o
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcTdi_o
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcTdo_i
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcTrstL_orn
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcScl_iok
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcSda_io
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcPgM2C_in
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcPgC2M_on
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcClk0M2CCmos_ik
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcClk1M2CCmos_ik
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcClk2Bidir_iok
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcClk3Bidir_iok
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcClkDir_i
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcDpC2M_ob10
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcDpM2C_ib10
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcGbtClk0M2CLeft_ik
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcGbtClk1M2CLeft_ik
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcGbtClk0M2CRight_ik
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcGbtClk1M2CRight_ik
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcGa0_o
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/FmcGa1_o
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/PushButton_i
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/GpIoLemo_iob4
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeDOeN_n
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeDDir
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeIackN_n
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeSysClk_k
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeAs_n
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeAOeN_n
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeADir
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeDtAck_e
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeWr_n
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeLWord_n
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeD_b32
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeA_b31
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeIrq_b7
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeIackInN_n
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeIackOutN_n
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeSysReset_rn
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeGapN_n
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeGaN_nb5
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeDsN_nb2
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/VmeAm_b6
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/GpIo_b4
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/a3_Ic19
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/b3_Ic19
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/GpIo1A2B
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/GpIo2A2B
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/GpIo34A2B
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/OeSi57x
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/Si57xSda
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/Si57xScl
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/Si57xClk_k
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/GbitTrxClkRefR_k
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/Clk20Vcxo_k
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/PushButtonN_n
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/WrPromScl
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/WrPromSda
add wave -noupdate -group {VFC Hd Top} /tb_BaseProject/i_VfcHdSlot1/DipSw_b8
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeAs_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeAm_ib6
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeA_iob31
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeLWord_ion
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeAOe_oen
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeADir_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeDs_inb2
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeWrite_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeD_iob32
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeDOe_oen
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeDDir_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeDtAckOe_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeIrq_ob7
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeIack_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeIackIn_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeIackOut_on
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeSysClk_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeSysReset_irn
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/AppSfpRx_ib4
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/AppSfpTx_ob4
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/BstSfpRx_i
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/BstSfpTx_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/EthSfpRx_i
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/EthSfpTx_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3AA_ob16
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3ABa_ob3
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3ACk_ok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3ACkE_oeb2
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3ACs_onb2
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3ALdm_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3AUdm_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3AOdt_ob2
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3ARas_on
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3ACas_on
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3AWe_on
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3AReset_orn
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3ADq_iob16
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3ALdqs_io
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3AUdqs_io
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BA_ob16
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BBa_ob3
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BCk_ok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BCkE_oeb2
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BCs_onb2
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BLdm_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BUdm_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BOdt_ob2
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BRas_on
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BCas_on
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BWe_on
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BReset_orn
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BDq_iob16
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BLdqs_io
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Ddr3BUdqs_io
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/TestIo1_io
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/TestIo2_io
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/I2cMuxSda_io
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/I2cMuxScl_ok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/I2CMuxIntN0_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/I2CMuxIntN1_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/I2CIoExpIntApp12_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/I2CIoExpIntApp34_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/I2CIoExpIntBstEth_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/I2CIoExpIntBlmIn_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/BstDataIn_i
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/CdrClkOut_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/CdrDataOut_i
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VAdcDout_i
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VAdcDin_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VAdcCs_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VAdcSclk_ok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcLaP_iob34
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcLaN_iob34
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcHaP_iob24
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcHaN_iob24
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcHbP_iob22
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcHbN_iob22
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcPrsntM2C_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcTck_ok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcTms_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcTdi_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcTdo_i
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcTrstL_orn
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcScl_iok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcSda_io
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcPgM2C_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcPgC2M_on
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcClk0M2CCmos_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcClk1M2CCmos_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcClk2Bidir_iok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcClk3Bidir_iok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcClkDir_i
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcDpC2M_ob10
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcDpM2C_ib10
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcGbtClk0M2CLeft_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcGbtClk1M2CLeft_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcGbtClk0M2CRight_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/FmcGbtClk1M2CRight_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/OeSi57x_oe
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Si57xClk_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/ClkFb_ok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/ClkFb_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Clk20VCOx_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/PllDac20Sync_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/PllDac25Sync_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/PllDacSclk_ok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/PllDacDin_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/PllRefSda_io
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/PllRefScl_ok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/PllRefInt_i
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/PllSourceMuxOut_ok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/PllRefClkOut_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/GbitTrxClkRefR_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VadjCs_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VadjSclk_ok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VadjDin_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VfmcEnableN_oen
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/NoGa_ib5
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/UseGa_i
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Switch_ib2
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/PcbRev_ib7
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/P2DataP_iob20
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/P2DataN_iob20
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/P0HwHighByte_ib8
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/P0HwLowByte_ib8
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/DaisyChain1Cntrl_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/DaisyChain2Cntrl_o
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeP0BunchClk_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/VmeP0Tclk_ik
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/WrPromSda_io
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/WrPromScl_ok
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/GpIo_iob4
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/PushButtonN_in
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/TempIdDq_ioz
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/ResetFpgaConfigN_orn
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/WbClk_k
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb1Cyc
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb1Stb
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb1Wr
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb1Ack
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb1Adr_b25
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb1DatMosi_b32
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb1DatMiso_b32
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb2Cyc
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb2Stb
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb2Wr
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb2Ack
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb2Adr_b25
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb2DatMosi_b32
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Wb2DatMiso_b32
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/TopLed_b2
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/BottomLed_b4
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/BstOn
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/BunchClk_k
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/TurnClk_p
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/BstByteAddress_b5
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/BstByte_b8
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/InterruptRequest_pb24
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/StreamerClk_k
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/SreamerDav
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/StreamerPckt
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/StreamerWait
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/StreamerData_b32
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/GpIo1DirOut
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/GpIo2DirOut
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/GpIo34DirOut
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/Reset_rqp
add wave -noupdate -group {FPGA Top} /tb_BaseProject/i_VfcHdSlot1/i_Fpga/ResetRequest_qp
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/g_SystemVersion_b8
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/g_SystemReleaseDay_b8
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/g_SystemReleaseMonth_b8
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/g_SystemReleaseYear_b8
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeAs_in
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeAm_ib6
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeA_iob31
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeLWord_ion
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeAOe_oen
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeADir_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeDs_inb2
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeWrite_in
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeD_iob32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeDOe_oen
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeDDir_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeDtAckOe_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeIrq_ob7
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeIack_in
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeIackIn_in
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeIackOut_on
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeSysClk_ik
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeSysReset_irn
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2cMuxSda_io
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2cMuxScl_ok
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2CMuxIntN0_in
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2CMuxIntN1_in
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2CIoExpIntApp12_in
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2CIoExpIntApp34_in
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2CIoExpIntBstEth_in
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/I2CIoExpIntBlmIn_in
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/BstDataIn_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/CdrClkOut_ik
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/CdrDataOut_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VAdcDout_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VAdcDin_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VAdcCs_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VAdcSclk_ok
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/GbitTrxClkRefR_ik
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VadjCs_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VadjSclk_ok
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VadjDin_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VfmcEnableN_oen
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/NoGa_ib5
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/UseGa_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/PcbRev_ib7
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WrPromSda_io
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WrPromScl_ok
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/TempIdDq_ioz
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/ResetFpgaConfigN_orn
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/Reset_orqp
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/ResetRequest_iqp
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbClk_ik
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterCyc_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterStb_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterAdr_ob25
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterWr_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterDat_ob32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterDat_ib32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterAck_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveCyc_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveStb_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveAdr_ib25
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveWr_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveDat_ib32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveDat_ob32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbSlaveAck_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/TopLed_ib2
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/BottomLed_ib4
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/BstOn_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/BunchClk_ok
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/TurnClk_op
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/BstByte_ob8
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/BstByteAddress_ob5
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/InterruptRequest_ipb24
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/StreamerClk_ik
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/StreamerData_ib32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/SreamerDav_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/StreamerPckt_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/StreamerWait_o
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/GpIo1DirOut_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/GpIo2DirOut_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/GpIo34DirOut_i
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeAccess
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeDtAck_n
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeIrq_nb7
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbCyc
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStb
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbWe
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAck
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAdr_b22
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatMiSo_b32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatMoSi_b32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/IntEnable
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/IntModeRoRa
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/IntSourceToRead
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/NewIntRequest
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/IntLevel_b3
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/IntVector_b8
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/IntRequestBus_ab32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeSysReset_d2
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckIntManager
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStbIntManager
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatMiSoIntManager_b32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckSpiMaster
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStbSpiMaster
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatSpiMaster_b32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckUniqueIdReader
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStbUniqueIdReader
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatUniqueIdReader_b32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckAppSlaveBus
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStbAppSlaveBus
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatAppSlaveBus_b32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStbI2cIoExpAndMux
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckI2cIoExpAndMux
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatI2cIoExpAndMux_b32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/SpiClk_k
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/SpiMoSi
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/SpiMiSo_b32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/SpiSs_nb32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbMasterStb_xd2
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckAppSlaveBus_xd2
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeGa_b5
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/VmeGap_n
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbAckI2cWrProm
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbStbI2cWrProm
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/WbDatI2cWrProm_b32
add wave -noupdate -group {System Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/Clk_k
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/g_LowestGaAddressBit
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/g_ClocksIn2us
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/s_Idle
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/s_RdWaitWbAnswer
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/s_RdCloseVmeCycle
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/s_WrCloseVmeCycle
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/s_WrWaitWbAnswer
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/s_IntAck
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/s_IAckPass
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/Clk_ik
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/Rst_irq
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeGa_ib5
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeGap_in
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeAs_in
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeDs_inb2
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeAm_ib6
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeWr_in
add wave -noupdate -expand -group VmeInterfaceWb -color Magenta -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeDtAck_on
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeLWord_in
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeA_ib31
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeD_iob32
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeIack_in
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeIackIn_in
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeIackOut_on
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeIrq_onb7
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeDataBuffsOutMode_o
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeAccess_o
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/IntEnable_i
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/IntModeRora_i
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/IntLevel_ib3
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/IntVector_ib8
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/IntSourceToRead_i
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/NewIntRequest_iqp
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/WbCyc_o
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/WbStb_o
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/WbWe_o
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/WbAdr_ob
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/WbDat_ib32
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/WbDat_ob32
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/WbAck_i
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/State_qb3
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/NextState_ab3
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/WbTimeoutCounter_c14
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/IntToAckCounter_c8
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/RoraTimeoutCounter_c10
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/InternalDataReg_b31
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeAddressInternal_b30
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeAsShReg_b3
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/NegedgeVmeAs_a
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/a_VmeAsSynch
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeDs0ShReg_b3
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/NegedgeVmeDs0_a
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/a_VmeDs0Synch
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeDs1ShReg_b3
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/NegedgeVmeDs1_a
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/a_VmeDs1Synch
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeIackInShReg_b3
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/a_VmeIackInSynch
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeGaPError_a
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeAmValid_a
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeRWAccess_a
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeIackCycle_a
add wave -noupdate -expand -group VmeInterfaceWb -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdSystem/i_VmeInterfaceWb/VmeDataBuffsOutEnable_e
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/g_ApplicationVersion_b8
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/g_ApplicationReleaseDay_b8
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/g_ApplicationReleaseMonth_b8
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/g_ApplicationReleaseYear_b8
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/BstSfpRx_i
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/BstSfpTx_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/EthSfpRx_i
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/EthSfpTx_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/AppSfpRx_ib4
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/AppSfpTx_ob4
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrBa_ob3
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrDm_ob8
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrDqs_iob8
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrDq_iob64
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrA_ob16
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrCk_okb2
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrCkE_ohb2
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrReset_orn
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrRas_on
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrCas_on
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrWe_on
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrCs_onb2
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrOdt_ob2
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrTempEvent_in
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrI2cScl_ok
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DdrI2cSda_io
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/TestIo1_io
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/TestIo2_io
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcLaP_iob34
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcLaN_iob34
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcHaP_iob24
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcHaN_iob24
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcHbP_iob22
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcHbN_iob22
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcPrsntM2C_in
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcTck_ok
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcTms_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcTdi_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcTdo_i
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcTrstL_orn
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcSda_io
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcPgM2C_in
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcPgC2M_on
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcClk0M2CCmos_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcClk1M2CCmos_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcClkDir_i
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcDpC2M_ob10
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcDpM2C_ib10
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcGbtClk0M2CLeft_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcGbtClk1M2CLeft_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcGbtClk0M2CRight_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/FmcGbtClk1M2CRight_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/OeSi57x_oe
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/Si57xClk_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/ClkFb_ok
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/ClkFb_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/Clk20VCOx_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/PllDac20Sync_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/PllDac25Sync_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/PllDacSclk_ok
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/PllDacDin_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/PllRefSda_io
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/PllRefScl_ok
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/PllRefInt_i
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/PllSourceMuxOut_ok
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/PllRefClkOut_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/GbitTrxClkRefR_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/Switch_ib2
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/P2DataP_iob20
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/P2DataN_iob20
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/P0HwHighByte_ib8
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/P0HwLowByte_ib8
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DaisyChain1Cntrl_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/DaisyChain2Cntrl_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/VmeP0BunchClk_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/VmeP0Tclk_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/GpIo_iob4
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/PushButtonN_in
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/Reset_irqp
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/ResetRequest_oqp
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbClk_ok
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbSlaveCyc_i
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbSlaveStb_i
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbSlaveAdr_ib25
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbSlaveWr_i
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbSlaveDat_ib32
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbSlaveDat_ob32
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbSlaveAck_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbMasterCyc_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbMasterStb_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbMasterAdr_ob25
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbMasterWr_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbMasterDat_ob32
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbMasterDat_ib32
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbMasterAck_i
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/TopLed_ob2
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/BottomLed_ob4
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/BstOn_i
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/BunchClk_ik
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/TurnClk_ip
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/BstByteAddress_ib5
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/BstByte_ib8
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/InterruptRequest_opb24
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/StreamerClk_ok
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/StreamerData_ob32
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/SreamerDav_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/StreamerPckt_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/StreamerWait_i
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/GpIo1DirOut_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/GpIo2DirOut_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/GpIo34DirOut_o
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/Clk_k
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbStbAppReleaseId
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbAckAppReleaseId
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbDatAppReleaseId_b32
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbStbCtrlReg
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbAckCtrlReg
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbDatCtrlReg_b32
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbStbStatReg
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbAckStatReg
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/WbDatStatReg_b32
add wave -noupdate -group {Application Top} -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/Reg0Value_b32
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/dly
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/c_SelNothing
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/c_SelAppRevisionId
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/c_SelCtrlReg
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/c_SelStatReg
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/Clk_ik
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/Adr_ib21
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/Stb_i
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/Dat_ob32
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/Ack_o
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/DatAppReleaseId_ib32
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/AckAppReleaseId_i
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/StbAppReleaseId_o
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/DatCtrlReg_ib32
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/AckCtrlReg_i
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/StbCtrlReg_o
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/DatStatReg_ib32
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/AckStatReg_i
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/StbStatReg_o
add wave -noupdate -group AppWbAddrDec -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_AddrDecoderWbApp/SelectedModule_b2
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Reg0Default
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Reg0AutoClrMask
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Reg1Default
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Reg1AutoClrMask
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Reg2Default
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Reg2AutoClrMask
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Reg3Default
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Reg3AutoClrMask
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Rst_irq
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Clk_ik
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Cyc_i
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Stb_i
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/We_i
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Adr_ib2
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Dat_ib32
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Dat_oab32
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Ack_oa
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Reg0Value_ob32
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Reg1Value_ob32
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Reg2Value_ob32
add wave -noupdate -group ControlRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_ControlRegs/Reg3Value_ob32
add wave -noupdate -group StatusRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_StatusRegs/Rst_irq
add wave -noupdate -group StatusRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_StatusRegs/Cyc_i
add wave -noupdate -group StatusRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_StatusRegs/Stb_i
add wave -noupdate -group StatusRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_StatusRegs/Clk_ik
add wave -noupdate -group StatusRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_StatusRegs/Adr_ib2
add wave -noupdate -group StatusRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_StatusRegs/Dat_oab32
add wave -noupdate -group StatusRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_StatusRegs/Ack_oa
add wave -noupdate -group StatusRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_StatusRegs/Reg0Value_ib32
add wave -noupdate -group StatusRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_StatusRegs/Reg1Value_ib32
add wave -noupdate -group StatusRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_StatusRegs/Reg2Value_ib32
add wave -noupdate -group StatusRegs -radix hexadecimal /tb_BaseProject/i_VfcHdSlot1/i_Fpga/i_VfcHdApplication/i_StatusRegs/Reg3Value_ib32
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {609700 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 340
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {67344400 ps}

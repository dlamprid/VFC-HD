//============================================================================================\\
//################################   Test Bench Information   ################################\\
//============================================================================================\\
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: tb_BaseProject.v  
// 
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 22/06/16      1.0          A. Boccardi        First test bench definition.               
//
// Language: Verilog 2005                                                              
//
// Module Under Test: VFC-HD
//
// Description:
//
//     System level simulation of the VFC-HD.
//                                                                                                   
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

`timescale 1ns/100ps

module tb_BaseProject;
//=======================================  Declarations  =====================================\\    

//==== Local parameters ====\\ 

localparam c_VmeAccessVerbose = 1'b1; 

// Ga Control:
localparam c_UseGa   = 1'b0;
localparam c_NoGa_b5 = 5'h6;

// Base Address:
localparam c_VfcSlot4BaseAddress = 32'h0600_0000;  // Comment: The address is for now fixed

// System Memory Addresses:
localparam c_SysIntSource      = 32'h0000_0000,  //Comment: Interrupt Source 
           c_SysIntConfig      = 32'h0000_0004,  //Comment: Interrupt Configuration 
           c_SysIntMask        = 32'h0000_0008,  //Comment: Interrupt source mask 
           c_SysReleaseId      = 32'h0000_000C,  //Comment: Release ID of the system block 
           c_SysIoExpStatus    = 32'h0000_0010,  //Comment: I2C mux and IO expander controller status  
           c_SysIoExpCommand   = 32'h0000_0014,  //Comment: I2C mux and IO expander controller command reg            
           c_SysUidStatus      = 32'h0000_0020,  //Comment: Unique Id and Temperature controller Status            
           c_SysUidCommand     = 32'h0000_0024,  //Comment: Unique Id and Temperature controller Command reg 
           c_SysWrPrmStatus    = 32'h0000_0030,  //Comment: WR PROM I2C controller status  
           c_SysWrPrmCommand   = 32'h0000_0034,  //Comment: WR PROM I2C controller command reg 
           c_SysSpiStatus      = 32'h0000_0040,  //Comment: Spi Master Status  
           c_SysSpiConfig1     = 32'h0000_0044,  //Comment: Spi Master Config 1  
           c_SysSpiConfig2     = 32'h0000_0048,  //Comment: Spi Master Config 2 
           c_SysSpiShiftOut    = 32'h0000_004C,  //Comment: Spi Master data to shift out 
           c_SysSpiShiftIn     = 32'h0000_0050;  //Comment: Spi Master data shifted in 

// Application Memory Addresses:
localparam c_AppAddrOffset     = 32'h0080_0000;                    // Comment: Address offset of the Aplication  
//--
localparam c_AppReleaseId      = c_AppAddrOffset + 32'h0000_0000,  //Comment: Interrupt Source 
           c_AppCtrlReg        = c_AppAddrOffset + 32'h0000_0010,  //Comment: Interrupt Configuration 
           c_AppStatReg        = c_AppAddrOffset + 32'h0000_0020;  //Comment: Interrupt source mask

//==== Wires & Regs ====\\ 
wire As_n;
wire [5:0] AM_b6;
wire [31:1] A_b31;
wire LWord;
wire [1:0] Ds_nb2;
wire Wr_n;
wire [31:0] D_b32;
wire DtAck_n;
wire [7:1] Irq_nb7;
wire Iack_n;
wire [20:1] IackIn_nb;
wire [20:1] IackOut_nb;
wire SysResetN_irn;
wire SysClk_k;
wire [ 4:0] Ga_nmb5 [20:1];
wire [20:1] Gap_nbm;
logic PushButton = 0;
logic [7:0] LastVmeAccessExitCode_b8;
logic [31:0] LastVmeReadData_b32;

//=====================================  Status & Control  ====================================\\    

task Read (input [31:0] Address_b32);	
	i_VmeBus.ReadA32D32(c_VfcSlot4BaseAddress + Address_b32, LastVmeReadData_b32, LastVmeAccessExitCode_b8, c_VmeAccessVerbose);
endtask:Read

task Write (input [31:0] Address_b32, input [31:0] Data_b32);	
	i_VmeBus.WriteA32D32(c_VfcSlot4BaseAddress + Address_b32, Data_b32, LastVmeAccessExitCode_b8, c_VmeAccessVerbose);
endtask:Write

task Reset();
	i_VmeBus.VmeReset(500, c_VmeAccessVerbose);	
endtask:Reset

//==== Stimulus & Display ====\\    
initial begin
    Reset();
	#100;
	//System:
    $display("-> System Simulation at   ", $time);
    Read(c_SysReleaseId);
	Read(c_SysIntMask);
    Write(c_SysIntMask, 32'habcd_0001);
    Read(c_SysIntMask);
    //Application:
    $display("-> Application Simulation at   ", $time);
    $display("-> Release ID register at   ", $time);
    Read(c_AppReleaseId);
    Read(c_AppReleaseId+4'h4);
    Read(c_AppReleaseId+4'h8);
    Read(c_AppReleaseId+4'hC);
    $display("-> Reading Control register at   ", $time);
    Read(c_AppCtrlReg);
    $display("-> Reading Status register at   ", $time);
    Read(c_AppStatReg);
    Read(c_AppStatReg+4'h4);
    Read(c_AppStatReg+4'h8);
    Read(c_AppStatReg+4'hC);
    $display("-> Writing Control register at   ", $time);
    Write(c_AppCtrlReg,      32'h89ABCDEF);
    $display("-> Reading Control register at   ", $time);
    Read(c_AppCtrlReg);
    $display("-> Reading Status register at   ", $time);
    Read(c_AppStatReg);
    Read(c_AppStatReg+4'h4);
    Read(c_AppStatReg+4'h8);
    Read(c_AppStatReg+4'hC);
	repeat(1000)@(posedge SysClk_k);
	#10000;	
	$stop();
end
   
//=== VME bus and transaction models ====\\ 
VmeBusModule #(.g_NSlots(20), .g_VerboseDefault(1))
    i_VmeBus(    
        .As_n,
        .AM_b6,
        .A_b31,
        .LWord,
        .Ds_nb2,
        .Wr_n,
        .D_b32,
        .DtAck_n,
        .Irq_nb7,
        .Iack_n,
        .IackIn_nb,
        .IackOut_nb,
        .SysResetN_irn,
        .SysClk_k,
        .Ga_nmb5,
        .Gap_nbm);
      
//==== VFC ====\\ 
VfcHd_v2_0 
    i_VfcHdSlot1(
        // VME interface:
        .As_in(As_n),
        .AM_ib6(AM_b6),
        .A_iob31(A_b31),
        .LWord_io(LWord),
        .Ds_inb2(Ds_nb2),
        .Wr_in(Wr_n),
        .D_iob32(D_b32),
        .DtAck_on(DtAck_n),
        .Irq_onb7(Irq_nb7),
        .Iack_in(Iack_n),
        .IackIn_in(IackIn_nb[1]),
        .IackOut_on(IackOut_nb[1]),
        .SysResetN_irn(SysResetN_irn),
        .SysClk_ik(SysClk_k),
        .Ga_ionb5(Ga_nmb5[1]),
        .Gap_ion(Gap_nbm [1]),
        // FMC Connector:
        .FmcLaP_iob34(),
        .FmcLaN_iob34(),
        .FmcHaP_iob24(),
        .FmcHaN_iob24(),
        .FmcHbP_iob22(),
        .FmcHbN_iob22(),
        .FmcPrsntM2C_in(1'b1),
        .FmcTck_ok(),
        .FmcTms_o(),
        .FmcTdi_o(),
        .FmcTdo_i(1'b0),
        .FmcTrstL_orn(),
        .FmcScl_iok(),
        .FmcSda_io(),
        .FmcPgM2C_in(1'b0),
        .FmcPgC2M_on(),
        .FmcClk0M2CCmos_ik(1'b0),
        .FmcClk1M2CCmos_ik(1'b0),
        .FmcClk2Bidir_iok(),
        .FmcClk3Bidir_iok(),
        .FmcClkDir_i(1'b0),
        .FmcDpC2M_ob10(),
        .FmcDpM2C_ib10(10'h0),
        .FmcGbtClk0M2CLeft_ik(1'b0),
        .FmcGbtClk1M2CLeft_ik(1'b0),
        .FmcGbtClk0M2CRight_ik(1'b0),
        .FmcGbtClk1M2CRight_ik(1'b0),
        .FmcGa0_o(),  
        .FmcGa1_o(),   
        // Ga Control (In VFC is done thought SW1):
        .NoGa_ib5(c_NoGa_b5),
        .UseGa_i(c_UseGa),        
        // Miscellaneous:
        .PushButton_i(PushButton),
        .GpIoLemo_iob4());
   
endmodule               
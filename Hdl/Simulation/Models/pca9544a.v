`timescale 1ns/1ns

module pca9544a (
    input  [2:0] A_ib3,
    output       Int_on,
    inout        Scl_io,
    inout        Sda_io,
    input        Int0_in,
    inout        Sc0_io,
    inout        Sd0_io, 
    input        Int1_in,
    inout        Sc1_io,
    inout        Sd1_io,
    input        Int2_in,
    inout        Sc2_io,
    inout        Sd2_io,
    input        Int3_in,
    inout        Sc3_io,
    inout        Sd3_io);

    
    
localparam c_MinPerDown = 2500;
wire [6:0] SlaveAddress_b7 = {4'b1110, A_ib3};

reg [3:0] B_b3 = 3'h0;

integer ShCount = 0;
reg [3:0] State=1;
reg RdWrBit;
reg Scl_q = 1'bz, Sda_q = 1'bz;
integer i;
reg [7:0] ShReg;    
    
assign Scl_io = Scl_q;
assign Sda_io = Sda_q;

localparam  s_Idle          = 1,
            s_Addressing    = 2,
            s_WrRdSel       = 3,
            s_AckAddress    = 4,
            s_GetByte       = 7,
            s_SendAckData   = 8,
            s_SendByte      = 9,
            s_GetAckByte    = 10;

always@(negedge Sda_io) 
    if (Scl_io) begin
        State = s_Addressing;  //Start Bit
        ShCount = 'h0;
    end

always@(posedge Sda_io) 
    if (Scl_io) begin
        State = s_Idle; //Stop Bit
    end


always@(posedge Scl_io) case (State)
    s_Addressing: begin
        ShReg = {ShReg[6:0], Sda_io};
        ShCount = ShCount + 1;
        if (ShCount == 7) begin
            State = (ShReg[6:0]==SlaveAddress_b7) ? s_WrRdSel : s_Idle;
        end
    end
    s_WrRdSel: begin
        RdWrBit = Sda_io;
        State = s_AckAddress;
    end
    s_AckAddress: begin
        ShCount = 0;
        if (RdWrBit) begin
            State = s_SendByte;
            ShReg = {Int3_in, Int2_in, Int1_in, Int0_in, 1'b0, B_b3};                       
        end else begin
            State = s_GetByte;
        end
    end
    s_GetByte: begin
        ShReg = {ShReg[6:0], Sda_io};
        ShCount = ShCount + 1;
        if (ShCount == 8) begin
            State = s_SendAckData; 
            B_b3 = ShReg[3:0];
        end
    end
    s_SendAckData:begin
        ShCount = 0;
        State = s_GetByte;
    end
    s_SendByte: begin
        ShReg = {ShReg[6:0], 1'b0};
        ShCount = ShCount + 1;
        if (ShCount == 8) begin
            State = s_GetAckByte;
            ShReg = {Int3_in, Int2_in, Int1_in, Int0_in, 1'b0, B_b3};          
        end
    end
    s_GetAckByte: begin       
        ShCount = 0;
        State = Sda_io ? s_Idle : s_SendByte;
    end
endcase
            
always@(negedge Scl_io) begin
    Scl_q = 1'b0;
    #c_MinPerDown;
    case (State)
    s_Addressing: begin
       Sda_q = 1'bz; 
       #100;
       Scl_q = 1'bz;
    end
    s_WrRdSel: begin
       Sda_q = 1'bz; 
       #100;
       Scl_q = 1'bz;
    end
    s_AckAddress: begin
       Sda_q = 1'b0; 
       #100;
       Scl_q = 1'bz;
    end
    s_GetByte: begin
       Sda_q = 1'bz; 
       #100;
       Scl_q = 1'bz;
    end
    s_SendAckData:begin
       Sda_q = 1'b0; 
       #100;
       Scl_q = 1'bz;    
    end
    s_SendByte: begin
       Sda_q = ShReg[7] ? 1'bz : 1'b0; 
       #100;
       Scl_q = 1'bz;
    end
    s_GetAckByte: begin       
       Sda_q = 1'bz; 
       #100;
       Scl_q = 1'bz;
    end
    default: begin
        Sda_q = 1'bz; 
       #100;
        Scl_q = 1'bz;
    end   
    endcase    
end    
  
wire En_0 = B_b3 == 4'b100;
wire En_1 = B_b3 == 4'b101;
wire En_2 = B_b3 == 4'b110;
wire En_3 = B_b3 == 4'b111;

tranif1 i_t0 (Scl_io, Sc0_io, En_0);
tranif1 i_t1 (Sda_io, Sd0_io, En_0);
         
tranif1 i_t2 (Scl_io, Sc1_io, En_1);
tranif1 i_t3 (Sda_io, Sd1_io, En_1);
         
tranif1 i_t4 (Scl_io, Sc2_io, En_2);
tranif1 i_t5 (Sda_io, Sd2_io, En_2);
         
tranif1 i_t6 (Scl_io, Sc3_io, En_3);
tranif1 i_t7 (Sda_io, Sd3_io, En_3);

assign Int_on = (Int0_in && Int1_in && Int2_in && Int3_in) ? 1'bz : 1'b0; 
  
endmodule

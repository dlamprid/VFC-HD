`timescale 1ns/100ps 

module VmeBusModule #(	parameter g_NSlots = 20, g_VerboseDefault = 0) 
(
  	inout wire As_n,
	inout wire [5:0] AM_b6,
	inout wire [31:1] A_b31,
	inout wire LWord,
	inout wire [1:0] Ds_nb2,
	inout wire Wr_n,
	inout wire [31:0] D_b32,
	inout wire DtAck_n,
	inout wire [7:1] Irq_nb7,
	inout wire Iack_n,
	inout wire [g_NSlots:1] IackIn_nb,
	inout wire [g_NSlots:1] IackOut_nb,
	inout wire SysResetN_irn,
	inout wire SysClk_k,
	inout wire [4:0] Ga_nmb5 [g_NSlots:1],
	inout wire [g_NSlots:1] Gap_nbm);//I use a inout wire to detect conflicts (the logic type doesn't have a resolver)
	
logic TaskAs_n = 'bz;
logic [5:0] TaskAM_b6 = 'bz;
logic [31:1] TaskA_b31 = 'bz;
logic TaskLWord = 'bz;
logic [1:0] TaskDs_nb2 = 'bz;
logic TaskWr_n = 'bz;
logic [31:0] TaskD_b32 = 'bz;
logic TaskIack_n = 'bz;
logic TaskSysResetN_irn = 'bz;	

logic InternalClk_k = 0;	
		
// PULL UPS AND IACK CHAIN	

assign (pull1, pull0) As_n = 1'b1;
assign (pull1, pull0) Ds_nb2 = 2'b11;
assign (pull1, pull0) DtAck_n = 1'b1;
assign (pull1, pull0) Irq_nb7 = 7'b111_1111;
assign (pull1, pull0) Iack_n = 1'b1;
assign (pull1, pull0) IackOut_nb = IackIn_nb;
assign IackIn_nb = {IackOut_nb[g_NSlots-1:1], Iack_n};
assign (pull1, pull0) SysResetN_irn = 1'b1;

	
// VME SYSCLK

always #12.5ns ++InternalClk_k;
assign SysClk_k = InternalClk_k; 

// LINKS BETWEEN TASK LOCIC AND inout wireS	

assign As_n		= TaskAs_n;
assign AM_b6	= TaskAM_b6;
assign A_b31	= TaskA_b31;
assign LWord	= TaskLWord;
assign Ds_nb2	= TaskDs_nb2;
assign Wr_n		= TaskWr_n;
assign D_b32	= TaskD_b32;
assign Iack_n	= TaskIack_n;
assign SysResetN_irn = TaskSysResetN_irn;
 
// GA	

for (genvar SlotNumber=1; SlotNumber<g_NSlots+1; SlotNumber++) begin
  assign (highz1, supply0) Ga_nmb5[SlotNumber] = ~SlotNumber[4:0];
  assign (highz1, supply0) Gap_nbm[SlotNumber] = ~^(~SlotNumber[4:0]);
end


// TASKS	

task ReadA32D32 (input [31:0] Address_b32, output [31:0] Data_b32, output [7:0] ExitCode_b8, input VerboseAccesses = g_VerboseDefault);	
	//Making sure everything is 'free' before the task
	TaskAs_n = 'b1;
	TaskAM_b6 = 'b1;
	TaskA_b31 = 'b1;
	TaskLWord = 'b1;
	TaskDs_nb2 = 'b1;
	TaskWr_n = 'b1;
	TaskD_b32 = 'bz;
	TaskIack_n = 'b1;
	TaskSysResetN_irn = 'b1;
	//The actual read sequence
	{TaskA_b31, TaskLWord} = {Address_b32[31:2], 2'b0};
	TaskAM_b6 = 6'h09;
	#10ns TaskAs_n = 1'b0;
	#10ns TaskDs_nb2 = 2'b00;	
	fork
		@(negedge DtAck_n);
		#1000ns;
	join_any;
	disable fork;  //not really needed except for saving memory and CPU
	if (DtAck_n) begin
		ExitCode_b8 = 8'h1; //timeout
		if (VerboseAccesses) $display("VME read cycle ERROR (time out): address %h at %t", Address_b32, $time);
		disable ReadA32D32;
	end
	Data_b32 = D_b32;
	#10ns TaskDs_nb2 = 'bz;	
	TaskAs_n = 'bz;
	fork
		@(posedge DtAck_n);
		#1000ns;
	join_any;
	disable fork;  //not really needed except for saving memory and CPU
	if (~DtAck_n) begin
		Data_b32 = 'hx;
		ExitCode_b8 = 8'h1; //timeout
		if (VerboseAccesses) $display("VME read cycle ERROR (time out): address %h at %t", Address_b32, $time);
		disable ReadA32D32;
	end		
	ExitCode_b8 = 8'h0; //Process executed
	#10ns;
	//Releasing explicitly all the lines 
	TaskAs_n = 'bz;
	TaskAM_b6 = 'bz;
	TaskA_b31 = 'bz;
	TaskLWord = 'bz;
	TaskDs_nb2 = 'bz;
	TaskWr_n = 'bz;
	TaskD_b32 = 'bz;
	TaskIack_n = 'bz;
	TaskSysResetN_irn = 'bz;
	if (VerboseAccesses) $display("VME read cycle OK: d = %h at address %h of A32 address space at %t", Data_b32, Address_b32, $time);
endtask:ReadA32D32

task WriteA32D32 (input [31:0] Address_b32, input [31:0] Data_b32, output [7:0] ExitCode_b8, input VerboseAccesses = g_VerboseDefault);
	//Making sure everything is 'free' before the task
	TaskAs_n = 'b1;
	TaskAM_b6 = 'b1;
	TaskA_b31 = 'b1;
	TaskLWord = 'b1;
	TaskDs_nb2 = 2'b11;
	TaskWr_n = 'b1;
	TaskD_b32 = 'bz;
	TaskIack_n = 'b1;
	TaskSysResetN_irn = 'b1;	
	//The actual write sequence
	{TaskA_b31, TaskLWord} = {Address_b32[31:2], 2'b0};
	TaskAM_b6 = 6'h09;
	#10ns TaskAs_n = 1'b0;
	TaskWr_n = 1'b0;
	TaskD_b32 = Data_b32;
	#10ns TaskDs_nb2 = 2'b00;
	fork
		@(negedge DtAck_n);
		#1000ns;
	join_any;
	disable fork;  //not really needed except for saving memory and CPU
	if (DtAck_n) begin
		ExitCode_b8 = 8'h1; //timeout
		if (VerboseAccesses) $display("VME write cycle ERROR (time out): d = %h at address %h at %t",Data_b32 ,Address_b32, $time);	
		disable WriteA32D32;
	end		
	#10ns TaskDs_nb2 = 'bz;	
	TaskAs_n = 'bz;
	#10ns TaskD_b32 = 'hz;
	fork
		@(posedge DtAck_n);
		#1000ns;
	join_any;
	disable fork;  //not really needed except for saving memory and CPU
	if (~DtAck_n) begin
		Data_b32 = 'hx;
		ExitCode_b8 = 8'h1; //timeout
		if (VerboseAccesses) $display("VME write cycle ERROR (time out): d = %h at address %h at %t",Data_b32 ,Address_b32, $time);	
		disable WriteA32D32;
	end		
	ExitCode_b8 = 8'h0; //Process executed
	#10ns;		
	//Releasing explicitly all the lines 
	TaskAs_n = 'bz;
	TaskAM_b6 = 'bz;
	TaskA_b31 = 'bz;
	TaskLWord = 'bz;
	TaskDs_nb2 = 'bz;
	TaskWr_n = 'bz;
	TaskD_b32 = 'bz;
	TaskIack_n = 'bz;
	TaskSysResetN_irn = 'bz;
	if (VerboseAccesses) $display("VME write cycle OK: d = %h of A32 address space at %t",Data_b32 ,Address_b32, $time); 		
endtask:WriteA32D32
			
task VmeReset(integer ResetTime, input VerboseAccesses = g_VerboseDefault);
	TaskSysResetN_irn = 1'b0;
	if (VerboseAccesses) begin
	   $display("VME reset asserted at          %t", $time); 
	end
	#ResetTime TaskSysResetN_irn = 1'bz;
	if (VerboseAccesses) $display("VME reset released at          %t", $time); 
endtask:VmeReset
	
endmodule
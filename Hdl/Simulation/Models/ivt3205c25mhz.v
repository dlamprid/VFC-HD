`timescale 1ns/1ns
module ivt3205c25mhz (output reg out);

initial out=0;
always #20 out = ~out;

endmodule

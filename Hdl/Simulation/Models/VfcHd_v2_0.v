//============================================================================================\\
//##################################   Module Information   ##################################\\
//============================================================================================\\
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: VfcHd_v2_0.v  
//
// File versions history:
//
//        DATE        VERSION             AUTHOR                   DESCRIPTION
//     14-01-2016       2.0       andrea.boccardi@cern.ch    Board behavioural model       
//
// Language: Verilog 2005                                                              
//                                                                                                   
//
// Description:
//
//     This is a behavioural model of the VFC-HD v2.0 board                                                                                                       
//                                                                                                   
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

`timescale 1ns/100ps 

module VfcHd_v2_0 
(   
    //==== VME interface ====\\ 
	input         As_in,
	input   [5:0] AM_ib6,
	inout  [31:1] A_iob31,
	inout         LWord_io,
	input   [1:0] Ds_inb2,
	input         Wr_in,
	inout  [31:0] D_iob32,
	output        DtAck_on,
	output  [7:1] Irq_onb7,
	input         Iack_in,
	input         IackIn_in,
	output        IackOut_on,
	input         SysResetN_irn,
	input         SysClk_ik,
	inout   [4:0] Ga_ionb5,
	inout         Gap_ion,
    //==== FMC Connector ====\\    
    inout  [33:0] FmcLaP_iob34,
    inout  [33:0] FmcLaN_iob34,
    inout  [23:0] FmcHaP_iob24,
    inout  [23:0] FmcHaN_iob24,
    inout  [21:0] FmcHbP_iob22,
    inout  [21:0] FmcHbN_iob22,
    input         FmcPrsntM2C_in,
    output        FmcTck_ok,
    output        FmcTms_o,
    output        FmcTdi_o,
    input         FmcTdo_i,
    output        FmcTrstL_orn,
    inout         FmcScl_iok,
    inout         FmcSda_io,
    input         FmcPgM2C_in,
    output        FmcPgC2M_on,
    input         FmcClk0M2CCmos_ik,
    input         FmcClk1M2CCmos_ik,
    inout         FmcClk2Bidir_iok,
    inout         FmcClk3Bidir_iok,
    input         FmcClkDir_i,
    output [ 9:0] FmcDpC2M_ob10,
    input  [ 9:0] FmcDpM2C_ib10,
    input         FmcGbtClk0M2CLeft_ik,
    input         FmcGbtClk1M2CLeft_ik,
    input         FmcGbtClk0M2CRight_ik,
    input         FmcGbtClk1M2CRight_ik,
    output        FmcGa0_o,  
    output        FmcGa1_o,
    //==== Ga Control (In VFC is done thought SW1) ====\\
    input  [ 4:0] NoGa_ib5,
    input         UseGa_i,
    //==== Miscellaneus ====\\     
    input         PushButton_i,
    inout   [4:1] GpIoLemo_iob4
);     
   
//=======================================  Declarations  =====================================\\    

wire VmeDOeN_n, VmeDDir, VmeIackN_n, VmeSysClk_k, VmeAs_n, VmeAOeN_n, VmeADir, VmeDtAck_e, VmeWr_n, VmeLWord_n;
wire [31:0] VmeD_b32;
wire [31:1] VmeA_b31;
wire [7:1] VmeIrq_b7;		
wire VmeIackInN_n, VmeIackOutN_n, VmeSysReset_rn, VmeGapN_n;
wire [4:0] VmeGaN_nb5;
wire [1:0] VmeDsN_nb2;
wire [5:0] VmeAm_b6;   
wire [4:1] GpIo_b4;
wire [7:0] a3_Ic19;
wire [7:0] b3_Ic19;
wire GpIo1A2B, GpIo2A2B, GpIo34A2B;
wire OeSi57x, Si57xSda, Si57xScl, Si57xClk_k;
reg  GbitTrxClkRefR_k = 1'b0;
reg  Clk20Vcxo_k = 1'b1;
wire PushButtonN_n;
wire WrPromScl, WrPromSda;
wire [7:0] DipSw_b8; 
 
//=======================================  Schematic  =======================================\\    
   
//==== Page 3..8 : FPGA ====\\ 
   
VfcHdTop  
    i_Fpga(	
   // VME interface:		
       .VmeAs_in(VmeAs_n),
       .VmeAm_ib6(VmeAm_b6),
       .VmeA_iob31(VmeA_b31),
       .VmeLWord_ion(VmeLWord_n),
       .VmeAOe_oen(VmeAOeN_n),
       .VmeADir_o(VmeADir),
       .VmeDs_inb2(VmeDsN_nb2),
       .VmeWrite_in(VmeWr_n),
       .VmeD_iob32(VmeD_b32),
       .VmeDOe_oen(VmeDOeN_n),
       .VmeDDir_o(VmeDDir),   
       .VmeDtAckOe_o(VmeDtAck_e),
       .VmeIrq_ob7(VmeIrq_b7),
       .VmeIack_in(VmeIackN_n),
       .VmeIackIn_in(VmeIackInN_n),
       .VmeIackOut_on(VmeIackOutN_n),
       .VmeSysClk_ik(VmeSysClk_k),
       .VmeSysReset_irn(VmeSysReset_rn),
   // SFP Gbit:
       .AppSfpRx_ib4(), 
       .AppSfpTx_ob4(), 
       .BstSfpRx_i(), 
       .BstSfpTx_o(), 
       .EthSfpRx_i(), 
       .EthSfpTx_o(), 
   // DDR3:
       .Ddr3AA_ob16(),
       .Ddr3ABa_ob3(),
       .Ddr3ACk_ok(), 
       .Ddr3ACkE_oeb2(),
       .Ddr3ACs_onb2(),
       .Ddr3ALdm_o(),
       .Ddr3AUdm_o(),
       .Ddr3AOdt_ob2(),
       .Ddr3ARas_on(),
       .Ddr3ACas_on(),  
       .Ddr3AWe_on(),
       .Ddr3AReset_orn(), 
       .Ddr3ADq_iob16(),
       .Ddr3ALdqs_io(), 
       .Ddr3AUdqs_io(), 
       .Ddr3BA_ob16(),
       .Ddr3BBa_ob3(),
       .Ddr3BCk_ok(), 
       .Ddr3BCkE_oeb2(),
       .Ddr3BCs_onb2(),
       .Ddr3BLdm_o(),
       .Ddr3BUdm_o(),
       .Ddr3BOdt_ob2(),
       .Ddr3BRas_on(),
       .Ddr3BCas_on(),  
       .Ddr3BWe_on(),
       .Ddr3BReset_orn(), 
       .Ddr3BDq_iob16(),
       .Ddr3BLdqs_io(), 
       .Ddr3BUdqs_io(),   
   //TestIo:
       .TestIo1_io(),
       .TestIo2_io(),
   //I2C Mux and IO expanders:
       .I2cMuxSda_io(),
       .I2cMuxScl_ok(),
       .I2CMuxIntN0_in(),
       .I2CMuxIntN1_in(),
       .I2CIoExpIntApp12_in(),
       .I2CIoExpIntApp34_in(),
       .I2CIoExpIntBstEth_in(),
       .I2CIoExpIntBlmIn_in(),  
   //BST:
       .BstDataIn_i(),
       .CdrClkOut_ik(),
       .CdrDataOut_i(),
   //ADC Voltage monitoring:   
       .VAdcDout_i(),
       .VAdcDin_o(),
       .VAdcCs_o(),
       .VAdcSclk_ok(),
   //FMC connector:
       .FmcLaP_iob34(FmcLaP_iob34),
       .FmcLaN_iob34(FmcLaN_iob34),
       .FmcHaP_iob24(FmcHaP_iob24),
       .FmcHaN_iob24(FmcHaN_iob24),   
       .FmcHbP_iob22(FmcHbP_iob22),
       .FmcHbN_iob22(FmcHbN_iob22),
       .FmcPrsntM2C_in(FmcPrsntM2C_in), 
       .FmcTck_ok(FmcTck_ok),
       .FmcTms_o(FmcTms_o),
       .FmcTdi_o(FmcTdi_o),
       .FmcTdo_i(FmcTdo_i),
       .FmcTrstL_orn(FmcTrstL_orn),
       .FmcScl_iok(FmcScl_iok),
       .FmcSda_io(FmcSda_io),
       .FmcPgM2C_in(FmcPgM2C_in),
       .FmcPgC2M_on(FmcPgC2M_on), 
       .FmcClk0M2CCmos_ik(FmcClk0M2CCmos_ik),
       .FmcClk1M2CCmos_ik(FmcClk1M2CCmos_ik),
       .FmcClk2Bidir_iok(FmcClk2Bidir_iok), 
       .FmcClk3Bidir_iok(FmcClk3Bidir_iok), 
       .FmcClkDir_i(FmcClkDir_i),
       .FmcDpC2M_ob10(FmcDpC2M_ob10), 
       .FmcDpM2C_ib10(FmcDpM2C_ib10),
       .FmcGbtClk0M2CLeft_ik(FmcGbtClk0M2CLeft_ik), 
       .FmcGbtClk1M2CLeft_ik(FmcGbtClk1M2CLeft_ik), 
       .FmcGbtClk0M2CRight_ik(FmcGbtClk0M2CRight_ik), 
       .FmcGbtClk1M2CRight_ik(FmcGbtClk1M2CRight_ik), 
   //Clock sources and control:
       .OeSi57x_oe(OeSi57x),
       .Si57xClk_ik(Si57xClk_k),
       .ClkFb_ok(),
       .ClkFb_ik(),
       .Clk20VCOx_ik(Clk20Vcxo_k),
       .PllDac20Sync_o(),
       .PllDac25Sync_o(),
       .PllDacSclk_ok(),
       .PllDacDin_o(),  
       .PllRefSda_io(),
       .PllRefScl_ok(),
       .PllRefInt_i(),
       .PllSourceMuxOut_ok(),
       .PllRefClkOut_ik(), 
       .GbitTrxClkRefR_ik(GbitTrxClkRefR_k), 
   //Fmc Voltage control:
       .VadjCs_o(),
       .VadjSclk_ok(),
       .VadjDin_o(),
       .VfmcEnableN_oen(),
   //SW1:
       .NoGa_ib5(DipSw_b8[4:0]),
       .UseGa_i(DipSw_b8[5]),
       .Switch_ib2(DipSw_b8[7:6]),
   //Pcb Revision resistor network:
       .PcbRev_ib7(),
   //P2 RTM:
       .P2DataP_iob20(), 
       .P2DataN_iob20(),
   //P0 Timing:
       .P0HwHighByte_ib8(),
       .P0HwLowByte_ib8(),
       .DaisyChain1Cntrl_o(),
       .DaisyChain2Cntrl_o(),
       .VmeP0BunchClk_ik(),
       .VmeP0Tclk_ik(),
   //WR PROM:
       .WrPromSda_io(WrPromSda),
       .WrPromScl_ok(WrPromScl),
   //GPIO:
       .GpIo_iob4(GpIo_b4),
   //Specials:
       .PushButtonN_in(PushButtonN_n),
       .TempIdDq_ioz(),
       .ResetFpgaConfigN_orn()
);

//==== Page 5 : FPGA-FmcConnections ====\\ 

// NoGa_ib5: Comment: forwarded to out for multi-VFC simulations.
assign DipSw_b8[4:0] = NoGa_ib5; 
// UseGa_i:  Comment: forwarded to out for multi-VFC simulations.
assign DipSw_b8[  5] = UseGa_i;
// Switch_ib2:
assign DipSw_b8[7:6] = 2'b00; 
   
//==== Page 9 : ClockGeneration ====\\ 
assign (pull1, pull0) OeSi57x = 1'b1;
assign (pull1, pull0) Si57xSda = 1'b1;
assign (pull1, pull0) Si57xScl = 1'b1;
si57x 
	i_Osc1(
        .oe(OeSi57x),
        .sda(Si57xSda),
        .scl(Si57xScl),
        .clk_p(Si57xClk_k),
        .clk_n());   

always #4 GbitTrxClkRefR_k = ~GbitTrxClkRefR_k;
always #25 Clk20Vcxo_k = ~Clk20Vcxo_k;

//==== Page 10 : FrontPanelAndMiscellaneous ====\\ 
// Push button:
assign (pull1, strong0) PushButtonN_n = PushButton_i ? 1'b0 : 1'b1; 

// WR PROM:
assign (pull1, pull0)  WrPromSda = 1'b1;   
assign (pull1, pull0)  WrPromScl = 1'b1;   

I2CSlave #(.g_Address(7'h51), .g_MinPerDown(3_000)) 
    i_Ic26(   
        .Scl_io(WrPromScl),
        .Sda_io(WrPromSda));

// GPIO:
assign a3_Ic19 = {GpIo_b4[3], 3'bz, GpIo_b4[4], 3'bz};
assign b3_Ic19 = {GpIoLemo_iob4[3], 3'bz, GpIoLemo_iob4[4], 3'bz};

sn74vmeh22501 
    i_Ic19(
        .oeab1(GpIo1A2B),
        .oeby1_n(GpIo1A2B),
        .a1(GpIo_b4[1]),
        .y1(GpIo_b4[1]),
        .b1(GpIoLemo_iob4[1]),
        .oeab2(GpIo2A2B),
        .oeby2_n(GpIo2A2B),
        .a2(GpIo_b4[2]),
        .y2(GpIo_b4[2]),
        .b2(GpIoLemo_iob4[2]),
        .oe_n(1'b0),
        .dir(GpIo34A2B),    
        .a3(a3_Ic19),
        .b3(b3_Ic19),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));	        
    
//==== Page 11 : FmcHpcConnector ====\\    
assign                FmcGa0_o      = 1'b0;
assign                FmcGa1_o      = 1'b0;
assign (pull1, pull0) FmcScl_iok    = 1'b1;
assign (pull1, pull0) FmcSda_io     = 1'b1;    
    
//==== Page 13 : VmeConnectors ====\\ 
assign (pull1, pull0) VmeIrq_b7     = 7'b0;   
assign (pull1, pull0) VmeIrq_b7     = 7'b0;
assign (pull1, pull0) VmeDOeN_n     = 1'b1;	
assign (pull1, pull0) VmeAOeN_n     = 1'b1;	
assign (pull1, pull0) VmeADir       = 1'b0;
assign (pull1, pull0) VmeDtAck_e    = 1'b0;		
assign (pull1, pull0) VmeIackOutN_n = VmeIackInN_n;	
assign (pull1, pull0) Ga_ionb5      = 5'b1;	
assign (pull1, pull0) Gap_ion       = 1'b1;

sn74vmeh22501 
    i_Ic1(
        .oeab1(VmeIrq_b7[1]),
        .oeby1_n(1'b1),
        .a1(1'b0),
        .y1(),
        .b1(Irq_onb7[1]),
        .oeab2(1'b0),
        .oeby2_n(1'b0),
        .a2(1'b0),
        .y2(VmeIackN_n),
        .b2(Iack_in),
        .oe_n(VmeDOeN_n),
        .dir(VmeDDir),    
        .a3(VmeD_b32[7:0]),
        .b3(D_iob32[7:0]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));	  
    
sn74vmeh22501 
    i_Ic5(
        .oeab1(VmeIrq_b7[3]),
        .oeby1_n(1'b1),
        .a1(1'b0),
        .y1(),
        .b1(Irq_onb7[3]),
        .oeab2(VmeIrq_b7[2]),
        .oeby2_n(1'b1),
        .a2(1'b0),
        .y2(),
        .b2(Irq_onb7[2]),
        .oe_n(VmeDOeN_n),
        .dir(VmeDDir),    
        .a3(VmeD_b32[15:8]),
        .b3(D_iob32[15:8]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));   
   
sn74vmeh22501 
    i_Ic18(
        .oeab1(1'b0),
        .oeby1_n(1'b0),
        .a1(1'b0),
        .y1(VmeSysClk_k),
        .b1(SysClk_ik),
        .oeab2(1'b0),
        .oeby2_n(1'b0),
        .a2(1'b0),
        .y2(VmeAs_n),
        .b2(As_in),
        .oe_n(VmeAOeN_n),
        .dir(VmeADir),    
        .a3(VmeA_b31[15:8]),
        .b3(A_iob31[15:8]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));	
        
sn74vmeh22501 
    i_Ic17(
        .oeab1(VmeDtAck_e),
        .oeby1_n(1'b1),
        .a1(1'b0),
        .y1(),
        .b1(DtAck_on),
        .oeab2(1'b0),
        .oeby2_n(1'b0),
        .a2(1'b0),
        .y2(VmeWr_n),
        .b2(Wr_in),
        .oe_n(VmeAOeN_n),
        .dir(VmeADir),    
        .a3(VmeA_b31[23:16]),
        .b3(A_iob31[23:16]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));
	
sn74vmeh22501 
    i_Ic39(
        .oeab1(VmeIrq_b7[7]),
        .oeby1_n(1'b1),
        .a1(1'b0),
        .y1(),
        .b1(Irq_onb7[7]),
        .oeab2(VmeIrq_b7[6]),
        .oeby2_n(1'b1),
        .a2(1'b0),
        .y2(),
        .b2(Irq_onb7[6]),
        .oe_n(VmeDOeN_n),
        .dir(VmeDDir),    
        .a3(VmeD_b32[23:16]),
        .b3(D_iob32[23:16]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));	
  
sn74vmeh22501 
    i_Ic44(		
        .oeab1(VmeIrq_b7[5]),
        .oeby1_n(1'b1),
        .a1(1'b0),
        .y1(),
        .b1(Irq_onb7[5]),
        .oeab2(VmeIrq_b7[4]),
        .oeby2_n(1'b1),
        .a2(1'b0),
        .y2(),
        .b2(Irq_onb7[4]),
        .oe_n(VmeDOeN_n),
        .dir(VmeDDir),    
        .a3(VmeD_b32[31:24]),
        .b3(D_iob32[31:24]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));	

sn74vmeh22501 
    i_Ic38(
        .oeab1(1'b0),
        .oeby1_n(1'b0),
        .a1(1'b0),
        .y1(VmeIackInN_n),
        .b1(IackIn_in),
        .oeab2(1'b1),
        .oeby2_n(1'b1),
        .a2(VmeIackOutN_n),
        .y2(),
        .b2(IackOut_on),
        .oe_n(VmeAOeN_n),
        .dir(VmeADir),    
        .a3(VmeA_b31[31:24]),
        .b3(A_iob31[31:24]),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));	

sn74vmeh22501 
    i_Ic20(
        .oeab1(1'b0),
        .oeby1_n(1'b0),
        .a1(1'b0),
        .y1(VmeDsN_nb2[1]),
        .b1(Ds_inb2[1]),
        .oeab2(1'b0),
        .oeby2_n(1'b0),
        .a2(1'b0),
        .y2(VmeDsN_nb2[0]),
        .b2(Ds_inb2[0]),
        .oe_n(VmeAOeN_n),
        .dir(VmeADir),    
        .a3({VmeA_b31[7:1], VmeLWord_n}),
        .b3({A_iob31[7:1], LWord_io}),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));	
    
 sn74vmeh22501 
    i_Ic13(
        .oeab1(1'b0),
        .oeby1_n(1'b1),
        .a1(),
        .y1(),
        .b1(),
        .oeab2(1'b0),
        .oeby2_n(1'b1),
        .a2(),
        .y2(),
        .b2(),
        .oe_n(1'b0),
        .dir(1'b0),    
        .a3({VmeAm_b6, VmeSysReset_rn}),
        .b3({AM_ib6, SysResetN_irn}),
        .clkab(1'b0),
        .le(1'b1),
        .clkba(1'b0));      
   
endmodule
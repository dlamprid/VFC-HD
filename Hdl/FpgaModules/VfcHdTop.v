`timescale 1ns/100ps 

module VfcHdTop  
(	
   //VME interface		
   input VmeAs_in,
   input [5:0] VmeAm_ib6,
   input [31:1] VmeA_ib31,
   input VmeLWord_in,
   output VmeAOe_oen,
   output VmeADir_o,
   input [1:0] VmeDs_inb2,
   input VmeWrite_in,
   inout [31:0] VmeD_iob32,
   output VmeDOe_oen,
   output VmeDDir_o,   
   output VmeDtAckOe_o,
   output [7:1] VmeIrq_ob7,
   input VmeIack_in,
   input VmeIackIn_in,
   output VmeIackOut_on,
   input VmeSysClk_ik,
   input VmeSysReset_irn,
   //SFP Gbit
   input EthSfpRx_i, //Differential
   output EthSfpTx_o, //Differential
   //I2C Mux and IO expanders
   inout I2cMuxSda_io,
   inout I2cMuxScl_iok,
   input I2CMuxIntN0_in,
   input I2CMuxIntN1_in,
   input I2CIoExpIntApp12_in,
   input I2CIoExpIntApp34_in,
   input I2CIoExpIntBstEth_in,
   input I2CIoExpIntBlmIn_in,  
   //BST
   input BstDataIn_i,
   input CdrClkOut_ik,
   input CdrDataOut_i,
   //ADC Voltage monitoring   
   input VAdcDout_i,
   output VAdcDin_o,
   output VAdcCs_o,
   output VAdcSclk_ok,
   //FMC connector
   output [33:0]FmcLaP_ob34,
   output [33:0]FmcLaN_ob34,
/* -----\/----- EXCLUDED -----\/-----
   inout [23:0]FmcHaP_iob24,
   inout [23:0]FmcHaN_iob24,   
   inout [21:0]FmcHbP_iob22,
   inout [21:0]FmcHbN_iob22,
   input FmcPrsntM2C_in, 
   output FmcTck_ok,
   output FmcTms_o,
   output FmcTdi_o,
   input FmcTdo_i,
   output FmcTrstL_orn,
   inout FmcScl_iok,
   inout FmcSda_io,
   input FmcPgM2C_in,
   output FmcPgC2M_on, 
   input FmcClk0M2CCmos_ik,
   input FmcClk1M2CCmos_ik,
   inout FmcClk2Bidir_iok, //differential signal
   inout FmcClk3Bidir_iok, //differential signal
   input FmcClkDir_i,
   output [9:0] FmcDpC2M_ob10, //diff output
   input [9:0] FmcDpM2C_ib10,
   input FmcGbtClk0M2CLeft_ik, //differential signal
   input FmcGbtClk1M2CLeft_ik, //differential signal
   input FmcGbtClk0M2CRight_ik, //differential signal
   input FmcGbtClk1M2CRight_ik, //differential signal   
 -----/\----- EXCLUDED -----/\----- */
   //Clock sources and control
   input Clk20VCOx_ik,
   output PllDac20Sync_o,
   output PllDac25Sync_o,
   output PllDacSclk_ok,
   output PllDacDin_o,  
   input GbitTrxClkRefR_ik, //Differential reference for the Gbit lines
   //Fmc Voltage control
   output VadjCs_o,
   output VadjSclk_ok,
   output VadjDin_o,
   output VfmcEnableN_oen,
   //SW1
   input [4:0] NoGa_ib5,
   input UseGa_i,
   //Pcb Revision resistor network
   input [7:0] PcbRev_ib7,
   //WR PROM
   inout WrPromSda_io,
   output WrPromScl_ok,
   //GPIO
   output [4:1] GpIo_ob4,
   //Miscellaneous:
   inout TempIdDq_ioz
);

//@@@@@@@@@@@@@@@@@@@
//WIRE DECLARATIONS
//@@@@@@@@@@@@@@@@@@@  
 
wire WbClk_k;
wire Wb1Cyc, Wb1Stb, Wb1Wr, Wb1Ack;
wire [24:0] Wb1Adr_b25;
wire [31:0] Wb1DatMosi_b32, Wb1DatMiso_b32;
wire Wb2Cyc, Wb2Stb, Wb2Wr, Wb2Ack;
wire [24:0] Wb2Adr_b25;
wire [31:0] Wb2DatMosi_b32, Wb2DatMiso_b32;
wire BstOn, BstClk_k, BunchClkFlag, TurnClkFlag;
wire [7:0] BstByteAddress_b8;
wire [7:0] BstByte_b8;
wire  BstByteStrobe, BstByteError;
wire [23:0] InterruptRequest_pb24;
wire Reset_rqp, ResetRequest_qp;   
wire [207:0] WrBtrainTxData;
wire WrBtrainTxValid;
wire WrBtrainTxDreq;
wire WrBtrainTxLast;
wire WrBtrainTxFlush;
wire WrBtrainRxFirst;
wire WrBtrainRxLast;
wire [207:0] WrBtrainRxData;
wire WrBtrainRxValid;
wire WrBtrainRxDreq;
wire WrpcTmTimeValid;
wire [39:0] WrpcTmTai;
wire [27:0] WrpcTmCycles;
wire WrpcSysClk_k;
wire WrpcLedLink;
wire WrpcLedAct;
wire [31:0]WrBtrainBvalue;
wire WrBtrainBvalueValid;
wire WrBtrainBvalueUpdated;
wire WrpcPps; 

//@@@@@@@@@@@@@@@@@@@
//SYSTEM MODULE
//@@@@@@@@@@@@@@@@@@@

VfcHdSystem i_VfcHdSystem(	
    //DIRECT CONNECTIONS TO THE FPGA IOs
    // VME interface		
    .VmeAs_in(VmeAs_in),
    .VmeAm_ib6(VmeAm_ib6),
    .VmeA_ib31(VmeA_ib31),
    .VmeLWord_in(VmeLWord_in),
    .VmeAOe_oen(VmeAOe_oen),
    .VmeADir_o(VmeADir_o),
    .VmeDs_inb2(VmeDs_inb2),
    .VmeWrite_in(VmeWrite_in),
    .VmeD_iob32(VmeD_iob32),
    .VmeDOe_oen(VmeDOe_oen),
    .VmeDDir_o(VmeDDir_o),   
    .VmeDtAckOe_o(VmeDtAckOe_o),
    .VmeIrq_ob7(VmeIrq_ob7),
    .VmeIack_in(VmeIack_in),
    .VmeIackIn_in(VmeIackIn_in),
    .VmeIackOut_on(VmeIackOut_on),
    .VmeSysClk_ik(VmeSysClk_ik),
    .VmeSysReset_irn(VmeSysReset_irn),
    .EthSfpRx_i(EthSfpRx_i), 
    .EthSfpTx_o(EthSfpTx_o), 
    .I2cMuxSda_io(I2cMuxSda_io),
    .I2cMuxScl_iok(I2cMuxScl_iok),
    .I2CMuxIntN0_in(I2CMuxIntN0_in),
    .I2CMuxIntN1_in(I2CMuxIntN1_in),
    .I2CIoExpIntApp12_in(I2CIoExpIntApp12_in),
    .I2CIoExpIntApp34_in(I2CIoExpIntApp34_in),
    .I2CIoExpIntBstEth_in(I2CIoExpIntBstEth_in),
    .I2CIoExpIntBlmIn_in(I2CIoExpIntBlmIn_in),  
    .BstDataIn_i(BstDataIn_i),
    .CdrClkOut_ik(CdrClkOut_ik),
    .CdrDataOut_i(CdrDataOut_i),
    .VAdcDout_i(VAdcDout_i),
    .VAdcDin_o(VAdcDin_o),
    .VAdcCs_o(VAdcCs_o),
    .VAdcSclk_ok(VAdcSclk_ok), 
    .GbitTrxClkRefR_ik(GbitTrxClkRefR_ik), 
    .Clk20VCOx_ik(Clk20VCOx_ik),
    .PllDac20Sync_o(PllDac20Sync_o),
    .PllDac25Sync_o(PllDac25Sync_o),
    .PllDacSclk_ok(PllDacSclk_ok),
    .PllDacDin_o(PllDacDin_o),
    .VadjCs_o(VadjCs_o),
    .VadjSclk_ok(VadjSclk_ok),
    .VadjDin_o(VadjDin_o),
    .NoGa_ib5(NoGa_ib5),
    .UseGa_i(UseGa_i),
    .PcbRev_ib7(PcbRev_ib7),
    .WrPromSda_io(WrPromSda_io),
    .WrPromScl_ok(WrPromScl_ok),
    .TempIdDq_ioz(TempIdDq_ioz),
    //CONNECTIONS SYSTAM<->APPLICATION
    .Reset_orqp(Reset_rqp), 
    .ResetRequest_iqp(ResetRequest_qp), 
    .WbClk_ik(WbClk_k),
    .WbMasterCyc_o(Wb1Cyc),
    .WbMasterStb_o(Wb1Stb),
    .WbMasterAdr_ob25(Wb1Adr_b25),   //The actual width of the address bus is set to 21 for the moment: top bits stuck to 0
    .WbMasterWr_o(Wb1Wr),
    .WbMasterDat_ob32(Wb1DatMosi_b32),
    .WbMasterDat_ib32(Wb1DatMiso_b32),
    .WbMasterAck_i(Wb1Ack),
    .BstOn_o(BstOn),
    .BstClk_ok(BstClk_k),
    .BunchClkFlag_o(BunchClkFlag),
    .TurnClkFlag_o(TurnClkFlag),
    .BstByteAddress_ob8(BstByteAddress_b8),
    .BstByte_ob8(BstByte_b8),
    .BstByteStrobe_o(BstByteStrobe),
    .BstByteError_o(BstByteError),
    .InterruptRequest_ipb24(InterruptRequest_pb24),
    //WR Btrain interface
    .WrBtrainTxData_i(WrBtrainTxData),
    .WrBtrainTxValid_i(WrBtrainTxValid),
    .WrBtrainTxDreq_o(WrBtrainTxDreq),
    .WrBtrainTxLast_i(WrBtrainTxLast),
    .WrBtrainTxFlush_i(WrBtrainTxFlush),
    .WrBtrainRxFirst_o(WrBtrainRxFirst),
    .WrBtrainRxLast_o(WrBtrainRxLast),
    .WrBtrainRxData_o(WrBtrainRxData),
    .WrBtrainRxValid_o(WrBtrainRxValid),
    .WrBtrainRxDreq_i(WrBtrainRxDreq),
    //WRPC timecode output and PPS
    .WrpcTmTimeValid_o(WrpcTmTimeValid),
    .WrpcTmTai_o(WrpcTmTai),
    .WrpcTmCycles_o(WrpcTmCycles),
    .WrpcPps_o(WrpcPps),
    // WRPC status LEDs
    .WrpcLedLink_o(WrpcLedLink),
    .WrpcLedAct_o(WrpcLedAct),				
    //WRPC system clock
    .WrpcSysClk_ok(WrpcSysClk_k));

//@@@@@@@@@@@@@@@@@@@
//APPLICATION MODULE
//@@@@@@@@@@@@@@@@@@@

VfcHdApplication i_VfcHdApplication
   (	
   //DIRECT CONNECTIONS TO THE FPGA IOs
   .VfmcEnableN_oen(VfmcEnableN_oen),
   .FmcLaP_ob34(FmcLaP_ob34),
   .FmcLaN_ob34(FmcLaN_ob34),
   .GbitTrxClkRefR_ik(GbitTrxClkRefR_ik), 
   .GpIo_ob4(GpIo_ob4),
   //CONNECTIONS SYSTAM<->APPLICATION 
   .Reset_irqp(Reset_rqp), 
   .ResetRequest_oqp(ResetRequest_qp),    
   .WbClk_ok(WbClk_k),
   .WbSlaveCyc_i(Wb1Cyc),
   .WbSlaveStb_i(Wb1Stb),
   .WbSlaveAdr_ib25(Wb1Adr_b25),
   .WbSlaveWr_i(Wb1Wr),
   .WbSlaveDat_ib32(Wb1DatMosi_b32),
   .WbSlaveDat_ob32(Wb1DatMiso_b32),
   .WbSlaveAck_o(Wb1Ack),   
   .InterruptRequest_opb24(InterruptRequest_pb24),
   //WR Btrain interface
   .WrBtrainTxData_o(WrBtrainTxData),
   .WrBtrainTxValid_o(WrBtrainTxValid),
   .WrBtrainTxDreq_i(WrBtrainTxDreq),
   .WrBtrainTxLast_o(WrBtrainTxLast),
   .WrBtrainTxFlush_o(WrBtrainTxFlush),
   .WrBtrainRxFirst_i(WrBtrainRxFirst),
   .WrBtrainRxLast_i(WrBtrainRxLast),
   .WrBtrainRxData_i(WrBtrainRxData),
   .WrBtrainRxValid_i(WrBtrainRxValid),
   .WrBtrainRxDreq_o(WrBtrainRxDreq),
   //WRPC timecode input and PPS
   .WrpcTmTimeValid_i(WrpcTmTimeValid),
   .WrpcTmTai_i(WrpcTmTai),
   .WrpcTmCycles_i(WrpcTmCycles),
   .WrpcPps_i(WrpcPps),
   // WRPC status LEDs
   .WrpcLedLink_i(WrpcLedLink),
   .WrpcLedAct_i(WrpcLedAct),
    //WRPC system clock
   .WrpcSysClk_ik(WrpcSysClk_k));

endmodule
	

`timescale 1ns/100ps

module AddrDecoderWbSys( 
    input   Clk_ik,
    input   [21:0] Adr_ib22,
    input   Stb_i,
    output  reg [31:0] Dat_ob32,
    output  reg Ack_o,

    input   [31:0] DatIntManager_ib32,
    input   AckIntManager_i,
    output  reg StbIntManager_o,
   
    input  [31:0] DatSpiMaster_ib32,
    input   AckSpiMaster_i,
    output  reg StbSpiMaster_o,
    
    input  [31:0] DatUniqueIdReader_ib32,
    input   AckUniqueIdReader_i,
    output  reg StbUniqueIdReader_o,
    
    input [31:0] DatI2cIoExpAndMux_ib32,
    input AckI2cIoExpAndMux_i,
    output reg StbI2cIoExpAndMux_o,

    input [31:0] DatAppSlaveBus_ib32,
    input AckAppSlaveBus_i,
    output reg StbAppSlaveBus_o,

    input [31:0] DatWrpcSlaveBus_ib32,
    input AckWrpcSlaveBus_i,
    output reg StbWrpcSlaveBus_o
 ); 
     
localparam dly = 1;

reg [7:0] SelectedModule_b8;

localparam  c_SelNothing = 8'h0,
            c_SelIntManager = 8'd1,
            c_SelIoExpAndMux = 8'd2,
            c_SelUniqueIdReader = 8'd3,
            c_SelWrPtpCore = 8'd4,
            c_SelSpiMaster = 8'd5,
            c_SelAppSlaveBus = 8'd6;
       
always @* 
    casez(Adr_ib22)
        22'b00000000000000000000??: SelectedModule_b8 = c_SelIntManager;     // FROM 00_0000 TO 00_0003 (WB) == FROM 00_0000 TO 00_000C (VME) <- 4 regs (16B)
        22'b00000000000000000001??: SelectedModule_b8 = c_SelIoExpAndMux;    // FROM 00_0004 TO 00_0007 (WB) == FROM 00_0010 TO 00_001C (VME) <- 4 regs (16B)
        22'b00000000000000000010??: SelectedModule_b8 = c_SelUniqueIdReader; // FROM 00_0008 TO 00_000B (WB) == FROM 00_0020 TO 00_002C (VME) <- 4 regs (16B)
        22'b0000000000000000010???: SelectedModule_b8 = c_SelSpiMaster;      // FROM 00_0010 TO 00_0017 (WB) == FROM 00_0040 TO 00_005C (VME) <- 8 regs (32B)                 
	22'b000001????????????????: SelectedModule_b8 = c_SelWrPtpCore;      // FROM 01_0000 TO 01_FFFF (WB) == FROM 04_0000 TO 07_FFFC (VME)
        22'b1?????????????????????: SelectedModule_b8 = c_SelAppSlaveBus;    // FROM 20_0000 TO 3F_FFFF (WB) == FROM 80_0000 TO FF_FFFC (VME) <- 2M regs (8MB)
        default: SelectedModule_b8 = c_SelNothing;
    endcase     

always @* begin
    Ack_o                   <= #dly 1'b0;
    Dat_ob32                <= #dly 32'h0;
    StbIntManager_o         <= #dly 1'b0;
    StbSpiMaster_o      <= #dly 1'b0;
    StbUniqueIdReader_o <= #dly 1'b0;
    StbAppSlaveBus_o  <= #dly 1'b0;
    StbI2cIoExpAndMux_o <= #dly 1'b0;
    StbWrpcSlaveBus_o <= #dly 1'b0;
    case (SelectedModule_b8)
        c_SelIntManager: begin            
            StbIntManager_o <= #dly  Stb_i;
            Dat_ob32        <= #dly  DatIntManager_ib32;
            Ack_o           <= #dly  AckIntManager_i;
        end
        c_SelIoExpAndMux: begin
            StbI2cIoExpAndMux_o <= # dly Stb_i;
            Dat_ob32            <= #dly  DatI2cIoExpAndMux_ib32;
            Ack_o           <= #dly  AckI2cIoExpAndMux_i;
        end
        c_SelUniqueIdReader: begin
            StbUniqueIdReader_o   <= #dly Stb_i;
            Dat_ob32        <= #dly DatUniqueIdReader_ib32;
            Ack_o           <= #dly AckUniqueIdReader_i;
        end
        c_SelSpiMaster: begin
            StbSpiMaster_o   <= #dly Stb_i;
            Dat_ob32        <= #dly DatSpiMaster_ib32;
            Ack_o           <= #dly AckSpiMaster_i;     
        end
        c_SelAppSlaveBus: begin
            StbAppSlaveBus_o <= #dly Stb_i;
            Dat_ob32 <= #dly DatAppSlaveBus_ib32;
            Ack_o <= #dly AckAppSlaveBus_i;
        end
	c_SelWrPtpCore: begin
	    StbWrpcSlaveBus_o <= #dly Stb_i;
	    Dat_ob32 <= #dly DatWrpcSlaveBus_ib32;
	    Ack_o <= #dly AckWrpcSlaveBus_i;
	end
        default: ;
    endcase
end 
    

endmodule      

//----------------------------------------------------------------------
// Title      : BST Decoder
// Project    : BST Decoder Core
//----------------------------------------------------------------------
// File       : BstDecoder.v
// Author     : T. Levens, J. Olexa
// Modified by: M. Barros Marin
// Company    : CERN BE-BI-QP
// Created    : 2016-10-04
// Last update: 2016-11-03
// Platform   : FPGA-generic
// Standard   : Verilog
//----------------------------------------------------------------------
// Description:
//
// Top level of the BST Decoder core
//----------------------------------------------------------------------

`timescale 1ns/100ps

module BstDecoder (
    // Module control:
    input           Reset_ir,
    output          BstOn_o,
    // CDR interface:
    input           CdrClk_ik,
    input           CdrDat_i,
    input           CdrLos_i,
    input           CdrLol_i,
    input           SfpLos_i,
    input           SfpPrsnt_i,
    // BST Message:
    output [7:0]    BstByteAddr_ob8,
    output [7:0]    BstByte_ob8,
    output          BstByteStrobe_o,
    output          BstByteError_o,
    // Timing:
    output          BstClk_ok,       // Comment: 160 MHz
    output reg      BunchClkFlag_oq, // Comment: 40  MHz
    output reg      TurnClkFlag_oq   // Comment: 11  kHz (LHC) | 44Khz (SPS)
);

wire        Reset_ra;
reg  [ 1:0] Reset_rnx2;
wire        Reset_rn;
reg         CdrDat_q;
wire        BiPhaseqxD1;       // Comment: Decoded data stream
wire        BiPhaseqxE1;       // Comment: Identifies the rising edge of output clock
wire        BiPhaseValidqxS1;  // Comment: Set when output data stream is valid
wire        TurnFlagxS1;       // Comment: Channel A data output
wire        DemuxedxD1;        // Comment: Channel B data output
wire        DxE1;              // Comment: Output enable
wire        SyncqxS1;          // Comment: Synchronisation output
wire        ValidqxS1;         // Comment: Output data valid
wire [31:0] HammingqxD1;       // Comment: Output data vector  32b
wire        HammingValidqxS1;  // Comment: Valid output
wire        HammingErrorqxS1;  // Comment: Error flag set when non-correctable error detected

//----------------------------------------------------------------------
// Module control
//----------------------------------------------------------------------

assign Reset_ra = ~SfpPrsnt_i | SfpLos_i | CdrLos_i | CdrLol_i | Reset_ir;
always @(posedge CdrClk_ik) Reset_rnx2 <= #1 {Reset_rnx2[0], ~Reset_ra};
assign Reset_rn = Reset_rnx2[1];

assign BstOn_o  = Reset_rnx2[1];

//----------------------------------------------------------------------
// CDR interface & BST Message
//----------------------------------------------------------------------

always @(posedge CdrClk_ik) CdrDat_q <= #1 CdrDat_i;

BiPhaseMark_Decoder i_BiPhaseMark_Decoder (
    .ClkxC     (CdrClk_ik),            
    .DxD       (CdrDat_q),               
    .ResetxRNA (Reset_rn),         
    .QxD       (BiPhaseqxD1),       // Comment: Decoded data stream
    .QxE       (BiPhaseqxE1),       // Comment: Identifies the rising edge of output clock
    .ValidQxS  (BiPhaseValidqxS1)); // Comment: Set when output data stream is valid

TTCrx_AB_Demux i_TTCrx_AB_Demux (
    .ClkxC     (CdrClk_ik),         
    .ResetxRNA (Reset_rn),         
    .DxD       (BiPhaseqxD1),      // Comment: Input data stream
    .DxE       (BiPhaseqxE1),      // Comment: Input enable
    .ValidDxS  (BiPhaseValidqxS1), // Comment: Input data valid
    .AChanQxD  (TurnFlagxS1),      // Comment: Channel A data output
    .BChanQxD  (DemuxedxD1),       // Comment: Channel B data output
    .QxE       (DxE1),             // Comment: Output enable
    .SyncQxS   (SyncqxS1),         // Comment: Synchronisation output
    .ValidQxS  (ValidqxS1));       // Comment: Output data valid

TTCrx_Hamming_Decoder i_TTCrx_Hamming_Decoder (
    .ClkxC     (CdrClk_ik),           
    .ResetxRNA (Reset_rn),        
    .DxD       (DemuxedxD1),        // Comment: Input data stream
    .DxE       (DxE1),              // Comment: Input clock enable
    .ValidDxS  (ValidqxS1),         // Comment: Valid flag
    .SyncDxS   (SyncqxS1),          // Comment: Synchronisation flag
    .QxD       (HammingqxD1),       // Comment: Output data vector
    .ValidQxS  (HammingValidqxS1),  // Comment: Valid output
    .ErrorQxS  (HammingErrorqxS1)); // Comment: Non-correctable error detected    

TTCrx_Frame_Decoder i_TTCrx_Frame_Decoder (
    .ClkxC      (CdrClk_ik),           
    .ResetxRNA  (Reset_rn),        
    .DxE        (DxE1),             // Comment: Input clock enable
    .DxD        (HammingqxD1),      // Comment: Input data stream
    .ValidDxD   (HammingValidqxS1), // Comment: Valid flag
    .ErrorDxS   (HammingErrorqxS1), // Comment: Error flag set when non-correctable error detected
    .SubAddrQxD (BstByteAddr_ob8),  // Comment: Address vector
    .CmdDataQxD (BstByte_ob8),      // Comment: Data vector
    .ValidQxS   (BstByteStrobe_o),  // Comment: Valid output
    .ErrorQxS   (BstByteError_o));  // Comment: Error flag

//----------------------------------------------------------------------
// Timing
//----------------------------------------------------------------------

assign BstClk_ok = CdrClk_ik;

always @(posedge CdrClk_ik) BunchClkFlag_oq <= #1 DxE1;

always @(posedge CdrClk_ik)
    if      (~Reset_rn) TurnClkFlag_oq <= #1 1'b0;
    else if (DxE1)      TurnClkFlag_oq <= #1 ValidqxS1 ? TurnFlagxS1 : 1'b0;

endmodule
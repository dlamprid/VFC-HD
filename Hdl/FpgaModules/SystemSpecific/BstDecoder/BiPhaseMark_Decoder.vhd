-------------------------------------------------------------------------------
-- Title      : BiPhaseMark Decoder
-- Project    :
-------------------------------------------------------------------------------
-- File       : BiPhaseMark_Decoder.vhd
-- Author     : Jan Kral  <jan.kral@cern.ch>
-- Company    :
-- Created    : 2014-07-21
-- Last update: 2014-07-22
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: the entity decodes a BiPhaseMark code which is used for outer
-- BST encoding. The bi-phase mark code encodes data and clock phase which
-- allows to receiver to recover both. Output clock frequency is half of the
-- input clock frequency.
-------------------------------------------------------------------------------
-- Copyright (c) 2014
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 2014-07-21  1.0      jakral  Created
-------------------------------------------------------------------------------
--       _______                 _______________         _______
-- RData        \_______________/               \_______/       \______________
--       ___     ___     ___     ___     ___     ___     ___     ___     ___
-- Rclk     \___/   \___/   \___/   \___/   \___/   \___/   \___/   \___/   \__
--       _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _   _
-- ClkxC  \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \_/ \
--       ___     _______         ___     ___     _______     ___         ______
-- DxD      \___/       \_______/   \___/   \___/       \___/   \_______/
--           _______                 _______________         _______
-- QxD   ---/       \_______________/               \_______/       \__________
--       ___     ___     ___     ___     ___     ___     ___     ___     ___
-- QxE      \___/   \___/   \___/   \___/   \___/   \___/   \___/   \___/   \__
--                   __________________________________________________________
-- ValidQxS _________/

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BiPhaseMark_Decoder is
  port (
    ClkxC     : in  std_logic;          -- input clock
    ResetxRNA : in  std_logic;          -- reset input
    DxD       : in  std_logic;          -- input data stream
    QxD       : out std_logic;          -- decoded data stream
    QxE       : out std_logic;  -- identifies the rising edge of output clock
    ValidQxS  : out std_logic           -- set when output data stream is valid
    );

end entity BiPhaseMark_Decoder;

architecture BiPhaseMark_Decoder_v1 of BiPhaseMark_Decoder is
  type BiPhaseStatesxQ is (Idle, DataX_L, DataX_H, Data0_L, Data0_H, Data1_L, Data1_H, Error_H, Error_L);  --! state machine type

  signal CodeStatexS : BiPhaseStatesxQ;  --! state machine signal declaration

begin  -- architecture BiPhaseMark_Decoder_v1

  process (ClkxC, ResetxRNA) is
  begin  --  process
    if ResetxRNA = '0' then             --! asynchronous reset (active low)
      CodeStatexS <= Idle;              --! reset state
      ValidQxS    <= '0';
      QxD         <= '0';
      QxE         <= '0';
    elsif rising_edge(ClkxC) then       --! rising clock edge
      QxE <= '0';                       -- when not specified otherwise, clock flag is not set

      case CodeStatexS is
        when Idle =>
          ValidQxS <= '0';
          -- as the first bit arrived it is not clear whether it is zero or one
          if DxD = '1' then
            CodeStatexS <= DataX_H;
          else
            CodeStatexS <= DataX_L;
          end if;

        when DataX_L =>
          -- generate enable pulse as it is just before change of the output
          QxE <= '1';

          -- received first half of output data bit,
          -- based on the second half (waiting for it here) deciding which
          -- output bit recover
          if DxD = '1' then             -- data = 'LH'
            CodeStatexS <= Data1_H;
          else                          -- data = 'LL'
            CodeStatexS <= Data0_L;
          end if;

        when DataX_H =>
          -- generate enable pulse as it is just before change of the output
          QxE <= '1';

          -- received first half of output data bit,
          -- based on the second half (waiting for it here) deciding which
          -- output bit recover
          if DxD = '1' then             -- data = 'HH'
            CodeStatexS <= Data0_H;
          else                          -- data = 'HL'
            CodeStatexS <= Data1_L;
          end if;

        when Data0_L =>
          -- received bit is zero in low level
          QxD      <= '0';
          ValidQxS <= '1';

          -- when low level received now it means three subsequent low levels
          -- which is an error
          if DxD = '1' then             -- data = 'LLH'
            CodeStatexS <= DataX_H;
          else                          -- data = 'LLL'
            CodeStatexS <= Error_L;
          end if;

        when Data0_H =>
          -- received bit is zero in high level
          QxD      <= '0';
          ValidQxS <= '1';

          -- when high level received now it means three subsequent high levels
          -- which is an error
          if DxD = '1' then             -- data = 'HHH'
            CodeStatexS <= Error_H;
          else                          -- data = 'HHL'
            CodeStatexS <= DataX_L;
          end if;

        when Data1_L =>
          -- received bit is one ending with low level
          QxD <= '1';

          -- when low level received now it means that received bit is not one
          -- but it is zero and synchronisation is performed automatically
          if DxD = '1' then             -- data = 'HLH'
            CodeStatexS <= DataX_H;
          else                          -- data = 'HLL'
            CodeStatexS <= Data0_L;
          end if;

        when Data1_H =>
          -- received bit is one ending with high level
          QxD <= '1';

          -- when low level received now it means that received bit is not one
          -- but it is zero and synchronisation is performed automatically
          if DxD = '1' then             -- data = 'LHH'
            CodeStatexS <= Data0_H;
          else                          -- data = 'LHL'
            CodeStatexS <= DataX_L;
          end if;

        when Error_H =>
          -- three bits of high level received which is an error
          ValidQxS <= '0';

          -- stay here until error lasts
          if DxD = '0' then             -- data = 'HHHL'
            CodeStatexS <= DataX_L;
          end if;

        when Error_L =>
          -- three bits of high level received which is an error
          ValidQxS <= '0';

          -- stay here until error lasts
          if DxD = '1' then             -- data = 'LLLH'
            CodeStatexS <= DataX_H;
          end if;

        when others =>
          CodeStatexS <= Idle;
      end case;

    end if;
  end process;

end architecture BiPhaseMark_Decoder_v1;


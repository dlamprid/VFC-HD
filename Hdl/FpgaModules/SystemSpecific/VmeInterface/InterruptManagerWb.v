/*

The module support 32 interrupt sources. Each source can be masked individually. The Interrupt requests are assumed synchronous to the clock. A new request is registered 
each time there is a change of state on its line from 0 to 1.
The interrupt release meachanism can be set dynamically to ROACK or RORA. In this last case the register to read to compleate the release mechanism is the fpga_status_reg.
The interrupts are prioritized by arrival time: a 8 location deep FIFO is used to store them. If 2 or more interrupt requests 
are asserted at the same time they are store all asserted in the FIFO at the same location and will appear as multiple bit set in the status register, that is actually a copy of the 
FIFO output and is cleared at each readout as at the same time a read request is sent to the FIFO itself.

The module has a configuration register, a status register where the source of the current interrupt can be read, a mask register (a 1 in bin n mask the interrupt source n) and a release ID register

Status register (Addr:0x0) : 
	bits[31:0] = interrupt source => reading this register also advances the pointer of the interrupt FIFO and therefore it should be read only once per interrupt
Configuration Register (Addr:0x1)
	bits[31:24] = 0	  
	bits[23] = 0	  
	bits[22:20] = Interrupt Level
	bits[19:12] = Interrupt Vector
	bit[11] = IntEnabele => Enables the interrupt handling
	bit[10:9] = 0
	bit[8] = RoraMode => a 1 set the module into RORA mode (int line released on the readout of the Status register), a 0 in ROAK
	bits[7:0] = 0
*/


`timescale 1ns/100ps 

module InterruptManagerWb 
#(parameter g_FpgaVersion_b8	= 8'hab,
	parameter g_ReleaseDay_b8 	= 8'h07, 
	parameter g_ReleaseMonth_b8 = 8'h04, 
	parameter g_ReleaseYear_b8 	= 8'h75,
   parameter g_FifoAddressWidth = 3) 
(	
   input   Rst_irq,
   input   Clk_ik,
   input   Cyc_i,
   input   Stb_i,
   input   We_i,
   input   [1:0] Adr_ib2,
   input   [31:0] Dat_ib32,
   output  reg [31:0] Dat_oab32,
   output  reg Ack_oa,  
	
   input [31:0] IntRequestLines_ib32,
   
   output IntEnable_o,
   output IntModeRora_o,
   output [2:0] IntLevel_ob3,
   output [7:0] IntVector_ob8,
   output IntSourceToRead_o, //Used for RORA
   output reg NewIntRequest_oqp //Used for ROAK
);

localparam c_StatusRegAddr_b2		= 2'b00;
localparam c_ConfigRegAddr_b2		= 2'b01;
localparam c_MaskRegAddr_b2		= 2'b10;
localparam c_FirmwareRelease_b2		= 2'b11; 

reg Ack_d = 0;  
	   
wire [31:0] IntSourceFifoOut_b32;  
wire IntSourceFifoFull, IntSourceFifoEmpty;  
reg IntSourceFifoRead;      
reg [31:0] ConfigReg_bq32 = 32'hFF; 
reg [31:0] MaskReg_bq32 = 32'hFFFF_FFFF; 
  
assign IntLevel_ob3 = ConfigReg_bq32[22:20]; 
assign IntVector_ob8 = ConfigReg_bq32[19:12]; 
assign IntEnable_o = ConfigReg_bq32[11];
assign IntModeRora_o = ConfigReg_bq32[8];
   
wire [31:0] IntRequestMasked_b32 = IntRequestLines_ib32 & ~MaskReg_bq32;
reg [31:0] IntRequestMasked_db32 = 32'hFFFFFFFF;
reg [31:0] IntRequestSource_b32 = 32'h0;

always @(posedge Clk_ik) IntRequestMasked_db32 <= #1 IntRequestMasked_b32;
always @(posedge Clk_ik) IntRequestSource_b32 <= #1 IntRequestMasked_b32 & ~IntRequestMasked_db32;
always @(posedge Clk_ik) NewIntRequest_oqp <= #1 ~IntSourceFifoFull && IntEnable_o && |(IntRequestMasked_b32 & ~IntRequestMasked_db32);
 
 
always @(posedge Clk_ik) 
	if (Rst_irq) ConfigReg_bq32	<= # 1 32'h0;
	else if (Cyc_i && We_i && Stb_i && Adr_ib2==c_ConfigRegAddr_b2) ConfigReg_bq32	<= # 1 Dat_ib32;
	
always @(posedge Clk_ik) 
	if (Rst_irq) MaskReg_bq32	<= # 1 32'hffffffff;
	else if (Cyc_i && We_i && Stb_i && Adr_ib2==c_MaskRegAddr_b2) MaskReg_bq32	<= # 1 Dat_ib32;   
   
wire IntSourceFifoReset = Rst_irq || ~IntEnable_o;
 
generic_fifo_dc_gray #(.dw(32), .aw(g_FifoAddressWidth)) 
   i_IntSourceFifo (   
      .rd_clk(Clk_ik), 
      .wr_clk(Clk_ik), 
      .rst(1'b1), 
      .clr(IntSourceFifoReset), 
      .din(IntRequestSource_b32), 
      .we(NewIntRequest_oqp),
      .dout(IntSourceFifoOut_b32), 
      .re(IntSourceFifoRead), 
      .full(IntSourceFifoFull), 
      .empty(IntSourceFifoEmpty),
      .wr_level(),
      .rd_level());
 
assign IntSourceToRead_o = ~IntSourceFifoEmpty; 
 
always @(posedge Clk_ik) IntSourceFifoRead <= #1 ~IntSourceFifoEmpty && (Adr_ib2==c_StatusRegAddr_b2) && Ack_d && ~Ack_oa;
 

always @(posedge Clk_ik) begin
   Ack_oa <= #1 Stb_i&&Cyc_i;
   Ack_d  <= #1 Ack_oa;
   case(Adr_ib2)
	c_StatusRegAddr_b2		: Dat_oab32 <= #1 IntSourceFifoEmpty ? 32'h0000_0000 : IntSourceFifoOut_b32;
	c_ConfigRegAddr_b2		: Dat_oab32 <= #1 ConfigReg_bq32; 
	c_MaskRegAddr_b2		: Dat_oab32 <= #1 MaskReg_bq32; 
	c_FirmwareRelease_b2	: Dat_oab32 <= #1 {g_FpgaVersion_b8, g_ReleaseDay_b8, g_ReleaseMonth_b8, g_ReleaseYear_b8};
	default: Dat_oab32  <= 32'hdead_beef;
	endcase
end
 
endmodule

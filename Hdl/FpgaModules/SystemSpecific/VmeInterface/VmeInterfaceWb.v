`timescale 1ns/1ns

module VmeInterfaceWb
#( 
   parameter g_LowestGaAddressBit = 24,
   parameter g_ClocksIn2us = 80_000)
(
   input Clk_ik,
   input Rst_irq,
 
   input [4:0] VmeGa_ib5,
   input VmeGap_in,
   input VmeAs_in,
   input [1:0] VmeDs_inb2,
   input [5:0] VmeAm_ib6,
   input VmeWr_in,
   output reg VmeDtAck_on,
   input VmeLWord_in,
   input [31:1] VmeA_ib31,
   inout [31:0] VmeD_iob32,
   input VmeIack_in,
   input VmeIackIn_in,
   output reg  VmeIackOut_on,
   output reg  [7:1] VmeIrq_onb7,
   
   output VmeDataBuffsOutMode_o, // request to set the data buffers direction to Board -> VME
   output reg  VmeAccess_o, //Output active during Vme Access recognized by the board

   input IntEnable_i,
   input IntModeRora_i,
   input [2:0] IntLevel_ib3,
   input [7:0] IntVector_ib8,
   input IntSourceToRead_i, //Used for RORA
   input NewIntRequest_iqp, //Used for ROAK
   
    output   reg WbCyc_o,
    output   reg WbStb_o,
    output   reg WbWe_o,
    output   reg [g_LowestGaAddressBit-3:0] WbAdr_ob,
    input   [31:0] WbDat_ib32,
    output  reg [31:0] WbDat_ob32,
    input  WbAck_i);
    

reg [2:0] State_qb3, NextState_ab3;

localparam  s_Idle            = 3'd0,
            s_RdWaitWbAnswer  = 3'd1,
            s_RdCloseVmeCycle = 3'd2,
            s_WrCloseVmeCycle = 3'd3,
            s_WrWaitWbAnswer  = 3'd4,
            s_IntAck          = 3'd5,
            s_IAckPass        = 3'd6;

reg [13:0] WbTimeoutCounter_c14 = 0;
reg [7:0] IntToAckCounter_c8 = 0;
reg [9:0] RoraTimeoutCounter_c10 = 10'h3ff;
reg [31:0] InternalDataReg_b31 = 'h0;
reg [31:2] VmeAddressInternal_b30 = 'h0;
            
//#####################################
// Strobe Signals Synchronization
//#####################################

reg [2:0] VmeAsShReg_b3 = 3'b111;
always @(posedge Clk_ik) VmeAsShReg_b3 <= #1 {VmeAsShReg_b3[1:0], VmeAs_in};
wire NegedgeVmeAs_a = VmeAsShReg_b3[2:1]==2'b10;
wire a_VmeAsSynch = VmeAsShReg_b3[1];

reg [2:0] VmeDs0ShReg_b3 = 3'b111;
always @(posedge Clk_ik) VmeDs0ShReg_b3 <= #1 {VmeDs0ShReg_b3[1:0], VmeDs_inb2[0]};
wire a_VmeDs0Synch = VmeDs0ShReg_b3[1];

reg [2:0] VmeDs1ShReg_b3 = 3'b111;
always @(posedge Clk_ik) VmeDs1ShReg_b3 <= #1 {VmeDs1ShReg_b3[1:0], VmeDs_inb2[1]};
wire a_VmeDs1Synch = VmeDs1ShReg_b3[1]; 

reg [2:0] VmeIackInShReg_b3 = 3'b111;
always @(posedge Clk_ik) VmeIackInShReg_b3 <= #1 {VmeIackInShReg_b3[1:0], VmeIackIn_in};
wire a_VmeIackInSynch = VmeIackInShReg_b3[1];           
    
reg [2:0] VmeIackShReg_b3 = 3'b111;
always @(posedge Clk_ik) VmeIackShReg_b3 <= #1 {VmeIackShReg_b3[1:0], VmeIack_in};
wire a_VmeIackSynch = VmeIackShReg_b3[1];           

//#####################################
// Access recognition signals
//#####################################  
  
wire VmeGaPError_a      = ^{VmeGa_ib5, ~VmeGap_in};
wire VmeAmValid_a       = (VmeAm_ib6==6'h0B) || (VmeAm_ib6==6'h09);  
wire VmeValidBaseAddr_a = (~VmeGaPError_a) && (VmeA_ib31[31:g_LowestGaAddressBit]=={ {(26-g_LowestGaAddressBit){1'b0}}, ~VmeGa_ib5});
wire VmeRWAccess_a      = ~a_VmeAsSynch && ~a_VmeDs0Synch && ~a_VmeDs1Synch && ~VmeLWord_in && VmeAmValid_a && VmeIack_in && VmeValidBaseAddr_a; 
wire VmeIackCycle_a     = ~a_VmeAsSynch && ~(a_VmeDs0Synch&&a_VmeDs1Synch) && ~a_VmeIackSynch;  
    
//#####################################
//State Machine
//#####################################
  
always @(posedge Clk_ik) State_qb3 <= #1 NextState_ab3;

always @* begin
   NextState_ab3 = State_qb3;
   if (Rst_irq) NextState_ab3 = s_Idle;
   else case(State_qb3)
   s_Idle: 
      if (VmeRWAccess_a && ~WbAck_i) begin
         if (VmeWr_in) NextState_ab3 = s_RdWaitWbAnswer;
         else NextState_ab3 = s_WrCloseVmeCycle;
      end else if (VmeIackCycle_a) begin
         if (VmeIrq_onb7[VmeA_ib31[3:1]]==1'b1 ) NextState_ab3 = s_IAckPass;
         else if (a_VmeIackInSynch==1'b0) begin
            if (~IntModeRora_i) NextState_ab3 = s_IntAck;
            else if (&RoraTimeoutCounter_c10) NextState_ab3 = s_IntAck;
            else NextState_ab3 = s_IAckPass;
         end
      end
   s_RdWaitWbAnswer: 
      if (WbAck_i || &WbTimeoutCounter_c14) NextState_ab3 = s_RdCloseVmeCycle;  
   s_RdCloseVmeCycle: 
      if (a_VmeDs0Synch && a_VmeDs1Synch) NextState_ab3 = s_Idle;
   s_WrCloseVmeCycle: 
      if (a_VmeDs0Synch && a_VmeDs1Synch) NextState_ab3 = s_WrWaitWbAnswer;
   s_WrWaitWbAnswer: 
      if (WbAck_i || &WbTimeoutCounter_c14) NextState_ab3 = s_Idle;
   s_IntAck: 
      if (a_VmeDs0Synch && a_VmeDs1Synch) NextState_ab3 = s_Idle; 
   s_IAckPass:
      if (a_VmeIackInSynch) NextState_ab3 = s_Idle;
   default: NextState_ab3 = s_Idle;
   endcase
end

reg VmeDataBuffsOutEnable_e = 0;

assign VmeD_iob32 = VmeDataBuffsOutEnable_e ? InternalDataReg_b31 : 32'hZZZZZZZZ;

assign VmeDataBuffsOutMode_o = VmeWr_in;

always @(posedge Clk_ik) begin
//reset conditions/assignments
   if (Rst_irq) begin
      VmeIackOut_on <= #1 1'b1;
      VmeIrq_onb7 <= #1 7'b1111111;
      VmeDataBuffsOutEnable_e  <= #1 1'b0;
      VmeAccess_o  <= #1 1'b0;
      VmeDtAck_on  <= #1 1'b1;
      WbCyc_o  <= #1 1'b0;
      WbStb_o  <= #1 1'b0;
      WbWe_o  <= #1 1'b0;
      WbAdr_ob  <= #1 'h0;
      WbDat_ob32  <= #1 'h0;
      IntToAckCounter_c8 <= #1 'h0;
      RoraTimeoutCounter_c10 <= #1 10'h3ff;
      InternalDataReg_b31 <= #1 'h0;
      VmeAddressInternal_b30 <= #1 'h0;
      WbTimeoutCounter_c14 <= #1 'h0;
   end else begin
//default assignments applyed in all states if not differently specified
      VmeIackOut_on <= #1 1'b1;
      VmeIrq_onb7 <= #1 7'b1111111;
      VmeDataBuffsOutEnable_e  <= #1 1'b0;
      VmeAccess_o  <= #1 1'b0;
      VmeDtAck_on  <= #1 1'b1;
      if (NegedgeVmeAs_a) VmeAddressInternal_b30 <= #1 VmeA_ib31[31:2];      
      if (IntEnable_i) begin  
         if (NewIntRequest_iqp) IntToAckCounter_c8 <= #1 IntToAckCounter_c8 + 1'b1;
         if (IntModeRora_i) begin
            if (IntSourceToRead_i) VmeIrq_onb7[IntLevel_ib3] <= #1 1'b0;
         end else begin
            if (|IntToAckCounter_c8) VmeIrq_onb7[IntLevel_ib3] <= #1 1'b0;
         end
      end else begin
         IntToAckCounter_c8 <= #1 'h0;
      end
      if (RoraTimeoutCounter_c10==g_ClocksIn2us) RoraTimeoutCounter_c10 <= #1 10'h3ff;
      else if (~&RoraTimeoutCounter_c10) RoraTimeoutCounter_c10 <= #1 RoraTimeoutCounter_c10 + 1'b1;
      InternalDataReg_b31 <= #1 InternalDataReg_b31;      
      WbCyc_o  <= #1 1'b0;
      WbStb_o  <= #1 1'b0;
      WbWe_o  <= #1 1'b0;
      WbDat_ob32  <= #1 WbDat_ob32;
      WbTimeoutCounter_c14 <= #1 'h0;
//state dependent assignments   
      case(State_qb3)
      s_Idle: begin
         if (NextState_ab3 == s_RdWaitWbAnswer) begin
            VmeAccess_o  <= #1 1'b1; 
            if (NegedgeVmeAs_a) WbAdr_ob  <= #1 VmeA_ib31[g_LowestGaAddressBit-1:2];
            else WbAdr_ob  <= #1 VmeAddressInternal_b30[g_LowestGaAddressBit-1:2];
         end else if (NextState_ab3 == s_WrCloseVmeCycle) begin 
            VmeAccess_o  <= #1 1'b1; 
            if (NegedgeVmeAs_a) WbAdr_ob  <= #1 VmeA_ib31[g_LowestGaAddressBit-1:2];
            else WbAdr_ob  <= #1 VmeAddressInternal_b30[g_LowestGaAddressBit-1:2];
         end else if (NextState_ab3 == s_IntAck) begin            
            VmeAccess_o  <= #1 1'b1; 
            InternalDataReg_b31 <= #1 {24'h0, IntVector_ib8};
         end
      end
      s_RdWaitWbAnswer: begin
         VmeDataBuffsOutEnable_e  <= #1 1'b1;
         VmeAccess_o  <= #1 1'b1;
         WbWe_o  <= #1 1'b0;
         WbTimeoutCounter_c14 <= #1 WbTimeoutCounter_c14 + 1'b1;
         if (WbAck_i) begin
	    WbStb_o  <= #1 1'b0;
            WbCyc_o  <= #1 1'b0;
	    InternalDataReg_b31 <= #1 WbDat_ib32;
	 end else begin
	    WbStb_o  <= #1 1'b1;
            WbCyc_o  <= #1 1'b1;
	    if (&WbTimeoutCounter_c14) InternalDataReg_b31 <= #1 32'hFFFF_FFFF;
	 end
      end 
      s_RdCloseVmeCycle: begin
         VmeDataBuffsOutEnable_e  <= #1 1'b1;
         VmeAccess_o  <= #1 1'b1;
         VmeDtAck_on  <= #1 1'b0;
         if (NextState_ab3==s_Idle && VmeAm_ib6==6'h0B) VmeAddressInternal_b30 <= #1 VmeAddressInternal_b30 + 1'b1;         
      end
      s_WrCloseVmeCycle: begin
         VmeAccess_o  <= #1 1'b1;
         if (WbTimeoutCounter_c14==14'h1) begin
            VmeDtAck_on  <= #1 1'b0;    //delaying of 1 cycle extra
            WbDat_ob32 <= #1 VmeD_iob32; 
         end
         if (~WbAck_i) WbTimeoutCounter_c14 <= #1 WbTimeoutCounter_c14 + 1'b1;
         if (NextState_ab3==s_WrWaitWbAnswer && VmeAm_ib6==6'h0B) VmeAddressInternal_b30 <= #1 VmeAddressInternal_b30 + 1'b1;          
      end
      s_WrWaitWbAnswer: begin
         VmeAccess_o  <= #1 1'b0;
         VmeDtAck_on  <= #1 1'b0;
         if (WbAck_i) begin
            WbCyc_o  <= #1 1'b0;
            WbWe_o  <= #1 1'b0;
            WbStb_o  <= #1 1'b0;
         end else begin
            WbCyc_o  <= #1 1'b1;
            WbWe_o  <= #1 1'b1;
            WbStb_o  <= #1 1'b1;
         end
         if (~WbAck_i) WbTimeoutCounter_c14 <= #1 WbTimeoutCounter_c14 + 1'b1;
      end
      s_IntAck: begin
         VmeDataBuffsOutEnable_e  <= #1 1'b1;
         VmeAccess_o  <= #1 1'b1;         
         if (~WbAck_i) WbTimeoutCounter_c14 <= #1 WbTimeoutCounter_c14 + 1'b1;        
         if (WbTimeoutCounter_c14==14'h7) VmeDtAck_on  <= #1 1'b0;    //delaying of 2 cycle extra
         else VmeDtAck_on  <= #1 VmeDtAck_on;                   
         if (NextState_ab3 == s_Idle) begin
            if (IntModeRora_i) RoraTimeoutCounter_c10 <= #1 'h0;
            if (~NewIntRequest_iqp) IntToAckCounter_c8 <= #1 IntToAckCounter_c8 - 1'b1;
         end  
      end
      s_IAckPass: begin
         VmeIackOut_on <= #1 a_VmeIackInSynch;
      end
      default: begin
      end
      endcase
   end
end
   
endmodule   

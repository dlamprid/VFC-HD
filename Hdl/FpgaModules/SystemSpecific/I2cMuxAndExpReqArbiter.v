//============================================================================================\\
//##################################   Module Information   ##################################\\
//============================================================================================\\
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: I2cExpAndMuxMaster.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - <date>        <version>    Andrea Boccardi    <description>               
//
// Language: Verilog 2005                                                              
//
// Description:
// 
//      
//
//      At start up and after each reset the module: 
//          - scan all the IO Exp to set them in the appropriate IO state and read the actual value
//          - for each moddef0 to ground the SFP is access to read the ID of the module
//      After start up the module goes trough all the external requests, in a round robin guaranteed
//      by a mute after each access reset once no requests remains unmuted (for the BlmIn the muting can be masked):
//          - if an IoExp has an interrupt is served with the following order
//              -- BlmIn
//              -- SfpApp12
//                  --- if one of the Sfp is inserted (ModDef going to 0) the ID is read immediately
//              -- SfpApp32
//                  --- if one of the Sfp is inserted (ModDef going to 0) the ID is read immediately
//              -- SfpBstEth
//                  --- if one of the Sfp is inserted (ModDef going to 0) the ID is read immediately        
//              -- SfpLos
//          - if one of the Sfp settings (rateselect and disable) is changed we propagate the change
//          - if one of the GpIo settings is changed we propagate the change
//          - if one of the Leds settings is changed we propagate the change
//          - if there is a request to access one I2C interface we execute it
//      BlmIn can be set to latency deterministic mode. If this is the case a BlmIn int mutes all the requests
//      for a time equal to the longest transaction and is than read and than the mute removed (the time is
//      programmable)
//  
//
//                                                                                                   
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

`timescale 1ns/100ps 

module I2cExpAndMuxReqArbiter 
//====================================  Global Parameters  ===================================\\   
#(  parameter g_SclHalfPeriod = 10'd256)      
//========================================  I/O ports  =======================================\\    

(   
    //==== Clocks & Resets ====\\ 
    input             Clk_ik,
    input             Rst_irq,
    //==== Master interface ====\\    
    // IO Expanders parameters:
    output reg        IoExpWrReq_oq,    
    input             IoExpWrOn_i,
    output reg        IoExpRdReq_oq,     
    input             IoExpRdOn_i,    
    output reg  [2:0] IoExpAddr_oqb3,
    output reg  [1:0] IoExpRegAddr_oqb2, 
    output reg  [7:0] IoExpData_oqb8,  
    // I2C Mux parameters:
    output reg        I2cSlaveWrReq_oq,
    input             I2cSlaveWrOn_i,
    output reg        I2cSlaveRdReq_oq,
    input             I2cSlaveRdOn_i,    
    output reg        I2cMuxAddress_oq,
    output reg  [1:0] I2cMuxChannel_oqb2, 
    output reg  [6:0] I2cSlaveAddr_oqb7,
    output reg  [7:0] I2cSlaveRegAddr_oqb8,
    output reg  [7:0] I2cSlaveByte_oqb8,   
    // Status and results: 
    input             MasterBusy_i,
    input             MasterNewByteRead_ip,
    input       [7:0] MasterByteOut_ib8,
    input             MasterAckError_i,
    //==== I2C IO expander interrupts ====\\    
    input             IoExpApp12Int_in,
    input             IoExpApp34Int_in,
    input             IoExpBstEthInt_in,
    input             IoExpLosInt_in,
    input             IoExpBlmInInt_in,   
    //==== System and Application Interface ====\\ 
    output reg        InitDone_oq,
    // Vme Ga and GaP:
    output reg  [4:0] VmeGa_onqb5,
    output reg        VmeGaP_onq,
    // Leds:
    input       [7:0] Led_ib8,
    output reg  [7:0] StatusLed_ob8,
    // GpIo:
    input             GpIo1A2B_i,
    input             EnGpIo1Term_i,
    input             GpIo2A2B_i,
    input             EnGpIo2Term_i,    
    input             GpIo34A2B_i,
    input             EnGpIo3Term_i,    
    input             EnGpIo4Term_i,    
    output reg        StatusGpIo1A2B_oq,
    output reg        StatusEnGpIo1Term_oq,
    output reg        StatusGpIo2A2B_oq,
    output reg        StatusEnGpIo2Term_oq,    
    output reg        StatusGpIo34A2B_oq,
    output reg        StatusEnGpIo3Term_oq,    
    output reg        StatusEnGpIo4Term_oq, 
    // BlmIn:
    output reg  [7:0] BlmIn_oqb8,
    // AppSfp1:
    output reg        AppSfp1Present_oq,
    output reg [15:0] AppSfp1Id_oq16,
    output reg        AppSfp1TxFalut_oq,
    output reg        AppSfp1Los_oq,
    input             AppSfp1TxDisable_i,
    input             AppSfp1RateSelect_i,
    output reg        StatusAppSfp1TxDisable_oq,
    output reg        StatusAppSfp1RateSelect_oq,    
    input             AppSfp1I2CAccReq_i,    
    output reg        AppSfp1I2CAccGranted_oq,
    input             AppSfp1I2CIntSel_i,  //Comment: 0 for int 0x50 1 for int 0x51    
    input             AppSfp1I2CAccModeWr_i,
    input             AppSfp1I2CRdReq_i,
    input       [7:0] AppSfp1I2CRegAddr_ib8,      
    input       [7:0] AppSfp1I2CRegData_ib8,
    output reg  [7:0] AppSfp1I2CRegData_ob8,     
    // AppSfp2:
    output reg        AppSfp2Present_oq,
    output reg [15:0] AppSfp2Id_oq16,
    output reg        AppSfp2TxFalut_oq,
    output reg        AppSfp2Los_oq,
    input             AppSfp2TxDisable_i,
    input             AppSfp2RateSelect_i, 
    output reg        StatusAppSfp2TxDisable_oq,
    output reg        StatusAppSfp2RateSelect_oq,    
    input             AppSfp2I2CAccReq_i,    
    output reg        AppSfp2I2CAccGranted_oq,
    input             AppSfp2I2CIntSel_i,  //Comment: 0 for int 0x50 1 for int 0x51    
    input             AppSfp2I2CAccModeWr_i,
    input             AppSfp2I2CRdReq_i,
    input       [7:0] AppSfp2I2CRegAddr_ib8,      
    input       [7:0] AppSfp2I2CRegData_ib8,
    output reg  [7:0] AppSfp2I2CRegData_ob8,     
    // AppSfp3:
    output reg        AppSfp3Present_oq,
    output reg [15:0] AppSfp3Id_oq16,
    output reg        AppSfp3TxFalut_oq,
    output reg        AppSfp3Los_oq,
    input             AppSfp3TxDisable_i,
    input             AppSfp3RateSelect_i,  
    output reg        StatusAppSfp3TxDisable_oq,
    output reg        StatusAppSfp3RateSelect_oq,
    input             AppSfp3I2CAccReq_i,    
    output reg        AppSfp3I2CAccGranted_oq,
    input             AppSfp3I2CIntSel_i,  //Comment: 0 for int 0x50 1 for int 0x51    
    input             AppSfp3I2CAccModeWr_i,
    input             AppSfp3I2CRdReq_i,
    input       [7:0] AppSfp3I2CRegAddr_ib8,      
    input       [7:0] AppSfp3I2CRegData_ib8,
    output reg  [7:0] AppSfp3I2CRegData_ob8,     
    // AppSfp4:
    output reg        AppSfp4Present_oq,
    output reg [15:0] AppSfp4Id_oq16,
    output reg        AppSfp4TxFalut_oq,
    output reg        AppSfp4Los_oq,
    input             AppSfp4TxDisable_i,
    input             AppSfp4RateSelect_i,  
    output reg        StatusAppSfp4TxDisable_oq,
    output reg        StatusAppSfp4RateSelect_oq,
    input             AppSfp4I2CAccReq_i,    
    output reg        AppSfp4I2CAccGranted_oq,
    input             AppSfp4I2CIntSel_i,  //Comment: 0 for int 0x50 1 for int 0x51    
    input             AppSfp4I2CAccModeWr_i,
    input             AppSfp4I2CRdReq_i,
    input       [7:0] AppSfp4I2CRegAddr_ib8,      
    input       [7:0] AppSfp4I2CRegData_ib8,
    output reg  [7:0] AppSfp4I2CRegData_ob8,     
    // BstSfp:
    output reg        BstSfpPresent_oq,
    output reg [15:0] BstSfpId_oq16,
    output reg        BstSfpTxFalut_oq,
    output reg        BstSfpLos_oq,
    input             BstSfpTxDisable_i,
    input             BstSfpRateSelect_i,  
    output reg        StatusBstSfpTxDisable_oq,
    output reg        StatusBstSfpRateSelect_oq,    
    input             BstSfpI2CAccReq_i,    
    output reg        BstSfpI2CAccGranted_oq,
    input             BstSfpI2CIntSel_i,  //Comment: 0 for int 0x50 1 for int 0x51    
    input             BstSfpI2CAccModeWr_i,
    input             BstSfpI2CRdReq_i,
    input       [7:0] BstSfpI2CRegAddr_ib8,      
    input       [7:0] BstSfpI2CRegData_ib8,
    output reg  [7:0] BstSfpI2CRegData_ob8,     
    // EthSfp:
    output reg        EthSfpPresent_oq,
    output reg [15:0] EthSfpId_oq16,
    output reg        EthSfpTxFalut_oq,
    output reg        EthSfpLos_oq,
    input             EthSfpTxDisable_i,
    input             EthSfpRateSelect_i, 
    output reg        StatusEthSfpTxDisable_oq,
    output reg        StatusEthSfpRateSelect_oq,     
    input             EthSfpI2CAccReq_i,    
    output reg        EthSfpI2CAccGranted_oq,
    input             EthSfpI2CIntSel_i,  //Comment: 0 for int 0x50 1 for int 0x51    
    input             EthSfpI2CAccModeWr_i,
    input             EthSfpI2CRdReq_i,
    input       [7:0] EthSfpI2CRegAddr_ib8,      
    input       [7:0] EthSfpI2CRegData_ib8,
    output reg  [7:0] EthSfpI2CRegData_ob8,     
    // CDR:
    output reg        CdrLos_oq,
    output reg        CdrLol_oq,
    input             CdrI2CAccReq_i,
    output reg        CdrI2CAccGranted_oq,
    input             CdrI2CAccModeWr_i,
    input             CdrI2CRdReq_i,
    input       [7:0] CdrI2CRegAddr_ib8,      
    input       [7:0] CdrI2CRegData_ib8,
    output reg  [7:0] CdrI2CRegData_ob8,
    // Si57x:
    input             Si57xI2CAccReq_i,
    output reg        Si57xI2CAccGranted_oq,
    input             Si57xI2CAccModeWr_i,
    input             Si57xI2CRdReq_i,
    input       [7:0] Si57xI2CRegAddr_ib8,      
    input       [7:0] Si57xI2CRegData_ib8,
    output reg  [7:0] Si57xI2CRegData_ob8,
    //==== WishBone interface ====\\ 
    input              WbCyc_i,
    input              WbStb_i,
    input              WbWe_i,
    input              WbAdr_ib,
    input       [31:0] WbDat_ib32,
    output      [31:0] WbDat_oqb32,
    output  reg        WbAck_oa    
);     
   
//=======================================  Declarations  =====================================\\    
//==== Local parameters ====\\ 
// FSM:
localparam  s_Idle            = 6'd00;
            s_GaRead          = 6'd01;
            s_LedWrite        = 6'd02;
            s_LedSetMode      = 6'd03;
            s_GpioWrite       = 6'd04; 
            s_GpioSetMode     = 6'd05; 
            s_LosRead         = 6'd06; 
            s_BlmInRead       = 6'd07;     
            s_BstEthSetMode   = 6'd08; 
            s_BstEthWrite     = 6'd09; 
            s_BstEthRead      = 6'd10; 
            s_BstReadId1      = 6'd11; 
            s_BstReadId2      = 6'd12; 
            s_BstI2cRead      = 6'd13; 
            s_BstI2cWrite     = 6'd14; 
            s_EthReadId1      = 6'd15; 
            s_EthReadId2      = 6'd16;      
            s_EthI2cRead      = 6'd17; 
            s_EthI2cWrite     = 6'd18;       
            s_App1App2SetMode = 6'd19; 
            s_App1App2Write   = 6'd20; 
            s_App1App2Read    = 6'd21; 
            s_App1ReadId1     = 6'd22; 
            s_App1ReadId2     = 6'd23; 
            s_App1I2cRead     = 6'd24; 
            s_App1I2cWrite    = 6'd25; 
            s_App2ReadId1     = 6'd26; 
            s_App2ReadId2     = 6'd27;       
            s_App2I2cRead     = 6'd28; 
            s_App2I2cWrite    = 6'd29; 
            s_App3App3SetMode = 6'd30; 
            s_App3App4Write   = 6'd31; 
            s_App3App4Read    = 6'd32; 
            s_App3ReadId1     = 6'd33; 
            s_App3ReadId2     = 6'd34; 
            s_App3I2cRead     = 6'd35; 
            s_App3I2cWrite    = 6'd36; 
            s_App4ReadId1     = 6'd37; 
            s_App4ReadId2     = 6'd38;       
            s_App4I2cRead     = 6'd39; 
            s_App4I2cWrite    = 6'd40; 
            s_CdrI2cRead      = 6'd41; 
            s_CdrI2cWrite     = 6'd42;        
            s_Si57xI2cRead    = 6'd43; 
            s_Si57xI2cWrite   = 6'd44; 
 
// Register Addresses:
localparam c_GlobalConfigAddr = 1'b0,
           c_BlmInConfigAddr  = 1'b1;
 
//==== Wires & Regs ====\\ 

reg [31:0] GlobalConfigReg_q32;
reg [31:0] BlmInConfigReg_q32;
reg [5:0] State_q = s_Idle, NextState_a, State_d = s_Idle;
reg AppSfp12ExpWrReqMask, AppSfp34ExpWrReqMask, BstEthSfpExpWrReqMask, GpioExpWrReqMask, LedExpWrReqMask, AppSfp12ExpRdReqMask, AppSfp34ExpRdReqMask, BstEthSfpExpRdReqMask, LosExRdReqMask;       
reg BlmInRdReqMask, AppSfp1I2CAccReqMask, AppSfp2I2CAccReqMask, AppSfp3I2CAccReqMask, AppSfp4I2CAccReqMask, BstSfpI2CAccReqMask, EthSfpI2CAccReqMask, CdrI2CAccReqMask, Si57xI2CAccReqMask; 
reg [31:0] BlmInMuteOthersCnt_c32 = 0;
reg PreAppSfp1Present_q = 0, 
    PreAppSfp2Present_q = 0, 
    PreAppSfp3Present_q = 0, 
    PreAppSfp4Present_q = 0, 
    PreEthSfpPresent_q  = 0, 
    PreBstSfpPresent_q  = 0; 

//=======================================  User Logic  =======================================\\    

//==== WishBone Interface ====\\ 

always @(posedge Clk_ik) 
    if (Rst_irq) begin
        GlobalConfigReg_q32 <= #1 32'd1_000_000;
        BlmInConfigReg_q32  <= #1 32'd500_000;
        WbAck_oa            <= #1 1'b0;
    end else begin  
        if (WbCyc_i && WbWe_i && WbStb_i) case(WbAdr_ib)
            c_GlobalConfigAddr: begin
                GlobalConfigReg_q32 <= #1 WbWe_i ? WbDat_ib32 : GlobalConfigReg_q32;
                WbDat_oqb32         <= #1 WbWe_i ? WbDat_ib32 : GlobalConfigReg_q32;
            end
            c_BlmInConfigAddr: begin
                BlmInConfigReg_q32 <= #1 WbWe_i ? WbDat_ib32 : BlmInConfigReg_q32;
                WbDat_oqb32        <= #1 WbWe_i ? WbDat_ib32 : BlmInConfigReg_q32;            
            end
            default: begin
                WbDat_oqb32        <= #1 32'hDead_Beef;
            end
        endcase    
        WbAck_oa       <= #1 WbStb_i&&WbCyc_i;
    end
 
wire        a_BlmInI2cIntTimeOutEnable   = GlobalConfigReg_q32[29];  //Comment: by default the BlmIn do not use the global timeout for its requests  
wire        a_BlmInI2cIntDisable         = GlobalConfigReg_q32[28]; 
wire        a_LosI2cIntDisable           = GlobalConfigReg_q32[27]; 
wire        a_BstEthSfpI2cIntDisable     = GlobalConfigReg_q32[26];
wire        a_AppSfp34I2cIntDisable      = GlobalConfigReg_q32[25];
wire        a_AppSfp12I2cIntDisable      = GlobalConfigReg_q32[24];
wire [31:0] a_BlmInMuteOthersTime_b32    = BlmInConfigReg_q32;

//==== I2C Exp and Mux Access Requests ====\\ 

wire BlmInRdReq        = ~BlmInRdReqMask        && ~(IoExpBlmInInt_in || a_BlmInI2cIntDisable);
wire AppSfp12ExpRdReq  = ~AppSfp12ExpRdReqMask  && ~(IoExpApp12Int_in || a_AppSfp12I2cIntDisable);
wire AppSfp34ExpRdReq  = ~AppSfp34ExpRdReqMask  && ~(IoExpApp34Int_in || a_AppSfp34I2cIntDisable);
wire BstEthSfpExpRdReq = ~BstEthSfpExpRdReqMask && ~(IoExpBstEthInt_in || a_BstEthSfpI2cIntDisable);
wire LosExRdReq        = ~LosExRdReqMask        && ~(IoExpLosInt_in || a_LosI2cIntDisable);
wire AppSfp12ExpWrReq  = ~AppSfp12ExpWrReqMask  && (AppSfp1TxDisable_i^StatusAppSfp1TxDisable_oq) || (AppSfp1RateSelect_i^StatusAppSfp1RateSelect_oq) || (AppSfp2TxDisable_i^StatusAppSfp2TxDisable_oq) || (AppSfp2RateSelect_i^StatusAppSfp2RateSelect_oq);
wire AppSfp32ExpWrReq  = ~AppSfp34ExpWrReqMask  && (AppSfp2TxDisable_i^StatusAppSfp2TxDisable_oq) || (AppSfp2RateSelect_i^StatusAppSfp2RateSelect_oq) || (AppSfp3TxDisable_i^StatusAppSfp3TxDisable_oq) || (AppSfp3RateSelect_i^StatusAppSfp3RateSelect_oq);
wire BstEthSfpExpWrReq = ~BstEthSfpExpWrReqMask && (BstSfpTxDisable_i^StatusBstSfpTxDisable_oq) || (BstSfpRateSelect_i^StatusBstSfpRateSelect_oq) || (EthSfpTxDisable_i^StatusEthSfpTxDisable_oq) || (EthSfpRateSelect_i^StatusEthSfpRateSelect_oq);
wire GpioExpWrReq      = ~GpioExpWrReqMask      && (GpIo1A2B_i^StatusGpIo1A2B_oq || EnGpIo1Term_i^StatusEnGpIo1Term_oq || GpIo2A2B_i^StatusGpIo2A2B_oq || EnGpIo2Term_i^StatusEnGpIo2Term_oq || GpIo34A2B_i^StatusGpIo34A2B_oq || EnGpIo3Term_i^StatusEnGpIo3Term_oq || EnGpIo4Term_i^StatusEnGpIo4Term_oq);    
wire LedExpWrReq       = ~LedExpWrReqMask       && (Led_ib8!=StatusLed_ob8);
wire AppSfp1I2CAccReq  = ~AppSfp1I2CAccReqMask  && AppSfp1I2CAccReq_i;
wire AppSfp2I2CAccReq  = ~AppSfp2I2CAccReqMask  && AppSfp2I2CAccReq_i;
wire AppSfp3I2CAccReq  = ~AppSfp3I2CAccReqMask  && AppSfp3I2CAccReq_i;
wire AppSfp4I2CAccReq  = ~AppSfp4I2CAccReqMask  && AppSfp4I2CAccReq_i;
wire BstSfpI2CAccReq   = ~BstSfpI2CAccReqMask   && BstSfpI2CAccReq_i;
wire EthSfpI2CAccReq   = ~EthSfpI2CAccReqMask   && EthSfpI2CAccReq_i;
wire CdrI2CAccReq      = ~CdrI2CAccReqMask      && CdrI2CAccReq_i;
wire Si57xI2CAccReq    = ~Si57xI2CAccReqMask    && Si57xI2CAccReq_i;

//==== I2C Exp and Mux End of transaction detection ====\\

always @(posedge Clk_ik) MasterBusy_d    <= MasterBusy_i;
always @(posedge Clk_ik) MasterTrnDone_p <= MasterBusy_d && ~MasterBusy_i;

//==== State Machine ====\\ 

always @(posedge Clk_ik) State_d <= #1 State_q;

always @(posedge Clk_ik) State_q <= #1 Rst_irq ?  s_Idle :  NextState_a;
 
always @* begin
    NextState_a = State_q;
    case (State_q)
    s_Idle            : if (~MasterBusy_i) begin
                            if (~InitDone_oq) NextState_a = s_GaRead;
                            else if (|BlmInMuteOthersCnt_c32) begin  //Comment: a BlmIn int triggered a mute all to assure the latency determinism of its readout
                                if ((BlmInMuteOthersCnt_c32 == a_BlmInMuteOthersTime_b32) && BlmInRdReq ) NextState_a = s_BlmInRead;
                            end else begin
                                if (BlmInRdReq && ~|a_BlmInMuteOthersTime_b32) NextState_a = s_BlmInRead; //comment: The MuteOthers is set to 0 so no need to wait
                                else if (AppSfp12ExpRdReq )                    NextState_a = s_App1App2Read;
                                else if (AppSfp34ExpRdReq )                    NextState_a = s_App3App4Read;
                                else if (BstEthSfpExpRdReq)                    NextState_a = s_BstEthRead;
                                else if (LosExRdReq       )                    NextState_a = s_LosRead;
                                else if (AppSfp12ExpWrReq )                    NextState_a = s_App1App2Write;
                                else if (AppSfp32ExpWrReq )                    NextState_a = s_App3App4Write;
                                else if (BstEthSfpExpWrReq)                    NextState_a = s_BstEthWrite;
                                else if (GpioExpWrReq     )                    NextState_a = s_GpioWrite;
                                else if (LedExpWrReq      )                    NextState_a = s_LedWrite;
                                else if (AppSfp1I2CAccReq )                    NextState_a = AppSfp1I2CAccModeWr_i ? s_App1I2cWrite  : s_App1I2cRead; 
                                else if (AppSfp2I2CAccReq )                    NextState_a = AppSfp2I2CAccModeWr_i ? s_App2I2cWrite  : s_App2I2cRead; 
                                else if (AppSfp3I2CAccReq )                    NextState_a = AppSfp3I2CAccModeWr_i ? s_App3I2cWrite  : s_App3I2cRead; 
                                else if (AppSfp4I2CAccReq )                    NextState_a = AppSfp4I2CAccModeWr_i ? s_App4I2cWrite  : s_App4I2cRead; 
                                else if (BstSfpI2CAccReq  )                    NextState_a = BstSfpI2CAccModeWr_i  ? s_BstI2cWrite   : s_BstI2cRead; 
                                else if (EthSfpI2CAccReq  )                    NextState_a = EthSfpI2CAccModeWr_i  ? s_EthI2cWrite   : s_EthI2cRead;
                                else if (CdrI2CAccReq     )                    NextState_a = CdrI2CAccModeWr_i     ? s_CdrI2cWrite   : s_CdrI2cRead;
                                else if (Si57xI2CAccReq   )                    NextState_a = Si57xI2CAccModeWr_i   ? s_Si57xI2cWrite : s_Si57xI2cRead;
                            end   
                        end
    s_GaRead          : if (MasterTrnDone_p) NextState_a = s_BlmInRead; //Comment: this is executed only in the initialization sequence
    s_BlmInRead       : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_LosRead        ;
    s_LosRead         : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_GpioWrite      ;
    s_GpioWrite       : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_GpioSetMode    ;
    s_GpioSetMode     : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_LedWrite       ;
    s_LedWrite        : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_LedSetMode     ;
    s_LedSetMode      : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_BstEthWrite    ;
    s_BstEthWrite     : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_BstEthSetMode  ;   
    s_BstEthSetMode   : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_App1App2Write  ;
    s_App1App2Write   : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_App1App2SetMode;
    s_App1App2SetMode : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_App3App4Write  ;
    s_App3App4Write   : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_App3App3SetMode;
    s_App3App3SetMode : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_BstEthRead     ;
    s_BstEthRead      : if (MasterTrnDone_p) begin
                            if      (PreBstSfpPresent_q && ~BstSfpPresent_oq) NextState_a = s_BstReadId1;
                            else if (PreEthSfpPresent_q && ~EthSfpPresent_oq) NextState_a = s_EthReadId1;
                            else                                              NextState_a = InitDone_oq ? s_Idle : s_App1App2Read;
                        end
    s_BstReadId1      : if (MasterTrnDone_p) NextState_a = s_BstReadId2;
    s_BstReadId2      : if (MasterTrnDone_p) begin
                            if (PreEthSfpPresent_q && ~EthSfpPresent_oq) NextState_a = s_EthReadId1;
                            else                                         NextState_a = InitDone_oq ? s_Idle : s_App1App2Read;
                        end
    s_EthReadId1      : if (MasterTrnDone_p) NextState_a = s_EthReadId2;
    s_EthReadId2      : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_App1App2Read;
    s_App1App2Read    : if (MasterTrnDone_p) begin
                            if      (PreAppSfp1Present_q && ~AppSfp1Present_q) NextState_a = s_App1ReadId1;
                            else if (PreAppSfp2Present_q && ~AppSfp2Present_q) NextState_a = s_App2ReadId1;
                            else                                               NextState_a = InitDone_oq ? s_Idle : s_App3App4Read;
                        end
    s_App1ReadId1     : if (MasterTrnDone_p) NextState_a = s_App1ReadId2;
    s_App1ReadId2     : if (MasterTrnDone_p) begin
                            if (PreAppSfp2Present_q && ~AppSfp2Present_q) NextState_a = s_App2ReadId1;
                            else                                          NextState_a = InitDone_oq ? s_Idle : s_App3App4Read;
                        end   
    s_App2ReadId1     : if (MasterTrnDone_p) NextState_a = s_App2ReadId2;
    s_App2ReadId2     : if (MasterTrnDone_p) NextState_a = InitDone_oq ? s_Idle : s_App3App4Read;  
    s_App3App4Read    : if (MasterTrnDone_p) begin
                            if      (PreAppSfp3Present_q && ~AppSfp3Present_q) NextState_a = s_App3ReadId1;
                            else if (PreAppSfp4Present_q && ~AppSfp4Present_q) NextState_a = s_App4ReadId1;
                            else                                               NextState_a = s_Idle;
                        end
    s_App3ReadId1     : if (MasterTrnDone_p) NextState_a = s_App3ReadId2;
    s_App3ReadId2     : if (MasterTrnDone_p) begin
                            if (PreAppSfp4Present_q && ~AppSfp4Present_q) NextState_a = s_App4ReadId1;
                            else                                          NextState_a = s_Idle;
                        end   
    s_App4ReadId1     : if (MasterTrnDone_p) NextState_a = s_App4ReadId2;
    s_App4ReadId2     : if (MasterTrnDone_p) NextState_a = s_Idle; 
    s_BstI2cRead      : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_BstI2cWrite     : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_EthI2cRead      : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_EthI2cWrite     : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_App1I2cRead     : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_App1I2cWrite    : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_App2I2cRead     : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_App2I2cWrite    : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_App3I2cRead     : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_App3I2cWrite    : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_App4I2cRead     : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_App4I2cWrite    : if (MasterTrnDone_p) NextState_a = s_Idle; 
    s_CdrI2cRead      : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_CdrI2cWrite     : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_Si57xI2cRead    : if (MasterTrnDone_p) NextState_a = s_Idle;
    s_Si57xI2cWrite   : if (MasterTrnDone_p) NextState_a = s_Idle;
    endcase
end
    
always @(posedge Clk_ik) begin
    if (Rst_irq) begin
        InitDone_oq                <= #1 1'b0;
        IoExpWrReq_oq              <= #1 1'b0;    
        IoExpRdReq_oq              <= #1 1'b0;     
        IoExpAddr_oqb3             <= #1 3'b0;
        IoExpRegAddr_oqb2          <= #1 2'b0; 
        IoExpData_oqb8             <= #1 8'b0;  
        I2cSlaveWrReq_oq           <= #1 1'b0;
        I2cSlaveRdReq_oq           <= #1 1'b0;
        I2cMuxAddress_oq           <= #1 1'b0;
        I2cMuxChannel_oqb2         <= #1 2'b0; 
        I2cSlaveAddr_oqb7          <= #1 7'b0;
        I2cSlaveRegAddr_oqb8       <= #1 8'b0;
        I2cSlaveByte_oqb8          <= #1 8'b0;      
        VmeGa_onqb5                <= #1 5'b0;
        VmeGaP_onq                 <= #1 1'b0;
        StatusLed_ob8              <= #1 8'b0;
        StatusGpIo1A2B_oq          <= #1 1'b0;
        StatusEnGpIo1Term_oq       <= #1 1'b0;
        StatusGpIo2A2B_oq          <= #1 1'b0;
        StatusEnGpIo2Term_oq       <= #1 1'b0;    
        StatusGpIo34A2B_oq         <= #1 1'b0;
        StatusEnGpIo3Term_oq       <= #1 1'b0;    
        StatusEnGpIo4Term_oq       <= #1 1'b0; 
        BlmIn_oqb8                 <= #1 8'b0;
        AppSfp1Present_oq          <= #1 1'b0;
        AppSfp1Id_oq16             <= #1 16'b0;
        AppSfp1TxFalut_oq          <= #1 1'b0;
        AppSfp1Los_oq              <= #1 1'b0;
        StatusAppSfp1TxDisable_oq  <= #1 1'b0;
        StatusAppSfp1RateSelect_oq <= #1 1'b0;    
        AppSfp1I2CAccGranted_oq    <= #1 1'b0;
        AppSfp1I2CRegData_ob8      <= #1 8'b0;     
        AppSfp2Present_oq          <= #1 1'b0;
        AppSfp2Id_oq16             <= #1 16'b0;
        AppSfp2TxFalut_oq          <= #1 1'b0;
        AppSfp2Los_oq              <= #1 1'b0;
        StatusAppSfp2TxDisable_oq  <= #1 1'b0;
        StatusAppSfp2RateSelect_oq <= #1 1'b0;    
        AppSfp2I2CAccGranted_oq    <= #1 1'b0;
        AppSfp2I2CRegData_ob8      <= #1 8'b0;     
        AppSfp3Present_oq          <= #1 1'b0;
        AppSfp3Id_oq16             <= #1 16'b0;
        AppSfp3TxFalut_oq          <= #1 1'b0;
        AppSfp3Los_oq              <= #1 1'b0;
        StatusAppSfp3TxDisable_oq  <= #1 1'b0;
        StatusAppSfp3RateSelect_oq <= #1 1'b0; 
        AppSfp3I2CAccGranted_oq    <= #1 1'b0;
        AppSfp3I2CRegData_ob8      <= #1 8'b0;         
        AppSfp4Present_oq          <= #1 1'b0;
        AppSfp4Id_oq16             <= #1 16'b0;
        AppSfp4TxFalut_oq          <= #1 1'b0;
        AppSfp4Los_oq              <= #1 1'b0;
        StatusAppSfp4TxDisable_oq  <= #1 1'b0;
        StatusAppSfp4RateSelect_oq <= #1 1'b0; 
        AppSfp4I2CAccGranted_oq    <= #1 1'b0;
        AppSfp4I2CRegData_ob8      <= #1 8'b0;     
        BstSfpPresent_oq           <= #1 1'b0;
        BstSfpId_oq16              <= #1 16'b0;
        BstSfpTxFalut_oq           <= #1 1'b0;
        BstSfpLos_oq               <= #1 1'b0;
        StatusBstSfpTxDisable_oq   <= #1 1'b0;
        StatusBstSfpRateSelect_oq  <= #1 1'b0;    
        BstSfpI2CAccGranted_oq     <= #1 1'b0;
        BstSfpI2CRegData_ob8       <= #1 8'b0;     
        EthSfpPresent_oq           <= #1 1'b0;
        EthSfpId_oq16              <= #1 16'b0;
        EthSfpTxFalut_oq           <= #1 1'b0;
        EthSfpLos_oq               <= #1 1'b0;
        StatusEthSfpTxDisable_oq   <= #1 1'b0;
        StatusEthSfpRateSelect_oq  <= #1 1'b0;     
        EthSfpI2CAccGranted_oq     <= #1 1'b0;
        EthSfpI2CRegData_ob8       <= #1 8'b0;     
        CdrLos_oq                  <= #1 1'b0;
        CdrLol_oq                  <= #1 1'b1;
        CdrI2CAccGranted_oq        <= #1 1'b0;
        CdrI2CRegData_ob8          <= #1 8'b0;
        Si57xI2CAccGranted_oq      <= #1 1'b0;
        Si57xI2CRegData_ob8        <= #1 8'b0;
        BlmInRdReqMask             <= #1 1'b0;
        AppSfp12ExpRdReqMask       <= #1 1'b0;
        AppSfp34ExpRdReqMask       <= #1 1'b0;
        BstEthSfpExpRdReqMask      <= #1 1'b0;
        LosExRdReqMask             <= #1 1'b0;
        AppSfp12ExpWrReqMask       <= #1 1'b0;
        AppSfp34ExpWrReqMask       <= #1 1'b0;
        BstEthSfpExpWrReqMask      <= #1 1'b0;
        GpioExpWrReqMask           <= #1 1'b0;
        LedExpWrReqMask            <= #1 1'b0;
        AppSfp1I2CAccReqMask       <= #1 1'b0;
        AppSfp2I2CAccReqMask       <= #1 1'b0;
        AppSfp3I2CAccReqMask       <= #1 1'b0;
        AppSfp4I2CAccReqMask       <= #1 1'b0;
        BstSfpI2CAccReqMask        <= #1 1'b0;
        EthSfpI2CAccReqMask        <= #1 1'b0;
        CdrI2CAccReqMask           <= #1 1'b0;
        Si57xI2CAccReqMask         <= #1 1'b0;
        BlmInMuteOthersCnt_c32     <= #1 32'h0;
        PreAppSfp1Present_q        <= #1 1'b0; 
        PreAppSfp2Present_q        <= #1 1'b0; 
        PreAppSfp3Present_q        <= #1 1'b0; 
        PreAppSfp4Present_q        <= #1 1'b0; 
        PreEthSfpPresent_q         <= #1 1'b0; 
        PreBstSfpPresent_q         <= #1 1'b0;  
    end else begin
        case(State_q)
        s_Idle            : begin
            if (~(BlmInRdReq||AppSfp12ExpRdReq||AppSfp34ExpRdReq||BstEthSfpExpRdReq||LosExRdReq||AppSfp12ExpWrReq||AppSfp32ExpWrReq||BstEthSfpExpWrReq||GpioExpWrReq||LedExpWrReq||AppSfp1I2CAccReq||AppSfp2I2CAccReq||AppSfp3I2CAccReq ||AppSfp4I2CAccReq||BstSfpI2CAccReq||EthSfpI2CAccReq||CdrI2CAccReq||Si57xI2CAccReq)) begin
                BlmInRdReqMask             <= #1 1'b0;
                AppSfp12ExpRdReqMask       <= #1 1'b0;
                AppSfp34ExpRdReqMask       <= #1 1'b0;
                BstEthSfpExpRdReqMask      <= #1 1'b0;
                LosExRdReqMask             <= #1 1'b0;
                AppSfp12ExpWrReqMask       <= #1 1'b0;
                AppSfp34ExpWrReqMask       <= #1 1'b0;
                BstEthSfpExpWrReqMask      <= #1 1'b0;
                GpioExpWrReqMask           <= #1 1'b0;
                LedExpWrReqMask            <= #1 1'b0;
                AppSfp1I2CAccReqMask       <= #1 1'b0;
                AppSfp2I2CAccReqMask       <= #1 1'b0;
                AppSfp3I2CAccReqMask       <= #1 1'b0;
                AppSfp4I2CAccReqMask       <= #1 1'b0;
                BstSfpI2CAccReqMask        <= #1 1'b0;
                EthSfpI2CAccReqMask        <= #1 1'b0;
                CdrI2CAccReqMask           <= #1 1'b0;
                Si57xI2CAccReqMask         <= #1 1'b0;           
            end
            if (|BlmInMuteOthersCnt_c32 || (BlmInRdReq && |a_BlmInMuteOthersTime_b32)) BlmInMuteOthersCnt_c32 <= #1 BlmInMuteOthersCnt_c32 + 1'b1;  
        end             
        s_GaRead          : begin
            if (State_q!=State_d) IoExpRdReq_oq <= #1 1'b1; 
            else if (IoExpRdOn_i) IoExpRdReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b011;
            IoExpRegAddr_oqb2         <= #1 2'b00; 
            if (MasterNewByteRead_ip) {VmeGaP_onq, VmeGa_onqb5} <= #1 MasterByteOut_ib8[5:0];
        end
        s_BlmInRead       : begin
            if (State_q!=State_d) IoExpRdReq_oq <= #1 1'b1; 
            else if (IoExpRdOn_i) IoExpRdReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b110;
            IoExpRegAddr_oqb2         <= #1 2'b00; 
            if (MasterNewByteRead_ip) BlmIn_oqb8 <= #1 MasterByteOut_ib8;
            BlmInRdReqMask         <= #1 a_BlmInI2cIntTimeOutEnable;
            BlmInMuteOthersCnt_c32 <= #1 32'b0;     
        end
        s_LosRead         : begin
            if (State_q!=State_d) IoExpRdReq_oq <= #1 1'b1; 
            else if (IoExpRdOn_i) IoExpRdReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b111;
            IoExpRegAddr_oqb2         <= #1 2'b00; 
            if (MasterNewByteRead_ip) {CdrLol_oq, CdrLos_oq, EthSfpLos_oq, BstSfpLos_oq, AppSfp4Los_oq, AppSfp3Los_oq, AppSfp2Los_oq, AppSfp1Los_oq} <= #1 MasterByteOut_ib8;
            LosExRdReqMask         <= #1 1'b1;
        end
        s_GpioWrite       : begin
            if (State_q!=State_d) begin 
                IoExpWrReq_oq  <= #1 1'b1; 
                IoExpData_oqb8 <= #1 {1'b0, EnGpIo4Term_i, EnGpIo3Term_i, EnGpIo2Term_i, EnGpIo1Term_i, GpIo34A2B_i, GpIo2A2B_i, GpIo1A2B_i};
            end else if (IoExpWrOn_i) IoExpWrReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b100;
            IoExpRegAddr_oqb2         <= #1 2'b01;             
            if (NextState_a != s_GpioWrite) {StatusEnGpIo4Term_oq, StatusEnGpIo3Term_oq, StatusEnGpIo2Term_oq, StatusEnGpIo1Term_oq, StatusGpIo34A2B_oq, StatusGpIo2A2B_oq, StatusGpIo1A2B_oq} <= #1 IoExpData_oqb8[6:0];
            GpioExpWrReqMask         <= #1 1'b1;
        end
        s_GpioSetMode     : begin
            if (State_q!=State_d) begin 
                IoExpWrReq_oq  <= #1 1'b1; 
                IoExpData_oqb8 <= #1 8'b0;
            end else if (IoExpWrOn_i) IoExpWrReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b100;
            IoExpRegAddr_oqb2         <= #1 2'b11;             
        end
        s_LedWrite        : begin
            if (State_q!=State_d) begin 
                IoExpWrReq_oq  <= #1 1'b1; 
                IoExpData_oqb8 <= #1 Led_ib8;
            end else if (IoExpWrOn_i) IoExpWrReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b101;
            IoExpRegAddr_oqb2         <= #1 2'b01;             
            if (NextState_a != s_LedWrite) StatusLed_ob8 <= #1 IoExpData_oqb8;
            LedExpWrReqMask         <= #1 1'b1;
        end
        s_LedSetMode      : begin
            if (State_q!=State_d) begin 
                IoExpWrReq_oq  <= #1 1'b1; 
                IoExpData_oqb8 <= #1 8'b0;
            end else if (IoExpWrOn_i) IoExpWrReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b101;
            IoExpRegAddr_oqb2         <= #1 2'b11;             
        end        
        s_BstEthWrite     : begin
            if (State_q!=State_d) begin 
                IoExpWrReq_oq  <= #1 1'b1; 
                IoExpData_oqb8 <= #1 {1'b0, EthSfpRateSelect_i, EthSfpTxDisable_i, 1'b0, 1'b0, BstSfpRateSelect_i, BstSfpTxDisable_i, 1'b0};
            end else if (IoExpWrOn_i) IoExpWrReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b010;
            IoExpRegAddr_oqb2         <= #1 2'b01;             
            if (NextState_a != s_BstEthWrite) begin
                StatusEthSfpRateSelect_oq <= #1 IoExpData_oqb8[6];
                StatusEthSfpTxDisable_oq  <= #1 IoExpData_oqb8[5];
                StatusBstSfpRateSelect_oq <= #1 IoExpData_oqb8[2];
                StatusBstSfpTxDisable_oq  <= #1 IoExpData_oqb8[1];
            end    
            BstEthSfpExpWrReqMask         <= #1 1'b1;
        end
        s_BstEthSetMode   : begin
            if (State_q!=State_d) begin 
                IoExpWrReq_oq  <= #1 1'b1; 
            end else if (IoExpWrOn_i) IoExpWrReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b010;
            IoExpRegAddr_oqb2         <= #1 2'b11; 
            IoExpData_oqb8            <= #1 8'b10011001;            
        end
        s_BstEthRead      : begin
            if (State_q!=State_d) IoExpRdReq_oq <= #1 1'b1; 
            else if (IoExpRdOn_i) IoExpRdReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b010;
            IoExpRegAddr_oqb2         <= #1 2'b00; 
            if (MasterNewByteRead_ip) begin
                PreEthSfpPresent_q <= #1 ~MasterByteOut_ib8[7];
                EthSfpTxFalut_oq   <= #1 MasterByteOut_ib8[4];
                PreBstSfpPresent_q <= #1 ~MasterByteOut_ib8[3];
                BstSfpTxFalut_oq   <= #1 MasterByteOut_ib8[0];
            end    
            BstEthSfpExpRdReqMask  <= #1 1'b1;
        end
        s_BstReadId1      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0;         
            I2cMuxAddress_oq          <= #1 1'b1;
            I2cMuxChannel_oqb2        <= #1 2'b00; 
            I2cSlaveAddr_oqb7         <= #1 7'h50;
            I2cSlaveRegAddr_oqb8      <= #1 8'b0;
            if (MasterNewByteRead_ip) BstSfpId_oq16[7:0] <= #1 MasterByteOut_ib8; 
        end
        s_BstReadId2      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0;       
            I2cMuxAddress_oq          <= #1 1'b1;
            I2cMuxChannel_oqb2        <= #1 2'b00; 
            I2cSlaveAddr_oqb7         <= #1 7'h50;
            I2cSlaveRegAddr_oqb8      <= #1 8'b1;
            if (MasterNewByteRead_ip) begin 
                BstSfpId_oq16[15:8] <= #1 MasterByteOut_ib8;
                BstSfpPresent_oq    <= #1 1'b1;
            end
        end   
        s_EthReadId1      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0;         
            I2cMuxAddress_oq          <= #1 1'b1;
            I2cMuxChannel_oqb2        <= #1 2'b00; 
            I2cSlaveAddr_oqb7         <= #1 7'h50;
            I2cSlaveRegAddr_oqb8      <= #1 8'b0;
            if (MasterNewByteRead_ip) EthSfpId_oq16[7:0] <= #1 MasterByteOut_ib8; 
        end
        s_EthReadId2      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0;         
            I2cMuxAddress_oq          <= #1 1'b1;
            I2cMuxChannel_oqb2        <= #1 2'b01; 
            I2cSlaveAddr_oqb7         <= #1 7'h50;
            I2cSlaveRegAddr_oqb8      <= #1 8'b1;
            if (MasterNewByteRead_ip) begin 
                EthSfpId_oq16[15:8] <= #1 MasterByteOut_ib8;
                EthSfpPresent_oq    <= #1 1'b1;
            end
        end 
        s_App1App2Write     : begin
            if (State_q!=State_d) begin 
                IoExpWrReq_oq  <= #1 1'b1; 
                IoExpData_oqb8 <= #1 {1'b0, AppSfp2RateSelect_i, AppSfp2TxDisable_i, 1'b0, 1'b0, AppSfp1RateSelect_i, AppSfp1TxDisable_i, 1'b0};
            end else if (IoExpWrOn_i) IoExpWrReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b000;
            IoExpRegAddr_oqb2         <= #1 2'b01;             
            if (NextState_a != s_App1App2Write) begin
                StatusAppSfp2RateSelect_oq <= #1 IoExpData_oqb8[6];
                StatusAppSfp2TxDisable_oq  <= #1 IoExpData_oqb8[5];
                StatusAppSfp1RateSelect_oq <= #1 IoExpData_oqb8[2];
                StatusAppSfp1TxDisable_oq  <= #1 IoExpData_oqb8[1];
            end    
            AppSfp12ExpWrReqMask         <= #1 1'b1;
        end
        s_App1App2SetMode   : begin
            if (State_q!=State_d) begin 
                IoExpWrReq_oq  <= #1 1'b1; 
            end else if (IoExpWrOn_i) IoExpWrReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b000;
            IoExpRegAddr_oqb2         <= #1 2'b11;
            IoExpData_oqb8            <= #1 8'b10011001;            
        end
        s_App1App2Read      : begin
            if (State_q!=State_d) IoExpRdReq_oq <= #1 1'b1; 
            else if (IoExpRdOn_i) IoExpRdReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3          <= #1 3'b000;
            IoExpRegAddr_oqb2       <= #1 2'b00; 
            if (MasterNewByteRead_ip) begin
                PreAppSfp2Present_q <= #1 ~MasterByteOut_ib8[7];
                AppSfp2TxFalut_oq   <= #1 MasterByteOut_ib8[4];
                PreAppSfp1Present_q <= #1 ~MasterByteOut_ib8[3];
                AppSfp1TxFalut_oq   <= #1 MasterByteOut_ib8[0];
            end    
            AppSfp12ExpRdReqMask    <= #1 1'b1;
        end
        s_App1ReadId1      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0;         
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b00; 
            I2cSlaveAddr_oqb7         <= #1 7'h50;
            I2cSlaveRegAddr_oqb8      <= #1 8'b0;
            if (MasterNewByteRead_ip) AppSfp1Id_oq16[7:0] <= #1 MasterByteOut_ib8; 
        end        
        s_App1ReadId2      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0;     
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b00; 
            I2cSlaveAddr_oqb7         <= #1 7'h50;
            I2cSlaveRegAddr_oqb8      <= #1 8'b1;
            if (MasterNewByteRead_ip) begin 
                AppSfp1Id_oq16[15:8]  <= #1 MasterByteOut_ib8;
                AppSfp1Present_oq     <= #1 1'b1;
            end
        end  
        s_App2ReadId1      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0;        
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b01; 
            I2cSlaveAddr_oqb7         <= #1 7'h50;
            I2cSlaveRegAddr_oqb8      <= #1 8'b0;
            if (MasterNewByteRead_ip) AppSfp2Id_oq16[7:0] <= #1 MasterByteOut_ib8; 
        end        
        s_App2ReadId2      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0;
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b01; 
            I2cSlaveAddr_oqb7         <= #1 7'h50;
            I2cSlaveRegAddr_oqb8      <= #1 8'b1;
            if (MasterNewByteRead_ip) begin 
                AppSfp2Id_oq16[15:8] <= #1 MasterByteOut_ib8;
                AppSfp2Present_oq    <= #1 1'b1;
            end
        end 
        s_App3App4Write     : begin
            if (State_q!=State_d) begin 
                IoExpWrReq_oq  <= #1 1'b1; 
                IoExpData_oqb8 <= #1 {1'b0, AppSfp4RateSelect_i, AppSfp4TxDisable_i, 1'b0, 1'b0, AppSfp3RateSelect_i, AppSfp3TxDisable_i, 1'b0};
            end else if (IoExpWrOn_i) IoExpWrReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b001;
            IoExpRegAddr_oqb2         <= #1 2'b01;             
            if (NextState_a != s_App1App2Write) begin
                StatusAppSfp4RateSelect_oq <= #1 IoExpData_oqb8[6];
                StatusAppSfp4TxDisable_oq  <= #1 IoExpData_oqb8[5];
                StatusAppSfp3RateSelect_oq <= #1 IoExpData_oqb8[2];
                StatusAppSfp3TxDisable_oq  <= #1 IoExpData_oqb8[1];
            end    
            AppSfp34ExpWrReqMask     <= #1 1'b1;
        end
        s_App3App4SetMode   : begin
            if (State_q!=State_d) begin 
                IoExpWrReq_oq  <= #1 1'b1; 
            end else if (IoExpWrOn_i) IoExpWrReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b001;
            IoExpRegAddr_oqb2         <= #1 2'b11; 
            IoExpData_oqb8            <= #1 8'b10011001;            
        end       
        s_App3App4Read      : begin
            if (State_q!=State_d) IoExpRdReq_oq <= #1 1'b1; 
            else if (IoExpRdOn_i) IoExpRdReq_oq <= #1 1'b0; 
            IoExpAddr_oqb3            <= #1 3'b001;
            IoExpRegAddr_oqb2         <= #1 2'b00; 
            if (MasterNewByteRead_ip) begin
                PreAppSfp4Present_q <= #1 ~MasterByteOut_ib8[7];
                AppSfp4TxFalut_oq   <= #1 MasterByteOut_ib8[4];
                PreAppSfp3Present_q <= #1 ~MasterByteOut_ib8[3];
                AppSfp3TxFalut_oq   <= #1 MasterByteOut_ib8[0];
            end    
            AppSfp34ExpRdReqMask  <= #1 1'b1;
            if (NextState_a!=s_Idle) InitDone_oq           <= #1 1'b1;
        end
        s_App3ReadId1      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b10; 
            I2cSlaveAddr_oqb7         <= #1 7'h50;
            I2cSlaveRegAddr_oqb8      <= #1 8'b0;
            if (MasterNewByteRead_ip) AppSfp3Id_oq16[7:0] <= #1 MasterByteOut_ib8; 
        end        
        s_App3ReadId2      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b10; 
            I2cSlaveAddr_oqb7         <= #1 7'h50;
            I2cSlaveRegAddr_oqb8      <= #1 8'b1;
            if (MasterNewByteRead_ip) begin 
                AppSfp3Id_oq16[15:8] <= #1 MasterByteOut_ib8;
                AppSfp3Present_oq    <= #1 1'b1;
            end
            if (NextState_a!=s_Idle) InitDone_oq           <= #1 1'b1;            
        end  
        s_App4ReadId1      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b11; 
            I2cSlaveAddr_oqb7         <= #1 7'h50;
            I2cSlaveRegAddr_oqb8      <= #1 8'b0;
            if (MasterNewByteRead_ip) AppSfp4Id_oq16[7:0] <= #1 MasterByteOut_ib8; 
        end        
        s_App4ReadId2      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1;
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0;
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b11; 
            I2cSlaveAddr_oqb7         <= #1 7'h50;
            I2cSlaveRegAddr_oqb8      <= #1 8'b1;
            if (MasterNewByteRead_ip) begin 
                AppSfp4Id_oq16[15:8] <= #1 MasterByteOut_ib8;
                AppSfp4Present_oq    <= #1 1'b1;
            end
            if (NextState_a!=s_Idle) InitDone_oq <= #1 1'b1;
        end     
        s_BstI2cRead      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b1;
            I2cMuxChannel_oqb2        <= #1 2'b00; 
            I2cSlaveAddr_oqb7         <= #1 {6'b101000, BstSfpI2CIntSel_i};
            I2cSlaveRegAddr_oqb8      <= #1 BstSfpI2CRegAddr_ib8;
            BstSfpI2CAccGranted_oq    <= #1 NextState_a == State_q;
            if (MasterNewByteRead_ip) BstSfpI2CRegData_ob8 <= #1 MasterByteOut_ib8;
            BstSfpI2CAccReqMask        <= #1 1'b1;
        end 
        s_BstI2cWrite     : begin
            if (State_q!=State_d)    I2cSlaveWrReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveWrReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b1;
            I2cMuxChannel_oqb2        <= #1 2'b00; 
            I2cSlaveAddr_oqb7         <= #1 {6'b101000, BstSfpI2CIntSel_i};
            I2cSlaveRegAddr_oqb8      <= #1 BstSfpI2CRegAddr_ib8;
            BstSfpI2CAccGranted_oq    <= #1 NextState_a == State_q;
            BstSfpI2CAccReqMask       <= #1 1'b1;
        end 
        s_EthI2cRead      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b1;
            I2cMuxChannel_oqb2        <= #1 2'b01; 
            I2cSlaveAddr_oqb7         <= #1 {6'b101000, EthSfpI2CIntSel_i};
            I2cSlaveRegAddr_oqb8      <= #1 EthSfpI2CRegAddr_ib8;
            EthSfpI2CAccGranted_oq    <= #1 NextState_a == State_q;
            if (MasterNewByteRead_ip) EthSfpI2CRegData_ob8 <= #1 MasterByteOut_ib8;
            EthSfpI2CAccReqMask        <= #1 1'b1;
        end 
        s_EthI2cWrite     : begin
            if (State_q!=State_d)    I2cSlaveWrReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveWrReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b1;
            I2cMuxChannel_oqb2        <= #1 2'b01; 
            I2cSlaveAddr_oqb7         <= #1 {6'b101000, EthSfpI2CIntSel_i};
            I2cSlaveRegAddr_oqb8      <= #1 EthSfpI2CRegAddr_ib8;
            EthSfpI2CAccGranted_oq    <= #1 NextState_a == State_q;
            EthSfpI2CAccReqMask       <= #1 1'b1;
        end        
        s_Si57xI2cRead      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b1;
            I2cMuxChannel_oqb2        <= #1 2'b10; 
            I2cSlaveAddr_oqb7         <= #1 7'h55;
            I2cSlaveRegAddr_oqb8      <= #1 Si57xI2CRegAddr_ib8;
            Si57xI2CAccGranted_oq     <= #1 NextState_a == State_q;
            if (MasterNewByteRead_ip) Si57xI2CRegData_ob8 <= #1 MasterByteOut_ib8;
            Si57xI2CAccReqMask        <= #1 1'b1;
        end 
        s_Si57xI2cWrite     : begin
            if (State_q!=State_d)    I2cSlaveWrReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveWrReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b1;
            I2cMuxChannel_oqb2        <= #1 2'b10; 
            I2cSlaveAddr_oqb7         <= #1 7'h55;
            I2cSlaveRegAddr_oqb8      <= #1 Si57xI2CRegAddr_ib8;
            Si57xI2CAccGranted_oq     <= #1 NextState_a == State_q;
            Si57xI2CAccReqMask        <= #1 1'b1;
        end 
        s_CdrI2cRead      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b1;
            I2cMuxChannel_oqb2        <= #1 2'b11; 
            I2cSlaveAddr_oqb7         <= #1 7'h40;
            I2cSlaveRegAddr_oqb8      <= #1 CdrI2CRegAddr_ib8;
            CdrI2CAccGranted_oq       <= #1 NextState_a == State_q;
            if (MasterNewByteRead_ip) CdrI2CRegData_ob8     <= #1 MasterByteOut_ib8;
            CdrI2CAccReqMask          <= #1 1'b1;
        end 
        s_CdrI2cWrite     : begin
            if (State_q!=State_d)    I2cSlaveWrReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveWrReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b1;
            I2cMuxChannel_oqb2        <= #1 2'b11; 
            I2cSlaveAddr_oqb7         <= #1 7'h40;
            I2cSlaveRegAddr_oqb8      <= #1 CdrI2CRegAddr_ib8;
            CdrI2CAccGranted_oq       <= #1 NextState_a == State_q;
            CdrI2CAccReqMask          <= #1 1'b1;
        end
        s_App1I2cRead      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b00; 
            I2cSlaveAddr_oqb7         <= #1 {6'b101000, AppSfp1I2CIntSel_i};
            I2cSlaveRegAddr_oqb8      <= #1 AppSfp1I2CRegAddr_ib8;
            AppSfp1I2CAccGranted_oq   <= #1 NextState_a == State_q;
            if (MasterNewByteRead_ip) AppSfp1I2CRegData_ob8 <= #1 MasterByteOut_ib8;
            AppSfp1I2CAccReqMask      <= #1 1'b1;
        end 
        s_App1I2cWrite     : begin
            if (State_q!=State_d)    I2cSlaveWrReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveWrReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b00; 
            I2cSlaveAddr_oqb7         <= #1 {6'b101000, AppSfp1I2CIntSel_i};
            I2cSlaveRegAddr_oqb8      <= #1 AppSfp1I2CRegAddr_ib8;
            AppSfp1I2CAccGranted_oq   <= #1 NextState_a == State_q;
            AppSfp1I2CAccReqMask      <= #1 1'b1;
        end 
        s_App2I2cRead      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b01; 
            I2cSlaveAddr_oqb7         <= #1 {6'b101000, AppSfp2I2CIntSel_i};
            I2cSlaveRegAddr_oqb8      <= #1 AppSfp2I2CRegAddr_ib8;
            AppSfp2I2CAccGranted_oq   <= #1 NextState_a == State_q;
            if (MasterNewByteRead_ip) AppSfp2I2CRegData_ob8 <= #1 MasterByteOut_ib8;
            AppSfp2I2CAccReqMask      <= #1 1'b1;
        end 
        s_App2I2cWrite     : begin
            if (State_q!=State_d)    I2cSlaveWrReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveWrReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b01; 
            I2cSlaveAddr_oqb7         <= #1 {6'b101000, AppSfp2I2CIntSel_i};
            I2cSlaveRegAddr_oqb8      <= #1 AppSfp2I2CRegAddr_ib8;
            AppSfp2I2CAccGranted_oq   <= #1 NextState_a == State_q;
            AppSfp2I2CAccReqMask      <= #1 1'b1;
        end  
        s_App3I2cRead      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b10; 
            I2cSlaveAddr_oqb7         <= #1 {6'b101000, AppSfp3I2CIntSel_i};
            I2cSlaveRegAddr_oqb8      <= #1 AppSfp3I2CRegAddr_ib8;
            AppSfp3I2CAccGranted_oq   <= #1 NextState_a == State_q;
            if (MasterNewByteRead_ip) AppSfp3I2CRegData_ob8 <= #1 MasterByteOut_ib8; 
            AppSfp3I2CAccReqMask      <= #1 1'b1;
        end 
        s_App3I2cWrite     : begin
            if (State_q!=State_d)    I2cSlaveWrReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveWrReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b10; 
            I2cSlaveAddr_oqb7         <= #1 {6'b101000, AppSfp3I2CIntSel_i};
            I2cSlaveRegAddr_oqb8      <= #1 AppSfp3I2CRegAddr_ib8;
            AppSfp3I2CAccGranted_oq   <= #1 NextState_a == State_q;
            AppSfp3I2CAccReqMask      <= #1 1'b1;
        end  
         s_App4I2cRead      : begin
            if (State_q!=State_d)    I2cSlaveRdReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveRdReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b10; 
            I2cSlaveAddr_oqb7         <= #1 {6'b101000, AppSfp4I2CIntSel_i};
            I2cSlaveRegAddr_oqb8      <= #1 AppSfp4I2CRegAddr_ib8;
            AppSfp4I2CAccGranted_oq   <= #1 NextState_a == State_q;
            if (MasterNewByteRead_ip)  AppSfp4I2CRegData_ob8 <= #1 MasterByteOut_ib8;           
            AppSfp4I2CAccReqMask      <= #1 1'b1;
        end 
        s_App4I2cWrite     : begin
            if (State_q!=State_d)    I2cSlaveWrReq_oq <= #1 1'b1; 
            else if (I2cSlaveRdOn_i) I2cSlaveWrReq_oq <= #1 1'b0; 
            I2cMuxAddress_oq          <= #1 1'b0;
            I2cMuxChannel_oqb2        <= #1 2'b10; 
            I2cSlaveAddr_oqb7         <= #1 {6'b101000, AppSfp4I2CIntSel_i};
            I2cSlaveRegAddr_oqb8      <= #1 AppSfp4I2CRegAddr_ib8;
            AppSfp4I2CAccGranted_oq   <= #1 NextState_a == State_q;
            AppSfp4I2CAccReqMask      <= #1 1'b1;
        end 

        default           : begin
            InitDone_oq                <= #1 1'b0;
            IoExpWrReq_oq              <= #1 1'b0;    
            IoExpRdReq_oq              <= #1 1'b0;     
            IoExpAddr_oqb3             <= #1 3'b0;
            IoExpRegAddr_oqb2          <= #1 2'b0; 
            IoExpData_oqb8             <= #1 8'b0;  
            I2cSlaveWrReq_oq           <= #1 1'b0;
            I2cSlaveRdReq_oq           <= #1 1'b0;
            I2cMuxAddress_oq           <= #1 1'b0;
            I2cMuxChannel_oqb2         <= #1 2'b0; 
            I2cSlaveAddr_oqb7          <= #1 7'b0;
            I2cSlaveRegAddr_oqb8       <= #1 8'b0;
            I2cSlaveByte_oqb8          <= #1 8'b0;      
            VmeGa_onqb5                <= #1 5'b0;
            VmeGaP_onq                 <= #1 1'b0;
            StatusLed_ob8              <= #1 8'b0;
            StatusGpIo1A2B_oq          <= #1 1'b0;
            StatusEnGpIo1Term_oq       <= #1 1'b0;
            StatusGpIo2A2B_oq          <= #1 1'b0;
            StatusEnGpIo2Term_oq       <= #1 1'b0;    
            StatusGpIo34A2B_oq         <= #1 1'b0;
            StatusEnGpIo3Term_oq       <= #1 1'b0;    
            StatusEnGpIo4Term_oq       <= #1 1'b0; 
            BlmIn_oqb8                 <= #1 8'b0;
            AppSfp1Present_oq          <= #1 1'b0;
            AppSfp1Id_oq16             <= #1 16'b0;
            AppSfp1TxFalut_oq          <= #1 1'b0;
            AppSfp1Los_oq              <= #1 1'b0;
            StatusAppSfp1TxDisable_oq  <= #1 1'b0;
            StatusAppSfp1RateSelect_oq <= #1 1'b0;    
            AppSfp1I2CAccGranted_oq    <= #1 1'b0;
            AppSfp1I2CRegData_ob8      <= #1 8'b0;     
            AppSfp2Present_oq          <= #1 1'b0;
            AppSfp2Id_oq16             <= #1 16'b0;
            AppSfp2TxFalut_oq          <= #1 1'b0;
            AppSfp2Los_oq              <= #1 1'b0;
            StatusAppSfp2TxDisable_oq  <= #1 1'b0;
            StatusAppSfp2RateSelect_oq <= #1 1'b0;    
            AppSfp2I2CAccGranted_oq    <= #1 1'b0;
            AppSfp2I2CRegData_ob8      <= #1 8'b0;     
            AppSfp3Present_oq          <= #1 1'b0;
            AppSfp3Id_oq16             <= #1 16'b0;
            AppSfp3TxFalut_oq          <= #1 1'b0;
            AppSfp3Los_oq              <= #1 1'b0;
            StatusAppSfp3TxDisable_oq  <= #1 1'b0;
            StatusAppSfp3RateSelect_oq <= #1 1'b0; 
            AppSfp3I2CAccGranted_oq    <= #1 1'b0;
            AppSfp3I2CRegData_ob8      <= #1 8'b0;         
            AppSfp4Present_oq          <= #1 1'b0;
            AppSfp4Id_oq16             <= #1 16'b0;
            AppSfp4TxFalut_oq          <= #1 1'b0;
            AppSfp4Los_oq              <= #1 1'b0;
            StatusAppSfp4TxDisable_oq  <= #1 1'b0;
            StatusAppSfp4RateSelect_oq <= #1 1'b0; 
            AppSfp4I2CAccGranted_oq    <= #1 1'b0;
            AppSfp4I2CRegData_ob8      <= #1 8'b0;     
            BstSfpPresent_oq           <= #1 1'b0;
            BstSfpId_oq16              <= #1 16'b0;
            BstSfpTxFalut_oq           <= #1 1'b0;
            BstSfpLos_oq               <= #1 1'b0;
            StatusBstSfpTxDisable_oq   <= #1 1'b0;
            StatusBstSfpRateSelect_oq  <= #1 1'b0;    
            BstSfpI2CAccGranted_oq     <= #1 1'b0;
            BstSfpI2CRegData_ob8       <= #1 8'b0;     
            EthSfpPresent_oq           <= #1 1'b0;
            EthSfpId_oq16              <= #1 16'b0;
            EthSfpTxFalut_oq           <= #1 1'b0;
            EthSfpLos_oq               <= #1 1'b0;
            StatusEthSfpTxDisable_oq   <= #1 1'b0;
            StatusEthSfpRateSelect_oq  <= #1 1'b0;     
            EthSfpI2CAccGranted_oq     <= #1 1'b0;
            EthSfpI2CRegData_ob8       <= #1 8'b0;     
            CdrLos_oq                  <= #1 1'b0;
            CdrLol_oq                  <= #1 1'b1;
            CdrI2CAccGranted_oq        <= #1 1'b0;
            CdrI2CRegData_ob8          <= #1 8'b0;
            Si57xI2CAccGranted_oq      <= #1 1'b0;
            Si57xI2CRegData_ob8        <= #1 8'b0;
            BlmInRdReqMask             <= #1 1'b0;
            AppSfp12ExpRdReqMask       <= #1 1'b0;
            AppSfp34ExpRdReqMask       <= #1 1'b0;
            BstEthSfpExpRdReqMask      <= #1 1'b0;
            LosExRdReqMask             <= #1 1'b0;
            AppSfp12ExpWrReqMask       <= #1 1'b0;
            AppSfp34ExpWrReqMask       <= #1 1'b0;
            BstEthSfpExpWrReqMask      <= #1 1'b0;
            GpioExpWrReqMask           <= #1 1'b0;
            LedExpWrReqMask            <= #1 1'b0;
            AppSfp1I2CAccReqMask       <= #1 1'b0;
            AppSfp2I2CAccReqMask       <= #1 1'b0;
            AppSfp3I2CAccReqMask       <= #1 1'b0;
            AppSfp4I2CAccReqMask       <= #1 1'b0;
            BstSfpI2CAccReqMask        <= #1 1'b0;
            EthSfpI2CAccReqMask        <= #1 1'b0;
            CdrI2CAccReqMask           <= #1 1'b0;
            Si57xI2CAccReqMask         <= #1 1'b0;
            BlmInMuteOthersCnt_c32     <= #1 32'h0;
            PreAppSfp1Present_q        <= #1 1'b0; 
            PreAppSfp2Present_q        <= #1 1'b0; 
            PreAppSfp3Present_q        <= #1 1'b0; 
            PreAppSfp4Present_q        <= #1 1'b0; 
            PreEthSfpPresent_q         <= #1 1'b0; 
            PreBstSfpPresent_q         <= #1 1'b0;         
        end
        endcase
    end
end

endmodule    
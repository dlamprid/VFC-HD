`timescale 1ns/100ps 

module VfcHdSystem 
#(parameter g_SystemVersion_b8      = 8'ha8,
            g_SystemReleaseDay_b8   = 8'h03, 
            g_SystemReleaseMonth_b8 = 8'h11, 
            g_SystemReleaseYear_b8  = 8'h16)   
(   
//@@@@@@@@@@@@@@@@@@@@@@@@@
//External connections
//@@@@@@@@@@@@@@@@@@@@@@@@@
   // VME interface:     
   input             VmeAs_in,
   input      [ 5:0] VmeAm_ib6,
   input      [31:1] VmeA_ib31,
   input             VmeLWord_in,
   output            VmeAOe_oen,
   output            VmeADir_o,
   input      [ 1:0] VmeDs_inb2,
   input             VmeWrite_in,
   inout      [31:0] VmeD_iob32,
   output            VmeDOe_oen,
   output            VmeDDir_o,   
   output            VmeDtAckOe_o,
   output     [ 7:1] VmeIrq_ob7,
   input             VmeIack_in,
   input             VmeIackIn_in,
   output            VmeIackOut_on,
   input             VmeSysClk_ik,
   input             VmeSysReset_irn,
   // System SFPs Gbit lanes:
   input             EthSfpRx_i, // Comment: Differential
   output            EthSfpTx_o, // Comment: Differential
   // I2C Mux and IO expanders:
   inout             I2cMuxSda_io,
   inout             I2cMuxScl_iok,
   input             I2CMuxIntN0_in,
   input             I2CMuxIntN1_in,
   input             I2CIoExpIntApp12_in,
   input             I2CIoExpIntApp34_in,
   input             I2CIoExpIntBstEth_in,
   input             I2CIoExpIntBlmIn_in,  
   // BST:
   input             BstDataIn_i,
   input             CdrClkOut_ik,
   input             CdrDataOut_i,
   // ADC Voltage monitoring:
   input             VAdcDout_i,
   output            VAdcDin_o,
   output            VAdcCs_o,
   output            VAdcSclk_ok, 
   // Clock sources and control:
   input             GbitTrxClkRefR_ik, // Comment: Differential reference for the Gbit lines
   input             Clk20VCOx_ik,
   output            PllDac20Sync_o,
   output            PllDac25Sync_o,
   output            PllDacSclk_ok,
   output            PllDacDin_o,
   // Fmc Voltage control:
   output            VadjCs_o,
   output            VadjSclk_ok,
   output            VadjDin_o,
   // SW1:
   input      [ 4:0] NoGa_ib5,
   input             UseGa_i,
   // Pcb Revision resistor network:
   input      [ 7:0] PcbRev_ib7,
   // WR PROM:
   inout             WrPromSda_io,
   output            WrPromScl_ok,
   // Specials:
   inout             TempIdDq_ioz,
//@@@@@@@@@@@@@@@@@@@@@@@@@
//System-Application interface
//@@@@@@@@@@@@@@@@@@@@@@@@@  
   // Reset:
   output            Reset_orqp,       // Comment: Reset Synchronous with the WbClk_ik
   input             ResetRequest_iqp, // Comment: Request to issue a reset
   // WishBone:
   input             WbClk_ik,
   output reg        WbMasterCyc_o,
   output reg        WbMasterStb_o,
   output reg [24:0] WbMasterAdr_ob25,
   output reg        WbMasterWr_o,
   output reg [31:0] WbMasterDat_ob32,
   input      [31:0] WbMasterDat_ib32,
   input             WbMasterAck_i,
   // BST output:
   output            BstOn_o,
   output            BstClk_ok,
   output            BunchClkFlag_o,
   output            TurnClkFlag_o,
   output     [ 7:0] BstByteAddress_ob8,
   output     [ 7:0] BstByte_ob8,
   output            BstByteStrobe_o,
   output            BstByteError_o,
   // Interrupt:
   input      [23:0] InterruptRequest_ipb24,
   //WR Btrain interface
   input [207:0]     WrBtrainTxData_i,
   input             WrBtrainTxValid_i,
   output            WrBtrainTxDreq_o,
   input             WrBtrainTxLast_i,
   input             WrBtrainTxFlush_i,
   output            WrBtrainRxFirst_o,
   output            WrBtrainRxLast_o,
   output [207:0]    WrBtrainRxData_o,
   output            WrBtrainRxValid_o,
   input             WrBtrainRxDreq_i,
    //WRPC timecode output
   output            WrpcPps_o,
   output            WrpcTmTimeValid_o,
   output [39:0]     WrpcTmTai_o,
   output [27:0]     WrpcTmCycles_o,
   // WRPC status LEDs
   output 	     WrpcLedLink_o,
   output 	     WrpcLedAct_o,
   // WRPC system clock output
   output            WrpcSysClk_ok
);

//****************************
//Declarations
//****************************

wire        VmeAccess, VmeDtAck_n;
wire [ 7:1] VmeIrq_nb7;
//reg  [ 1:0] ResetWb_xd2 = 2'b11; // Comment: Initialised to 2'b11 for Power-On Reset
wire        WbCyc, WbStb, WbWe, WbAck;  
wire [21:0] WbAdr_b22; 
wire [31:0] WbDatMiSo_b32, WbDatMoSi_b32;
wire        IntEnable, IntModeRoRa, IntSourceToRead, NewIntRequest;
wire [ 2:0] IntLevel_b3;
wire [ 7:0] IntVector_b8; 
wire [31:0] IntRequestBus_ab32;
//reg  [ 1:0] VmeSysReset_d2 = 2'b11;
wire        WbAckIntManager, WbStbIntManager;
wire [31:0] WbDatMiSoIntManager_b32;
wire        WbAckSpiMaster, WbStbSpiMaster;
wire [31:0] WbDatSpiMaster_b32;
wire        WbAckUniqueIdReader, WbStbUniqueIdReader;
wire [31:0] WbDatUniqueIdReader_b32;
reg         WbAckAppSlaveBus; 
wire        WbStbAppSlaveBus;
reg  [31:0] WbDatAppSlaveBus_b32;
wire        WbStbI2cIoExpAndMux, WbAckI2cIoExpAndMux;
wire [31:0] WbDatI2cIoExpAndMux_b32;
wire        SpiClk_k, SpiMoSi;
wire [31:0] SpiMiSo_b32, SpiSs_nb32;
reg  [ 1:0] WbMasterStb_xd2;
reg  [ 1:0] WbAckAppSlaveBus_xd2;
wire [ 4:0] VmeGa_b5; // Comment: Need to be accessed from the I2C exp
wire        VmeGap_n; // Comment: Need to be accessed from the I2C exp
//reg         Reset_rq;
wire        TempIdDqIn, TempIdDqDriver;
wire        WrOwrIn, WrOwrOutEn;
wire        WbStbWrpcSlaveBus;
wire [31:0] WbDatWrpcSlaveBus_b32;
wire        WbAckWrpcSlaveBus; 
wire [31:0] WbAdr_b32; 

//****************************
//Clocking
//****************************

//Comment:
//The main clock is the 125MHz coming from the 125MHz VCTCXO
//The main clock is also the WB clock for the internal connections, but the WB ports receive a different one from outside
//The WbClk_ik is used only for the interface with the application module

   wire Clk_k; // = GbitTrxClkRefR_ik;
   assign WrpcSysClk_ok = Clk_k;   

//****************************
//Reset
//****************************

   wire Reset_rq;
   assign Reset_orqp = Reset_rq;   
   
/* -----\/----- EXCLUDED -----\/-----
always @(posedge Clk_k) VmeSysReset_d2 <= #1 {VmeSysReset_d2[0], ~VmeSysReset_irn};
always @(posedge Clk_k) Reset_rq = VmeSysReset_d2[1];
always @(posedge WbClk_ik) ResetWb_xd2 <= #1 {ResetWb_xd2[0], Reset_rq};
assign Reset_orqp = ResetWb_xd2[1];
 -----/\----- EXCLUDED -----/\----- */

//****************************
//VME interface & Wishbone Master
//****************************

//Comment:
//For the moment the address is manually set

assign VmeAOe_oen = 1'b0;
assign VmeADir_o  = 1'b0;

assign IntRequestBus_ab32[31:24] = {2'b0, I2CMuxIntN0_in, I2CMuxIntN1_in, I2CIoExpIntApp12_in, I2CIoExpIntApp34_in, I2CIoExpIntBstEth_in, I2CIoExpIntBlmIn_in};    
assign IntRequestBus_ab32[23:0]  = InterruptRequest_ipb24; 
 
wire [4:0] Ga_b5 = 4; 
 
assign VmeGa_b5 = UseGa_i ?    ~Ga_b5  :    ~NoGa_ib5;
assign VmeGap_n = UseGa_i ? ~^(~Ga_b5) : ~^(~NoGa_ib5);  

VmeInterfaceWb #(
    .g_LowestGaAddressBit  (24),
    .g_ClocksIn2us         (250_000))
i_VmeInterfaceWb (
    .Clk_ik                (Clk_k),
    .Rst_irq               (Reset_rq),  
    .VmeGa_ib5             (VmeGa_b5),
    .VmeGap_in             (VmeGap_n),
    .VmeAs_in              (VmeAs_in),
    .VmeDs_inb2            (VmeDs_inb2),
    .VmeAm_ib6             (VmeAm_ib6),
    .VmeWr_in              (VmeWrite_in),
    .VmeDtAck_on           (VmeDtAck_n),
    .VmeLWord_in           (VmeLWord_in),
    .VmeA_ib31             (VmeA_ib31),
    .VmeD_iob32            (VmeD_iob32),
    .VmeIack_in            (VmeIack_in),
    .VmeIackIn_in          (VmeIackIn_in),
    .VmeIackOut_on         (VmeIackOut_on),
    .VmeIrq_onb7           (VmeIrq_nb7),
    .VmeDataBuffsOutMode_o (VmeDDir_o),
    .VmeAccess_o           (VmeAccess),      
    .IntEnable_i           (IntEnable),
    .IntModeRora_i         (IntModeRoRa),
    .IntLevel_ib3          (IntLevel_b3),
    .IntVector_ib8         (IntVector_b8),
    .IntSourceToRead_i     (IntSourceToRead), 
    .NewIntRequest_iqp     (NewIntRequest),   
    .WbCyc_o               (WbCyc),
    .WbStb_o               (WbStb),
    .WbWe_o                (WbWe),
    .WbAdr_ob              (WbAdr_b22),
    .WbDat_ib32            (WbDatMiSo_b32),
    .WbDat_ob32            (WbDatMoSi_b32),
    .WbAck_i               (WbAck));

assign VmeIrq_ob7   = ~VmeIrq_nb7;
assign VmeDOe_oen   = ~VmeAccess;
assign VmeDtAckOe_o = ~VmeDtAck_n;    
  
AddrDecoderWbSys i_AddrDecoderWbSys ( 
    .Clk_ik                 (Clk_k),
    .Adr_ib22               (WbAdr_b22),
    .Stb_i                  (WbStb),
    .Dat_ob32               (WbDatMiSo_b32),
    .Ack_o                  (WbAck),
    //---
    .DatIntManager_ib32     (WbDatMiSoIntManager_b32),
    .AckIntManager_i        (WbAckIntManager),
    .StbIntManager_o        (WbStbIntManager),
    //---
    .DatSpiMaster_ib32      (WbDatSpiMaster_b32),
    .AckSpiMaster_i         (WbAckSpiMaster),
    .StbSpiMaster_o         (WbStbSpiMaster),
    //---   
    .DatUniqueIdReader_ib32 (WbDatUniqueIdReader_b32),
    .AckUniqueIdReader_i    (WbAckUniqueIdReader),
    .StbUniqueIdReader_o    (WbStbUniqueIdReader),
    //---   
    .DatAppSlaveBus_ib32    (WbDatAppSlaveBus_b32),
    .AckAppSlaveBus_i       (WbAckAppSlaveBus),
    .StbAppSlaveBus_o       (WbStbAppSlaveBus),
    //---
    .DatI2cIoExpAndMux_ib32 (WbDatI2cIoExpAndMux_b32),
    .AckI2cIoExpAndMux_i    (WbAckI2cIoExpAndMux),
    .StbI2cIoExpAndMux_o    (WbStbI2cIoExpAndMux),
    //---
    .DatWrpcSlaveBus_ib32   (WbDatWrpcSlaveBus_b32),
    .AckWrpcSlaveBus_i      (WbAckWrpcSlaveBus),
    .StbWrpcSlaveBus_o      (WbStbWrpcSlaveBus));
  
InterruptManagerWb #(
    .g_FpgaVersion_b8     (g_SystemVersion_b8), 
    .g_ReleaseDay_b8      (g_SystemReleaseDay_b8), 
    .g_ReleaseMonth_b8    (g_SystemReleaseMonth_b8), 
    .g_ReleaseYear_b8     (g_SystemReleaseYear_b8))
i_InterruptManagerWb (    
    .Rst_irq              (Reset_rq),
    .Clk_ik               (Clk_k),
    .Cyc_i                (WbCyc),
    .Stb_i                (WbStbIntManager),
    .We_i                 (WbWe),
    .Adr_ib2              (WbAdr_b22[1:0]),
    .Dat_ib32             (WbDatMoSi_b32),
    .Dat_oab32            (WbDatMiSoIntManager_b32),
    .Ack_oa               (WbAckIntManager),   
    .IntRequestLines_ib32 (IntRequestBus_ab32),
    .IntEnable_o          (IntEnable),
    .IntModeRora_o        (IntModeRoRa),
    .IntLevel_ob3         (IntLevel_b3),
    .IntVector_ob8        (IntVector_b8),
    .IntSourceToRead_o    (IntSourceToRead), 
    .NewIntRequest_oqp    (NewIntRequest));   

//****************************
//BST Decoder
//****************************
    
BstDecoder i_BstDecoder (
    .Reset_ir        (Reset_rq),
    .BstOn_o         (BstOn_o),
    .CdrClk_ik       (CdrClkOut_ik),
    .CdrDat_i        (CdrDataOut_i),
    .CdrLos_i        (1'b0), // Note!! To be modified
    .CdrLol_i        (1'b0), // Note!! To be modified
    .SfpLos_i        (1'b0), // Note!! To be modified
    .SfpPrsnt_i      (1'b1), // Note!! To be modified
    .BstByteAddr_ob8 (BstByteAddress_ob8),
    .BstByte_ob8     (BstByte_ob8),
    .BstByteStrobe_o (BstByteStrobe_o),
    .BstByteError_o  (BstByteError_o),
    .BstClk_ok       (BstClk_ok),
    .BunchClkFlag_oq (BunchClkFlag_o),  
    .TurnClkFlag_oq  (TurnClkFlag_o));
 
//****************************
//SPI INTERFACES: Vadj pot, ADC
//****************************

SpiMasterWb i_SpiMaster (
    .Clk_ik    (Clk_k),
    .Rst_irq   (Reset_rq),
    .Cyc_i     (WbCyc),
    .Stb_i     (WbStbSpiMaster),
    .We_i      (WbWe),
    .Adr_ib3   (WbAdr_b22[2:0]),
    .Dat_ib32  (WbDatMoSi_b32),
    .Dat_oab32 (WbDatSpiMaster_b32),
    .Ack_oa    (WbAckSpiMaster),
    .WaitingNewData_o(),
    .ModuleIdle_o(),
    .SClk_o    (SpiClk_k),
    .MoSi_o    (SpiMoSi),
    .MiSo_ib32 (SpiMiSo_b32),
    .SS_onb32  (SpiSs_nb32));     

assign VadjCs_o     = SpiSs_nb32[2];
assign VadjSclk_ok  = SpiClk_k;
assign VadjDin_o    = SpiMoSi;

assign SpiMiSo_b32[3] = VAdcDout_i;
assign VAdcDin_o      = SpiMoSi;
assign VAdcCs_o       = SpiSs_nb32[3];
assign VAdcSclk_ok    = SpiClk_k;    
    
//****************************
//UNIQUE-ID/TEMP CONTROLLER 
//****************************

UniqueIdReader #(
    .g_OneUsClkCycles(125)) 
i_UniqueIdReader (   
    .Rst_irq            (Reset_rq),
    .Clk_ik             (Clk_k),
    .Cyc_i              (WbCyc),
    .Stb_i              (WbStbUniqueIdReader),
    .We_i               (WbWe),
    .Adr_ib2            (WbAdr_b22[1:0]),
    .Dat_ib32           (WbDatMoSi_b32),
    .Dat_oab32          (WbDatUniqueIdReader_b32),
    .Ack_oa             (WbAckUniqueIdReader),
    .OneWireBus_i       (TempIdDqIn),
    .OneWireBusDriver_o (TempIdDqDriver));

//****************************
//IO EXP and MUX
//****************************

//Comment:
//This module allows the access to the following I2C interfaces:
//  -AppSfp(1,2,3,4) 
//  -BstSfp   
//  -EthSfp
//  -Si57x clock
//  -CDR
//This module also allow the control and monitoring of the following bits/busses:
//  -AppSfp(1, 2, 3, 4): TxFault, TxDisable, RateSelect, ModDef0
//  -BstSfp: TxFault, TxDisable, RateSelect, ModDef0
//  -EthSfp: TxFault, TxDisable, RateSelect, ModDef0
//  -Vme geographical address bits
//  -GpIo(1, 2, 3, 4): Direction and termination controls
//  -Front panel leds
//  -P0 BLMIn bus   
//For the moment the interrupt lines from the chips controlled are connected to VME interrupt sources

/* -----\/----- EXCLUDED -----\/-----
I2cMasterWb #(
    .g_CycleLenght (10'd256))
i_I2cIoExpAndMux (   
    .Clk_ik        (Clk_k),
    .Rst_irq       (Reset_rq),
    .Cyc_i         (WbCyc),
    .Stb_i         (WbStbI2cIoExpAndMux),
    .We_i          (WbWe),
    .Adr_ib2       (WbAdr_b22[1:0]),
    .Dat_ib32      (WbDatMoSi_b32),
    .Dat_oab32     (WbDatI2cIoExpAndMux_b32),
    .Ack_oa        (WbAckI2cIoExpAndMux),
    .Scl_ioz       (I2cMuxScl_iok),
    .Sda_ioz       (I2cMuxSda_io));
 -----/\----- EXCLUDED -----/\----- */
    
   //*****************************
   //WR PTP core Wrapper for VFCHD
   //*****************************
   xwrc_board_vfchd #(.g_simulation(0),
		      .g_fabric_iface("streamers"),
		      .g_streamer_width(208),
		      .g_dpram_initf("wrc.mif"))
   i_WrpcWrapper
     (
      .clk_board_125m_i     (GbitTrxClkRefR_ik),
      .clk_board_20m_i      (Clk20VCOx_ik),
      .areset_n_i           (VmeSysReset_irn),
      .clk_sys_62m5_o       (Clk_k),
      .clk_ref_125m_o       (),
      .rst_sys_62m5_o       (Reset_rq),
      .dac_ref_sync_n_o     (PllDac25Sync_o),
      .dac_dmtd_sync_n_o    (PllDac20Sync_o),
      .dac_din_o            (PllDacDin_o),
      .dac_sclk_o           (PllDacSclk_ok),
      .sfp_tx_o             (EthSfpTx_o),
      .sfp_rx_i             (EthSfpRx_i),
/* -----\/----- EXCLUDED -----\/-----
      .sfp_det_valid_i      (1'b1),
      .sfp_data_i           (128'h415847452d313235342d303533312020),
 -----/\----- EXCLUDED -----/\----- */
      .sfp_i2c_scl_b        (I2cMuxScl_iok),
      .sfp_i2c_sda_b        (I2cMuxSda_io),
      .eeprom_sda_b         (WrPromSda_io),
      .eeprom_scl_o         (WrPromScl_ok),
      .onewire_i            (WrOwrIn),
      .onewire_oen_o        (WrOwrOutEn),
      .wb_adr_i             (WbAdr_b32),
      .wb_dat_i             (WbDatMoSi_b32),
      .wb_dat_o             (WbDatWrpcSlaveBus_b32),
      .wb_sel_i             (4'b1111),
      .wb_we_i              (WbWe),
      .wb_cyc_i             (WbCyc),
      .wb_stb_i             (WbStbWrpcSlaveBus),
      .wb_ack_o             (WbAckWrpcSlaveBus),
      .wb_int_o             (),
      .wb_err_o             (),
      .wb_rty_o             (),
      .wb_stall_o           (),
      .wrf_src_adr_o        (),
      .wrf_src_dat_o        (),
      .wrf_src_cyc_o        (),
      .wrf_src_stb_o        (),
      .wrf_src_we_o         (),
      .wrf_src_sel_o        (),
      .wrf_src_ack_i        (1'b0),
      .wrf_src_stall_i      (1'b0),
      .wrf_src_err_i        (1'b0),
      .wrf_src_rty_i        (1'b0),
      .wrf_snk_adr_i        (1'b0),
      .wrf_snk_dat_i        (1'b0),
      .wrf_snk_cyc_i        (1'b0),
      .wrf_snk_stb_i        (1'b0),
      .wrf_snk_we_i         (1'b0),
      .wrf_snk_sel_i        (1'b0),
      .wrf_snk_ack_o        (),
      .wrf_snk_stall_o      (),
      .wrf_snk_err_o        (),
      .wrf_snk_rty_o        (),
      .wrs_tx_data_i        (WrBtrainTxData_i),
      .wrs_tx_valid_i       (WrBtrainTxValid_i),
      .wrs_tx_dreq_o        (WrBtrainTxDreq_o),
      .wrs_tx_last_i        (WrBtrainTxLast_i),
      .wrs_tx_flush_i       (WrBtrainTxFlush_i),
      .wrs_rx_first_o       (WrBtrainRxFirst_o),
      .wrs_rx_last_o        (WrBtrainRxLast_o),
      .wrs_rx_data_o        (WrBtrainRxData_o),
      .wrs_rx_valid_o       (WrBtrainRxValid_o),
      .wrs_rx_dreq_i        (WrBtrainRxDreq_i),
      .pps_p_o              (WrpcPps_o),
      .tm_time_valid_o      (WrpcTmTimeValid_o),
      .tm_tai_o             (WrpcTmTai_o),
      .tm_cycles_o          (WrpcTmCycles_o),
      .led_link_o           (WrpcLedLink_o),
      .led_act_o            (WrpcLedAct_o)
      );

   // Multiplex onewire access between WR PTP core and
   // existing OneWire master. WR core has priority.
   assign WrOwrIn      = TempIdDq_ioz;
   assign TempIdDqIn   = TempIdDq_ioz;
   assign TempIdDq_ioz = WrOwrOutEn ? 1'b0 : (TempIdDqDriver ? 1'b0 : 1'bZ);

   assign WbAdr_b32 = {10'h00, WbAdr_b22};
   
//****************************
//SYS <=> APP Wishbone interface    
//****************************      

always @(posedge WbClk_ik) WbMasterStb_xd2 <= #1 {WbMasterStb_xd2[0], WbStbAppSlaveBus&&WbCyc};  
always @(posedge WbClk_ik) begin
   if (WbMasterStb_xd2[1]&&~WbMasterStb_o) begin
      WbMasterCyc_o    <= #1 1'b1;
      WbMasterStb_o    <= #1 1'b1;
      WbMasterAdr_ob25 <= #1 {4'b0, WbAdr_b22[20:0]}; //NB: for the moment only 21 bits are available
      WbMasterDat_ob32 <= #1 WbDatMoSi_b32;
      WbMasterWr_o     <= #1 WbWe;
   end else if (~WbMasterStb_xd2[1]) begin
      WbMasterCyc_o    <= #1 1'b0;
      WbMasterStb_o    <= #1 1'b0;   
   end else begin
      WbMasterStb_o    <= #1 WbMasterStb_xd2[1];
   end
end

always @(posedge Clk_k) WbAckAppSlaveBus_xd2 <= #1 {WbAckAppSlaveBus_xd2[0], WbMasterAck_i};    
always @(posedge Clk_k) begin
   WbAckAppSlaveBus <= #1 WbAckAppSlaveBus_xd2[1];
   if (WbAckAppSlaveBus_xd2[1]&&~WbAckAppSlaveBus) WbDatAppSlaveBus_b32 <= #1 WbMasterDat_ib32; 
end  
   
endmodule

-------------------------------------------------------------------------------
-- Title      : Btrain over White Rabbit
-- Project    : Btrain
-------------------------------------------------------------------------------
-- File       : WrBtrainWrapper.vhd
-- Author     : Maciej Lipinski
-- Company    : CERN
-- Created    : 2016-07-01
-- Platform   : FPGA-generics
-- Standard   : VHDL
-------------------------------------------------------------------------------
-- Description:
--
-------------------------------------------------------------------------------
--
-- Copyright (c) 2016 CERN BE-CO-HT
--
-- This source file is free software; you can redistribute it
-- and/or modify it under the terms of the GNU Lesser General
-- Public License as published by the Free Software Foundation;
-- either version 2.1 of the License, or (at your option) any
-- later version.
--
-- This source is distributed in the hope that it will be
-- useful, but WITHOUT ANY WARRANTY; without even the implied
-- warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
-- PURPOSE.  See the GNU Lesser General Public License for more
-- details
--
-- You should have received a copy of the GNU Lesser General
-- Public License along with this source; if not, download it
-- from http://www.gnu.org/licenses/lgpl-2.1.html
--
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author          Description
-- 2016-07-01  1.0     mlipinsk         Created

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.WRBtrain_pkg.all;
use work.wishbone_pkg.all;

library work;

entity WrBtrainWrapper is
  generic (
    g_st_data_width     : integer:=c_BTRAIN_STREAMER_DATA_WIDTH; -- wr streamer data width
    g_wb_addr_width     : integer:=25;                           -- wishbone address width
    g_wb_data_width     : integer:=32                            -- wishbone data width
    );
  port(
    clk_i               : in std_logic;
    rst_n_i             : in std_logic;

    ----------------------------------------------------------------
    -- Interface with wr_transmission
    ----------------------------------------------------------------
    -- tx
    tx_data_o  : out std_logic_vector(g_st_data_width-1 downto 0);
    tx_valid_o : out std_logic;
    tx_dreq_i  : in std_logic;
    tx_last_o  : out std_logic;
    tx_flush_o : out std_logic;
    -- rx
    rx_data_i  : in std_logic_vector(g_st_data_width-1 downto 0);
    rx_valid_i : in std_logic;
    rx_first_i : in std_logic;
    rx_dreq_o  : out std_logic;
    rx_last_i  : in std_logic;
    ----------------------------------------------------------------
    -- Interface with Btrain FMC
    ----------------------------------------------------------------
    -- 32-bit value of magnetic field received from Btrain master via WR
    Bvalue_o           : out std_logic_vector(31 downto 0);
    -- HIGH indicates that the value in the register have been received (basically, it is low
    -- between reset/powerup and first reception)
    BvalueValid_o      : out std_logic;
    -- single period strobe to which indates update of the Bvalue (first cycle of new value)
    BvalueUpdated_p1_o : out std_logic;
    ----------------------------------------------------------------
    -- Wishbone interface
    ----------------------------------------------------------------
    wb_adr_i   : in  std_logic_vector(g_wb_addr_width-1 downto 0)   := (others => '0');
    wb_dat_i   : in  std_logic_vector(g_wb_data_width-1 downto 0)      := (others => '0');
    wb_dat_o   : out std_logic_vector(g_wb_data_width-1 downto 0);
    wb_sel_i   : in  std_logic_vector(g_wb_addr_width/8-1 downto 0) := (others => '0');
    wb_we_i    : in  std_logic                                               := '0';
    wb_cyc_i   : in  std_logic                                               := '0';
    wb_stb_i   : in  std_logic                                               := '0';
    wb_ack_o   : out std_logic;
    wb_err_o   : out std_logic;
    wb_rty_o   : out std_logic;
    wb_stall_o : out std_logic
  );
end WrBtrainWrapper;

architecture Behavioral of WrBtrainWrapper is

  ------------------------------------------------------------------------------
  ---Wishbone access to WRPC's: RAM, peripherals, vuart, etc..
  ------------------------------------------------------------------------------
  signal wb_slave_out : t_wishbone_slave_out;
  signal wb_slave_in  : t_wishbone_slave_in;

  ------------------------------------------------------------------------------
  ---Btrain data records
  ------------------------------------------------------------------------------
  signal rx_BFramePayloads    : t_BFramePayload;
  signal rx_Frame_valid_pX    : std_logic;
  signal tx_FrameHeader       : t_FrameHeader;
  signal tx_BFramePayloads    : t_BFramePayload;

  -- ---------------------------------------------------------------------------
  -- The function below provides conversion from two's complement to Offset
  -- Binary encoding. The encoding is done by adding the offset without overflow.
  --
  -- The encoding of Bvalue needed by the VFC-HD-based WR-Btrain receiver is 
  -- an Offset Binary (access-2^31), the value provided by the btrain is signed
  -- encoded as two's complement.
  -- ---------------------------------------------------------------------------
  -- | description |offest binary(access-32 binary)|  two's complement        |
  -- |             |     (hex)       |    MSB      |    MSB    |    (hex)     |
  -- | +Full Scale | 0x FFFF FFFF    |  1111 1111  | 0111 1111 | 0x 7FFF FFFF |
  -- | +Half Scale | 0x C000 0000    |  1100 0000  |           | 0x 4000 0000 |
  -- |        zero | 0x 8000 0000    |  1000 0000  | 0000 0000 | 0x 0000 0000 |
  -- | -Half Scale | 0x 4000 0000    |  0100 0000  |           | 0x C000 0000 |
  -- | -Full Scale | 0x 0000 0000    |  0000 0000  | 1000 0000 | 0x 8000 0000 |
  -- ---------------------------------------------------------------------------
  function f_encodeOffsetBinary(BvalueIn : std_logic_vector) return std_logic_vector is
    variable ret : std_logic_vector(31 downto 0);
    begin
      ret := not BvalueIn(31) & BvalueIn(30 downto 0);
    return ret;
  end;

begin

  U_WR_BTRAIN_STUFF: WRBTrain 
    generic map(
      g_slave_granularity        => WORD
      )
    port map(
      clk_i                      => clk_i,
      rst_n_i                    => rst_n_i,

      tx_data_o                  => tx_data_o,
      tx_valid_o                 => tx_valid_o,
      tx_dreq_i                  => tx_dreq_i,
      tx_last_p1_o               => tx_last_o,
      tx_flush_p1_o              => tx_flush_o,
      -- rx
      rx_data_i                  => rx_data_i,
      rx_valid_i                 => rx_valid_i,
      rx_first_p1_i              => rx_first_i,
      rx_dreq_o                  => rx_dreq_o,
      rx_last_p1_i               => rx_last_i,

      rx_FrameHeader_o           => open,
      rx_BFramePayloads_o        => rx_BFramePayloads,
      rx_IFramePayloads_o        => open,
      rx_Frame_valid_pX_o        => rx_Frame_valid_pX,

      tx_FrameHeader_i           => tx_FrameHeader,
      tx_BFramePayloads_i        => tx_BFramePayloads,
      tx_IFramePayloads_i        => c_IFramePayload_zero,

      wb_slave_i                 => wb_slave_in,
      wb_slave_o                 => wb_slave_out
  );
  
  -- process to provide output signals for the BI FMC
  process(clk_i)
    begin
      if rising_edge(clk_i) then 
        if (rst_n_i='0') then
          Bvalue_o               <= (others =>'0');
          BvalueValid_o          <= '0';
          BvalueUpdated_p1_o     <= '0';
         else
           Bvalue_o           <= f_encodeOffsetBinary(rx_BFramePayloads.B);
           if(rx_Frame_valid_pX='1') then
              BvalueValid_o      <= '1';
              BvalueUpdated_p1_o <= '1';
           else
              BvalueUpdated_p1_o <= '0';
           end if;
         end if;
      end if;
    end process;
  ------------------------------------------------------------------------------
  --- records to vectors
  ------------------------------------------------------------------------------
  wb_slave_in.adr(g_wb_addr_width-1 downto 0)   <= wb_adr_i;
  wb_slave_in.dat(g_wb_data_width-1 downto 0)   <= wb_dat_i;
  wb_slave_in.sel(g_wb_addr_width/8-1 downto 0) <= wb_sel_i;
  wb_slave_in.we                                <= wb_we_i;
  wb_slave_in.cyc                               <= wb_cyc_i;
  wb_slave_in.stb                               <= wb_stb_i;
  wb_dat_o                                      <= wb_slave_out.dat(g_wb_data_width-1 downto 0);
  wb_ack_o                                      <= wb_slave_out.ack;
  wb_stall_o                                    <= wb_slave_out.stall;
  wb_err_o                                      <= '0';
  wb_rty_o                                      <= '0';

  wb_slave_in.adr(c_wishbone_address_width-1   downto g_wb_addr_width)   <= (others => '0');
  wb_slave_in.sel(c_wishbone_address_width/8-1 downto g_wb_addr_width/8) <= (others => '0');

 

  ------------------------------------------------------------------------------
  --- temporary dummy assigment
  ------------------------------------------------------------------------------
  tx_FrameHeader.bit_14to15   <= (others => '0');
  tx_FrameHeader.d_low_marker <= '0';
  tx_FrameHeader.f_low_marker <= '0';
  tx_FrameHeader.zero_cycle   <= '0';
  tx_FrameHeader.C0           <= '0';
  tx_FrameHeader.error        <= '0';
  tx_FrameHeader.sim_eff      <= '0';
  tx_FrameHeader.frame_type   <= c_ID_BkFrame;

  tx_BFramePayloads.B         <= x"cafebabe";
  tx_BFramePayloads.Bdot      <= x"deadbeef";
  tx_BFramePayloads.oldB      <= x"10123456";
  tx_BFramePayloads.measB     <= x"20123456";
  tx_BFramePayloads.simB      <= x"30123456";
  tx_BFramePayloads.synB      <= x"40123456";

end Behavioral;


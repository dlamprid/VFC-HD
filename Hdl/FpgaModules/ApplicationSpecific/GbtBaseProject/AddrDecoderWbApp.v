`timescale 1ns/100ps

module AddrDecoderWBApp( 
    input   Clk_ik,
    input   [20:0] Adr_ib21,
    input   Stb_i,
    output  reg [31:0] Dat_ob32,
    output  reg Ack_o,

    input   [31:0] DatAppReleaseId_ib32,
    input   AckAppReleaseId_i,
    output  reg StbAppReleaseId_o,
    
    input   [31:0] DatCtrlReg_ib32,
    input   AckCtrlReg_i,
    output  reg StbCtrlReg_o,
    
    input   [31:0] DatStatReg_ib32,
    input   AckStatReg_i,
    output  reg StbStatReg_o,
    
    input   [31:0] DatPllRef_ib32,
    input   AckPllRef_i,
    output  reg StbPllRef_o,
    
    input   [31:0] DatGbtCtrlReg_ib32,
    input   AckGbtCtrlReg_i,
    output  reg StbGbtCtrlReg_o,
    
    input   [31:0] DatGbtStatReg_ib32,
    input   AckGbtStatReg_i,
    output  reg StbGbtStatReg_o
    
 ); 

localparam dly = 1;

reg [2:0] SelectedModule_b3;

localparam  c_SelNothing       = 3'd0,
            c_SelAppRevisionId = 3'd1,
            c_SelCtrlReg       = 3'd2,
            c_SelStatReg       = 3'd3, 
            c_SelPllRef        = 3'd4,
            c_SelGbtCtrlReg    = 3'd5,
            c_SelGbtStatReg    = 3'd6;             

always @* 
    casez(Adr_ib21)
        21'b0_0000_0000_0000_0000_00??: SelectedModule_b3 = c_SelAppRevisionId; // FROM 00_0000 TO 00_0003 (WB) == FROM 00_0000 TO 00_000C (VME) <- 4 regs (16B)
        21'b0_0000_0000_0000_0000_01??: SelectedModule_b3 = c_SelCtrlReg;       // FROM 00_0004 TO 00_0007 (WB) == FROM 00_0010 TO 00_001C (VME) <- 4 regs (16B)    
        21'b0_0000_0000_0000_0000_10??: SelectedModule_b3 = c_SelStatReg;       // FROM 00_0008 TO 00_000B (WB) == FROM 00_0020 TO 00_002C (VME) <- 4 regs (16B)
        21'b0_0000_0000_0000_0000_110?: SelectedModule_b3 = c_SelPllRef;        // FROM 00_000C TO 00_000D (WB) == FROM 00_0030 TO 00_0034 (VME) <- 2 regs ( 8B)
        21'b0_0000_0000_0000_0001_00??: SelectedModule_b3 = c_SelGbtCtrlReg;    // FROM 00_0010 TO 00_0013 (WB) == FROM 00_0040 TO 00_004C (VME) <- 4 regs (16B)    
        21'b0_0000_0000_0000_0001_01??: SelectedModule_b3 = c_SelGbtStatReg;    // FROM 00_0014 TO 00_0017 (WB) == FROM 00_0050 TO 00_005C (VME) <- 4 regs (16B)        
        default:                        SelectedModule_b3 = c_SelNothing;
    endcase 
            
always @(posedge Clk_ik) begin
    Ack_o                     <= #dly  1'b0;
    Dat_ob32                  <= #dly 32'h0;
    StbAppReleaseId_o         <= #dly  1'b0;
    StbCtrlReg_o              <= #dly  1'b0;
    StbStatReg_o              <= #dly  1'b0;
    StbPllRef_o               <= #dly  1'b0;
    StbGbtCtrlReg_o           <= #dly  1'b0;
    StbGbtStatReg_o           <= #dly  1'b0;
    case(SelectedModule_b3) 
        c_SelAppRevisionId: begin
            StbAppReleaseId_o <= #dly  Stb_i;
            Dat_ob32          <= #dly  DatAppReleaseId_ib32;
            Ack_o             <= #dly  AckAppReleaseId_i;
        end
        c_SelCtrlReg: begin
            StbCtrlReg_o      <= #dly  Stb_i;
            Dat_ob32          <= #dly  DatCtrlReg_ib32;
            Ack_o             <= #dly  AckCtrlReg_i;
        end        
        c_SelStatReg: begin
            StbStatReg_o      <= #dly  Stb_i;
            Dat_ob32          <= #dly  DatStatReg_ib32;
            Ack_o             <= #dly  AckStatReg_i;        
        end
        c_SelPllRef: begin
            StbPllRef_o       <= #dly  Stb_i;
            Dat_ob32          <= #dly  DatPllRef_ib32;
            Ack_o             <= #dly  AckPllRef_i;        
        end
        c_SelGbtCtrlReg: begin
            StbGbtCtrlReg_o   <= #dly  Stb_i;
            Dat_ob32          <= #dly  DatGbtCtrlReg_ib32;
            Ack_o             <= #dly  AckGbtCtrlReg_i;
        end        
        c_SelGbtStatReg: begin
            StbGbtStatReg_o   <= #dly  Stb_i;
            Dat_ob32          <= #dly  DatGbtStatReg_ib32;
            Ack_o             <= #dly  AckGbtStatReg_i;        
        end
    endcase
end        

endmodule
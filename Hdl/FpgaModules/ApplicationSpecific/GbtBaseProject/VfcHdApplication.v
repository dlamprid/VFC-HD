`timescale 1ns/100ps 

module VfcHdApplication   
#(parameter g_ApplicationVersion_b8	     = 8'h02,
            g_ApplicationReleaseDay_b8   = 8'h28, 
            g_ApplicationReleaseMonth_b8 = 8'h07, 
            g_ApplicationReleaseYear_b8  = 8'h16)   
(	
//@@@@@@@@@@@@@@@@@@@@@@@@@
//External connections
//@@@@@@@@@@@@@@@@@@@@@@@@@
   input         BstSfpRx_i, // Comment: Differential signal
   output        BstSfpTx_o, // Comment: Differential signal
   input         EthSfpRx_i, // Comment: Differential signal
   output        EthSfpTx_o, // Comment: Differential signal
   // SFP Gbit:
   input  [ 4:1] AppSfpRx_ib4, // Comment: Differential signal
   output [ 4:1] AppSfpTx_ob4, // Comment: Differential signal
   // DDR3 SO-DIMM:
   output [ 2:0] DdrBa_ob3,
   output [ 7:0] DdrDm_ob8,
   inout  [ 7:0] DdrDqs_iob8, // Comment: Differential signal
   inout  [63:0] DdrDq_iob64,
   output [15:0] DdrA_ob16,
   output [ 1:0] DdrCk_okb2,  // Comment: Differential signal
   output [ 1:0] DdrCkE_ohb2,
   output        DdrReset_orn, 
   output        DdrRas_on,
   output        DdrCas_on,
   output        DdrWe_on,
   output [ 1:0] DdrCs_onb2,
   output [ 1:0] DdrOdt_ob2,
   input         DdrTempEvent_in,
   output        DdrI2cScl_ok,
   inout         DdrI2cSda_io,
   // TestIo:
   inout         TestIo1_io,
   inout         TestIo2_io, 
   // FMC connector:
   output        VfmcEnableN_oen,
   inout  [33:0] FmcLaP_iob34,
   inout  [33:0] FmcLaN_iob34,
   inout  [23:0] FmcHaP_iob24,
   inout  [23:0] FmcHaN_iob24,   
   inout  [21:0] FmcHbP_iob22,
   inout  [21:0] FmcHbN_iob22,
   input         FmcPrsntM2C_in, 
   output        FmcTck_ok,
   output        FmcTms_o,
   output        FmcTdi_o,
   input         FmcTdo_i,
   output        FmcTrstL_orn,
   inout         FmcScl_iok,
   inout         FmcSda_io,
   input         FmcPgM2C_in,
   output        FmcPgC2M_on, 
   input         FmcClk0M2CCmos_ik,
   input         FmcClk1M2CCmos_ik,
   inout         FmcClk2Bidir_iok, // Comment: Differential signal
   inout         FmcClk3Bidir_iok, // Comment: Differential signal
   input         FmcClkDir_i,
   output [ 9:0] FmcDpC2M_ob10, // Comment: Differential signal
   input  [ 9:0] FmcDpM2C_ib10,
   input         FmcGbtClk0M2CLeft_ik,  // Comment: Differential signal
   input         FmcGbtClk1M2CLeft_ik,  // Comment: Differential signal
   input         FmcGbtClk0M2CRight_ik, // Comment: Differential signal
   input         FmcGbtClk1M2CRight_ik, // Comment: Differential signal  
   // Clock sources and control:
   output        OeSi57x_oe,
   input         Si57xClk_ik,
   output        ClkFb_ok,
   input         ClkFb_ik,
   input         Clk20VCOx_ik,
   output        PllDac20Sync_o,
   output        PllDac25Sync_o,
   output        PllDacSclk_ok,
   output        PllDacDin_o,  
   output        PllRefScl_ok,
   inout         PllRefSda_io,
   input         PllRefInt_i,
   output        PllSourceMuxOut_ok,
   input         PllRefClkOut_ik,   // Comment: Differential reference for the Gbit lines
   input         GbitTrxClkRefR_ik, // Comment: Differential reference for the Gbit lines ~125MHz
   // SW1:
   input  [ 1:0] Switch_ib2,
   // P2 RTM:
   inout  [19:0] P2DataP_iob20, //Comment: The 0 is a clock capable input
   inout  [19:0] P2DataN_iob20,
   // P0 Timing:
   input  [ 7:0] P0HwHighByte_ib8,
   input  [ 7:0] P0HwLowByte_ib8,
   output        DaisyChain1Cntrl_o,
   output        DaisyChain2Cntrl_o,
   input         VmeP0BunchClk_ik,
   input         VmeP0Tclk_ik,
   // GPIO:
   inout  [ 4:1] GpIo_iob4,
   // Specials:
   input         PushButtonN_in,
//@@@@@@@@@@@@@@@@@@@@@@@@@
//System-Application interface
//@@@@@@@@@@@@@@@@@@@@@@@@@ 
   // Reset:
   input         Reset_irqp,       // Comment: Reset Synchronous with the WbClk_ik
   output        ResetRequest_oqp, // Comment: Request to issue a reset 
   // WishBone:
   output        WbClk_ok,
   input         WbSlaveCyc_i,
   input         WbSlaveStb_i,
   input  [24:0] WbSlaveAdr_ib25,
   input         WbSlaveWr_i,
   input  [31:0] WbSlaveDat_ib32,
   output [31:0] WbSlaveDat_ob32,
   output        WbSlaveAck_o,   
   output        WbMasterCyc_o,
   output        WbMasterStb_o,
   output [24:0] WbMasterAdr_ob25,
   output        WbMasterWr_o,
   output [31:0] WbMasterDat_ob32,
   input  [31:0] WbMasterDat_ib32,
   input         WbMasterAck_i,
   // LED control:
   output [ 1:0] TopLed_ob2,
   output [ 3:0] BottomLed_ob4,
   // BST input:
   input         BstOn_i,
   input         BstClk_ik,
   input         BunchClkFlag_i,
   input         TurnClkFlag_i,
   input  [ 7:0] BstByteAddress_ib8,
   input  [ 7:0] BstByte_ib8,
   output        BstByteStrobe_i,
   output        BstByteError_i,   
   // Interrupt:
   output [23:0] InterruptRequest_opb24,
   // Ethernet streamer:
   output        StreamerClk_ok,
   output [31:0] StreamerData_ob32,
   output        SreamerDav_o,
   output        StreamerPckt_o,
   input         StreamerWait_i,  
   // GPIO direction:
   output        GpIo1DirOut_o,
   output        GpIo2DirOut_o,
   output        GpIo34DirOut_o   
);

//****************************
//Declarations
//****************************

genvar      i;

wire        Clk_k;
//--
wire        WbStbAppReleaseId, WbAckAppReleaseId;
wire [31:0] WbDatAppReleaseId_b32;
wire        WbStbCtrlReg, WbAckCtrlReg;
wire [31:0] WbDatCtrlReg_b32;
wire        WbStbStatReg, WbAckStatReg;
wire [31:0] WbDatStatReg_b32;
wire        WbStbPllRef, WbAckPllRef;
wire [31:0] WbDatPllRef_b32;
wire        WbStbGbtCtrlReg, WbAckGbtCtrlReg;
wire [31:0] WbDatGbtCtrlReg_b32;
wire        WbStbGbtStatReg, WbAckGbtStatReg;
wire [31:0] WbDatGbtStatReg_b32;
//--
wire [31:0] Reg0Value_b32;
//--
wire        GbtTxFrameClk40Mhz_k;
wire        GbtFpgaPllLocked;
//--
wire [31:0] GbtCtrlReg_2x32    [0:1];
wire [31:0] GbtCtrlRegCdc_2x32 [0:1];
wire        GbtBankManualResetTx_1_r;
wire        GbtBankManualResetTx_2_r;
wire        GbtBankManualResetTx_3_r;
wire        GbtBankManualResetTx_4_r;
wire        GbtBankManualResetRx_1_r;
wire        GbtBankManualResetRx_2_r;
wire        GbtBankManualResetRx_3_r;
wire        GbtBankManualResetRx_4_r;
wire        GbtBankTxIsDataSel;
wire [ 1:0] GbtBankTestPatternSel;
wire        GbtBankResetGbtRxReadyLostFlag_1;
wire        GbtBankResetGbtRxReadyLostFlag_2;
wire        GbtBankResetGbtRxReadyLostFlag_3;
wire        GbtBankResetGbtRxReadyLostFlag_4;
wire        GbtBankResetDataErrorSeenFlag_1;
wire        GbtBankResetDataErrorSeenFlag_2;
wire        GbtBankResetDataErrorSeenFlag_3;
wire        GbtBankResetDataErrorSeenFlag_4;
wire        GbtBankLoopBack_1;
wire        GbtBankLoopBack_2;
wire        GbtBankLoopBack_3;
wire        GbtBankLoopBack_4;
wire        GbtBankTxPol_1;
wire        GbtBankTxPol_2;
wire        GbtBankTxPol_3;
wire        GbtBankTxPol_4;
wire        GbtBankRxPol_1;
wire        GbtBankRxPol_2;
wire        GbtBankRxPol_3;
wire        GbtBankRxPol_4;
wire        GbtBankTxWordClkMonEn;
//--
wire [31:0] GbtStatReg_2x32    [0:1];
wire [31:0] GbtStatRegCdc_2x32 [0:1];
wire        GbtBankLinkReady_1;
wire        GbtBankLinkReady_2;
wire        GbtBankLinkReady_3;
wire        GbtBankLinkReady_4;
wire        GbtBankLinkTxReady_1;
wire        GbtBankLinkTxReady_2;
wire        GbtBankLinkTxReady_3;
wire        GbtBankLinkTxReady_4;
wire        GbtBankLinkRxReady_1;
wire        GbtBankLinkRxReady_2;
wire        GbtBankLinkRxReady_3;
wire        GbtBankLinkRxReady_4;
wire        GbtBankGbtRxReady_1;
wire        GbtBankGbtRxReady_2;
wire        GbtBankGbtRxReady_3;
wire        GbtBankGbtRxReady_4;
wire        GbtBankRxIsDataSel_1;
wire        GbtBankRxIsDataSel_2;
wire        GbtBankRxIsDataSel_3;
wire        GbtBankRxIsDataSel_4;
wire        GbtBankGbtRxReadyLostFlag_1;
wire        GbtBankGbtRxReadyLostFlag_2;
wire        GbtBankGbtRxReadyLostFlag_3;
wire        GbtBankGbtRxReadyLostFlag_4;
wire        GbtBankRxDataErrorSeenFlag_1;
wire        GbtBankRxDataErrorSeenFlag_2;
wire        GbtBankRxDataErrorSeenFlag_3;
wire        GbtBankRxDataErrorSeenFlag_4;
wire        GbtBankRxExtraDataWbErrorSeenFlag_1;
wire        GbtBankRxExtraDataWbErrorSeenFlag_2;
wire        GbtBankRxExtraDataWbErrorSeenFlag_3;
wire        GbtBankRxExtraDataWbErrorSeenFlag_4;

//****************************
//Fixed assignments
//****************************

assign OeSi57x_oe = 1'b1;

//****************************
//Clocking
//****************************

assign Clk_k    = GbitTrxClkRefR_ik; // Comment: ~125MHz
assign WbClk_ok = Clk_k;

// PLL Ref (and GBT-FPGA) reference clock generation:
ObufDdr i_ObufDdr (
	.datain_h             (1'b1),
	.datain_l             (1'b0),
	.outclock             (Clk20VCOx_ik),
	.dataout              (PllSourceMuxOut_ok));
    
gbt_tx_frameclk_pll i_GbtFpgaPll (
    .refclk               (PllRefClkOut_ik), // Comment: 120MHz
    .rst                  (Reset_irqp),     
    .outclk_0             (GbtTxFrameClk40Mhz_k),
    .locked               (GbtFpgaPllLocked));        

//****************************
//WB address decoder
//****************************

AddrDecoderWBApp i_AddrDecoderWbApp( 
    .Clk_ik               (Clk_k),
    .Adr_ib21             (WbSlaveAdr_ib25[20:0]),
    .Stb_i                (WbSlaveStb_i),
    .Dat_ob32             (WbSlaveDat_ob32),
    .Ack_o                (WbSlaveAck_o),
    //--
    .DatAppReleaseId_ib32 (WbDatAppReleaseId_b32),
    .AckAppReleaseId_i    (WbAckAppReleaseId),
    .StbAppReleaseId_o    (WbStbAppReleaseId),
    //--
    .DatCtrlReg_ib32      (WbDatCtrlReg_b32),
    .AckCtrlReg_i         (WbAckCtrlReg),
    .StbCtrlReg_o         (WbStbCtrlReg),
    //--
    .DatStatReg_ib32      (WbDatStatReg_b32),
    .AckStatReg_i         (WbAckStatReg),
    .StbStatReg_o         (WbStbStatReg),
    //--
    .DatPllRef_ib32       (WbDatPllRef_b32),
    .AckPllRef_i          (WbAckPllRef),
    .StbPllRef_o          (WbStbPllRef),
    //--
    .DatGbtCtrlReg_ib32   (WbDatGbtCtrlReg_b32),
    .AckGbtCtrlReg_i      (WbAckGbtCtrlReg),
    .StbGbtCtrlReg_o      (WbStbGbtCtrlReg),
    //--
    .DatGbtStatReg_ib32   (WbDatGbtStatReg_b32),
    .AckGbtStatReg_i      (WbAckGbtStatReg),
    .StbGbtStatReg_o      (WbStbGbtStatReg));    

//****************************
//Release ID
//****************************

Generic4InputRegs i_AppReleaseId(
    .Rst_irq        (Reset_irqp),
    .Clk_ik         (Clk_k),
    .Cyc_i          (WbSlaveCyc_i),
    .Stb_i          (WbStbAppReleaseId),
    .Adr_ib2        (WbSlaveAdr_ib25[1:0]),
    .Dat_oab32      (WbDatAppReleaseId_b32),
    .Ack_oa         (WbAckAppReleaseId),
    .Reg0Value_ib32 ("VFC-"),
    .Reg1Value_ib32 ("HD G"),
    .Reg2Value_ib32 ("BT  "),
    .Reg3Value_ib32 ({g_ApplicationVersion_b8, g_ApplicationReleaseDay_b8, g_ApplicationReleaseMonth_b8, g_ApplicationReleaseYear_b8}));

//****************************
//Example registers
//****************************

// Control register bank:
Generic4OutputRegs  #(  
    .Reg0Default     (32'hBABEB00B), 
    .Reg0AutoClrMask (32'hFFFFFFFF), 
    .Reg1Default     (32'h00000000),
    .Reg1AutoClrMask (32'hFFFFFFFF), 
    .Reg2Default     (32'h00000000),
    .Reg2AutoClrMask (32'hFFFFFFFF), 
    .Reg3Default     (32'h00000000),
    .Reg3AutoClrMask (32'hFFFFFFFF))
i_ControlRegs (
    .Clk_ik          (Clk_k),
    .Rst_irq         (Reset_irqp),
    .Cyc_i           (WbSlaveCyc_i),
    .Stb_i           (WbStbCtrlReg),
    .We_i            (WbSlaveWr_i),
    .Adr_ib2         (WbSlaveAdr_ib25[1:0]),
    .Dat_ib32        (WbSlaveDat_ib32),
    .Dat_oab32       (WbDatCtrlReg_b32),
    .Ack_oa          (WbAckCtrlReg),
    //--
    .Reg0Value_ob32  (Reg0Value_b32),
    .Reg1Value_ob32  (),
    .Reg2Value_ob32  (),
    .Reg3Value_ob32  ());

// Status registers bank:
Generic4InputRegs i_StatusRegs  (
    .Clk_ik         (Clk_k),
    .Rst_irq        (Reset_irqp),
    .Cyc_i          (WbSlaveCyc_i),
    .Stb_i          (WbStbStatReg),
    .Adr_ib2        (WbSlaveAdr_ib25[1:0]),
    .Dat_oab32      (WbDatStatReg_b32),
    .Ack_oa         (WbAckStatReg),
    //--          
    .Reg0Value_ib32 (Reg0Value_b32),
    .Reg1Value_ib32 (32'hCAFEAC1D),
    .Reg2Value_ib32 (32'hACDCDEAD),
    .Reg3Value_ib32 (32'hFEEDBEEF)); 
    
//****************************
//PLL Ref (Si5338) I2C control
//****************************

// I2C control:
I2cMasterWb #(
    .g_CycleLenght (10'd256)) 
i_I2cPllRef	(   
	 .Clk_ik       (Clk_k),
	 .Rst_irq      (Reset_irqp),
	 .Cyc_i        (WbSlaveCyc_i),
	 .Stb_i        (WbStbPllRef),
	 .We_i         (WbSlaveWr_i),
	 .Adr_ib2      (WbSlaveAdr_ib25[1:0]),
	 .Dat_ib32     (WbSlaveDat_ib32),
	 .Dat_oab32    (WbDatPllRef_b32),
	 .Ack_oa       (WbAckPllRef),
     .Scl_ioz      (PllRefScl_ok),  
	 .Sda_ioz      (PllRefSda_io));

// Clocks forwarding:
assign TestIo1_io = PllRefClkOut_ik;
assign TestIo2_io = GbtTxFrameClk40Mhz_k;
        
//****************************
// GBT-FPGA example design
//****************************    
         
// GBT-FPGA Example Deging (Features 4 GBT Link):   
alt_av_gbt_example_design_wrap_verilog #(
    .g_GbtBankId                           (1), 
    .g_TxOptimization                      (0), // Comment: Standard
    .g_RxOptimization                      (0), // Comment: Standard
    .g_TxEncoding                          (0), // Comment: GBT Frame
    .g_RxEncoding                          (0), // Comment: GBT Frame
    // Extended configuration:
    .g_DataGeneratorEnable                 (1),
    .g_DataCheckerEnable                   (1),
    .g_MatchFlagEnable                     (1))
i_GbtExampleDesign_4xGbtLinks (
    // Clocks:
    .TxFrameClk40Mhz_ik                    (GbtTxFrameClk40Mhz_k),
    .MgtRefClk120Mhz_ik                    (PllRefClkOut_ik),
    .TxFrameClk_1_ok                       (), 
    .TxFrameClk_2_ok                       (),
    .TxFrameClk_3_ok                       (),
    .TxFrameClk_4_ok                       (),
    .RxFrameClk_1_ok                       (),
    .RxFrameClk_2_ok                       (),
    .RxFrameClk_3_ok                       (),
    .RxFrameClk_4_ok                       (),
    .TxWordClk_1_ok                        (),
    .TxWordClk_2_ok                        (),
    .TxWordClk_3_ok                        (),
    .TxWordClk_4_ok                        (),
    .RxWordClk_1_ok                        (),
    .RxWordClk_2_ok                        (),
    .RxWordClk_3_ok                        (),
    .RxWordClk_4_ok                        (),
    // Reset:
    .GbtBankGeneralReset_ir                (Reset_irqp),
    .GbtBankManualResetTx_1_ir             (GbtBankManualResetTx_1_r),
    .GbtBankManualResetTx_2_ir             (GbtBankManualResetTx_2_r),
    .GbtBankManualResetTx_3_ir             (GbtBankManualResetTx_3_r),
    .GbtBankManualResetTx_4_ir             (GbtBankManualResetTx_4_r),
    .GbtBankManualResetRx_1_ir             (GbtBankManualResetRx_1_r),
    .GbtBankManualResetRx_2_ir             (GbtBankManualResetRx_2_r),
    .GbtBankManualResetRx_3_ir             (GbtBankManualResetRx_3_r),
    .GbtBankManualResetRx_4_ir             (GbtBankManualResetRx_4_r),
    // Serial lanes:
    .GbtBankMgtRx_1_i                      (AppSfpRx_ib4[1]),                    
    .GbtBankMgtRx_2_i                      (AppSfpRx_ib4[2]),                    
    .GbtBankMgtRx_3_i                      (AppSfpRx_ib4[3]),                    
    .GbtBankMgtRx_4_i                      (AppSfpRx_ib4[4]),
    .GbtBankMgtTx_1_o                      (AppSfpTx_ob4[1]),
    .GbtBankMgtTx_2_o                      (AppSfpTx_ob4[2]),
    .GbtBankMgtTx_3_o                      (AppSfpTx_ob4[3]),
    .GbtBankMgtTx_4_o                      (AppSfpTx_ob4[4]),
    // Data:
    .GbtBankGbtTxData_1_ib84               ( 84'h0),     
    .GbtBankGbtTxData_2_ib84               ( 84'h0),     
    .GbtBankGbtTxData_3_ib84               ( 84'h0),     
    .GbtBankGbtTxData_4_ib84               ( 84'h0),     
    .GbtBankWbTxData_1_ib116               (116'h0),
    .GbtBankWbTxData_2_ib116               (116'h0),
    .GbtBankWbTxData_3_ib116               (116'h0),
    .GbtBankWbTxData_4_ib116               (116'h0),
    .GbtBankGbtRxData_1_ob84               (),
    .GbtBankGbtRxData_2_ob84               (),
    .GbtBankGbtRxData_3_ob84               (),
    .GbtBankGbtRxData_4_ob84               (),
    .GbtBankWbRxData_1_ob32                (),
    .GbtBankWbRxData_2_ob32                (),
    .GbtBankWbRxData_3_ob32                (),
    .GbtBankWbRxData_4_ob32                (),
    // Reconfiguration:
    .GbtBankReconfAvmmRst_ir               (Reset_irqp),
    .GbtBankReconfAvmmClk_ik               (PllRefClkOut_ik),
    .GbtBankReconfAvmmAddr_ib7             ( 7'h0),
    .GbtBankReconfAvmmRead_i               ( 1'b0),
    .GbtBankReconfAvmmWrite_i              ( 1'b0),
    .GbtBankReconfAvmmWriteData_ib32       (32'h0),
    .GbtBankReconfAvmmReadData_ob32        (),
    .GbtBankReconfAvmmWaitRequest_o        (),
    // Control:
    .GbtBankTxIsDataSel_1_i                (GbtBankTxIsDataSel),         
    .GbtBankTxIsDataSel_2_i                (GbtBankTxIsDataSel),         
    .GbtBankTxIsDataSel_3_i                (GbtBankTxIsDataSel),         
    .GbtBankTxIsDataSel_4_i                (GbtBankTxIsDataSel),  
    .GbtBankTestPatternSel_ib2             (GbtBankTestPatternSel),
    .GbtBankResetGbtRxReadyLostFlag_1_i    (GbtBankResetGbtRxReadyLostFlag_1),        
    .GbtBankResetGbtRxReadyLostFlag_2_i    (GbtBankResetGbtRxReadyLostFlag_2),        
    .GbtBankResetGbtRxReadyLostFlag_3_i    (GbtBankResetGbtRxReadyLostFlag_3),        
    .GbtBankResetGbtRxReadyLostFlag_4_i    (GbtBankResetGbtRxReadyLostFlag_4),
    .GbtBankResetDataErrorSeenFlag_1_i     (GbtBankResetDataErrorSeenFlag_1),
    .GbtBankResetDataErrorSeenFlag_2_i     (GbtBankResetDataErrorSeenFlag_2),
    .GbtBankResetDataErrorSeenFlag_3_i     (GbtBankResetDataErrorSeenFlag_3),
    .GbtBankResetDataErrorSeenFlag_4_i     (GbtBankResetDataErrorSeenFlag_4),   
    // Status:
    .GbtBankLinkReady_1_o                  (GbtBankLinkReady_1),
    .GbtBankLinkReady_2_o                  (GbtBankLinkReady_2),
    .GbtBankLinkReady_3_o                  (GbtBankLinkReady_3),
    .GbtBankLinkReady_4_o                  (GbtBankLinkReady_4),
    .GbtBankLinkTxReady_1_o                (GbtBankLinkTxReady_1),
    .GbtBankLinkTxReady_2_o                (GbtBankLinkTxReady_2),
    .GbtBankLinkTxReady_3_o                (GbtBankLinkTxReady_3),
    .GbtBankLinkTxReady_4_o                (GbtBankLinkTxReady_4),
    .GbtBankLinkRxReady_1_o                (GbtBankLinkRxReady_1),
    .GbtBankLinkRxReady_2_o                (GbtBankLinkRxReady_2),
    .GbtBankLinkRxReady_3_o                (GbtBankLinkRxReady_3),
    .GbtBankLinkRxReady_4_o                (GbtBankLinkRxReady_4),
    .GbtBankGbtRxReady_1_o                 (GbtBankGbtRxReady_1),
    .GbtBankGbtRxReady_2_o                 (GbtBankGbtRxReady_2),
    .GbtBankGbtRxReady_3_o                 (GbtBankGbtRxReady_3),
    .GbtBankGbtRxReady_4_o                 (GbtBankGbtRxReady_4),
    .GbtBankRxIsDataSel_1_o                (GbtBankRxIsDataSel_1),
    .GbtBankRxIsDataSel_2_o                (GbtBankRxIsDataSel_2),
    .GbtBankRxIsDataSel_3_o                (GbtBankRxIsDataSel_3),
    .GbtBankRxIsDataSel_4_o                (GbtBankRxIsDataSel_4),
    .GbtBankGbtRxReadyLostFlag_1_o         (GbtBankGbtRxReadyLostFlag_1),
    .GbtBankGbtRxReadyLostFlag_2_o         (GbtBankGbtRxReadyLostFlag_2),
    .GbtBankGbtRxReadyLostFlag_3_o         (GbtBankGbtRxReadyLostFlag_3),
    .GbtBankGbtRxReadyLostFlag_4_o         (GbtBankGbtRxReadyLostFlag_4),
    .GbtBankRxDataErrorSeenFlag_1_o        (GbtBankRxDataErrorSeenFlag_1),
    .GbtBankRxDataErrorSeenFlag_2_o        (GbtBankRxDataErrorSeenFlag_2),
    .GbtBankRxDataErrorSeenFlag_3_o        (GbtBankRxDataErrorSeenFlag_3),
    .GbtBankRxDataErrorSeenFlag_4_o        (GbtBankRxDataErrorSeenFlag_4),
    .GbtBankRxExtraDataWbErrorSeenFlag_1_o (GbtBankRxExtraDataWbErrorSeenFlag_1),
    .GbtBankRxExtraDataWbErrorSeenFlag_2_o (GbtBankRxExtraDataWbErrorSeenFlag_2),
    .GbtBankRxExtraDataWbErrorSeenFlag_3_o (GbtBankRxExtraDataWbErrorSeenFlag_3),
    .GbtBankRxExtraDataWbErrorSeenFlag_4_o (GbtBankRxExtraDataWbErrorSeenFlag_4),
    .GbtBankTxMatchFlag_o                  (FmcLaP_iob34[ 0]),
    .GbtBankRxMatchFlag_1_o                (FmcLaP_iob34[ 7]),
    .GbtBankRxMatchFlag_2_o                (FmcLaP_iob34[ 8]),
    .GbtBankRxMatchFlag_3_o                (FmcLaP_iob34[ 9]),
    .GbtBankRxMatchFlag_4_o                (FmcLaP_iob34[10]),   
    // XCVR ctrl:
    .GbtBankLoopBack_1_i                   (GbtBankLoopBack_1),                
    .GbtBankLoopBack_2_i                   (GbtBankLoopBack_2),                
    .GbtBankLoopBack_3_i                   (GbtBankLoopBack_3),                
    .GbtBankLoopBack_4_i                   (GbtBankLoopBack_4),
    .GbtBankTxPol_1_i                      (GbtBankTxPol_1),
    .GbtBankTxPol_2_i                      (GbtBankTxPol_2),
    .GbtBankTxPol_3_i                      (GbtBankTxPol_3),
    .GbtBankTxPol_4_i                      (GbtBankTxPol_4),
    .GbtBankRxPol_1_i                      (GbtBankRxPol_1),
    .GbtBankRxPol_2_i                      (GbtBankRxPol_2),
    .GbtBankRxPol_3_i                      (GbtBankRxPol_3),
    .GbtBankRxPol_4_i                      (GbtBankRxPol_4),
    .GbtBankTxWordClkMonEn_i               (GbtBankTxWordClkMonEn));

// Control registers bank, Clock Domain Crossing & wires mapping:
Generic4OutputRegs  #(  
    .Reg0Default     (32'h00000000), 
    .Reg0AutoClrMask (32'hF807FFFF), 
    .Reg1Default     (32'h00000000),
    .Reg1AutoClrMask (32'hFFFFFFFF), 
    .Reg2Default     (32'h00000000),
    .Reg2AutoClrMask (32'hFFFFFFFF), 
    .Reg3Default     (32'h00000000),
    .Reg3AutoClrMask (32'hFFFFFFFF))
i_GbtControlRegs (
    .Clk_ik          (Clk_k),
    .Rst_irq         (Reset_irqp),
    .Cyc_i           (WbSlaveCyc_i),
    .Stb_i           (WbStbGbtCtrlReg),
    .We_i            (WbSlaveWr_i),
    .Adr_ib2         (WbSlaveAdr_ib25[1:0]),
    .Dat_ib32        (WbSlaveDat_ib32),
    .Dat_oab32       (WbDatGbtCtrlReg_b32),
    .Ack_oa          (WbAckGbtCtrlReg),
    //--
    .Reg0Value_ob32  (GbtCtrlReg_2x32[0]),
    .Reg1Value_ob32  (GbtCtrlReg_2x32[1]),
    .Reg2Value_ob32  (),
    .Reg3Value_ob32  ());

generate for (i=0;i<2;i=i+1) begin: GenGbtCtrlRegCdc
    generic_dpram #(
        .aw     ( 1), 
        .dw     (32))
    i_GbtCtrlRegCdc (
        .rclk  (GbtTxFrameClk40Mhz_k), 
        .rrst  (Reset_irqp),
        .rce   (1'b1), 
        .oe    (1'b1),
        .raddr (1'b0), 
        .do    (GbtCtrlRegCdc_2x32[i]),
        .wclk  (Clk_k), 
        .wrst  (Reset_irqp),
        .wce   (1'b1),
        .we    (1'b1),
        .waddr (1'b0),
        .di    (GbtCtrlReg_2x32[i]));    
end endgenerate    
    
assign GbtBankManualResetTx_1_r         = GbtCtrlRegCdc_2x32[0][    0];   
assign GbtBankManualResetTx_2_r         = GbtCtrlRegCdc_2x32[0][    1];   
assign GbtBankManualResetTx_3_r         = GbtCtrlRegCdc_2x32[0][    2];   
assign GbtBankManualResetTx_4_r         = GbtCtrlRegCdc_2x32[0][    3];   
assign GbtBankManualResetRx_1_r         = GbtCtrlRegCdc_2x32[0][    4];   
assign GbtBankManualResetRx_2_r         = GbtCtrlRegCdc_2x32[0][    5];   
assign GbtBankManualResetRx_3_r         = GbtCtrlRegCdc_2x32[0][    6];   
assign GbtBankManualResetRx_4_r         = GbtCtrlRegCdc_2x32[0][    7];   
//--
assign GbtBankTxIsDataSel               = GbtCtrlRegCdc_2x32[0][   16];  
//--    
assign GbtBankTestPatternSel            = GbtCtrlRegCdc_2x32[0][18:17]; 
//--    
assign GbtBankResetGbtRxReadyLostFlag_1 = GbtCtrlRegCdc_2x32[0][   19]; // Comment: Auto-clear  
assign GbtBankResetGbtRxReadyLostFlag_2 = GbtCtrlRegCdc_2x32[0][   20]; // Comment: Auto-clear  
assign GbtBankResetGbtRxReadyLostFlag_3 = GbtCtrlRegCdc_2x32[0][   21]; // Comment: Auto-clear  
assign GbtBankResetGbtRxReadyLostFlag_4 = GbtCtrlRegCdc_2x32[0][   22]; // Comment: Auto-clear  
//--    
assign GbtBankResetDataErrorSeenFlag_1  = GbtCtrlRegCdc_2x32[0][   23]; // Comment: Auto-clear    
assign GbtBankResetDataErrorSeenFlag_2  = GbtCtrlRegCdc_2x32[0][   24]; // Comment: Auto-clear    
assign GbtBankResetDataErrorSeenFlag_3  = GbtCtrlRegCdc_2x32[0][   25]; // Comment: Auto-clear    
assign GbtBankResetDataErrorSeenFlag_4  = GbtCtrlRegCdc_2x32[0][   26]; // Comment: Auto-clear  

assign GbtBankLoopBack_1                = GbtCtrlRegCdc_2x32[1][    0];
assign GbtBankLoopBack_2                = GbtCtrlRegCdc_2x32[1][    1];
assign GbtBankLoopBack_3                = GbtCtrlRegCdc_2x32[1][    2];
assign GbtBankLoopBack_4                = GbtCtrlRegCdc_2x32[1][    3];
//--
assign GbtBankTxPol_1                   = GbtCtrlRegCdc_2x32[1][    8];
assign GbtBankTxPol_2                   = GbtCtrlRegCdc_2x32[1][    9];
assign GbtBankTxPol_3                   = GbtCtrlRegCdc_2x32[1][   10];
assign GbtBankTxPol_4                   = GbtCtrlRegCdc_2x32[1][   11];
assign GbtBankRxPol_1                   = GbtCtrlRegCdc_2x32[1][   12];
assign GbtBankRxPol_2                   = GbtCtrlRegCdc_2x32[1][   13];
assign GbtBankRxPol_3                   = GbtCtrlRegCdc_2x32[1][   14];
assign GbtBankRxPol_4                   = GbtCtrlRegCdc_2x32[1][   15];
//--
assign GbtBankTxWordClkMonEn            = GbtCtrlRegCdc_2x32[1][   24];
    
// Status registers bank, Clock Domain Crossing & wires mapping:
Generic4InputRegs i_GbtStatusRegs  (
    .Clk_ik         (Clk_k),
    .Rst_irq        (Reset_irqp),
    .Cyc_i          (WbSlaveCyc_i),
    .Stb_i          (WbStbGbtStatReg),
    .Adr_ib2        (WbSlaveAdr_ib25[1:0]),
    .Dat_oab32      (WbDatGbtStatReg_b32),
    .Ack_oa         (WbAckGbtStatReg),
    //--          
    .Reg0Value_ib32 (GbtStatRegCdc_2x32[0]),
    .Reg1Value_ib32 (GbtStatRegCdc_2x32[1]),
    .Reg2Value_ib32 (32'h01234567),
    .Reg3Value_ib32 (32'h89ABCDEF));
    
generate for (i=0;i<2;i=i+1) begin: GenGbtStatRegCdc
    generic_dpram #(
        .aw     ( 1), 
        .dw     (32))
    i_GbtStatRegCdc (
        .rclk  (Clk_k), 
        .rrst  (Reset_irqp),
        .rce   (1'b1), 
        .oe    (1'b1),
        .raddr (1'b0), 
        .do    (GbtStatRegCdc_2x32[i]),
        .wclk  (GbtTxFrameClk40Mhz_k), 
        .wrst  (Reset_irqp),
        .wce   (1'b1),
        .we    (1'b1),
        .waddr (1'b0),
        .di    (GbtStatReg_2x32[i]));    
end endgenerate
    
assign GbtStatReg_2x32[0][ 0] = GbtBankLinkReady_1;
assign GbtStatReg_2x32[0][ 1] = GbtBankLinkReady_2;
assign GbtStatReg_2x32[0][ 2] = GbtBankLinkReady_3;
assign GbtStatReg_2x32[0][ 3] = GbtBankLinkReady_4;

assign GbtStatReg_2x32[0][ 8] = GbtBankLinkTxReady_1;
assign GbtStatReg_2x32[0][ 9] = GbtBankLinkTxReady_2;
assign GbtStatReg_2x32[0][10] = GbtBankLinkTxReady_3;
assign GbtStatReg_2x32[0][11] = GbtBankLinkTxReady_4;
assign GbtStatReg_2x32[0][12] = GbtBankLinkRxReady_1;
assign GbtStatReg_2x32[0][13] = GbtBankLinkRxReady_2;
assign GbtStatReg_2x32[0][14] = GbtBankLinkRxReady_3;
assign GbtStatReg_2x32[0][15] = GbtBankLinkRxReady_4;

assign GbtStatReg_2x32[0][16] = GbtBankGbtRxReady_1;
assign GbtStatReg_2x32[0][17] = GbtBankGbtRxReady_2;
assign GbtStatReg_2x32[0][18] = GbtBankGbtRxReady_3;
assign GbtStatReg_2x32[0][19] = GbtBankGbtRxReady_4;

assign GbtStatReg_2x32[1][ 0] = GbtBankRxIsDataSel_1;
assign GbtStatReg_2x32[1][ 1] = GbtBankRxIsDataSel_2;
assign GbtStatReg_2x32[1][ 2] = GbtBankRxIsDataSel_3;
assign GbtStatReg_2x32[1][ 3] = GbtBankRxIsDataSel_4;

assign GbtStatReg_2x32[1][ 8] = GbtBankGbtRxReadyLostFlag_1;
assign GbtStatReg_2x32[1][ 9] = GbtBankGbtRxReadyLostFlag_2;
assign GbtStatReg_2x32[1][10] = GbtBankGbtRxReadyLostFlag_3;
assign GbtStatReg_2x32[1][11] = GbtBankGbtRxReadyLostFlag_4;

assign GbtStatReg_2x32[1][12] = GbtBankRxDataErrorSeenFlag_1;
assign GbtStatReg_2x32[1][13] = GbtBankRxDataErrorSeenFlag_2;
assign GbtStatReg_2x32[1][14] = GbtBankRxDataErrorSeenFlag_3;
assign GbtStatReg_2x32[1][15] = GbtBankRxDataErrorSeenFlag_4;

assign GbtStatReg_2x32[1][16] = GbtBankRxExtraDataWbErrorSeenFlag_1;
assign GbtStatReg_2x32[1][17] = GbtBankRxExtraDataWbErrorSeenFlag_2;
assign GbtStatReg_2x32[1][18] = GbtBankRxExtraDataWbErrorSeenFlag_3;
assign GbtStatReg_2x32[1][19] = GbtBankRxExtraDataWbErrorSeenFlag_4;
   
endmodule
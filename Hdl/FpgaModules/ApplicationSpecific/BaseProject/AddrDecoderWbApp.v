`timescale 1ns/100ps

module AddrDecoderWBApp( 
    input   Clk_ik,
    input   [20:0] Adr_ib21,
    input   Stb_i,
    output  reg [31:0] Dat_ob32,
    output  reg Ack_o,

    input   [31:0] DatAppReleaseId_ib32,
    input   AckAppReleaseId_i,
    output  reg StbAppReleaseId_o,
    
    input   [31:0] DatWrBtrain_ib32,
    input   AckWrBtrain_i,
    output  reg StbWrBtrain_o
    
 ); 

localparam dly = 1;

reg [1:0] SelectedModule_b2;

localparam  c_SelNothing       = 2'd0,
            c_SelAppRevisionId = 2'd1,
            c_SelWRBtrain      = 2'd2;

always @* 
    casez(Adr_ib21)
        21'b0_0000_0000_0000_0000_00??: SelectedModule_b2 = c_SelAppRevisionId; // FROM 00_0000 TO 00_0003 (WB) == FROM 00_0000 TO 00_000C (VME) <- 4 regs (16B)
        21'b0_0000_0000_0000_0001_????: SelectedModule_b2 = c_SelWRBtrain;      // FROM 00_0010 TO 00_001F (WB) == FROM 00_0040 TO 00_007C (VME) <-16 regs (64B)
        default:                        SelectedModule_b2 = c_SelNothing;
    endcase 
            
always @* begin
    Ack_o                     <= #dly  1'b0;
    Dat_ob32                  <= #dly 32'h0;
    StbAppReleaseId_o         <= #dly  1'b0;
    StbWrBtrain_o             <= #dly  1'b0;
    case(SelectedModule_b2) 
        c_SelAppRevisionId: begin
            StbAppReleaseId_o <= #dly  Stb_i;
            Dat_ob32          <= #dly  DatAppReleaseId_ib32;
            Ack_o             <= #dly  AckAppReleaseId_i;
        end
        c_SelWRBtrain: begin
            StbWrBtrain_o     <= #dly  Stb_i;
            Dat_ob32          <= #dly  DatWrBtrain_ib32;
            Ack_o             <= #dly  AckWrBtrain_i;
        end
        default: ;
    endcase
end        

endmodule

`timescale 1ns/100ps 

module VfcHdApplication   
#(parameter g_ApplicationVersion_b8	     = 8'hC5,
            g_ApplicationReleaseDay_b8 	 = 8'h17, 
            g_ApplicationReleaseMonth_b8 = 8'h07, 
            g_ApplicationReleaseYear_b8  = 8'h16)   
(	
   // FMC connector:
   output        VfmcEnableN_oen,
   output  [33:0] FmcLaP_ob34,
   output  [33:0] FmcLaN_ob34,
   // GPIO:
   output [3:0]  GpIo_ob4,
   // Clock sources and control:
   input         GbitTrxClkRefR_ik, // Comment: Differential reference for the Gbit lines ~125MHz
   // Reset:
   input         Reset_irqp,       // Comment: Reset Synchronous with the WbClk_ik
   output        ResetRequest_oqp, // Comment: Request to issue a reset 
   // WishBone:
   output        WbClk_ok,
   input         WbSlaveCyc_i,
   input         WbSlaveStb_i,
   input  [24:0] WbSlaveAdr_ib25,
   input         WbSlaveWr_i,
   input  [31:0] WbSlaveDat_ib32,
   output [31:0] WbSlaveDat_ob32,
   output        WbSlaveAck_o,   
   // Interrupt:
   output [23:0] InterruptRequest_opb24,
   // WR Btrain interface:
   output [207:0] WrBtrainTxData_o,
   output         WrBtrainTxValid_o,
   input          WrBtrainTxDreq_i,
   output         WrBtrainTxLast_o,
   output         WrBtrainTxFlush_o,
   input          WrBtrainRxFirst_i,
   input          WrBtrainRxLast_i,
   input [207:0]  WrBtrainRxData_i,
   input          WrBtrainRxValid_i,
   output         WrBtrainRxDreq_o,
   // WRPC timecode input:
   input          WrpcPps_i,
   input          WrpcTmTimeValid_i,
   input [39:0]   WrpcTmTai_i,
   input [27:0]   WrpcTmCycles_i,
   input          WrpcLedLink_i,
   input          WrpcLedAct_i,
   // WRPC system clock:
   input          WrpcSysClk_ik   
);

//****************************
//Declarations
//****************************

wire        Clk_k;

wire        WbStbAppReleaseId, WbAckAppReleaseId;
wire [31:0] WbDatAppReleaseId_b32;
wire        WbStbWrBtrain, WbAckWrBtrain;
wire [31:0] WbDatWrBtrain_b32;

wire [31:0] WrBtrainBvalue;
wire        WrBtrainBvalueValid;
wire        WrBtrainBvalueUpdated;
   
//****************************
//Fixed assignments
//****************************

assign InterruptRequest_opb24 = 24'b0;

assign ResetRequest_oqp = 1'b0;

//****************************
//Clocking
//****************************

//assign Clk_k = GbitTrxClkRefR_ik; //~125MHz
assign Clk_k = WrpcSysClk_ik;
assign WbClk_ok = Clk_k;

//****************************
//WB address decoder
//****************************

AddrDecoderWBApp i_AddrDecoderWbApp( 
    .Clk_ik(Clk_k),
    .Adr_ib21(WbSlaveAdr_ib25[20:0]),
    .Stb_i(WbSlaveStb_i),
    .Dat_ob32(WbSlaveDat_ob32),
    .Ack_o(WbSlaveAck_o),
    
    .DatAppReleaseId_ib32(WbDatAppReleaseId_b32),
    .AckAppReleaseId_i(WbAckAppReleaseId),
    .StbAppReleaseId_o(WbStbAppReleaseId),

    .DatWrBtrain_ib32(WbDatWrBtrain_b32),
    .AckWrBtrain_i(WbAckWrBtrain),
    .StbWrBtrain_o(WbStbWrBtrain));

//****************************
//Release ID
//****************************

Generic4InputRegs i_AppReleaseId(
    .Rst_irq        (Reset_irqp),
    .Clk_ik         (Clk_k),
    .Cyc_i          (WbSlaveCyc_i),
    .Stb_i          (WbStbAppReleaseId),
    .Adr_ib2        (WbSlaveAdr_ib25[1:0]),
    .Dat_oab32      (WbDatAppReleaseId_b32),
    .Ack_oa         (WbAckAppReleaseId),
    .Reg0Value_ib32 ("VFC-"),
    .Reg1Value_ib32 ("HD b"),
    .Reg2Value_ib32 ("ase "),
    .Reg3Value_ib32 ({g_ApplicationVersion_b8, g_ApplicationReleaseDay_b8, g_ApplicationReleaseMonth_b8, g_ApplicationReleaseYear_b8}));

//****************************
////WR-Btrain
//****************************

WrBtrainWrapper #(
    .g_st_data_width (208),
    .g_wb_addr_width (25),
    .g_wb_data_width (32))
i_WrBtrainWrapper (
    .clk_i           (Clk_k),
    .rst_n_i         (~Reset_irqp),

    .tx_data_o       (WrBtrainTxData_o),
    .tx_valid_o      (WrBtrainTxValid_o),
    .tx_dreq_i       (WrBtrainTxDreq_i),
    .tx_last_o       (WrBtrainTxLast_o),
    .tx_flush_o      (WrBtrainTxFlush_o),

    .rx_data_i       (WrBtrainRxData_i),
    .rx_valid_i      (WrBtrainRxValid_i),
    .rx_first_i      (WrBtrainRxFirst_i),
    .rx_dreq_o       (WrBtrainRxDreq_o),
    .rx_last_i       (WrBtrainRxLast_i),

    .Bvalue_o           (WrBtrainBvalue),
    .BvalueValid_o      (WrBtrainBvalueValid),
    .BvalueUpdated_p1_o (WrBtrainBvalueUpdated),

    .wb_adr_i        (WbSlaveAdr_ib25),
    .wb_dat_i        (WbSlaveDat_ib32),
    .wb_dat_o        (WbDatWrBtrain_b32),
    .wb_sel_i        (3'b111),
    .wb_we_i         (WbSlaveWr_i),
    .wb_cyc_i        (WbSlaveCyc_i),
    .wb_stb_i        (WbStbWrBtrain),
    .wb_ack_o        (WbAckWrBtrain),
    .wb_err_o        (),
    .wb_rty_o        (),
    .wb_stall_o      ());

//@@@@@@@@@@@@@@@@@@@
//FMC signal mapping
//@@@@@@@@@@@@@@@@@@@

   assign VfmcEnableN_oen = 1'b0;

   assign FmcLaP_ob34[0]  = WrBtrainBvalue[31];
   assign FmcLaP_ob34[2]  = WrBtrainBvalue[30];
   assign FmcLaN_ob34[0]  = WrBtrainBvalue[29];
   assign FmcLaN_ob34[2]  = WrBtrainBvalue[28];
   assign FmcLaP_ob34[3]  = WrBtrainBvalue[27];
   assign FmcLaP_ob34[4]  = WrBtrainBvalue[26];
   assign FmcLaN_ob34[3]  = WrBtrainBvalue[25];
   assign FmcLaN_ob34[4]  = WrBtrainBvalue[24];
   assign FmcLaP_ob34[8]  = WrBtrainBvalue[23];
   assign FmcLaP_ob34[7]  = WrBtrainBvalue[22];
   assign FmcLaN_ob34[8]  = WrBtrainBvalue[21];
   assign FmcLaN_ob34[7]  = WrBtrainBvalue[20];
   assign FmcLaP_ob34[12] = WrBtrainBvalue[19];
   assign FmcLaP_ob34[11] = WrBtrainBvalue[18];
   assign FmcLaN_ob34[12] = WrBtrainBvalue[17];
   assign FmcLaN_ob34[11] = WrBtrainBvalue[16];
   assign FmcLaP_ob34[16] = WrBtrainBvalue[15];
   assign FmcLaP_ob34[15] = WrBtrainBvalue[14];
   assign FmcLaN_ob34[16] = WrBtrainBvalue[13];
   assign FmcLaN_ob34[15] = WrBtrainBvalue[12];
   assign FmcLaP_ob34[20] = WrBtrainBvalue[11];
   assign FmcLaP_ob34[19] = WrBtrainBvalue[10];
   assign FmcLaN_ob34[20] = WrBtrainBvalue[9];
   assign FmcLaN_ob34[19] = WrBtrainBvalue[8];
   assign FmcLaP_ob34[22] = WrBtrainBvalue[7];
   assign FmcLaP_ob34[21] = WrBtrainBvalue[6];
   assign FmcLaN_ob34[22] = WrBtrainBvalue[5];
   assign FmcLaN_ob34[21] = WrBtrainBvalue[4];
   assign FmcLaP_ob34[25] = WrBtrainBvalue[3];
   assign FmcLaP_ob34[24] = WrBtrainBvalue[2];
   assign FmcLaN_ob34[25] = WrBtrainBvalue[1];
   assign FmcLaN_ob34[24] = WrBtrainBvalue[0];

   assign FmcLaP_ob34[28] = WrBtrainBvalueUpdated;
   assign FmcLaP_ob34[29] = WrBtrainBvalueValid;

   assign FmcLaN_ob34[28] = WrpcLedAct_i;
   assign FmcLaN_ob34[29] = WrpcLedLink_i;

   assign FmcLaP_ob34[31] = WrpcTmTimeValid_i;

   assign FmcLaP_ob34[30] = 1'b0; // BT_UP_o
   assign FmcLaN_ob34[31] = 1'b0; // BT_DN_o

   // unused, driven to ground
   assign FmcLaP_ob34[1]  = 1'b0;
   assign FmcLaN_ob34[1]  = 1'b0;
   assign FmcLaP_ob34[5]  = 1'b0;
   assign FmcLaN_ob34[5]  = 1'b0;
   assign FmcLaP_ob34[6]  = 1'b0;
   assign FmcLaN_ob34[6]  = 1'b0;
   assign FmcLaP_ob34[9]  = 1'b0;
   assign FmcLaN_ob34[9]  = 1'b0;
   assign FmcLaP_ob34[10] = 1'b0;
   assign FmcLaN_ob34[10] = 1'b0;
   assign FmcLaP_ob34[13] = 1'b0;
   assign FmcLaN_ob34[13] = 1'b0;
   assign FmcLaP_ob34[14] = 1'b0;
   assign FmcLaN_ob34[14] = 1'b0;
   assign FmcLaP_ob34[17] = 1'b0;
   assign FmcLaN_ob34[17] = 1'b0;
   assign FmcLaP_ob34[18] = 1'b0;
   assign FmcLaN_ob34[18] = 1'b0;
   assign FmcLaP_ob34[23] = 1'b0;
   assign FmcLaN_ob34[23] = 1'b0;
   assign FmcLaP_ob34[26] = 1'b0;
   assign FmcLaN_ob34[26] = 1'b0;
   assign FmcLaP_ob34[27] = 1'b0;
   assign FmcLaN_ob34[27] = 1'b0;
   assign FmcLaN_ob34[30] = 1'b0;
   assign FmcLaP_ob34[32] = 1'b0;
   assign FmcLaN_ob34[32] = 1'b0;
   assign FmcLaP_ob34[33] = 1'b0;
   assign FmcLaN_ob34[33] = 1'b0;

//@@@@@@@@@@@@@@@@@@@
//GPIO signal mapping
//@@@@@@@@@@@@@@@@@@@

   assign GpIo_ob4[0] = WrpcPps_i;
   assign GpIo_ob4[1] = WrBtrainBvalue[12];
   assign GpIo_ob4[2] = WrBtrainBvalueValid;
   assign GpIo_ob4[3] = WrBtrainBvalueUpdated;

endmodule

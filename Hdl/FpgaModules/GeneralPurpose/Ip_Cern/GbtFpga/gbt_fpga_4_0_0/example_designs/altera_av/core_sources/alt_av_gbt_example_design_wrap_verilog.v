//============================================================================================\\
//##################################   Module Information   ##################################\\
//============================================================================================\\
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: alt_av_gbt_example_design_wrap_verilog.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 19/07/16      1.0          M. Barros Marin    First module definition.               
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor:  Altera
//     - Model:   Arria V GX
//
// Description:
//
//     Wrapper to instantiate the GBT-FPGA Example Design featuring 4 GBT Link in Verilog.
//                                                                                                   
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

`timescale 1ns/100ps 

module alt_av_gbt_example_design_wrap_verilog  
//====================================  Global Parameters  ===================================\\   
#(  parameter g_GbtBankId           = 1, 
              g_TxOptimization      = 0, // Comment: Standard
              g_RxOptimization      = 0, // Comment: Standard
              g_TxEncoding          = 0, // Comment: GBT Frame
              g_RxEncoding          = 0, // Comment: GBT Frame
              // Extended configuration:
    parameter g_DataGeneratorEnable = 1,
              g_DataCheckerEnable   = 1,
              g_MatchFlagEnable     = 1)
//========================================  I/O ports  =======================================\\
(
    //==== Clocks ====\\

    input         TxFrameClk40Mhz_ik,
    input         MgtRefClk120Mhz_ik,
    //--   
    output        TxFrameClk_1_ok, 
    output        TxFrameClk_2_ok,
    output        TxFrameClk_3_ok,
    output        TxFrameClk_4_ok,
    //--
    output        RxFrameClk_1_ok,
    output        RxFrameClk_2_ok,
    output        RxFrameClk_3_ok,
    output        RxFrameClk_4_ok,
    //--
    output        TxWordClk_1_ok,
    output        TxWordClk_2_ok,
    output        TxWordClk_3_ok,
    output        TxWordClk_4_ok,
    //--
    output        RxWordClk_1_ok,
    output        RxWordClk_2_ok,
    output        RxWordClk_3_ok,
    output        RxWordClk_4_ok,

    //==== Reset ====\\
   
    input         GbtBankGeneralReset_ir,
    //--
    input         GbtBankManualResetTx_1_ir,
    input         GbtBankManualResetTx_2_ir,
    input         GbtBankManualResetTx_3_ir,
    input         GbtBankManualResetTx_4_ir,
    //--
    input         GbtBankManualResetRx_1_ir,
    input         GbtBankManualResetRx_2_ir,
    input         GbtBankManualResetRx_3_ir,
    input         GbtBankManualResetRx_4_ir,
   
    //==== Serial lanes ====\\
   
    input         GbtBankMgtRx_1_i,                    
    input         GbtBankMgtRx_2_i,                    
    input         GbtBankMgtRx_3_i,                    
    input         GbtBankMgtRx_4_i,
    //--
    output        GbtBankMgtTx_1_o,
    output        GbtBankMgtTx_2_o,
    output        GbtBankMgtTx_3_o,
    output        GbtBankMgtTx_4_o,
   
    //==== Data ====\\
   
    input  [ 83:0] GbtBankGbtTxData_1_ib84,     
    input  [ 83:0] GbtBankGbtTxData_2_ib84,     
    input  [ 83:0] GbtBankGbtTxData_3_ib84,     
    input  [ 83:0] GbtBankGbtTxData_4_ib84,     
    input  [115:0] GbtBankWbTxData_1_ib116,
    input  [115:0] GbtBankWbTxData_2_ib116,
    input  [115:0] GbtBankWbTxData_3_ib116,
    input  [115:0] GbtBankWbTxData_4_ib116,
    //--
    output [ 83:0] GbtBankGbtRxData_1_ob84,
    output [ 83:0] GbtBankGbtRxData_2_ob84,
    output [ 83:0] GbtBankGbtRxData_3_ob84,
    output [ 83:0] GbtBankGbtRxData_4_ob84,
    output [115:0] GbtBankWbRxData_1_ob116,
    output [115:0] GbtBankWbRxData_2_ob116,
    output [115:0] GbtBankWbRxData_3_ob116,
    output [115:0] GbtBankWbRxData_4_ob116,
   
    //==== Reconf. ====\\
   
    input         GbtBankReconfAvmmRst_ir,
    input         GbtBankReconfAvmmClk_ik,
    input  [ 6:0] GbtBankReconfAvmmAddr_ib7,
    input         GbtBankReconfAvmmRead_i,
    input         GbtBankReconfAvmmWrite_i,
    input  [31:0] GbtBankReconfAvmmWriteData_ib32,
    output [31:0] GbtBankReconfAvmmReadData_ob32,
    output        GbtBankReconfAvmmWaitRequest_o,
   
    //==== Control ====\\
   
    input         GbtBankTxIsDataSel_1_i,         
    input         GbtBankTxIsDataSel_2_i,         
    input         GbtBankTxIsDataSel_3_i,         
    input         GbtBankTxIsDataSel_4_i,  
    //--
    input  [ 1:0] GbtBankTestPatternSel_ib2,
    //--   
    input         GbtBankResetGbtRxReadyLostFlag_1_i,        
    input         GbtBankResetGbtRxReadyLostFlag_2_i,        
    input         GbtBankResetGbtRxReadyLostFlag_3_i,        
    input         GbtBankResetGbtRxReadyLostFlag_4_i,
    //--
    input         GbtBankResetDataErrorSeenFlag_1_i,
    input         GbtBankResetDataErrorSeenFlag_2_i,
    input         GbtBankResetDataErrorSeenFlag_3_i,
    input         GbtBankResetDataErrorSeenFlag_4_i,
   
    //==== Status ====\\   

    output        GbtBankLinkReady_1_o,
    output        GbtBankLinkReady_2_o,
    output        GbtBankLinkReady_3_o,
    output        GbtBankLinkReady_4_o,
    //--
    output        GbtBankLinkTxReady_1_o,
    output        GbtBankLinkTxReady_2_o,
    output        GbtBankLinkTxReady_3_o,
    output        GbtBankLinkTxReady_4_o,
    //-
    output        GbtBankLinkRxReady_1_o,
    output        GbtBankLinkRxReady_2_o,
    output        GbtBankLinkRxReady_3_o,
    output        GbtBankLinkRxReady_4_o,
    //--
    output        GbtBankGbtRxReady_1_o,
    output        GbtBankGbtRxReady_2_o,
    output        GbtBankGbtRxReady_3_o,
    output        GbtBankGbtRxReady_4_o,
    //--
    output        GbtBankRxIsDataSel_1_o,
    output        GbtBankRxIsDataSel_2_o,
    output        GbtBankRxIsDataSel_3_o,
    output        GbtBankRxIsDataSel_4_o,
    //--
    output        GbtBankGbtRxReadyLostFlag_1_o,
    output        GbtBankGbtRxReadyLostFlag_2_o,
    output        GbtBankGbtRxReadyLostFlag_3_o,
    output        GbtBankGbtRxReadyLostFlag_4_o,
    //--
    output        GbtBankRxDataErrorSeenFlag_1_o,
    output        GbtBankRxDataErrorSeenFlag_2_o,
    output        GbtBankRxDataErrorSeenFlag_3_o,
    output        GbtBankRxDataErrorSeenFlag_4_o,
    //--
    output        GbtBankRxExtraDataWbErrorSeenFlag_1_o,
    output        GbtBankRxExtraDataWbErrorSeenFlag_2_o,
    output        GbtBankRxExtraDataWbErrorSeenFlag_3_o,
    output        GbtBankRxExtraDataWbErrorSeenFlag_4_o,
    //--
    output        GbtBankTxMatchFlag_o,
    //--
    output        GbtBankRxMatchFlag_1_o,
    output        GbtBankRxMatchFlag_2_o,
    output        GbtBankRxMatchFlag_3_o,
    output        GbtBankRxMatchFlag_4_o,
   
   //==== XCVR ctrl ====\\
   
    input         GbtBankLoopBack_1_i,                
    input         GbtBankLoopBack_2_i,                
    input         GbtBankLoopBack_3_i,                
    input         GbtBankLoopBack_4_i,
    //--
    input         GbtBankTxPol_1_i,
    input         GbtBankTxPol_2_i,
    input         GbtBankTxPol_3_i,
    input         GbtBankTxPol_4_i,
    //--
    input         GbtBankRxPol_1_i,
    input         GbtBankRxPol_2_i,
    input         GbtBankRxPol_3_i,
    input         GbtBankRxPol_4_i,
    //--
    input         GbtBankTxWordClkMonEn_i
   
);
//=======================================  User Logic  =======================================\\ 

alt_av_gbt_example_design_wrap_vhdl #(
   .GBT_BANK_ID                               (g_GbtBankId),
   .TX_OPTIMIZATION                           (g_TxOptimization),
   .RX_OPTIMIZATION                           (g_RxOptimization),
   .TX_ENCODING                               (g_TxEncoding),    
   .RX_ENCODING                               (g_RxEncoding),    
   // Extended configuration:
   .DATA_GENERATOR_ENABLE                     (g_DataGeneratorEnable),
   .DATA_CHECKER_ENABLE                       (g_DataCheckerEnable),
   .MATCH_FLAG_ENABLE                         (g_MatchFlagEnable))
i_GbtExmplDsgn_Vhdl (
   // Clocks:
   .TX_FRAMECLK_40MHZ_I                       (TxFrameClk40Mhz_ik),
   .MGT_REFCLK_120MHZ_I                       (MgtRefClk120Mhz_ik),
   .TX_FRAMECLK_1_O                           (TxFrameClk_1_ok),
   .TX_FRAMECLK_2_O                           (TxFrameClk_2_ok),
   .TX_FRAMECLK_3_O                           (TxFrameClk_3_ok),
   .TX_FRAMECLK_4_O                           (TxFrameClk_4_ok),
   .RX_FRAMECLK_1_O                           (RxFrameClk_1_ok),
   .RX_FRAMECLK_2_O                           (RxFrameClk_2_ok),
   .RX_FRAMECLK_3_O                           (RxFrameClk_3_ok),
   .RX_FRAMECLK_4_O                           (RxFrameClk_4_ok),
   .TX_WORDCLK_1_O                            (TxWordClk_1_ok),
   .TX_WORDCLK_2_O                            (TxWordClk_2_ok),
   .TX_WORDCLK_3_O                            (TxWordClk_3_ok),
   .TX_WORDCLK_4_O                            (TxWordClk_4_ok),
   .RX_WORDCLK_1_O                            (RxWordClk_1_ok),
   .RX_WORDCLK_2_O                            (RxWordClk_2_ok),
   .RX_WORDCLK_3_O                            (RxWordClk_3_ok),
   .RX_WORDCLK_4_O                            (RxWordClk_4_ok),
   // Reset:
   .GBTBANK_GENERAL_RESET_I                   (GbtBankGeneralReset_ir),
   .GBTBANK_MANUAL_RESET_TX_1_I               (GbtBankManualResetTx_1_ir),
   .GBTBANK_MANUAL_RESET_TX_2_I               (GbtBankManualResetTx_2_ir),
   .GBTBANK_MANUAL_RESET_TX_3_I               (GbtBankManualResetTx_3_ir),
   .GBTBANK_MANUAL_RESET_TX_4_I               (GbtBankManualResetTx_4_ir),
   .GBTBANK_MANUAL_RESET_RX_1_I               (GbtBankManualResetRx_1_ir),
   .GBTBANK_MANUAL_RESET_RX_2_I               (GbtBankManualResetRx_2_ir),
   .GBTBANK_MANUAL_RESET_RX_3_I               (GbtBankManualResetRx_3_ir),
   .GBTBANK_MANUAL_RESET_RX_4_I               (GbtBankManualResetRx_4_ir),
   // Serial lanes:
   .GBTBANK_MGT_RX_1_I                        (GbtBankMgtRx_1_i),                    
   .GBTBANK_MGT_RX_2_I                        (GbtBankMgtRx_2_i),                    
   .GBTBANK_MGT_RX_3_I                        (GbtBankMgtRx_3_i),                    
   .GBTBANK_MGT_RX_4_I                        (GbtBankMgtRx_4_i),                    
   .GBTBANK_MGT_TX_1_O                        (GbtBankMgtTx_1_o),
   .GBTBANK_MGT_TX_2_O                        (GbtBankMgtTx_2_o),
   .GBTBANK_MGT_TX_3_O                        (GbtBankMgtTx_3_o),
   .GBTBANK_MGT_TX_4_O                        (GbtBankMgtTx_4_o),
   // Data:
   .GBTBANK_GBT_TX_DATA_1_I                   (GbtBankGbtTxData_1_ib84),     
   .GBTBANK_GBT_TX_DATA_2_I                   (GbtBankGbtTxData_2_ib84),     
   .GBTBANK_GBT_TX_DATA_3_I                   (GbtBankGbtTxData_3_ib84),     
   .GBTBANK_GBT_TX_DATA_4_I                   (GbtBankGbtTxData_4_ib84),     
   .GBTBANK_WB_TX_DATA_1_I                    (GbtBankWbTxData_1_ib116),
   .GBTBANK_WB_TX_DATA_2_I                    (GbtBankWbTxData_2_ib116),
   .GBTBANK_WB_TX_DATA_3_I                    (GbtBankWbTxData_3_ib116),
   .GBTBANK_WB_TX_DATA_4_I                    (GbtBankWbTxData_4_ib116),
   .GBTBANK_GBT_RX_DATA_1_O                   (GbtBankGbtRxData_1_ob84),
   .GBTBANK_GBT_RX_DATA_2_O                   (GbtBankGbtRxData_2_ob84),
   .GBTBANK_GBT_RX_DATA_3_O                   (GbtBankGbtRxData_3_ob84),
   .GBTBANK_GBT_RX_DATA_4_O                   (GbtBankGbtRxData_4_ob84),
   .GBTBANK_WB_RX_DATA_1_O                    (GbtBankWbRxData_1_ob116),
   .GBTBANK_WB_RX_DATA_2_O                    (GbtBankWbRxData_2_ob116),
   .GBTBANK_WB_RX_DATA_3_O                    (GbtBankWbRxData_3_ob116),
   .GBTBANK_WB_RX_DATA_4_O                    (GbtBankWbRxData_4_ob116),
   // Reconf:
   .GBTBANK_RECONF_AVMM_RST_I                 (GbtBankReconfAvmmRst_ir),
   .GBTBANK_RECONF_AVMM_CLK_I                 (GbtBankReconfAvmmClk_ik),
   .GBTBANK_RECONF_AVMM_ADDR_I                (GbtBankReconfAvmmAddr_ib7),
   .GBTBANK_RECONF_AVMM_READ_I                (GbtBankReconfAvmmRead_i),
   .GBTBANK_RECONF_AVMM_WRITE_I               (GbtBankReconfAvmmWrite_i),
   .GBTBANK_RECONF_AVMM_WRITEDATA_I           (GbtBankReconfAvmmWriteData_ib32),
   .GBTBANK_RECONF_AVMM_READDATA_O            (GbtBankReconfAvmmReadData_ob32),
   .GBTBANK_RECONF_AVMM_WAITREQUEST_O         (GbtBankReconfAvmmWaitRequest_o),
   // Control:
   .GBTBANK_TX_ISDATA_SEL_1_I                 (GbtBankTxIsDataSel_1_i),         
   .GBTBANK_TX_ISDATA_SEL_2_I                 (GbtBankTxIsDataSel_2_i),         
   .GBTBANK_TX_ISDATA_SEL_3_I                 (GbtBankTxIsDataSel_3_i),         
   .GBTBANK_TX_ISDATA_SEL_4_I                 (GbtBankTxIsDataSel_4_i),         
   .GBTBANK_TEST_PATTERN_SEL_I                (GbtBankTestPatternSel_ib2),
   .GBTBANK_RESET_GBTRXREADY_LOST_FLAG_1_I    (GbtBankResetGbtRxReadyLostFlag_1_i),              
   .GBTBANK_RESET_GBTRXREADY_LOST_FLAG_2_I    (GbtBankResetGbtRxReadyLostFlag_2_i),              
   .GBTBANK_RESET_GBTRXREADY_LOST_FLAG_3_I    (GbtBankResetGbtRxReadyLostFlag_3_i),              
   .GBTBANK_RESET_GBTRXREADY_LOST_FLAG_4_I    (GbtBankResetGbtRxReadyLostFlag_4_i),              
   .GBTBANK_RESET_DATA_ERRORSEEN_FLAG_1_I     (GbtBankResetDataErrorSeenFlag_1_i),
   .GBTBANK_RESET_DATA_ERRORSEEN_FLAG_2_I     (GbtBankResetDataErrorSeenFlag_2_i),
   .GBTBANK_RESET_DATA_ERRORSEEN_FLAG_3_I     (GbtBankResetDataErrorSeenFlag_3_i),
   .GBTBANK_RESET_DATA_ERRORSEEN_FLAG_4_I     (GbtBankResetDataErrorSeenFlag_4_i),
   // Status:
   .GBTBANK_LINK_READY_1_O                    (GbtBankLinkReady_1_o),
   .GBTBANK_LINK_READY_2_O                    (GbtBankLinkReady_2_o),
   .GBTBANK_LINK_READY_3_O                    (GbtBankLinkReady_3_o),
   .GBTBANK_LINK_READY_4_O                    (GbtBankLinkReady_4_o),
   .GBTBANK_LINK_TX_READY_1_O                 (GbtBankLinkTxReady_1_o),
   .GBTBANK_LINK_TX_READY_2_O                 (GbtBankLinkTxReady_2_o),
   .GBTBANK_LINK_TX_READY_3_O                 (GbtBankLinkTxReady_3_o),
   .GBTBANK_LINK_TX_READY_4_O                 (GbtBankLinkTxReady_4_o),
   .GBTBANK_LINK_RX_READY_1_O                 (GbtBankLinkRxReady_1_o),
   .GBTBANK_LINK_RX_READY_2_O                 (GbtBankLinkRxReady_2_o),
   .GBTBANK_LINK_RX_READY_3_O                 (GbtBankLinkRxReady_3_o),
   .GBTBANK_LINK_RX_READY_4_O                 (GbtBankLinkRxReady_4_o),
   .GBTBANK_GBTRX_READY_1_O                   (GbtBankGbtRxReady_1_o),
   .GBTBANK_GBTRX_READY_2_O                   (GbtBankGbtRxReady_2_o),
   .GBTBANK_GBTRX_READY_3_O                   (GbtBankGbtRxReady_3_o),
   .GBTBANK_GBTRX_READY_4_O                   (GbtBankGbtRxReady_4_o),
   .GBTBANK_RX_ISDATA_SEL_1_O                 (GbtBankRxIsDataSel_1_o),
   .GBTBANK_RX_ISDATA_SEL_2_O                 (GbtBankRxIsDataSel_2_o),
   .GBTBANK_RX_ISDATA_SEL_3_O                 (GbtBankRxIsDataSel_3_o),
   .GBTBANK_RX_ISDATA_SEL_4_O                 (GbtBankRxIsDataSel_4_o),   
   .GBTBANK_GBTRXREADY_LOST_FLAG_1_O          (GbtBankGbtRxReadyLostFlag_1_o),
   .GBTBANK_GBTRXREADY_LOST_FLAG_2_O          (GbtBankGbtRxReadyLostFlag_2_o),
   .GBTBANK_GBTRXREADY_LOST_FLAG_3_O          (GbtBankGbtRxReadyLostFlag_3_o),
   .GBTBANK_GBTRXREADY_LOST_FLAG_4_O          (GbtBankGbtRxReadyLostFlag_4_o),
   .GBTBANK_RXDATA_ERRORSEEN_FLAG_1_O         (GbtBankRxDataErrorSeenFlag_1_o),
   .GBTBANK_RXDATA_ERRORSEEN_FLAG_2_O         (GbtBankRxDataErrorSeenFlag_2_o),
   .GBTBANK_RXDATA_ERRORSEEN_FLAG_3_O         (GbtBankRxDataErrorSeenFlag_3_o),
   .GBTBANK_RXDATA_ERRORSEEN_FLAG_4_O         (GbtBankRxDataErrorSeenFlag_4_o),
   .GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_1_O (GbtBankRxExtraDataWbErrorSeenFlag_1_o),
   .GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_2_O (GbtBankRxExtraDataWbErrorSeenFlag_2_o),
   .GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_3_O (GbtBankRxExtraDataWbErrorSeenFlag_3_o),
   .GBTBANK_RXEXTRADATA_WB_ERRORSEEN_FLAG_4_O (GbtBankRxExtraDataWbErrorSeenFlag_4_o),
   .GBTBANK_TX_MATCHFLAG_O                    (GbtBankTxMatchFlag_o),
   .GBTBANK_RX_MATCHFLAG_1_O                  (GbtBankRxMatchFlag_1_o),
   .GBTBANK_RX_MATCHFLAG_2_O                  (GbtBankRxMatchFlag_2_o),
   .GBTBANK_RX_MATCHFLAG_3_O                  (GbtBankRxMatchFlag_3_o),
   .GBTBANK_RX_MATCHFLAG_4_O                  (GbtBankRxMatchFlag_4_o),
   // XCVR ctrl:
   .GBTBANK_LOOPBACK_1_I                      (GbtBankLoopBack_1_i),                
   .GBTBANK_LOOPBACK_2_I                      (GbtBankLoopBack_2_i),                
   .GBTBANK_LOOPBACK_3_I                      (GbtBankLoopBack_3_i),                
   .GBTBANK_LOOPBACK_4_I                      (GbtBankLoopBack_4_i),                
   .GBTBANK_TX_POL_1_I                        (GbtBankTxPol_1_i),
   .GBTBANK_TX_POL_2_I                        (GbtBankTxPol_2_i),
   .GBTBANK_TX_POL_3_I                        (GbtBankTxPol_3_i),
   .GBTBANK_TX_POL_4_I                        (GbtBankTxPol_4_i),
   .GBTBANK_RX_POL_1_I                        (GbtBankRxPol_1_i),
   .GBTBANK_RX_POL_2_I                        (GbtBankRxPol_2_i),
   .GBTBANK_RX_POL_3_I                        (GbtBankRxPol_3_i),
   .GBTBANK_RX_POL_4_I                        (GbtBankRxPol_4_i),
   .GBTBANK_TXWORDCLKMON_EN_I                 (GbtBankTxWordClkMonEn_i));

endmodule
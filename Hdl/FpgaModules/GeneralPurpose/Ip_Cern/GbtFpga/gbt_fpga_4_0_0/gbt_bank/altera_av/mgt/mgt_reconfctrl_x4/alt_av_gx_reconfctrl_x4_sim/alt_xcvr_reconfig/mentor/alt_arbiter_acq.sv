// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:05:35 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
O49dTe+JnSfTzlxymuDKqf1PUcykurSGsSDfokDy1cgRWgPgutweH3TYLZPCvhrp
1UqIj3k0reGqhonbhMaiVoGD2WbPwbYZIjCN0DzZWlpm4ZvxR9rAHI8ZORDXRkn4
u4Ji20owuAAZphT2OiiYRbbnRUtGiR6EWJiToAmjNuk=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3280)
MwfZZPZnnrt1VRQoDXLGHu5a3464NclqCDkqGT5Fx4PRh199+sF4X3Zxa7KcY0tD
tYnObhdYyBcWuV/39E+AdAW208/MqFnYYuHv2lLfpd0jVHQiA+kBPFIK3BwvCUGH
CoIvML4X9ryRG9lTKae5COdXoEWQgs/MmXoi30Gh7ZKpxYUz/X83qU4CqLBeUlws
7V1l+ZHFAFyRjfjIZcdvabcJ/i9O1QRSBt/mRo1iS7G+jYF/8SJ6d6Dq7Q21ArEv
YB0x00Q+8PaUtcAYHwP5ib7ioXrG+eWEc4eLBFVtiavnfOY8BqDjdmBdwnckWM5j
R2I5reYgn8uwxx++l8h3axSZwFP53jqoTqT+VPMo9NMyYdvspReJvihYAoGqyiVo
ldDttK1Spjwo0cMntFb6eQfrfh+tDMkX/LyUpRV4xxwzIgoJ+/vSJI9muiIkgSZg
H5vgH9S0HUtDaKhf6B03vF0Xb5Fa5/kRkeBuADIkNbOpIyghI33TF8EfqADDH2YA
+qMyXcoOzyIdPNizQL+D4VrAIkv3RL4V01CWd8TDc2LCGp8p3XD/EI3k2VDGWtv4
JCkEmrAwUDC1AJ3CLmKhqNN3jqjJiR1GEASlNr+DyPXU7xGXMK7iNb0iNit2ILsR
b7fHdt/9kV1HAVoETKM5ayHy4racjjZz0SPymFwyNsrOGR1ILOZzA3LC4YfpOA8u
v0ZTHIfY4pSZjWq6INpzuGWdlMuU9CV2D7E1CD4wEJhfMPUBHSbT0yOgNEG1qE3B
9NCdNH+LcbyToA23MtWDstSPWQ71pfI8aX/TZKGQFDwRKJsQ3vXEQ6KMdPpEiVYW
AR+HHTHUDcgPo+wlwSFSAtH9ucokkJUDtp6LTUpVoMVb1PSjMQfVKOXWSW06gJQo
05uWjc00XqJoWu5bUOmwla/3NgLKNbA7rYE3OCCx2/NcOhakbfQ15YUUEopsMCal
xIIzu7NXyDYbzWStgQXywloLEthle8sYGI/LEUrFCJwzre4WMnC1hwRwHqIHyz6W
JOXRZ/ZbVyfdoAgCoycNi1GEaU6Q6U+fIwCOq2w/41X5KSv+ITEm4O0uBWQwIjLP
o8m37wQ1pRFxyyKcJzz+bhIKpQBpiFeU/i1WxaeCnOR8nu4lJMtQT4UwtvgNchvU
9n4RjbNabX/54rg6ILAa75JX8N2g/YiMU+svZDBRrFqa9ixPHN4bC2oGzUjJKfNX
Nr7x1h+uck0LSLg0Dp1Cn+By6Jax83q1njDA9EjLvbFyWXNwK2TU7Avb114J3h7W
NwGFIOSvt4QfHxQWT8iMHMBDaZXpK+zwq+VUxditVHTULKqQVAg4DNDcVbNJVfNq
bZ8s3gh3ijsl+ZIjJ27TnaSWCO+Jo+lh7R55vdf0dyZGfogLP/lD3OxXo5rly7tc
42Rg9lXAfyeG/TTM5VeQh9kfJPghfshW7/GP9ZDiqhYeKAgsL6qfF0eQRYLtwhYy
Y+AGcqImX+NrsOUXCl36FI1FAim4zQeAoXR7XNaHu3Iid3ydTerKtomrx0NKYQoA
8m+uziGgCzkcwCcz2Y9RwYIPyIR8FRTT1IL2XSUiLuB8yASLMrM3N44EgSeaK2vB
NovepHPP+GyK157HACUSe/wI0SZ96zbYJEp5Bz0nMYzLQK0PyGcplQPzV2Yv8pkP
Z4Y+GF6qS1951j+wqquEPKwC0Td+NhS3qINRQ2gPlkfem2Jvl9ZlcYaQC4gfdZu9
DbiNyh4LqfaYT1L4riuC4ZrXAaL2R5EhngnVeF09s/B2L22tEkFCN3TdBibqGx6I
1qGi2okpZ/+eWb5ltJzK9A7BVPdof6sarDB1YyFbfmIRMRes75AbmzLAsU8nkp2U
tOeXhP+mOx+VA4iNWq7T4Ked/6hkstygfAvNYw9U1R/qx4nGR24tzTQ5ekiEB4rf
L71OMfTO52kUQ8ZJGh1M1QHAQx9+ApVgKD+kHszVAWJ6EyWjBA7bRVteEau7I7CA
i09gyVeOoo+AXs4TsBuBBizQLjMVwSJjTmMDV1hGMBvZjRS3SyQJdCi5nr4cWXeu
RGcDKqwGDDt+bq8jeBes/ANKbd98U5Pyk7c4vvmAFWgmgUpZZBLE8vfDDdeEOpO+
K/n/BtYJlt//Ef7Q3HDl8p899AlQEfLwF86ARhenGcYA6wTyGx1JgyCYxR8gpiO6
2nnnpGtR4WJpxjedt8gsgrmQpATOwyX2kL/g8HMwSjHoc1QCHBNl/YXLt4bNsUHz
UFj/F1dKxEcp4GfR/cWQFJh3+vLGH7Aos2AF33TfvCMhqK8YxqW24IDYs8WHH0dv
kd9DAXgvK9Z2Q2z1c174S9lX+PwmmryAhXhs+qzJnt9mnav88qR96J8MZRd1HI6v
RQff7QtKi6BZ6MfmJzPuOjvV2QTxC0OkEsbzClnMtezpiNRy+bEgO6ILCkR0oRFD
avqVP3bhSWsv56VwlRExiykfzlPRcOb6nfUNhbuSCgo4QLx5zq1KilftASNMQSxT
+gVzMGi5fP5eeAIC5Ky0BKR4rBbyX0L49yetLQj1pD9hhrNn6Sz6BXps/CYzuizY
pUYlQsDDX72YhPuh7X2Sptt+kng/a5hcPjrsRBfLkbXpZPAgvfaU/OAtrScZdy7G
XrcNLKObrbNBIY0IsbWezX9GsUJZ/CA7W5V/8SucXXKeFJWGJkTa8NGb5yfanvDE
TV8KNEFfac4t/PY2Gk6kaKomIPBVeZQP0AqsXSZbNx1+OZTNcAt5FAkhPqYVUjbz
YdJTLwMgwgpwpBSTbq45ZzL4m8SxuWz9Qy8Tv52ZQi4hE+EEyb9FCm5IcNFAZI9Y
Lo/0rTLwK2xmOlVjaxDDoGM1EO6c1GexEL5emnv/xxtQs1c5pgT5o7Gh54LCK8XO
nsB8DlitzIRV8g0nVtqZB8M7vjPeaTrx9kSUn9DTBPGS5CiTVr+Q1+UWyfqY8NwQ
1ZVLvPa4b0Jmz2D5trwHkVhtlLuGgkB+AjEWR84OiNib1uXkGnH6N22b63qix71t
rNip3s48f7darXLnkh45pnqhpM+LH35c4+K0hsVnJTic8dx+QtaUuyh7r10zHt3f
O2nD7bphtcfkC/lZVhVc7oOHQiAwYTM5aX0RU0IapEVYsKsVkSKj9+J6rR+f24Te
+sK62IRppLE6NoePCInWMt5F1xfTry97Gbix/b2L9ET5whk7ysSk0920kquZZwLf
YVfML7FKWshcyp+/ZWfyZ93r40bpXE6c95Oqq1t4LuC1ZxDb+3T75u2x1F1PNjeX
iozZW94h47TD6WfhYtllRWi9cQ2e9NpNUPYMSChfvREcKHijX3KaHOXr1GE6IwX6
N81TfCda/qC38D2OTv0pzZqTVcn+kjN+zSA8KjwGDQsf2PYtQacB13iBfFU3B3s0
Ws3WDfq8yCvspA7G5IkAkimxcSrQhXwZmwQlWpi6ly8eBOn1JBh9o1v346E4//rg
OIIxtS830I1FTgUK+8RCNlLebgHCzKZgU03b1PVDbL89e/3lZx4hsSHt5icVN+vL
ACF6dMX+yGOsKA8z+TMU+X6FzAYTlMHZ8dWKLJyunlEgvfAzp6L/bugzGxPoYGuJ
P9FCFcGT/gOKdoepZI0O5fIEDKkXLKoq2yELsuUtnz8wlNl5izsQOfJlzlJx4AW0
iVgIujLUsG/i/FFKP8QS48G1vJAbIisNS6XurefdFOQI1fMT+pIvAdqvBgQPYAjq
J8SleaPDdENVJb/69bgHLGSfI4eHPXQg+lfuO/8JYxlvy2iQfLJcJTqS1ynTCxp2
D7dX9cHPiKQoTObT/8U8eluoP/FGZzLCJmi2ZJg3C9nl263Chi3XNVdIo0JJWjYf
ApsyW7MQ//pYx1pEBXS7kKa5M5L2/188ns9iEOTG2ni0+YeyosbVbv1ULAQ1EoXF
GP9iC4d9rz48rlM0fjaQbv5nwMlLoxJYJ2kL622uqJJl3Q27t8MGdYiee1DDOgER
Xo83rBizF9pJctNo+2II2UO6EVam7CMXAjZ7m+mFd1vYBTl6nh01QP9y5/ntCWBq
UCcfqSE05uG2bX/e6TmZgFIxswbz8ZL3EezL3bER8lSYmWaR5HWlvv8rGl5THyG0
6tnC9tCDJS+ulUZCR2xxBLs3kSdcGO4PCYjG+HsyuQJwf9qI4AM3YYvg2dQIOa8z
MsvoIhtOfopkUlRRsbZUevcVyNmNlugFjqWQG3RRCPNvwTDJ7jyTFUFrcZfdzH6c
BKLjrBHJS5DpsedawGAibj14byP+m1dZ4A5gZSvwxc13gAGlk7vDxYCD3vcr0nxJ
YBJGyH0CcuYRYHfM4JSNXCZ0e9mzBFCoinm6+I2otBlqLDPq4SFEQautfW+C0Qcn
OmX+f1lKnn7cylqZnAwcuw==
`pragma protect end_protected

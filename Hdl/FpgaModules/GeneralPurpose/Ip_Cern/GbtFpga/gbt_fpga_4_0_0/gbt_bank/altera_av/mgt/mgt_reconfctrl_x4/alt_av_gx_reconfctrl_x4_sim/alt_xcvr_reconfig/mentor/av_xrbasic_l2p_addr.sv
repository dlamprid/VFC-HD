// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:05:36 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
b06sTYIs5mJkKFIQSZB0KVtn8DAIomDsTXrbszDD87K9c7ENELl+BjnjOGI+e/2w
y3B/RaWg9sL7zQpbzpmEb5I0HQSLE95e65CV/aYpOlaIQZsHvilR4U6tCtCALI5a
6nl2zFM4F5tUZ1rc/z0V9CWJvFNs9Hxf5impIdVctWg=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 2224)
s89pBuf/ZwhcLfX4rmkzWqnLzr/FJmG87r/giTvA3bR0j/0bndVqscmWGwkF3ger
fjvcOdFuIZwR3h9S4WjtnOZ4Vbb7FgP+gG/I6TeTiqdft2t6pAqXd6Sx/uO1nqZc
lkuT6wqX6w6EBIshvXqU5knf4EZN9Fg8xaNJK0yh6BeT/3UoLrQiN/xWs9MCr6jj
bz2VszdJzz0p6ibaAxVGL2ZCnuBDKx/8wB5eO2FNaTAOGa8j15CNQ2r7/x2NCyJs
BsiVSn8uoopXbTqq+jPtJqwZwYo2UP+a2S3IMSJEDwrvphYzBHTfQarvGu2p3xuT
3oJQvY+6Zh1gIYUe6KzFnpTwl9wB+vwq7/kldL6LT+MLTw1hH/HqAsgenOQ432Ns
aawF6M8wNwGN7/z2cbW5ZDZz15ynmMP1/5ZsbvG5XvWNYpgjTTvHRBjGtts8iD26
zuw9AyCM+hYYbXSTPIxvnvLbFN/gznrDCz5eNDqscGh9665GaAwxvceyW5/XOWp7
FEpqFoy/74+BZYgdfaFIHkFKwcqNk8oNRfQZxKwP0bEc+b81VpYBUV7rfyjtXntb
kKuRD4LeTkyaFFl5RNY8wvH/BK1gbQ08+W1gPYJ+yxbQUgjlhiFhDG3+v/aqIEWh
303cMQcsR9ygMYK4GueQ71SGN6Ai+G+pyl7Q+Wy2c/Bz4uVVtep9Fn2L5QH+Itd/
3H2+J+bTI3pT0BswuaqixwMwfCDldU1TK4FYhmF2g/WusPWmorwVH754Dk5r6WRQ
M+wrJnupNt6MEZ/7syntjLtxFn/WpuQItnhdYygTXSN2aQZEWVKwO2+o08mfeBVs
myGAuSlZzxSKXL26rRjkS+yOQJZUtgsBOZQUa0Zd6oHc17aq/HDUVW6D1adkvdMf
UtvoW7HWJz3u5lUNN9mVi4i2VsMMwZu+ETAivNCsk4yYpE5lXSUkCsSxhuN9tb+l
nuXTxbqoRweASTcxDZpZuzYCu8ZsRtLVMqIPBORuFXBuaDWNQweq67WIdmOMJEVI
AauvU351oveJ0US5feRNiJcekgIODQLvg/FIkR/MQ7SqwIJ3tzDpycwEVtGUBMgq
ZNvmmqOUP9Yy2yaO5N/sLhQbqeXNm8EnFaK9l5telA0vh4cCrqcdR4cO/4vk31gB
1wSPS/+sTsC4tE1yRkAKim+t3h+zLeG2LZ5E2PetcxG3swSKMOkDpeCdtcz/7/er
4vozphcjsKKImxUf6VFdZRpuZ9xNBUToqYxqTbnJOjG7lKMN3LcLwmczzCNuK3VE
6mIzF2tDXvKFChcqHxuDxqxScbSRnu5CZOOcd1MB77cH8Y6iNqW/V+U8LXNlFbMg
d3jqtv5bfGPkYJpWW2H5FfkcGlRlUX/Z37Usb81wp9orqchG2JcFDLnPqIwmG/Ic
Ikv1B72BlMeZ2MHryp+RsYWNmq1bwHDB2zraQN+iJd/h5a/6XSMt9LazSkKY7JU9
bY/GrTYdDI9Gkk1r6djZeZ4BzbN7aMbf9NWv9NOyxdwcvjsOhSzi4j+DC6zyzO3H
1Z/vz5yDXeMcbgKTsnKzT+L/rvO3D7UKZnkN2o5ByxGi9ERHqfbz+b3HpAvUM0qS
9FBbfkfnk/oalZbg9X8buW7GKpPWzoVytyEbDD6rqXpyfIJiu1A5BGdw+81HvzhV
ONT/eZRBdt2q4ofMZjM91lKCy3QYjja1EsaNTmJxnVQcfuaXzusOFtF5J/e4En0D
7ddmNSQQV7fRo4SHmYZF5vaLJBYV95qXjMejK6PvW2C4AAoO43iBbsX2orbKwT2z
Pry1z5b/r0AkpH4McDAzm7ZovzxACpkLgutss5I0etlPm1QXCVRCXE1+1c0vxNc9
agA87sNa+NIofrx7s7uHEoDu9t0XAkTX3QDoDreiZUOR1ALmRC6hJw0qqkljpnjA
J83ZWPEZR1ndEyFU5OjWgabffavMbjkSH4DdcL6pMFDVr51mqLTPWk1bbEDrn3sr
Q4Z2SnKMOLafzkVYR2z75YjKQITJln04sUT5VdAgH7wNJH2r+mYsYVnYzxgOZh3+
J4EH9DN6aWgIocxa8nFdOesa54dwuI+AmzNzZHG6msXI0OcDMKTO6Wjp9ZamKrVP
LaIzczUVf4eGp8RTLrd11Y6typY+kA/FFD6zCcsHrzeVTyEnIsXYKkKVpY/+joFG
wOp88+/z+lQrjIzFfmIx8eG/co9MaHiq+Abg2jX5FrVa7VyeOtCBHt3D2KTKunMN
WCy/cUcBSn4uyFtoS7cMvlDHxcWeJzuu1iNyznljNDo9uDOkvtLzwIEouhiuc5o5
yadKobVpHOvQcH9p8rHfbkROPew/h2UDoR/Zl8cx4Qcb4Dod5SDqh33ZvkTzvN8/
ivHlxdYGT629lp2wrqSdAYmi4h8V6Ju9liu25TYiUSTnujm43/OMjYfcSjXxNwAQ
qONuz3bnHMnjuZ4TR843yLVPzq+VDU5lunTJTkjnKAmJ8h+4Cxk+AqRDJddA0T6w
kqhnx5PRdnPhI55q3XrTP79iD0Fs8i3gEDoyJdV4ib6ldS1AKOjp4vE0U12hTRa5
YG2j51/07EG+BP0SGHS1g7PvOhtpr6SNnK/txqUCKfyAv3ev+iNjESXp5xNLAHLD
9q3tZwfYUGJEpo6Bqk6eFl6kUjG4l/qwHoCDYxRQIdzpWLX8SWK+qq4TnOyftqgD
7HyewqyG46o3p4L+Pxm/kXmBcaeSSPcibM6nP7wCe/kZDH5rIaS9zXi16r2PNXjr
GNDNCVto6KlRBmRfseH3BAoRQO2/jztN2lfNm6MXkFTLtxDq7EX62G8hZL46RVvg
cKN0WnktT6OHTFkmiNpDb9yOG76Xg4Eilz3BHKdZMdTPEMKfliSPL68Mv+DQws3A
BgmUKTPIB7fG231DqdBrcttJ1rXmtlZrVT70uZu/c71gNYbaadvM4FTi+7v7iWRF
QNJBGI0Bu/87wfA6taARqw==
`pragma protect end_protected

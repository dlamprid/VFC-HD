// Copyright (C) 1991-2016 Altera Corporation. All rights reserved.
// This simulation model contains highly confidential and
// proprietary information of Altera and is being provided
// in accordance with and subject to the protections of the
// applicable Altera Program License Subscription Agreement
// which governs its use and disclosure. Your use of Altera
// Corporation's design tools, logic functions and other
// software and tools, and its AMPP partner logic functions,
// and any output files any of the foregoing (including device
// programming or simulation files), and any associated
// documentation or information are expressly subject to the
// terms and conditions of the Altera Program License Subscription
// Agreement, the Altera Quartus Prime License Agreement, the Altera
// MegaCore Function License Agreement, or other applicable 
// license agreement, including, without limitation, that your
// use is for the sole purpose of simulating designs for use 
// exclusively in logic devices manufactured by Altera and sold
// by Altera or its authorized distributors. Please refer to the
// applicable agreement for further details. Altera products and
// services are protected under numerous U.S. and foreign patents,
// maskwork rights, copyrights and other intellectual property laws.
// Altera assumes no responsibility or liability arising out of the
// application or use of this simulation model.
// ACDS 16.0
// ALTERA_TIMESTAMP:Thu Apr 28 07:02:11 PDT 2016
// encrypted_file_type : local_encrypted
`pragma protect begin_protected
`pragma protect version = 1
`pragma protect encrypt_agent = "Model Technology", encrypt_agent_info = "6.6e"
`pragma protect author = "Altera"
`pragma protect data_method = "aes128-cbc"
`pragma protect key_keyowner = "MTI" , key_keyname = "MGC-DVT-MTI" , key_method = "rsa"
`pragma protect key_block encoding = (enctype = "base64", line_length = 64, bytes = 128)
eTxivRdzf+1b5oVTO7//sRxRA6qX6ScwJONktzZzzTut31QYWF39+X0JnbB4M49p
lNHpVhxFe3olGJUVbLJDfWj04NXQY9MlSgM9uNuhXndIec5p31+uoH3ceqTjEZMD
GdUYALM5k81fUc3C6/yoizc6Az7pk7k9/dy1J9j36SA=
`pragma protect data_block encoding = (enctype = "base64", line_length = 64, bytes = 3056)
AjP3wonslG1opQbyh6Y6ICgTrK38Fi+FeLnfF6LO17IJkmgxCNJ9yikkgp1dDzfc
n9/sA5HA11NwfK5oIPlMFUdi4hq8v/zwm7/NvdXCWrcKNFI/cO+sTokJVeq8w2mD
Y731Yhc5OJPQaYdzzb93isY0EQOZEc/yKjmuxupIpW9k+W82LKlUx0bZJJabQx82
mwRV3ch0GYUBDuG89gYWefBjOkT5W8niPjRlIWuLi8TUlbgYtgBnmo7CvQ8of/9i
6aFnB+58eNl7q01owQ8N+XIi9sGWSzkyEr43NZUGULAF5yivwCXCQZ+Rlhq0P5gq
NaBLMc0Mx4gu3OOkUM3ESqXpE6tNU0uko0bI3L0J/jnzI/r9Sj7kEh29MKCSSjhH
P/lkzwpG6rgRaT46Gb+rZ9W9qHGVS6QDPGagKdghq4Oq446cKb6Is0Qvt+d/K+8M
fPt+ekzRq8M9KpuKPasXk9B2p/lkb7kz77p8h/0l7yzqozFuM0febBbnLQHrGjNq
B7K/ziGvoTHKThKbP0sca42AkXDVkCraC/8wWmW3RjLiQGZ98wcy23PYkbXvvQ/0
Q+43alJhMH5lvy/d874y5noPcd5CpzBOJq8IUmrHoHdWT4/h159YpucOYwlbChS9
HPeDf+Jmbb9eT4dPmX5AcFcIAxmcMIHRoPNHVanKguEhiHG2FMljyueiBHIYir4z
2X8Br9swucH8z0efWhyTSjMsD+NBYzoiuAc5XFeBXcxtHtx3LsyR3DU92S9mB+07
mqa3VDueq2V83qud4lJYCtfamt0Br/Z6dVHygoIIf70QqYNUsj4tR30jNubu+CcU
wahunCmrwJAx0k/uOpXR3zVcoEOC0AN/Rh3Zlp8NQKZG3RLf3p/wQeiSfixfy8W/
k1AUUfuHlLmCY3lFzX1/HMbEz5ZDasG7hXAMXEur9wxv6zNuQQ2/o4RUumuFTGHs
0qyOEjJ854KjpyenvboYcgLI8beXRGtv59WwOftXWu9iOmfLa8z4MsZSPOuvIrKg
R2Dr45tjejt9DVI7i9ZmXxX4LSC0TZyVx1otKkjlPP8kpWCT/nLgR29lcwESxGBD
1L33P5aWzzLg+fAGuQarmmmDsBzzYWY9Um9mjaM4VQEMM34BbzIDljWT/qAlE9BT
oUYxkAo5/SS4yQVWuQA2EJ4dJK/GgN63WCWwHdMow0FbIlt90ADgFIoGqnJlKaY+
gWl20q7VGyHrL38vnFEq1Lw+y8VWUTefweGJGgm3KcZYm9Z4vGeUus8TCE3GHF8b
RWwzu8Cjh0CU5r+JFV/1QwuSvGqb01MnC0KYyoqp//wWF/vdUmZ+4nQowPc4fw0x
ojCirNC1Ho6SyxHKegjNV1fH5bltVMzUpmCadJLySTc9W16HqtpH154X/fWh65YY
MJ0ROl5GYFO5UeZcF3xs+ZRtXSUghdtU/tW9YydBxbUVHZ7o5K3Lg4qil6Rmcqb5
faMJ6aMN68P11k4tSVCKBwGI3URbpQfT5CrLdPCe6r/7H5Z3XhiHtjdVJB7Y9Ddv
EB8d/qjr/Y0F7CIa2tqpqSzgVccOGnXYrY29MHV5NEZ7EbKzFJUPsZ1Fj3n8zbw7
ALbPZd57yFY4G014RtTdAG+A2k4W9lFnaEHcPdck0WBMG1442ZyiNWb7oW0R4en2
47d00I5Axpjfc3zHC++WnXp4q+w0m/mh1WIksLmlT89Et+qh2SRHagcrtIPYs9Pg
Vd90yKzoTjzGo74MLf3O7l724KoxsE6DI4mUUHfILszavwefQyvDSZZpU0KUwLoX
46OzqFBSc+zYPn3vVjgSZlbfomE5cSCgtCUjPT3mFY2+ApOntrfIvnue0w52sSky
EV7OyFYyo+hyIAwphfZJvGTBM5SnCNP+bhODdd0YWPlgKvQeM/lDGgpGG1SQXbZJ
686f05YGaUKauP+Zxu2iYREA9h4YdxUIR38LzYmEIQoCt7a8qpDjOJmr9GauqyA5
QFSuQBZK74+toyucVQrGSJC5QXQxItbGxb/sUDnTQbfb/9igR3DPcrVjn85yExM8
/GJkKcM7KU1HUaRnGGEHz8oERcI1DeDEREzoqOp3xPjRioLnG7Iqa2ylevUuVQTc
zBEeFitds6JObyl+mLjrgcoiYiC3HUQvGZRrHkgibcxWq1fJk52WGbR5IVF7b9J3
yOw36/4b11gg5FYY8k757rT581Gj3/NfEXFIPm2lf7wT04e1R9xwkLKOeVROqhvl
UrR243UAo6jOSxYYhfOjGJYqjSJ9Q2UwoZtMN8fN5VmNPShpKMpFWpHs4AIDq7Vt
Xbewjw7kfpinbPypQ+Rh3CUuL2uFHAPYW4zvMZXLAkj0joIYisNVkb7B0yDzI1RL
8Xta1IgcDCNU3jWDvKqDSGhN+F7bUuZ0WLkhXBNdPpppJ3v08bNKG3CgtK1hsFPA
/H6CYBkpI3z8lWPUAouL3f4x3+Doi/aEshkg/Ag8nl64bEvZV4J83O4R5eT9bp+p
kW5wFhWCBcMRg6q5luJIEFL9dnrcxKlHQhHQ04Z3lDCtPA/bdFSnOHzNUKtg7cVD
79KqjuGj0VgY7g5Znr4/iXMmfzKrxtyJdftyaD24BKxac2Gxw/e9k2Y3vV11W6tJ
ILrrj8/L/bwmtk2eKGEGUjjY1PGsl1F0oUT+Hnz+JakM93aRZClQAVEKUGWFMZLN
4JB2xSGATz8kmUY+P+CDEcq3zLjJNDWJeA1DidKJgIpFMsElFBAE2cY8bffoIiFu
pXsQJ9NY+LWq7oAiRQvN8Ka5qYsDpAZAaL636h6Q6l9U/X/hDRj07fiwGIQhmEA6
ErVOPYiHqyKJChvqb+wXI5Ecjv5/8tLb8U794TfoBiu83EmLdHHtoVSZ/GCH9vai
w7MrXciIi/ZAu3Y8RlVKyceG51+LIVnEwhSnUBcVsyl9tlzFzNt9nl2ZJ7/llny/
CLGss/ze5id6AuEnjsG7cZBVYVO9y2AxKR3PU6vULRSWbXa8ORlry4WPayiXCMMc
fRnBb5BD227nFe6MtTjJbeoAUU9A3mlHFLaIcgGy0a5eUS3IVIhixnglSYY51mPy
7aldifYXeRr02D/uSvi/WCOKroexLHxC2j8mdvMKSbTQAtatenTqZhK8p+wZcXyC
6HXiU+AqqQzx2HRca/vUUdMouo5+9uJdpK/4wpBlZKJBY8tgscC+ys7+g/9cfyBN
jpGjEa5BvmG8f7LgMvmjJHUNdqqc458kChQuX3HxB+1zNMzDvsY0Pdqu30t+cFJJ
9xfU2rHLTNME3kGr+FSo4Lr3ClwutUVVpQ//FBz5OqSW4ItFh6WksXSZc3xlwZw4
U58jFelGLoTjQn0u3XZH1PJZX8SvcP1MQ7yavxCzWTPXLA8XjJHHQ/fjGIzkA0q5
JLIqePg7uP8s663L9r3RqPu1Q2qoMf9CiFbMNdS13SQqdNpv9Bj5BQkbDTFcLJ1B
/lbGLPKsW2uI6g4fkhdJxVg/EiYCR2X8rii+YpT+SOTlERssY0UMuwp4s3e9ePJ5
q0HCKigCAuwR5P+aZLhtJNCFTm3XHPn9ssM6f/z0a6ZfQICVt76DDcTXy/vtGWir
+MWuph5ITR9ArL4FEJKm9giWfjEcR8+McH5NAiKfUt1TeW0eaPzAYFjpQ9rDVwOp
LFJ+q46B5H1RAHzxf2ObQwEoh0Hq21hj3u4RblgymoYs6FwllJfUVvgHWNdmP5RN
10TAZtPOvrTOlcmi1IQDoOq4YMLSKFKKsyGYM5c33c5wshS3vFCex+dmvenVL0o1
1U9I0BIxPLgudAIFSH7e5j3snrdEFWNcmcCvqgWgvNvLq1nYpo+TvPgd3foVR53m
Uq44CLyr7VYfB60KsXhhp7xSiqyAC2vuSO2+ZnrwhA+BfGRwj+4LKAlQjF6+7r1q
vMse+RaO8ZOqZHajuZ2bEi23C/KPfxMPo9gugTMP3O/3QbFuOEG3Js9t5YunMhoe
BZSkNDNA7i6idS6M+KicpGo6nfE0iQ1OzYMGf1qBxh14xSTbcCoUrzw5eihs7FV3
NqpgUdMb7mfpsEtiNlik2ux9cS3SSWP0Gu495trN4Pg=
`pragma protect end_protected

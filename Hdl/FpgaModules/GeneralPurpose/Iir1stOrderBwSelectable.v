`timescale 1ns/100ps

module Iir1stOrderBwSelectable 
#(
      parameter g_InputBits = 8, 
      parameter g_OutputBits = 16,
      parameter g_BW0BitShits = 3,
      parameter g_BW1BitShits = 4,
      parameter g_BW2BitShits = 5,
      parameter g_BW3BitShits = 6,
      parameter g_BW4BitShits = 7,
      parameter g_BW5BitShits = 8,
      parameter g_BW6BitShits = 9,
      parameter g_BW7BitShits = 10
)(
      input Clk_ik,
      input Rst_ir,
      input [2:0] BWSelection_ib3, 
      input NewDataIn_i,
      input signed [g_InputBits-1:0] Input_ib,
      output reg NewDataOut_o,
      output signed [g_OutputBits -1 :0] Output_ob
);

localparam c_StoringBits = g_OutputBits + g_BW7BitShits;

wire signed [g_OutputBits -1 : 0] Diff_ab = (Input_ib <<<(g_OutputBits-g_InputBits)) - Output_ob; 

reg signed [c_StoringBits - 1 :0] Storing_qb = 'h0;

assign Output_ob = Storing_qb[c_StoringBits - 1 : g_BW7BitShits];


reg FirstSample = 1'b1; 
      
always @(posedge Clk_ik) begin
      if (Rst_ir) begin
            NewDataOut_o      <= #1 1'b0;
            Storing_qb        <= #1 'h0;
            FirstSample       <= #1 1'b1;
      end else begin
            NewDataOut_o <= #1 NewDataIn_i;
            if (NewDataIn_i) begin
                  FirstSample <= #1 1'b0;
                  if (FirstSample)
                     Storing_qb     <= #1 Input_ib <<<(g_OutputBits-g_InputBits+g_BW7BitShits); 
                  else case (BWSelection_ib3)
                    4'd0 : Storing_qb <= #1 Storing_qb + ((Diff_ab<<<g_BW7BitShits) >>> g_BW0BitShits);
                    4'd1 : Storing_qb <= #1 Storing_qb + ((Diff_ab<<<g_BW7BitShits) >>> g_BW1BitShits);
                    4'd2 : Storing_qb <= #1 Storing_qb + ((Diff_ab<<<g_BW7BitShits) >>> g_BW2BitShits);
                    4'd3 : Storing_qb <= #1 Storing_qb + ((Diff_ab<<<g_BW7BitShits) >>> g_BW3BitShits);
                    4'd4 : Storing_qb <= #1 Storing_qb + ((Diff_ab<<<g_BW7BitShits) >>> g_BW4BitShits);
                    4'd5 : Storing_qb <= #1 Storing_qb + ((Diff_ab<<<g_BW7BitShits) >>> g_BW5BitShits);
                    4'd6 : Storing_qb <= #1 Storing_qb + ((Diff_ab<<<g_BW7BitShits) >>> g_BW6BitShits);
                    4'd7 : Storing_qb <= #1 Storing_qb + ((Diff_ab<<<g_BW7BitShits) >>> g_BW7BitShits);
                    default : Storing_qb <= #1 Storing_qb + ((Diff_ab<<<g_BW7BitShits) >>> g_BW0BitShits);
                  endcase
            end
      end
end
endmodule
      

 


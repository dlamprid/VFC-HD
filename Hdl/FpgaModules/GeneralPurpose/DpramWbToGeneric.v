//============================================================================================\\
//##################################   Module Information   ##################################\\
//============================================================================================\\
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: DpramWbToGeneric.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 10/06/16      1.1          M. Barros Marin    Fixed bug in WB write.               
//     - 01/02/16      1.0          M. Barros Marin    First module definition.               
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor:  Agnostic
//     - Model:   Agnostic
//
// Description:
//
//     DPRAM with Wishbone Write interface and Generic Read interface.                                                                                                     
//                                                                                                   
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

`timescale 1ns/100ps 

module DpramWbToGeneric 
//====================================  Global Parameters  ===================================\\   
#(  parameter g_AddrWidth = 10)
//========================================  I/O ports  =======================================\\
(

    //==== Wishbone interface (Read) ====\\ 
    
    input                    Rst_ir,
    input                    Clk_ik,
    input                    Cyc_i,
    input                    Stb_i,
    input                    We_i,
    input  [31:0]            Adr_ib32,
    input  [31:0]            Dat_ib32,
    output                   Ack_o,

    //==== Generic interface  (Write) ====\\
    
    input                    RdClk_ik, 
    input                    RdRst_ir, 
    input                    RdEn_i,  
    input  [g_AddrWidth-1:0] RdAddr_ib,
    output [31:0]            RdData_ib32
    
); 
//=======================================  Declarations  =====================================\\    

//==== Wires & Regs ====\\ 

wire WriteEn;        
    
//=======================================  User Logic  =======================================\\    

// Wishbone interface:
assign WriteEn = Cyc_i && Stb_i && We_i;
assign Ack_o   = Cyc_i && Stb_i;
    
// Generic DPRAM:
generic_dpram #(
    .aw             (g_AddrWidth),
    .dw             (32))
i_Dpram (    
    .rclk           (RdClk_ik),  
    .rrst           (RdRst_ir),  
    .rce            (1'b1),   
    .oe             (RdEn_i),    
    .raddr          (RdAddr_ib), 
    .do             (RdData_ib32),    
    //---
    .wclk           (Clk_ik),  
    .wrst           (Rst_ir),  
    .wce            (1'b1),    
    .we             (WriteEn),    
    .waddr          (Adr_ib32[g_AddrWidth-1:0]), 
    .di             (Dat_ib32));

endmodule
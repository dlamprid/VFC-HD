//============================================================================================\\
//##################################   Module Information   ##################################\\
//============================================================================================\\
//                                                                                         
// Company: CERN (BE-BI) 
//                                                        
// File Name: DpramGenericToWb.v  
//
// File versions history:
//
//       DATE          VERSION      AUTHOR             DESCRIPTION
//     - 01/02/16      1.0          M. Barros Marin    First module definition.               
//
// Language: Verilog 2005                                                              
//                                                                                                   
// Targeted device:
//
//     - Vendor:  Agnostic
//     - Model:   Agnostic
//
// Description:
//
//     DPRAM with Generic Write interface and Wishbone Read interface.                                                                                                     
//                                                                                                   
//============================================================================================\\
//############################################################################################\\
//============================================================================================\\

`timescale 1ns/100ps 

module DpramGenericToWb 
//====================================  Global Parameters  ===================================\\   
#(  parameter g_AddrWidth = 10)
//========================================  I/O ports  =======================================\\
(
    //==== Generic interface (Write) ====\\
    
    input                         WrClk_ik, 
    input                         WrRst_ir, 
    input                         WrEn_i,  
    input       [g_AddrWidth-1:0] WrAddr_ib,
    input       [31:0]            WrData_ib32,   
    
    //==== Wishbone interface (Read) ====\\ 
    
    input                         Rst_ir,
    input                         Clk_ik,
    input                         Cyc_i,
    input                         Stb_i,
    input       [g_AddrWidth-1:0] Adr_ib,
    output  reg [31:0]            Dat_oqb32,
    output  reg                   Ack_oq
    
); 
//=======================================  Declarations  =====================================\\    

//==== Wires & Regs ====\\ 

wire [31:0] Dat_b32;        
    
//=======================================  User Logic  =======================================\\    

// Generic DPRAM:
generic_dpram #(
    .aw             (g_AddrWidth),
    .dw             (32))
i_Dpram (    
    .rclk           (Clk_ik),  
    .rrst           (Rst_ir),  
    .rce            (1'b1),   
    .oe             (1'b1),    
    .raddr          (Adr_ib), 
    .do             (Dat_b32),    
    //---
    .wclk           (WrClk_ik),  
    .wrst           (WrRst_ir),  
    .wce            (1'b1),    
    .we             (WrEn_i),    
    .waddr          (WrAddr_ib), 
    .di             (WrData_ib32));

// Wisghbone interface:
always @(posedge Clk_ik)
    if (Rst_ir) begin
        Ack_oq          <= #1  1'b0;
        Dat_oqb32       <= #1 32'h0;
    end else begin
        Ack_oq          <= #1 Cyc_i && Stb_i;
        Dat_oqb32       <= #1 Dat_b32;
    end

endmodule
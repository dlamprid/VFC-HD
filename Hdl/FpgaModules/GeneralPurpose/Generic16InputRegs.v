`timescale 1ns/100ps 

// Added by M. Barros Marin (10/05/16)

module Generic16InputRegs (
	input	          Clk_ik,
    input             Rst_irq,
    input             Cyc_i,
    input             Stb_i,
    input      [ 3:0] Adr_ib4,
    output reg [31:0] Dat_oab32,
    output reg        Ack_oa,
    input      [31:0] Reg0Value_ib32,
    input      [31:0] Reg1Value_ib32,
    input      [31:0] Reg2Value_ib32,
    input      [31:0] Reg3Value_ib32,
    input      [31:0] Reg4Value_ib32,
    input      [31:0] Reg5Value_ib32,
    input      [31:0] Reg6Value_ib32,
    input      [31:0] Reg7Value_ib32,
    input      [31:0] Reg8Value_ib32,
    input      [31:0] Reg9Value_ib32,
    input      [31:0] Reg10Value_ib32,
    input      [31:0] Reg11Value_ib32,
    input      [31:0] Reg12Value_ib32,
    input      [31:0] Reg13Value_ib32,
    input      [31:0] Reg14Value_ib32,
    input      [31:0] Reg15Value_ib32
);
	
always @(posedge Clk_ik)
    if (Rst_irq) begin
        Dat_oab32                  <= #1 32'h0;
        Ack_oa                     <= #1  1'b0;
    end else begin
        if (~Ack_oa) begin
            case (Adr_ib4)
                4'b0000: Dat_oab32 <= #1 Reg0Value_ib32;
                4'b0001: Dat_oab32 <= #1 Reg1Value_ib32;
                4'b0010: Dat_oab32 <= #1 Reg2Value_ib32;
                4'b0011: Dat_oab32 <= #1 Reg3Value_ib32;
                4'b0100: Dat_oab32 <= #1 Reg4Value_ib32;
                4'b0101: Dat_oab32 <= #1 Reg5Value_ib32;
                4'b0110: Dat_oab32 <= #1 Reg6Value_ib32;
                4'b0111: Dat_oab32 <= #1 Reg7Value_ib32;
                4'b1000: Dat_oab32 <= #1 Reg8Value_ib32;
                4'b1001: Dat_oab32 <= #1 Reg9Value_ib32;
                4'b1010: Dat_oab32 <= #1 Reg10Value_ib32;
                4'b1011: Dat_oab32 <= #1 Reg11Value_ib32;
                4'b1100: Dat_oab32 <= #1 Reg12Value_ib32;
                4'b1101: Dat_oab32 <= #1 Reg13Value_ib32;
                4'b1110: Dat_oab32 <= #1 Reg14Value_ib32;
                4'b1111: Dat_oab32 <= #1 Reg15Value_ib32;
                default: Dat_oab32 <= #1 Reg0Value_ib32;
            endcase
        end
        Ack_oa                     <= #1 Stb_i && Cyc_i;
    end

endmodule
## Parent class import:
import sys
sys.path.append('../')
from Class_VfcHd_System import *

## Additional Python modules:
#import re

## Child class declaration:
class VfcHd_Base(VfcHd_System):
    
    '############################################################################################################'
    '#### Note!! This is a CHILD class of the VfcHd_System class.                                            ####'
    '####        All attributes and methods related to the Application module of the VFC-HD firmware must be ####'
    '####        declared here.                                                                              ####'
    '############################################################################################################'
    
    def __init__(self, Driver="vfc_hd_base", Lun=1, IntLevel = 3, IntVector = 0xA0, RORA="ROACK", Timeout=1000):
        VfcHd_System.__init__(self, Driver, Lun, IntLevel, IntVector, RORA, Timeout)        
        print
        print "##################################################"
        print
        AppReleaseIdBuffer = []
        AppProjecBuffer    = [[],[],[]]
        AppProject         = ""
        AppVersion         = ""
        AppDate            = ""
        print "-> Reading Application Project & Release ID..."
        AppReleaseIdBuffer = VfcHd_System.ReadMem(self, "App_ReleaseId")
        for i in range(0,3):
            for j in range(0,4):
                AppProjecBuffer[i].append(chr(int(HexN(AppReleaseIdBuffer[i])[(j*2):((j*2)+2)],16)))    
                AppProject = AppProject + AppProjecBuffer[i][j]
        print "-> - Project:   ", AppProject
        AppVersion = HexN(AppReleaseIdBuffer[3])[0:2]    
        for i in range(2,8):  
            AppDate = AppDate + str(int(HexN(AppReleaseIdBuffer[3])[i],16))
        print "-> - Release ID:", AppVersion + " (" + AppDate[0:2] + "/" + AppDate[2:4] + "/" + AppDate[4:6] + ")"
        print
        print "##################################################"
        print
    
    # Application Project & Release ID access: 
    def ReadAppReleaseId(self):
        return VfcHd_System.ReadMem(self, "App_ReleaseId")
    
    # Control Register access:   
    def WriteCtrlReg(self, DataLw, Offset=0):
        return VfcHd_System.Write(self, "App_CtrlReg", DataLw, Offset)
        
    def WriteMemCtrlReg(self, DataLw, Offset=0, Dma=0, Time=0):
        return VfcHd_System.WriteMem(self, "App_CtrlReg",DataLw, Offset, Dma, Time)
        
    def ReadCtrlReg(self, Offset=0):
        return VfcHd_System.Read(self, "App_CtrlReg", Offset)
        
    def ReadMemCtrlReg(self, NbLongWords=-1, Offset=0, Dma=0, Time=0):
        return VfcHd_System.ReadMem(self, "App_CtrlReg", NbLongWords, Offset, Dma, Time)           
        
    def RdModWrCtrlReg(self, DataLw, Mask=0xFFFFFFFF, Offset=0):         
        return VfcHd_System.RdModWr(self, "App_CtrlReg", DataLw, Mask, Offset)         
        
    # Status Register access:    
    def ReadStatReg(self, Offset=0):
        return VfcHd_System.Read(self, "App_StatReg", Offset)
        
    def ReadMemStatReg(self, NbLongWords=-1, Offset=0, Dma=0, Time=0):
        return VfcHd_System.ReadMem(self, "App_StatReg", NbLongWords, Offset, Dma, Time)
        
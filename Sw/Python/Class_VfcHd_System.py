import sys
sys.path.append('/usr/local/encore')

import os
import ctypes
import encoreio
import thread
import numpy as np
from   array import *
from   time  import *

def HexN(Value, N=8):
    return hex(Value).lstrip('0x').rstrip('L').zfill(N)

class VfcHd_System:

    '############################################################################################################'
    '####                                                                                                    ####'
    '####                              **** PLEASE, DO NOT MODIFY THIS FILE ****                             ####'
    '####                                                                                                    ####'
    '#### Note!! This is the PARENT class containing all attributes and methods related to the System module ####'
    '####        of the VFC-HD firmware.                                                                     ####'
    '####        All attributes and methods related to the Application module of the VFC-HD firmware must be ####'
    '####        declared in a separated CHILD class.                                                        ####'
    '####                                                                                                    ####'
    '############################################################################################################'

    def __init__(self, Driver="vfc_hd_base", Lun=1, IntLevel = 3, IntVector = 0xA0, RORA="ROACK", Timeout=1000):
        self.Driver = Driver
        self.Lun = Lun
        self.VfcHd = encoreio.Module.get_instance(Driver, Lun)        
        SysReleaseIdBuffer = self.VfcHd.read("Sys_ReleaseId")[0]
        SysVersion = HexN(SysReleaseIdBuffer)[0:2]    
        SysDate = ""
        print
        for i in range(2,8):  
            SysDate = SysDate + str(int(HexN(SysReleaseIdBuffer)[i],16))        
        print "-> VFC-HD System has -Release ID- :", SysVersion + " (" + SysDate[0:2] + "/" + SysDate[2:4] + "/" + SysDate[4:6] + ")"
        self.VfcHd.write("Sys_IntConfig", (((IntLevel<<20)+(IntVector<<12)),)) 
        self.FirstWaitInterrupt = 1;
        self.LastInterrupt = 0
        self.VfcHd.timeout(Timeout)
        self.WaitingInterrupt = False
        self.ReceivedInterrupts = 0
        self.ExpectedInterrupts = 0
        self.RORA = (RORA == "RORA")
        self.CheckIntSourceManual = True
    
    def Write(self, RegName, DataLw, Offset=0):
        Am    = -1 # Default
        Start =  Offset
        To    =  Offset + 1
        Dma   =  0 # Disabled
        Time  =  0 # Transaction time report disabled
        return self.VfcHd.write(RegName, (DataLw, ), Am, Start, To, Dma, Time)
    
    def WriteMem(self, RegName, DataLw, Offset=0, Dma=0, Time=0):
        DataLwTuple =  tuple(DataLw)
        Am          = -1 # Default
        Start       =  Offset
        To          =  Start + len(DataLwTuple)
        return self.VfcHd.write(RegName, DataLwTuple, Am, Start, To, Dma, Time)    
        
    def Read(self, RegName, Offset=0):
        Am    = -1 # Default    
        Start =  Offset
        To    =  Offset + 1
        Dma   =  0 # Disabled
        Time  =  0 # Transaction time report disabled    
        return self.VfcHd.read(RegName, Am, Start, To, Dma, Time)[0]
        
    def ReadMem(self, RegName, NbLongWords=-1, Offset=0, Dma=0, Time=0):    
        Am     = -1  # Default
        Start  = Offset
        if NbLongWords == -1:
            To = -1 # Full memory block
        else:
            To = Start + NbLongWords
        return self.VfcHd.read(RegName, Am, Start, To, Dma, Time)    
        
    def RdModWr(self, RegName, DataLw, Mask=0xFFFFFFFF, Offset=0): # In the Mask, set to 1 the bits to be modified
        Am                   = -1 # Default    
        Start                =  Offset
        To                   =  Offset + 1
        Dma                  =  0 # Disabled
        Time                 =  0 # Transaction time report disabled    
        if Mask == 0xFFFFFFFF:
            NewDataLw        = DataLw
        else:
            PrevDataLw       =  self.VfcHd.read(RegName, Am, Start, To, Dma, Time)[0]        
            PrevDataLwMasked =  PrevDataLw & (~Mask)
            DataLwMasked     =  DataLw     &  Mask
            NewDataLw        =  PrevDataLwMasked + DataLwMasked
        return self.VfcHd.write(RegName, (NewDataLw, ), Am, Start, To, Dma, Time) 
        
    # Interrupt functions: 
    def EnableInterrupt(self, Mask=0):
        Value = self.Read("Sys_IntConfig")  
        Value = Value&0xFFFFF000
        Value = Value + 0x800 + Mask  
        if (self.RORA):
            Value = Value + 0x100          
        self.Write("Sys_IntConfig", Value)
    def DisableInterrupts(self):
        Value = self.Read("Sys_IntConfig")  
        Value = Value&0xFFFFF000
        self.Write("Sys_IntConfig", Value) 
    def CheckForPendingInterrupts(self):
        intStatus = self.Read("Sys_IntSource") 
        while (intStatus) :
            print "VFC HD in slot", self.Lun, " @expected Interrupt ", self.ExpectedInterrupts, " FIFO was not empty: ",HexN(intStatus)     
            intStatus = self.Read("Sys_IntSource")   
    def WaitForInterrupt(self, ExpectedStatus=0):
        ReturnCode = 0   
        self.ExpectedInterrupts = self.ExpectedInterrupts + 1 
        self.WaitingInterrupt = True
        Status, irq_count, res = self.VfcHd.wait() #self.VfcHd.wait_or_pending() 
        if (res): 
            print "VFC HD in slot", self.Lun, " @expected Interrupt ", self.ExpectedInterrupts, " ERROR =====> Interrupt missing" 
            ReturnCode = 1 
        else:
            if (self.CheckIntSourceManual):
                Status =  self.Read("Sys_IntSource")  
                if ((not self.FirstWaitInterrupt)and(irq_count != self.LastInterrupt+1)):
                    print "VFC HD in slot", self.Lun, " @expected Interrupt ", self.ExpectedInterrupts, " ERROR =====> irq_count old: ", self.LastInterrupt , "irq_count: ", irq_count 
                ReturnCode = 1                    
                if ((ExpectedStatus) and (Status != ExpectedStatus)):
                    print "VFC HD in slot", self.Lun, " @expected Interrupt ", self.ExpectedInterrupts, " ERROR =====> Expected status ", ExpectedStatus," but received: ", Status  
                    ReturnCode = 1
                self.ReceivedInterrupts = self.ReceivedInterrupts + 1    
                self.FirstWaitInterrupt = 0
                self.LastInterrupt = irq_count
            self.CheckForPendingInterrupts()               
            self.WaitingInterrupt = False    
            return ReturnCode  
       
    # Basic SPI access:
    def SysSpiAccess(self, Channel, CPol, CPha, LSB1st, Lenght, Data, HalfPeriod, WaitTime):
        if Channel>=32 or Channel<0 :
            print "Error: SPI Channel out of range"
        elif CPol>1 or CPol<0 : 
            print "Error: SPI CPol can be only 0 or 1"
        elif CPha>1 or CPha<0 :
            print "Error: SPI CPha can be only 0 or 1"
        elif LSB1st>1 or LSB1st<0 :
            print "Error: SPI LSB1st can be only 0 or 1"
        elif Lenght>=2**12 or Lenght<0 :
            print "Error: SPI Length out of range"
        elif HalfPeriod>=2**16 or HalfPeriod<0 :
            print "Error:SPI  Half period out of range"
        elif WaitTime>=2**16 or WaitTime<0 :
            print "Error: SPI Half period out of range"
        elif Data<0 :
            print "SPI data cannot be negative"
        else :
            SpiStatusReg = self.Read("Sys_SpiStatus") 
            SpiState = (SpiStatusReg/2**16)&0x7 
            SpiFree = SpiState==4 or SpiState==0
            while SpiFree==False :
                SpiStatusReg = self.Read("Sys_SpiStatus")
                SpiState = (SpiStatusReg/2**16)&0x7 
                SpiFree = SpiState==4 or SpiState==0           
            Config1 = CPol*2**31 + CPha*2**30 + LSB1st*2**29 + Channel*2**16 + Lenght 
            Config2 = HalfPeriod*2**16 + WaitTime
            self.Write("Sys_SpiConfig1", Config1)
            self.Write("Sys_SpiConfig2", Config2)
            Cycles=int(np.ceil(Lenght/32.0))
            ReadValue = 0
            if not LSB1st :
                if Lenght%32: Data = Data<<(32-Lenght%32)
            for i in range(Cycles):
                if LSB1st :
                    DataToSend = Data%2**32
                    self.Write("Sys_SpiShiftOut", DataToSend)
                    Data = Data/2**32
                else:
                    DataToSend = Data/2**(32*(Cycles-i-1))
                    self.Write("Sys_SpiShiftOut", DataToSend)
                    Data = Data%2**(32*(Cycles-(i+1)))
                PartialShift = self.Read("Sys_SpiShiftIn")
                SpiStatusReg = self.Read("Sys_SpiStatus")
                SpiState = (SpiStatusReg/2**16)&0x7 
                SpiFree = SpiState==4 or SpiState==0
                while SpiFree==False :
                    SpiStatusReg = self.Read("Sys_SpiStatus")
                    SpiState = (SpiStatusReg/2**16)&0x7 
                    SpiFree = SpiState==4 or SpiState==0
                PartialShift = self.Read("Sys_SpiShiftIn")
                if LSB1st :
                    if i==Cycles-1 :
                        PartialShift = PartialShift>>(Cycles*32-Lenght)
                    ReadValue = ReadValue + (PartialShift<<(32*i))
                else :
                    if i==Cycles-1 :
                        Residual = Lenght-(Cycles-1)*32
                        ReadValue = (ReadValue<<Residual)+PartialShift
                    else :
                        ReadValue = (ReadValue<<32)+PartialShift                    
        return ReadValue

    # Vadj control:
    def SetVadjWiper(self, Value):
        self.SysSpiAccess(2, 1, 1, 0, 24, Value*2**6, 32, 0)
    def SaveVadjWiper(self):
        self.SysSpiAccess(2, 1, 1, 0, 8, 0x20, 32, 0) 
    def LoadVadjWiper(self):
        self.SysSpiAccess(2, 1, 1, 0, 8, 0x30, 32, 0)
    
    # ADC access:
    def ReadAdc(self, Channel=-1):
        Coef = [2, 2, 2, 2, 2, 2, 2, 5.7]
        if Channel in range(0, 8):
            Value = self.SysSpiAccess(3, 1, 1, 0, 16, (Channel << 11), 50, 0) #writing the address for the next readout
            Value = self.SysSpiAccess(3, 1, 1, 0, 16, (Channel << 11), 50, 0) #actual reading
            Value = (Value & 0xfff)/4095.*2.5*Coef[Channel]
            return Value
        else:
            ValueArray = []
            Value = self.SysSpiAccess(3, 1, 1, 0, 16, 0, 50, 0) # writing 0 in the address for the next read out
            for Channel in range (1, 8):
                Value = self.SysSpiAccess(3, 1, 1, 0, 16, (Channel << 11), 50, 0)
                Value = (Value & 0xfff)/4095.*2.5*Coef[Channel-1]
                ValueArray.append(Value)
            Value = self.SysSpiAccess(3, 1, 1, 0, 16, 0, 50, 0)
            Value = (Value & 0xfff)/4095.*2.5*Coef[7]
            ValueArray.append(Value)        
            return  ValueArray 
                    
    # 1-Wire interface   
    def Init1Wire(self):
        while (self.Read("Sys_UidStatus")&0x0F000000)!=0 :
            pass 
        self.Write("Sys_UidCommand", 0x01000000)
        while (self.Read("Sys_UidStatus")&0x0F000000)!=0 :
            pass
        if self.Read("Sys_UidStatus")&0x80000000==0x80000000:  #checking if the 1wire chip answered to the init
            return True
        else :
            return False

    def OneWireWrite(self, Lenght, Data):
        if Lenght>16:
            print "Max is 16 bit"
            return False
        else:
            while (self.Read("Sys_UidStatus")&0x0F000000)!=0 :
                pass 
            RegValue = 0x02000000 | Lenght << 16 | Data 
            self.Write("Sys_UidCommand", RegValue)
            while (self.Read("Sys_UidStatus")&0x0F000000)!=0 :
                pass
            return True
    
    def OneWireRead(self, Lenght):
        if Lenght>16:
            print "Max is 16 bit"
            return False
        else:
            while (self.Read("Sys_UidStatus")&0x0F000000)!=0 :
                pass
            RegValue = 0x03000000 | Lenght << 16
            self.Write("Sys_UidCommand", RegValue)
            while (self.Read("Sys_UidStatus")&0x0F000000)!=0 :
                pass        
            Value =  self.Read("Sys_UidStatus")&0x0000FFFF;                    
            return Value
                
    def ReadUniqueId(self):
        ChipDetected = self.Init1Wire()
        if ChipDetected==False:
            print "No chip detected on the bus"
            return 0
        else:
            self.OneWireWrite(8, 0x33)
            Value = self.OneWireRead(16)&0x0000FFFF
            Value = Value | (self.OneWireRead(16)&0x0000FFFF)<<16      
            Value = Value | (self.OneWireRead(16)&0x0000FFFF)<<32
            Value = Value | (self.OneWireRead(16)&0x0000FFFF)<<48
            return Value
              
    def ConvertT(self):
        ChipDetected = self.Init1Wire()
        if ChipDetected==False:
            print "No chip detected on the bus"
            return 0
        else:    
            self.OneWireWrite(8, 0xCC)
            self.OneWireWrite(8, 0x44)
            Value = self.OneWireRead(1)&0x00000001
            while  Value==1 :
                Value = self.OneWireRead(1)&0x00000001
                              
    def ReadTemp(self):
        self.ConvertT()
        ChipDetected = self.Init1Wire()
        if ChipDetected==False:
            print "No chip detected on the bus"
            return 0
        else:
            self.OneWireWrite(8, 0xCC)
            self.OneWireWrite(8, 0xBE)
            Value = (self.OneWireRead(16)&0x0000FFFF)/16.        
            return Value    

    # I2C access and WR PROM control:
    def I2cPromStartBit(self):
        self.Write("Sys_WrPrmCommand", 0x01000000)
        while (self.Read("Sys_WrPrmStatus") &0xff000000):
            pass

    def I2cPromSelectSlave(self, ReadWrite, SlaveAddress=0x51):
        if (ReadWrite == 'read') :
            ReadWriteBit = 1
        elif  (ReadWrite == 'write'):
            ReadWriteBit = 0
        else :
            print "Only the options 'read' and 'write' are valid"
            return "ERROR"
        Data = 0x02000000 + (SlaveAddress<<1) +  ReadWriteBit
        self.Write("Sys_WrPrmCommand", Data)
        while (self.Read("Sys_WrPrmStatus") &0xff000000):
            pass        
        if (self.Read("Sys_WrPrmStatus") &0x100):
            print "No module acknowledge the I2C transaction sending the slave address"
            return 1
        else: 
            return 0

    def I2cPromWriteByte(self, Byte):
        self.Write("Sys_WrPrmCommand", 0x02000000+Byte) #Sending the byte address
        while (self.Read("Sys_WrPrmStatus") &0xff000000):
            pass
        if (self.Read("Sys_WrPrmStatus") &0x100):
            print "No module acknowledge the I2C transaction on the byte address"
            return 1
        else:
            return 0
    
    def I2cPromReadByte(self, Last=0):
        if (Last==1 or Last==0):
            self.Write("Sys_WrPrmCommand", 0x03000000+Last)
            while (self.Read("Sys_WrPrmStatus") &0xff000000):
                pass
        else:
            print "only 0 and 1 are valid options"
            return "ERROR"
        Data = self.Read("Sys_WrPrmStatus") &0xff
        return Data
    
    def I2cPromStopBit(self):
        self.Write("Sys_WrPrmCommand", 0x04000000)
        while (self.Read("Sys_WrPrmStatus") &0xff000000):
            pass        
            
    def WrPromWrite(self, Data, Address):
        self.I2cPromStartBit()
        self.I2cPromSelectSlave('write')
        self.I2cPromWriteByte(Address>>8)
        self.I2cPromWriteByte(Address%255)
        self.I2cPromWriteByte(Data)
        self.I2cPromStopBit()
    
    def WrPromRead(self, Address):
        self.I2cPromStartBit()
        self.I2cPromSelectSlave('write')
        self.I2cPromWriteByte(Address>>8)
        self.I2cPromWriteByte(Address%255)
        self.I2cPromStartBit()
        self.I2cPromSelectSlave('read')
        Data = self.I2cPromReadByte(1)
        self.I2cPromStopBit()               
        return Data   
 
    # I2C access and IOexp control:
    def I2cIoExpStartBit(self):
        self.Write("Sys_IoExpCommand", 0x01000000)
        while (self.Read("Sys_IoExpStatus") &0xff000000):
            pass

    def I2cIoExpSelectSlave(self, ReadWrite, SlaveAddress=0x0):
        if (ReadWrite == 'read') :
            ReadWriteBit = 1
        elif  (ReadWrite == 'write'):
            ReadWriteBit = 0
        else :
            print "Only the options 'read' and 'write' are valid"
            return "ERROR"
        Data = 0x02000000 + (SlaveAddress<<1) +  ReadWriteBit
        self.Write("Sys_IoExpCommand", Data)
        while (self.Read("Sys_IoExpStatus") &0xff000000):
            pass        
        if (self.Read("Sys_IoExpStatus") &0x100):
            print "No module acknowledge the I2C transaction sending the slave address"
            return 1
        else: 
            return 0

    def I2cIoExpWriteByte(self, Byte):
        self.Write("Sys_IoExpCommand", 0x02000000+Byte) #Sending the byte address
        while (self.Read("Sys_IoExpStatus") &0xff000000):
            pass
        if (self.Read("Sys_IoExpStatus") &0x100):
            print "No module acknowledge the I2C transaction on the byte address"
            return 1
        else:
            return 0
    
    def I2cIoExpReadByte(self, Last=0):
        if (Last==1 or Last==0):
            self.Write("Sys_IoExpCommand", 0x03000000+Last)
            while (self.Read("Sys_IoExpStatus") &0xff000000):
                pass
        else:
            print "only 0 and 1 are valid options"
            return "ERROR"
        Data = self.Read("Sys_IoExpStatus") &0xff
        return Data
    
    def I2cIoExpStopBit(self):
        self.Write("Sys_IoExpCommand", 0x04000000)
        while (self.Read("Sys_IoExpStatus") &0xff000000):
            pass        
            
    def IoExpWrite(self, IoExpAddr, Command, Data):
        self.I2cIoExpStartBit()
        self.I2cIoExpSelectSlave('write', IoExpAddr+0x20)
        self.I2cIoExpWriteByte(Command)
        self.I2cIoExpWriteByte(Data)
        self.I2cIoExpStopBit()
    
    def IoExpRead(self, IoExpAddr, Command):
        self.I2cIoExpStartBit()
        self.I2cIoExpSelectSlave('write', IoExpAddr+0x20)
        self.I2cIoExpWriteByte(Command)
        self.I2cIoExpStartBit()
        self.I2cIoExpSelectSlave('read', IoExpAddr+0x20)
        Data = self.I2cIoExpReadByte(1)
        self.I2cIoExpStopBit()               
        return Data 

    def I2cExpDisable(self):
        for I2CExpAddr in range(2):
            self.I2cIoExpStartBit()
            self.I2cIoExpSelectSlave('write', I2CExpAddr+0x70)
            self.I2cIoExpWriteByte(0)
            self.I2cIoExpStopBit() 
        
    def I2cExpEnableCh(self, I2CExpAddr, Channel):
        self.I2cExpDisable()
        self.I2cIoExpStartBit()
        self.I2cIoExpSelectSlave('write', I2CExpAddr+0x70)
        self.I2cIoExpWriteByte(Channel+4)
        self.I2cIoExpStopBit()    
        
    def Si57xReadReg(self, RegAddress):
        self.I2cExpDisable()
        self.I2cExpEnableCh(1, 2)        
        self.I2cIoExpStartBit()
        self.I2cIoExpSelectSlave('write', 0x55)
        self.I2cIoExpWriteByte(RegAddress)
        self.I2cIoExpStartBit()
        self.I2cIoExpSelectSlave('read', 0x55)
        Data = self.I2cIoExpReadByte(1)
        self.I2cIoExpStopBit()               
        return Data   

    def Si57xWriteReg(self, Address, Data):
        self.I2cExpDisable()
        self.I2cExpEnableCh(1, 2)      
        self.I2cIoExpStartBit()
        self.I2cIoExpSelectSlave('write', 0x55)
        self.I2cIoExpWriteByte(Address)
        self.I2cIoExpWriteByte(Data)
        self.I2cIoExpStopBit()

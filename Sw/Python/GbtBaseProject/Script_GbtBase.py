##============================================================================================##
####################################   Script Information   ####################################
##============================================================================================##
##                                                                                               
## Company: CERN (<Department/Group/Section>)                                                     
##
## File Name: <File Name>
##                                                                                                          
## File versions history:
##
##       DATE          VERSION      AUTHOR      DESCRIPTION
##     - <dd/mm/yy>    <Version>    <Author>    - <Description>.
##     - <dd/mm/yy>    <Version>    <Author>    - First script definition.                                                  
##
## Python Version: 2.7.11                                                                          
##                                                                                                                  
## Description:                                                                                             
## 
##     <Description>    
##                                                                                                                  
##============================================================================================##
##============================================================================================##

## Importing Application specific VFC-HD class:
from Class_VfcHd_GbtBase import *

## Opening VME connection with VFC-HD:
f = open('./VfcHdSetup.dat', 'r')
Driver = f.readline().strip('\r\n')
Lun    = int(f.readline())
f.close()
VfcHd = VfcHd_GbtBase(Driver, Lun, 3, 164, "ROACK", 200)

##============================================================================================##
##======================================== Script Body =======================================## 
##============================================================================================##

Args      = sys.argv
ArgLength = len(Args)

ReadMemBuffer  = []
WriteMemBuffer = []

if ArgLength == 2:     

    # Commment: The argument with the Ref PLL settings file name is implicitly passed
    #           to "Script_PllRefSetting.py" when executed with the command "execfile()".
    execfile("Script_PllRefSetting.py") 

else:

    print "-> Memory read to GBT Status registers..."   
    ReadMemBuffer = VfcHd.ReadMemGbtStatReg()
    for i in range(0,len(ReadMemBuffer)):
        print "-> Content of GBT Status  register %d: %s" % (i, HexN(ReadMemBuffer[i]))
    print "->"
    print "-> Memory read to GBT Control register..."
    ReadMemBuffer = VfcHd.ReadMemGbtCtrlReg()
    for i in range(0,len(ReadMemBuffer)):
        print "-> Content of GBT Control register %d: %s" % (i, HexN(ReadMemBuffer[i]))    
    print
    
#    print "-> Single write (0x0000000F) to GBT Control register 1..."
#    VfcHd.WriteGbtCtrlReg(0x0000000F, 1)
#    print "-> Wait for 1 second..."
#    sleep(1)
#    print "-> Single write (0x000000FF) to GBT Control register 0..."
#    VfcHd.WriteGbtCtrlReg(0x000000FF, 0)
#    print "-> Wait for 1 second..."
#    sleep(1)
#    print "-> Single write (0x00000000) to GBT Control register 0..."
#    VfcHd.WriteGbtCtrlReg(0x00000000, 0)
#    print "-> Wait for 1 second..."
#    sleep(1)
#    print "-> Single write (0x00040000) to GBT Control register 0..."
#    VfcHd.WriteGbtCtrlReg(0x00040000, 0)
#    print "-> Wait for 1 second..."
#    sleep(1)
#    print "-> Single write (0x07FC0000) to GBT Control register 0..."
#    VfcHd.WriteGbtCtrlReg(0x07FC0000, 0)
#    print "-> Wait for 1 second..."
#    sleep(1)
#    print "-> Single write (0x00040000) to GBT Control register 0..."
#    VfcHd.WriteGbtCtrlReg(0x00040000, 0)
#    print "-> Wait for 1 second..."
#    sleep(1)
    
#    print "-> Memory read to GBT Status registers..."   
#    ReadMemBuffer = VfcHd.ReadMemGbtStatReg()
#    for i in range(0,len(ReadMemBuffer)):
#        print "-> Content of GBT Status  register %d: %s" % (i, HexN(ReadMemBuffer[i]))
#    print "->"
#    print "-> Memory read to GBT Control register..."
#    ReadMemBuffer = VfcHd.ReadMemGbtCtrlReg()
#    for i in range(0,len(ReadMemBuffer)):
#        print "-> Content of GBT Control register %d: %s" % (i, HexN(ReadMemBuffer[i]))    
#    print    
    

#print "-> Single read & write example:"
#print "-> ----------------------------"
#print "->"
#print "-> Single read to Control register 0..."
#print "-> Control register 0 content:", HexN(VfcHd.ReadCtrlReg())
#print "-> Single write (0x01234567) to Control register 0..."
#VfcHd.WriteCtrlReg(0x01234567)
#print "-> Single read to Control register 0..."
#print "-> Control register 0 content:", HexN(VfcHd.ReadCtrlReg())
#print "->"
#print "-> Single read to Control register 2..."
#print "-> Control register 2 content:", HexN(VfcHd.ReadCtrlReg(2))
#print "-> Single write (0x89ABCDEF) to Control register 2..."
#VfcHd.WriteCtrlReg(0x89ABCDEF, 2)
#print "-> Single read to Control register 2..."
#print "-> Control register 2 content:", HexN(VfcHd.ReadCtrlReg(2))
#print
#print "-> Memory read & write example:"
#print "-> ----------------------------"
#print "->"
#print "-> Memory read to Status registers..."   
#ReadMemBuffer = VfcHd.ReadMemStatReg()
#for i in range(0,len(ReadMemBuffer)):
#    print "-> Content of  Status register %d: %s" % (i, HexN(ReadMemBuffer[i]))
#print "->"
#print "-> Memory read to Control register..."
#ReadMemBuffer = VfcHd.ReadMemCtrlReg()
#for i in range(0,len(ReadMemBuffer)):
#    print "-> Content of Control register %d: %s" % (i, HexN(ReadMemBuffer[i]))    
#print "-> Memory write [0x11111111,0x22222222,0x33333333,0x44444444] to Control registers..."
#WriteMemBuffer = [0x11111111,0x22222222,0x33333333,0x44444444]
#VfcHd.WriteMemCtrlReg(WriteMemBuffer)
#print "-> Memory read to Control register..."
#ReadMemBuffer = VfcHd.ReadMemCtrlReg()
#for i in range(0,len(ReadMemBuffer)):
#    print "-> Content of Control register %d: %s" % (i, HexN(ReadMemBuffer[i]))
#print
#print "-> Block read & write example:"
#print "-> ---------------------------"
#print "->"
#print "-> Block read to Status registers 1 & 2..."   
#ReadMemBuffer = VfcHd.ReadMemStatReg(2, 1)
#for i in range(0,len(ReadMemBuffer)):
#    print "-> Content of  Status register %d: %s" % ((i+1), HexN(ReadMemBuffer[i]))
#print "->"
#print "-> Block read to Control registers 2 & 3..."
#ReadMemBuffer = VfcHd.ReadMemCtrlReg(2, 2)
#for i in range(0,len(ReadMemBuffer)):
#    print "-> Content of Control register %d: %s" % ((i+2), HexN(ReadMemBuffer[i]))    
#print "-> Block write [0x00112233,0x44556677] to Control registers 2 & 3..."
#WriteMemBuffer = [0x00112233,0x44556677]
#VfcHd.WriteMemCtrlReg(WriteMemBuffer, 2)
#print "-> Block read to Control registers 2 & 3..."
#ReadMemBuffer = VfcHd.ReadMemCtrlReg(2, 2)
#for i in range(0,len(ReadMemBuffer)):
#    print "-> Content of Control register %d: %s" % ((i+2), HexN(ReadMemBuffer[i]))
#print
#print "-> Read-Modify-Write (RmW) example:"
#print "-> --------------------------"
#print "->"
#print "-> Memory write [0xAAAAAAAA,0xBBBBBBBB,0xCCCCCCCC,0xDDDDDDDD] to Control registers..."
#WriteMemBuffer = [0xAAAAAAAA,0xBBBBBBBB,0xCCCCCCCC,0xDDDDDDDD]
#VfcHd.WriteMemCtrlReg(WriteMemBuffer)
#print "-> RmW [0x00000000,0x000000FF] to Control register 0..."
#VfcHd.RdModWrCtrlReg(0x00000000, 0x000000FF,0)
#print "-> RmW [0x00000000,0x0000FF00] to Control register 1..."
#VfcHd.RdModWrCtrlReg(0x00000000, 0x0000FF00,1)
#print "-> RmW [0x00000000,0x00FF0000] to Control register 2..."
#VfcHd.RdModWrCtrlReg(0x00000000, 0x00FF0000,2)
#print "-> RmW [0xFFFF0000,0x00FFFF00] to Control register 3..."
#VfcHd.RdModWrCtrlReg(0xFFFF0000, 0x00FFFF00,3)
#print "->" 
#print "-> Single read to Control register 0..."
#print "-> Control register 0 content:", HexN(VfcHd.ReadCtrlReg())
#print "->"
#print "-> Single read to Control register 1..."
#print "-> Control register 1 content:", HexN(VfcHd.ReadCtrlReg(1))
#print "->"
#print "-> Single read to Control register 2..."
#print "-> Control register 2 content:", HexN(VfcHd.ReadCtrlReg(2))
#print "->"
#print "-> Single read to Control register 3..."
#print "-> Control register 3 content:", HexN(VfcHd.ReadCtrlReg(3))
#print
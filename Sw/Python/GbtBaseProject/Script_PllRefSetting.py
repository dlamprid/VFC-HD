##============================================================================================##
####################################   Script Information   ####################################
##============================================================================================##
##                                                                                               
## Company: CERN (BE/BI/QP)                                                     
##
## File Name: Script_PllRefSetting
##                                                                                                          
## File versions history:
##
##       DATE          VERSION      AUTHOR             DESCRIPTION
##     - 15/07/16      1.0          M. Barros Marin    - First script definition.                                                  
##
## Python Version: 2.7.11                                                                          
##                                                                                                                  
## Description:                                                                                             
## 
##     - Script for setting the Clock synthesizer "Pll Ref" (Si5338) of the VFC-HD.
##     - It is based on the figure 9, in page 23, of the Si5338 User Guide (Rev. 1.5 6/15).
##                                                                                                           
##============================================================================================##
##============================================================================================##

## Importing Application specific VFC-HD class:
from Class_VfcHd_GbtBase import *

## Opening VME connection with VFC-HD:
f = open('./VfcHdSetup.dat', 'r')
Driver = f.readline().strip('\r\n')
Lun    = int(f.readline())
f.close()
VfcHd = VfcHd_GbtBase(Driver, Lun, 3, 164, "ROACK", 200)

##============================================================================================##
##======================================== Script Body =======================================## 
##============================================================================================##

Args      = sys.argv
ArgLength = len(Args)

if ArgLength == 2:     
    Eof         = 0
    RegAddrBuff = []
    RegDataBuff = []
    MaskBuff    = []
    LossOfLock  = 0xFF
    TimeOut     = 0 
    print "-> Disabling all outputs (Reg230 = 0x1F)..."
    VfcHd.IoPllRefRdModWr(230, 0x1F, 0xFF)
    print "-> - Reading register %d..." % 230
    RegDataRead = HexN(VfcHd.IoPllRefRead(230))[6:8]        
    print "-> - Content of register %d: %s" % (230, RegDataRead)
    print "->"
    print "-> Pausing LOL (Reg241 = 0xE5)..."
    VfcHd.IoPllRefRdModWr(241, 0xE5, 0xFF)
    print "-> - Reading register %d..." % 241
    RegDataRead = HexN(VfcHd.IoPllRefRead(241))[6:8]        
    print "-> - Content of register %d: %s" % (241, RegDataRead)
    print "->"
    print "-> Parsing header file with configuration registers:", Args[1]
    print "-> Reading header file content..." 
    RegsConfigFile = open(Args[1], 'r')
    print "-> Obtaining: Register Address, Data and Mask iteratively..."
    while Eof == 0:
        RegsConfigLine = RegsConfigFile.readline() 
        if RegsConfigLine[0:13] == "//End of file":
            Eof = 1
            print "-> End Of File reached"            
            RegsConfigFile.close()
        elif RegsConfigLine[0] == "{":              
            RegsConfig = re.search("([a-z 0-9]*),([a-z 0-9]*),([a-z 0-9]*)", RegsConfigLine, re.I)
            RegAddr    = int(RegsConfig.group(1), 10)
            RegData    = int(RegsConfig.group(2), 16)
            Mask       = int(RegsConfig.group(3), 16)
            RegAddrBuff.append(RegAddr)
            RegDataBuff.append(RegData)
            MaskBuff.append(Mask)
        else:
            pass
    print "->"
    print "-> Writing configuration registers iteratively..."    
    for i in range(0,len(RegAddrBuff)):
        print "-> -"
        print "-> - Writing register %d: Data = %s | Mask = %s" % (RegAddrBuff[i], HexN(RegDataBuff[i])[6:8], HexN(MaskBuff[i])[6:8])
        VfcHd.IoPllRefRdModWr(RegAddrBuff[i], RegDataBuff[i], MaskBuff[i])
        print "-> - Reading register %d..." % RegAddrBuff[i]
        RegDataRead = HexN(VfcHd.IoPllRefRead(RegAddrBuff[i]))[6:8]        
        print "-> - Content of register %d:     %s" % (RegAddrBuff[i], RegDataRead)
    print "->"
    print "-> Validating input clock status (Reg218[2] = 0)..."
    while ((LossOfLock != 0x00) and (TimeOut != 0xFF)):
        RegDataRead = VfcHd.IoPllRefRead(218)
        LossOfLock  = (RegDataRead & 0x04) # Comment: Checking LOS_CLKIN
        TimeOut     = TimeOut + 1
        if TimeOut == 0xFF:
            LossOfLockStatus = "-> - Loss Of Lock Time Out!!!"
        else:
            LossOfLockStatus = "-> - Input clock present"
    print LossOfLockStatus
    print "->"
    print "-> Configuring PLL for locking (Reg49[7] = 0)..."
    VfcHd.IoPllRefRdModWr(49, 0x00, 0x80)
    VfcHd.IoPllRefWrite(49, RegDataRead)
    print "-> - Reading register %d..." % 49
    RegDataRead = HexN(VfcHd.IoPllRefRead(49))[6:8]        
    print "-> - Content of register %d: %s" % (49, RegDataRead)
    print "->"    
    print "-> Asserting soft reset (Reg246[1] = 1)..."    
    VfcHd.IoPllRefRdModWr(246, 0x02, 0xFF)    
    print "-> - Waiting 25ms for Pll Ref (Si5338) to soft reset..." 
    sleep(0.25)
    print "->"    
    print "-> Restarting Loss Of Lock (Reg49[7] = 0)..."
    VfcHd.IoPllRefRdModWr(241, 0x65, 0xFF)
    print "-> - Reading register %d..." % 241
    RegDataRead = HexN(VfcHd.IoPllRefRead(241))[6:8]        
    print "-> - Content of register %d: %s" % (RegAddrBuff[i], RegDataRead)
    print "->"
    print "-> Confirming PLL lock status (Reg218[2] = 0)..."
    LossOfLock = 0xFF
    while ((LossOfLock != 0x00) and (TimeOut != 0xFF)):
        RegDataRead = VfcHd.IoPllRefRead(218)
        LossOfLock  = (RegDataRead & 0x15) # Comment: Checking SYS_CAL, LOS_CLKIN and PLL_LOL 
        TimeOut     = TimeOut + 1
        if TimeOut == 0xFF:
            LossOfLockStatus = "-> - PLL lock Time Out!!!"
        else:
            LossOfLockStatus = "-> - PLL locked"
    print LossOfLockStatus 
    print "->" 
    print "-> Copying FCAL values to active registers..."
    print "-> -"
    print "-> - Copying from Reg235 to Reg45..."    
    RegNewData = VfcHd.IoPllRefRead(235)    
    VfcHd.IoPllRefWrite(45, RegNewData)    
    print "-> - Reading register %d..." % 45
    RegDataRead = HexN(VfcHd.IoPllRefRead(45))[6:8]        
    print "-> - Content of register %d: %s" % (45, RegDataRead)
    print "-> -" 
    print "-> - Copying from Reg236 to Reg46..."    
    RegNewData = VfcHd.IoPllRefRead(236)    
    VfcHd.IoPllRefWrite(46, RegNewData)    
    print "-> - Reading register %d..." % 46
    RegDataRead = HexN(VfcHd.IoPllRefRead(46))[6:8]        
    print "-> - Content of register %d: %s" % (46, RegDataRead)
    print "-> -"    
    print "-> - Copying Reg237[1:0] to Reg47[1:0]..."    
    RegSource  = VfcHd.IoPllRefRead(237) & 0x03
    RegDest    = VfcHd.IoPllRefRead( 47) & 0xFC
    RegNewData = RegSource + RegDest
    VfcHd.IoPllRefWrite(47, RegNewData)  
    print "-> - Reading register %d..." % 47
    RegDataRead = HexN(VfcHd.IoPllRefRead(47))[6:8]        
    print "-> - Content of register %d: %s" % (47, RegDataRead)
    print "-> -" 
    print "-> - Setting Reg47[7:2] 000101b..."    
    VfcHd.IoPllRefRdModWr(47, 0x14, 0xFC)    
    print "-> - Reading register %d..." % 47
    RegDataRead = HexN(VfcHd.IoPllRefRead(47))[6:8]        
    print "-> - Content of register %d: %s" % (47, RegDataRead)
    print "-> "    
    print "-> Setting PLL to use FCAL values (Reg49[7] = 1)..."
    VfcHd.IoPllRefRdModWr(49, 0x80, 0x80)
    print "-> - Reading register %d..." % 49
    RegDataRead = HexN(VfcHd.IoPllRefRead(49))[6:8]        
    print "-> - Content of register %d: %s" % (49, RegDataRead)
    print "->" 
    print "-> Note!! Down-Spread feature not used. Skipping this setting step..."       
    print "->"      
    print "-> Enabling output 0 (Reg230 = 0x0E)..."
    VfcHd.IoPllRefRdModWr(230, 0x0E, 0xFF)
    print "-> - Reading register %d..." % 230
    RegDataRead = HexN(VfcHd.IoPllRefRead(230))[6:8]        
    print "-> - Content of register %d: %s" % (230, RegDataRead) 
    print "->"
    print "-> End of VFC-HD PLL Ref (Si5338) setting."
    print
else:
    print
    print "-> Error!! One argument is required."
    print "-> "
    print "-> Syntax:"
    print "-> "
    print "-> Script_PllRefSetting.py <si5338_regs_config_file.h>"
    print "-> " 
    print "-> (Note!! The registers configuration file is a C/C++ header"
    print "->         file generated using \"ClockBuilder Desktop\"." 
    print "-> " 
    print
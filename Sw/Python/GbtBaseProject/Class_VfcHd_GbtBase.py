## Parent class import:
import sys
sys.path.append('../')
from Class_VfcHd_System import *

## Additional Python modules:
import re

## Child class declaration:
class VfcHd_GbtBase(VfcHd_System):
    
    '############################################################################################################'
    '#### Note!! This is a CHILD class of the VfcHd_System class.                                            ####'
    '####        All attributes and methods related to the Application module of the VFC-HD firmware must be ####'
    '####        declared here.                                                                              ####'
    '############################################################################################################'
    
    def __init__(self, Driver="vfc_hd_base", Lun=1, IntLevel = 3, IntVector = 0xA0, RORA="ROACK", Timeout=1000):
        VfcHd_System.__init__(self, Driver, Lun, IntLevel, IntVector, RORA, Timeout)        
        print
        print "##################################################"
        print
        AppReleaseIdBuffer = []
        AppProjecBuffer    = [[],[],[]]
        AppProject         = ""
        AppVersion         = ""
        AppDate            = ""
        print "-> Reading Application Project & Release ID..."
        AppReleaseIdBuffer = VfcHd_System.ReadMem(self, "App_ReleaseId")
        for i in range(0,3):
            for j in range(0,4):
                AppProjecBuffer[i].append(chr(int(HexN(AppReleaseIdBuffer[i])[(j*2):((j*2)+2)],16)))    
                AppProject = AppProject + AppProjecBuffer[i][j]
        print "-> - Project:   ", AppProject
        AppVersion = HexN(AppReleaseIdBuffer[3])[0:2]    
        for i in range(2,8):  
            AppDate = AppDate + str(int(HexN(AppReleaseIdBuffer[3])[i],16))
        print "-> - Release ID:", AppVersion + " (" + AppDate[0:2] + "/" + AppDate[2:4] + "/" + AppDate[4:6] + ")"
        print
        print "##################################################"
        print
    
    # Application Project & Release ID access: 
    def ReadAppReleaseId(self):
        return VfcHd_System.ReadMem(self, "App_ReleaseId")
    
    # Test registers access:      
    def WriteCtrlReg(self, DataLw, Offset=0):
        return VfcHd_System.Write(self, "App_CtrlReg", DataLw, Offset)
        
    def WriteMemCtrlReg(self, DataLw, Offset=0, Dma=0, Time=0):
        return VfcHd_System.WriteMem(self, "App_CtrlReg",DataLw, Offset, Dma, Time)
        
    def ReadCtrlReg(self, Offset=0):
        return VfcHd_System.Read(self, "App_CtrlReg", Offset)
        
    def ReadMemCtrlReg(self, NbLongWords=-1, Offset=0, Dma=0, Time=0):
        return VfcHd_System.ReadMem(self, "App_CtrlReg", NbLongWords, Offset, Dma, Time)           
        
    def RdModWrCtrlReg(self, DataLw, Mask=0xFFFFFFFF, Offset=0):         
        return VfcHd_System.RdModWr(self, "App_CtrlReg", DataLw, Mask, Offset)         
        
    def ReadStatReg(self, Offset=0):
        return VfcHd_System.Read(self, "App_StatReg", Offset)
        
    def ReadMemStatReg(self, NbLongWords=-1, Offset=0, Dma=0, Time=0):
        return VfcHd_System.ReadMem(self, "App_StatReg", NbLongWords, Offset, Dma, Time)
        
    # PLL Ref I2C control:
    def I2cPllRefStartBit(self):
        self.Write("App_PllRefCtrl", 0x01000000)
        while (self.Read("App_PllRefStat") & 0xff000000):            
            pass

    def I2cPllRefSelectSlave(self, ReadWrite):
        if (ReadWrite == 'read') :
            ReadWriteBit = 1
        elif  (ReadWrite == 'write'):
            ReadWriteBit = 0
        else :
            print "Only the options 'read' and 'write' are valid"
            return "ERROR"
        Data = 0x02000000 + (0x70<<1) +  ReadWriteBit
        self.Write("App_PllRefCtrl", Data)
        while (self.Read("App_PllRefStat") & 0xff000000):
            pass        
        if (self.Read("App_PllRefStat") & 0x100):
            print "No module acknowledge the I2C transaction sending the slave address"
            return 1
        else: 
            return 0

    def I2cPllRefWriteByte(self, DataByte):
        self.Write("App_PllRefCtrl", 0x02000000+DataByte) #Sending the byte address
        while (self.Read("App_PllRefStat") & 0xff000000):
            pass
        if (self.Read("App_PllRefStat") & 0x100):
            print "No module acknowledge the I2C transaction on the byte address"
            return 1
        else:
            return 0

    def I2cPllRefReadByte(self, Last=0):
        if (Last==1 or Last==0):
            self.Write("App_PllRefCtrl", 0x03000000+Last)
            while (self.Read("App_PllRefStat") & 0xff000000):
                pass
        else:
            print "only 0 and 1 are valid options"
            return "ERROR"
        Data = self.Read("App_PllRefStat") & 0xff
        return Data

    def I2cPllRefStopBit(self):
        self.Write("App_PllRefCtrl", 0x04000000)
        while (self.Read("App_PllRefStat") & 0xff000000):
            pass        
            
    def IoPllRefWrite(self, RegAddress, DataByte):
        self.I2cPllRefStartBit()
        self.I2cPllRefSelectSlave('write')
        self.I2cPllRefWriteByte(RegAddress)
        self.I2cPllRefWriteByte(DataByte)
        self.I2cPllRefStopBit()

    def IoPllRefRead(self, RegAddress):
        self.I2cPllRefStartBit()
        self.I2cPllRefSelectSlave('write')
        self.I2cPllRefWriteByte(RegAddress)
        self.I2cPllRefStopBit()
        self.I2cPllRefStartBit()
        self.I2cPllRefSelectSlave('read')
        DataByte = self.I2cPllRefReadByte(1)
        self.I2cPllRefStopBit()      
        return DataByte

    def IoPllRefRdModWr(self, RegAddress, DataByte, Mask=0xFF, Verbose=0):
        if Mask == 0x00:
            if Verbose == 1:
                print "-> - Register %d skipped" % RegAddress
            else:
                pass
        else:
            if Mask == 0xFF:
                NewDataByte        = DataByte 
                if Verbose == 1:
                    print "-> - Register %d fully written" % RegAddress
            else:
                PrevDataByte       = self.IoPllRefRead(RegAddress)    
                PrevDataByteMasked = PrevDataByte & ~Mask
                DataByteMasked     = DataByte     &  Mask
                NewDataByte        = PrevDataByteMasked + DataByteMasked
                if Verbose == 1:
                    print "-> - Register %d RdModWr:" % RegAddress
                    print "-> - + Data         :", HexN(DataByte)[6:8]
                    print "-> - + Mask         :", HexN(Mask)[6:8]
                    print "-> - + Previous data:", HexN(PrevDataByte)[6:8]
                    print "-> - + New data     :", HexN(NewDataByte)[6:8]
            self.IoPllRefWrite(RegAddress, NewDataByte) 
 
    # GBT registers access:      
    def WriteGbtCtrlReg(self, DataLw, Offset=0):
        return VfcHd_System.Write(self, "App_GbtCtrlReg", DataLw, Offset)
        
    def WriteMemGbtCtrlReg(self, DataLw, Offset=0, Dma=0, Time=0):
        return VfcHd_System.WriteMem(self, "App_GbtCtrlReg",DataLw, Offset, Dma, Time)
        
    def ReadGbtCtrlReg(self, Offset=0):
        return VfcHd_System.Read(self, "App_GbtCtrlReg", Offset)
        
    def ReadMemGbtCtrlReg(self, NbLongWords=-1, Offset=0, Dma=0, Time=0):
        return VfcHd_System.ReadMem(self, "App_GbtCtrlReg", NbLongWords, Offset, Dma, Time)           
        
    def RdModWrGbtCtrlReg(self, DataLw, Mask=0xFFFFFFFF, Offset=0):         
        return VfcHd_System.RdModWr(self, "App_GbtCtrlReg", DataLw, Mask, Offset)         
        
    def ReadGbtStatReg(self, Offset=0):
        return VfcHd_System.Read(self, "App_GbtStatReg", Offset)
        
    def ReadMemGbtStatReg(self, NbLongWords=-1, Offset=0, Dma=0, Time=0):
        return VfcHd_System.ReadMem(self, "App_GbtStatReg", NbLongWords, Offset, Dma, Time) 

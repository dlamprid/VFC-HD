/* A simple tool to update the WR PTP core software from a Linux terminal. */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <termios.h>
#include <string.h>
#include <errno.h>
#include <arpa/inet.h>

#include <libvmebus.h>

volatile void *vme_base_ptr;

uint32_t wrc_mem_base = 0x40000;
uint32_t wrc_sys_base = 0x60400;

void vfchd_wrc_loader (const char *fname) {
  
  volatile uint32_t *wrc_mem_ptr = vme_base_ptr + wrc_mem_base;
  volatile uint32_t *wrc_sys_ptr = vme_base_ptr + wrc_sys_base;

  FILE *f = fopen(fname, "rb");
  if ( f == NULL )
    {
      fprintf( stderr, "Could not open file\n" );
      return;
    }
  
  fseek(f, 0, SEEK_END);
  
  long fsize32 = ftell(f) >> 2;
  
  fseek(f, 0, SEEK_SET);

  uint32_t datum, readback;

  int i = 0;
  
  wrc_sys_ptr[0] = htonl(0x1deadbee);
  
  for (i=0; i < fsize32; i++) {
    
    fread((void *)&datum, 4, 1, f);

    wrc_mem_ptr[i] = datum;

    readback = wrc_mem_ptr[i];
    
    if ( readback != datum) {
      fprintf(stderr, "\nerror writing value 0x%.8x to address 0x%.8x. Read-back value was 0x%.8x\n",
    	      htonl(datum), i, readback);
      break;
    }
    
  }

  fclose(f);

  wrc_sys_ptr[0] = htonl(0x0deadbee);
}

int main(int argc, char **argv)
{
  struct vme_mapping map;
  struct vme_mapping *mapp = &map;

  memset(mapp, 0, sizeof(*mapp));

  mapp->am         = VME_A32_USER_DATA_SCT;
  mapp->data_width = VME_D32;
  mapp->sizel      = 0x80000;
  mapp->vme_addrl  = atoi(argv[1]) << 24;

  if ((vme_base_ptr = vme_map(mapp, 1)) == NULL) {
    printf("could not map at 0x%08x\n", mapp->vme_addrl);
    exit(1);
  }

  fprintf(stderr, "vme 0x%08x kernel %p user %p\n",
	  mapp->vme_addrl, mapp->kernel_va, mapp->user_va);


  vfchd_wrc_loader(argv[2]);
	
  vme_unmap(mapp, 1);
	
  return 0;
    
}
